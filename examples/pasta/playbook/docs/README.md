# Playbook Pasta

[TOC]

## Intro

This is an example project to show off the `autovnet pasta` feature.

It is a mock version of [adversary playbooks](https://unit42.paloaltonetworks.com/unit42-introducing-the-adversary-playbook-first-up-oilrig/).

The goal is to describe a structured attack narrative in a way that is easy for Red Team players of various experience levels to recreate.

## Start

Start with [basic_linux](modules/basic_linux/README.md).


