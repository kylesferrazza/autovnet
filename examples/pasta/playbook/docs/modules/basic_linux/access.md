# Access

[TOC]

## Vars

| Variable      | Value             | Description |
| --------      | ----------        | ----------- |
| TARGET        | {{TARGET}}        | The target running SSH |
| TARGET_USER   | {{TARGET_USER}}   | The SSH user on the target system |
| TARGET_PASS   | {{TARGET_PASS}}   | The SSH password on the target system |
| PHOST         | {{PHOST}}         | Proxy IP used to connect to TARGET |
| HOSTNAME      | {{HOSTNAME}}      | Your system's hostname, determined by `shell_exec` |

## Description

Example play showing an SSH connection to `{{TARGET}}`

## Steps

### Connect to TARGET

```bash
AVN_NET={{PHOST}} .proxy-ssh {{TARGET_USER}}@{{TARGET}}
```
Password:
```bash
{{TARGET_PASS}}
```

### Verify Access

```bash
id
```
