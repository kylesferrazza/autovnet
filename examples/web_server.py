#!/usr/bin/env python3

"""A simple web server that replies with your source IP

"""
import http.server
import socketserver

PORT = 8080


class IPHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        ip, port = self.client_address
        self.wfile.write(f"[+] Path: {self.path} | Requested By: {ip}\r\n".encode())


if __name__ == "__main__":
    server = http.server.HTTPServer(("", PORT), IPHandler)
    print(f"[+] Running Web Service on :{PORT}")
    server.serve_forever()
