site_name: autovnet
site_url: https://autovtools.gitlab.io/autovrtfm
repo_url: https://gitlab.com/autovtools/autovrtfm/autovnet
theme:
  name: material
  features:
    - search.suggest
    - search.highlight
    - navigation.instant
    - navigation.tracking
    - navigation.tabs
    - navigation.tabs.sticky
    #- navigation.sections
    - navigation.expand
    - toc.integrate
    #- navigations.indexes
    - toc.follow
    - navigation.top
  palette:
    - scheme: slate
      toggle:
        icon: material/toggle-switch
        name: Switch to dark mode
    - scheme: default
      toggle:
        icon: material/toggle-switch-off-outline
        name: Switch to light mode
  font:
    text: Roboto
    code: Roboto Mono
    
markdown_extensions:
- tables
- toc:
    permalink: True
plugins:
  - search:
      separator: '[\s\-\._]'
  - minify:
      minify_html: true
nav:
  - Home:
    - README.md

  - RTFM:
    - rtfm/README.md
    - Paradigm: rtfm/paradigm.md
    - autovnet: rtfm/autovnet.md
    - Goals: rtfm/goals.md

  - Demo:
    - demo/README.md
    - Proxy: demo/proxy.md
    - Callback: demo/callback.md

  - Setup: 
    - setup/README.md
    - Network Design: setup/network.md
    - Install: setup/install.md
    - Configure: setup/configure.md
    - Local Install: setup/local.md

  - Usage:
    - usage/README.md

    - RDN:
      - usage/rdn/README.md
      - Proxy: usage/rdn/proxy.md
      - Listen: usage/rdn/listen.md
      - Tun: usage/rdn/tun.md
      - DNS: usage/rdn/dns.md

    - RTFM:
      - usage/rtfm/README.md
      - APT: usage/rtfm/apt.md
      - Pasta: usage/rtfm/pasta.md

    - Pwn:
      - usage/pwn/README.md
      - File Hosting: usage/pwn/share.md
      - pwncat: usage/pwn/pwncat.md
      - evil-winrm: usage/pwn/winrm.md
      - metasploit: usage/pwn/metasploit.md
      - merlin: usage/pwn/merlin.md
      - sliver: usage/pwn/sliver.md

    - Collaboration: 
      - usage/collaboration/README.md
      - Services: usage/collaboration/svc.md
      - Shared Directories: usage/collaboration/fs.md
      - Screen Sharing: usage/collaboration/vnc.md
      - Shared Notes: usage/collaboration/hedgedoc.md
      - msfdb: usage/collaboration/msfdb.md
      - gitea: usage/collaboration/gitea.md
      - focalboard: usage/collaboration/focalboard.md
      - drawio: usage/collaboration/drawio.md
      - cyberchef: usage/collaboration/cyberchef.md
      - matrix: usage/collaboration/matrix.md
      - nextcloud: usage/collaboration/nextcloud.md
      - owncast: usage/collaboration/owncast.md

    - Util:
      - usage/util/README.md
      - tmp: usage/util/tmp.md

  - Reference:
    - usage/help.md

  - Resources:
    - FAQ: resources/faq.md
    - Troubleshooting: resources/troubleshooting.md
    - Changelog: resources/changelog.md
    - In The Wild: resources/wild.md
    - References: resources/refs.md
