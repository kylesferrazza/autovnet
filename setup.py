import re
from pathlib import Path
from setuptools import setup, find_packages

PKG_NAME = "autovnet"


def get_version():
    path = Path(PKG_NAME) / "version.py"
    if path.is_file():
        text = Path("autovnet/version.py").read_text()
        return re.search('"(.*)"', text).group(1)
    return "0.0.0"


def get_scripts():
    return [str(p) for p in Path(f"{PKG_NAME}/scripts/").glob("*")]


setup(
    name=PKG_NAME,
    version=get_version(),
    packages=find_packages(),
    entry_points={
        "console_scripts": [f"{PKG_NAME}={PKG_NAME}.main:entrypoint"],
    },
    scripts=get_scripts(),
    install_requires=[
        "typer[all]",
        "typer-cli",
        "pylint",
        "colorama",
        "tabulate",
        "pyyaml",
        "pydantic>=1.9.0",
        "codenamize",
        "pycryptodome",
        "requests",
        "mkdocs",
        "mkdocs-material",
        "mkdocs-minify-plugin",
        "uvicorn",
        "fastapi",
        "python-powerdns",
        "pyfiglet",
        "pwncat-cs",
        "grpcio",
        "grpcio-tools",
        "sliver-py @ git+https://github.com/moloch--/sliver-py.git",
    ],
    include_package_data=True,
)
