#!/bin/bash

# Optional script that will try to install metasploit-framework on Ubuntu

set -eu

MSF_INSTALL="/opt/msf"

sudo mkdir -p "${MSF_INSTALL}"
sudo chown "${USER}:${USER}" "${MSF_INSTALL}"

sudo apt update
sudo apt install -y \
    build-essential \
    zlib1g \
    zlib1g-dev \
    libpq-dev \
    libpcap-dev \
    libsqlite3-dev \
    ruby \
    ruby-dev

if [ -d "${MSF_INSTALL}/.git" ]; then
    cd "${MSF_INSTALL}"
    git pull
else
    git clone https://github.com/rapid7/metasploit-framework.git "${MSF_INSTALL}"
    cd "${MSF_INSTALL}"
fi

sudo gem install bundler

# This prompts for a password, not supposed to run as sudo
bundle install

# None of the binaries get added to PATH :/

# Now they we have ruby, install evil-winrm

sudo gem install evil-winrm

echo "[+] Adding ${MSF_INSTALL} to PATH"
SHELL_NAME="$(basename "${SHELL}")"
echo 'PATH="${PATH}:/opt/msf"' >> "${HOME}/.${SHELL_NAME}rc"
echo "export RUBYOPT='-W:no-deprecated'" >> "${HOME}/.${SHELL_NAME}rc"

cat << EOF
# Run:
. ~/.${SHELL_NAME}rc
EOF
