#!/bin/bash

if [ "${UID}" = "0" ]; then
    cat << EOF
Do not run as root.
Run as your regular user.
EOF
    exit 1
fi

# This is a best-effort installer only tested and intended to work on Kali 2021.4
# You may need to modify this script or refer to the documentation.

APT() {
    sudo DEBIAN_FRONTEND=noninteractive apt $@
}

install_deps() {
    APT update
    APT install -y \
        openssh-client \
        sshfs \
        rsync \
        openssl \
        xtightvncviewer \
        git \
        wireguard-tools \
        iperf3

    APT install -y python3 python3-venv

    APT install -y \
        apt-transport-https \
        software-properties-common \
        ca-certificates \
        curl \
        wget \
        gnupg \
        lsb-release
}

install_docker() {
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

    DEBIAN_NAME=bullseye
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian ${DEBIAN_NAME} stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

    APT update
    APT install -y docker-ce docker-ce-cli containerd.io
    sudo usermod -aG docker "${USER}"
}

install_docker_compose() {
    VERSION=1.29.2
    sudo curl -L "https://github.com/docker/compose/releases/download/${VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
}

install_element() {
    # https://element.io/get-started#linux
    sudo wget -O /usr/share/keyrings/element-io-archive-keyring.gpg https://packages.element.io/debian/element-io-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/element-io-archive-keyring.gpg] https://packages.element.io/debian/ default main" | sudo tee /etc/apt/sources.list.d/element-io.list
    APT update
    APT install -y element-desktop
}

install_obs() {
    # https://obsproject.com/wiki/install-instructions
    APT install -y ffmpeg v4l2loopback-dkms
    APT install -y obs-studio
}

init_venv () {
    mkdir -p ~/.venv/
    python3 -m venv ~/.venv/rtfm

    ACTIVATE=". ${HOME}/.venv/rtfm/bin/activate"
    eval $ACTIVATE && echo "${ACTIVATE}" >> "${HOME}/.$(basename "${SHELL}")rc"
    pip install --upgrade pip
}

reinstall_autovnet() {
    git reset HEAD --hard || true
    if [ ! -z "${AUTOVNET_INSTALL_BRANCH-}" ]; then
        git checkout "${AUTOVNET_INSTALL_BRANCH}"

        # Switch to the alt stream
        SHELL_RC="${HOME}/.$(basename "$(echo "${SHELL}")rc")"
        sed -i.avn.bak "/AUTOVNET_INSTALL_BRANCH/d" "${SHELL_RC}"
        echo "export AUTOVNET_INSTALL_BRANCH=${AUTOVNET_INSTALL_BRANCH}" >> "${SHELL_RC}"
    fi
    git pull || true
    "${SHELL}" ./reinstall.sh
}

install_autovnet() {
    cd
    git clone https://gitlab.com/autovtools/autovrtfm/autovnet.git
    pushd autovnet > /dev/null
    reinstall_autovnet
    popd > /dev/null
    # Fix for Kali completion with ZSH
    if [ "$(basename "${SHELL}")" == "zsh" ]; then
        echo 'compinit -D' >> ~/.zshrc
    fi
}

setup_dirs() {
    TOOLS="/opt/rtfm"
    sudo mkdir -p "${TOOLS}"
    sudo chown -R "${USER}:${USER}" "${TOOLS}"
    for path in mnt payloads c2 fs svc hedgedoc msf msfdb vnc drawio focalboard winrm/ps1 winrm/exe; do
        mkdir -p "${TOOLS}/${path}"
    done
    if [ ! -d ~/.mnt ]; then
        ln -s "${TOOLS}/fs" ~/.mnt
    fi
}

install_client_deps() {

    # https://github.com/Hackplayers/evil-winrm
    # Kali should have gem installed already

    # ruby has issues with IPv6 on some systems
    if ! sudo gem install evil-winrm; then
        echo "[-] Trying IPv6 workaround"
        # This is a bad idea
        cat << EOF | sudo tee -a /etc/hosts
151.101.65.227  rubygems.org
EOF
        sudo gem install evil-winrm
    fi

    # element
    install_element

    # obs
    install_obs

    # proxychains4
    APT install -y proxychains4

    # (optional) socat
    APT install -y socat

    # (optional) ncat
    APT install -y ncat

}

set -eu

# Always try to prep dirs
setup_dirs

# Purge any rtfm-* images, in case they need rebuilding
docker image ls rtfm-* | awk '{print $1}' | tail -n+2 | xargs docker image rm -f 2>/dev/null || true

# If we already ran, do an update instead of an install
if [ -d ~/autovnet ]; then
    cat << EOF
==============================================================================
| Updating existing install found in ~/autovnet
==============================================================================
EOF
    pushd ~/autovnet > /dev/null
    reinstall_autovnet
    popd > /dev/null
    exit 0
fi

cat << EOF
==============================================================================
| Installing dependencies
==============================================================================
EOF

install_deps

if ! command -v docker &> /dev/null ; then
    cat << EOF
==============================================================================
| Installing docker
==============================================================================
EOF
    install_docker
fi

if ! command -v docker-compose &> /dev/null ; then
    cat << EOF
==============================================================================
| Installing docker-compose
==============================================================================
EOF
    install_docker_compose
fi

cat << EOF
==============================================================================
| Initializing venv
==============================================================================
EOF

init_venv

cat << EOF
==============================================================================
| Initializing client dependencies
==============================================================================
EOF

install_client_deps

cat << EOF
==============================================================================
| Installing autovnet
==============================================================================
EOF

install_autovnet

if [ ! -f /etc/docker/daemon.json ] ; then
    cat << EOF
==============================================================================
| Configuring docker
==============================================================================
EOF
    autovnet config docker --install
else
    cat << EOF
[!] You may want to manually apply these options to your /etc/docker/daemon.json :
EOF
    autovnet config docker
fi

cat << EOF
==============================================================================
| autovnet install complete!
| Please reboot to ensure all changes are applied, then run:
| ~/autovnet/scripts/test_install.sh
| To verify your installation was succesful
==============================================================================
EOF
