#!/bin/bash

cat << EOF
==============================================================================
| Checking docker installation...
==============================================================================
EOF
docker -v

docker run hello-world

cat << EOF
==============================================================================
| Checking docker-compose installation...
==============================================================================
EOF
docker-compose -v

cat << EOF
==============================================================================
| Checking autovnet installation...
==============================================================================
EOF
autovnet -h

cat << EOF
==============================================================================
| Checking autovnet configuration...
==============================================================================
EOF

if [ -d ~/.autovnet ]; then

    cat << EOF
Found configuration: ~/.autovnet
EOF

else

    cat << EOF
Configuraiton directory: ~/.autovnet
does not exist.

This will be provided by your autovnet server admin
EOF

fi
