import re
import secrets
import shutil
from pathlib import Path
from typing import Optional

from fastapi import FastAPI, APIRouter, HTTPException
from fastapi.responses import FileResponse

from autovnet import __version__
from autovnet.utils import (
    compose_up,
)
from .router import router


class SliverApi(FastAPI):
    """SliverApi

    Not the actual sliver api.
    This is an autovnet-custom API to make using autovnet + sliver (multiplayer) easier.
    Minimally, it distributes multiplayer bundles
        - sliver operator config
        - autovnet svc mount config (for the actual multiplayer API)
    """

    def __init__(
        self,
        avn,
        *args,
        payload_dir=None,
        server_codename=None,
        multiplayer_host=None,
        multiplayer_port=None,
        token=None,
        save_configs=False,
        **kwargs,
    ):
        self.avn = avn
        self.token = token
        self.save_configs = save_configs

        self.multiplayer_host = multiplayer_host
        self.multiplayer_port = multiplayer_port

        self.player_pattern = re.compile("^[a-z_]+$")

        self.server_codename = server_codename
        self.payload_dir = Path(payload_dir)
        self.server_dir = self.payload_dir / self.server_codename
        self.player_bundles_dir = self.server_dir / "players"
        self.client_dir = self.server_dir / ".sliver-client"

        # Purge and recreate any saved configs, because we generated a new password
        if self.player_bundles_dir.is_dir():
            shutil.rmtree(self.player_bundles_dir)

        for d in [self.client_dir, self.player_bundles_dir]:
            d.mkdir(exist_ok=True, parents=True)

        super().__init__(*args, **kwargs)

    def validate_token(self, token):
        # implement some kind of auth in case of misconfiguration + port scan
        # (actual security should come from svc auth)
        # compare_digest because why not
        return secrets.compare_digest(self.token, token)

    def validate_player(self, player):
        # MEGA DANGER: user input
        # WARNING: SHELL INJECTION
        return self.player_pattern.fullmatch(player)


def sliver_api(
    *args,
    **kwargs,
) -> SliverApi:
    app = SliverApi(
        *args,
        title="autovnet-sliver-api",
        description="micro-API for sliver multiplayer",
        version=__version__,
        **kwargs,
    )
    app.include_router(router, prefix="/sliver", tags=["sliver"])
    return app
