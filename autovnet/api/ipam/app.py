import json
import shlex
import shutil
import secrets
import threading
from secrets import token_urlsafe
from threading import Lock
from ipaddress import IPv4Network
from typing import Optional
from pathlib import Path
from ipaddress import IPv4Address

from fastapi import FastAPI, APIRouter, HTTPException
from fastapi.responses import FileResponse

import typer

from autovnet import __version__
from autovnet.models import IPAssignment, IPMapping
from autovnet.utils import (
    gen_codename,
    compose_up,
    hash_file,
    get_ip,
    Timer,
    TimerQueue,
)
from .router import router

DEFAULT_NETWORK = IPv4Network("169.254.0.0/22")
DEFAULT_BASE_DIR = Path("/opt/rtfm/ipam/")

TOKEN_LEN = 256
RESERVATION_INTERVAL = 60 * 15


class IpamApi(FastAPI):
    def __init__(
        self,
        avn,
        *args,
        base_dir=DEFAULT_BASE_DIR,
        network=DEFAULT_NETWORK,
        server_codename=None,
        ca_key=None,
        ca_crt=None,
        clean=False,
        lighthouse_connect=None,
        **kwargs,
    ):
        self.avn = avn
        self.lock = threading.Lock()

        self.server_codename = server_codename
        self.network = network
        self.base_dir = Path(base_dir)
        self.assignment_dir = self.base_dir / "assignment"
        if clean and self.assignment_dir.is_dir():
            typer.echo(f"[!] Deleting {self.assignment_dir}")
            shutil.rmtree(self.assignment_dir)
        self.assignment_dir.mkdir(parents=True, exist_ok=True)

        self.ca_key = self.base_dir / "ca.key"
        self.ca_crt = self.base_dir / "ca.crt"

        if ca_key is not None:
            shutil.copyfile(ca_key, self.ca_key)
        if ca_crt is not None:
            shutil.copyfile(ca_crt, self.ca_crt)

        if not ca_key.is_file():
            raise FileNotFoundError(f"[-] {self.__class__.__name__} missing {self.ca_key} ")
        if not ca_crt.is_file():
            raise FileNotFoundError(f"[-] {self.__class__.__name__} missing {self.ca_crt} ")

        if lighthouse_connect is None:
            lighthouse_connect = f"{get_ip()}:4242"
        self.lighthouse_connect = lighthouse_connect

        self.ca_hash = hash_file(self.ca_crt)

        self.ip_generator = self.network.hosts()
        # ip -> IPAssignment

        # Consume the lighthouse IPs
        self.lighthouse_ip = next(self.ip_generator)

        self.assignments = {
            str(self.lighthouse_ip): None,
        }
        # Lookup by  autovnet ip (map_ip)
        self.ip_mappings = {}
        # Lookup by nebula ip (tun_ip)
        self.ip_mappings_reverse = {}

        self.timer_queue = TimerQueue()
        self.timer_queue.run()

        super().__init__(*args, **kwargs)

    def gen_codename(self, ip):
        return gen_codename(f"rtfm://{self.server_codename}/{self.network}/{ip}")

    def _revoke_saved(self, ip):
        # Delete the stuff on disk
        codename = self.gen_codename(ip)
        for name in [f"{codename}.json", f"{codename}.etgz"]:
            path = self.assignment_dir / name
            path.unlink()

    def _revoke(self, ip):
        """Remove the assignment

        Caller must hold self.lock
        """
        assignment = self.assignments.pop(ip)
        typer.echo(f"[!] Timed out: {assignment}")
        assignment.timer.cancel()
        self._revoke_mapping(ip)

        self._revoke_saved(ip)

    def _revoke_mapping(self, tun_ip, remap_cmds=None):
        """Undo any iptables mappings

        Caller must hold self.lock
        """
        mapping = self.ip_mappings_reverse.pop(tun_ip, None)
        # typer.echo(f"[*] revoke_mapping: {mapping}")
        if mapping is not None:
            # Both mapping objects should be the same
            mapping = self.ip_mappings.pop(mapping.map_ip, None)
            # typer.echo(f"[*] revoke_mapping: {mapping}")

        cmds = []
        if mapping is not None:
            cmds.extend(mapping.clean_cmds)
        if remap_cmds is not None:
            cmds.extend(remap_cmds)

        if cmds:
            # WARNING: VERY dangerous, and possibly wrong
            cmd = f"/bin/sh -c {shlex.quote('; '.join(cmds))}"
            # typer.echo(f"[*] remap: {cmd}")

            env = {
                "IPTABLES_CMD": cmd,
            }
            iptables_project = f"iptables_cleanup"
            iptables_docker_path = self.avn.docker_path(f"plugins/iptables/")
            compose_up(iptables_docker_path, env=env, project=iptables_project, quiet=True)

    def revoke(self, ip):
        """Remove the assignment

        WARNING: this is not a "secure" certificate revokation.
        It's a cooperative AFK timeout.
        A party pooper could save their cert files, which are still cryptographically valid,
        and use them to manually connect to nebula with an IP leased to someone else.

        But the same party pooper could just spam the /assign API and legitimately reserve
        every IP before anyone else could.
        So the threat model is lax - users who can touch the API are trusted to be cooperative.
        """
        with self.lock:
            self._revoke(ip)

    def extend(self, ip):
        """Extend assignment reservation

        Caller must hold self.lock
        """
        self.assignments[ip].timer.update_deadline(dirty=True)

    def store_assignment(self, ip, assignment):
        """Store assignment in cache

        Caller must hold self.lock
        """

        if assignment.timer is not None:
            # If there is an item in the queue, cancel it
            assignment.timer.cancel()

        assignment.timer = Timer(lambda: self.revoke(ip), interval=RESERVATION_INTERVAL)
        self.timer_queue.add(assignment.timer)

        self.assignments[str(ip)] = assignment

    def load_reservation(self, ip):
        """Load reservation from disk

        Caller must hold self.lock
        """
        codename = self.gen_codename(ip)
        assignment_file = self.assignment_dir / f"{codename}.json"
        if assignment_file.is_file():
            try:
                prev = IPAssignment.parse_obj(json.loads(assignment_file.read_text()))
                if not prev.token.startswith(self.ca_hash):
                    # CA rolled, revoke assignment
                    typer.echo(f"[!] CA revoked {prev.token[:len(self.ca_hash)]} -> {self.ca_hash}")
                    self._revoke_saved(ip)
                    return False
                # Already assigned; cache and deny
                self.store_assignment(ip, prev)
                return True
            except Exception as e:
                typer.echo(f"[-] Failed to load {assignment_file}: {e}")
        return False

    def try_reserve(self, ip) -> Optional[IPAssignment]:
        """Reserve IP, if available

        Caller must hold self.lock
        """
        if ip in self.assignments:
            return None

        # This is a secure-ish, deterministic hash operation that should be fine
        codename = self.gen_codename(ip)
        assignment_file = self.assignment_dir / f"{codename}.json"

        if self.load_reservation(ip):
            return None

        # Not assigned
        token = self.ca_hash + token_urlsafe(TOKEN_LEN)
        assignment = IPAssignment(
            codename=codename,
            ip=str(ip),
            network=str(self.network),
            lighthouse_ip=str(self.lighthouse_ip),
            lighthouse_connect=str(self.lighthouse_connect),
            token=token,
        )
        assignment_file.write_text(assignment.json(indent=True))

        self.store_assignment(ip, assignment)
        return assignment

    def unmap(self, tun_ip):
        with self.lock:
            self._revoke_mapping(tun_ip)

    def cleanup(self):
        with self.lock:
            mappings = list(self.ip_mappings)
            typer.echo(f"[*] unmapping: {len(mappings)} ips")
            for tun_ip in list(self.ip_mappings_reverse):
                typer.echo(f"[*] unmap: {tun_ip}")
                self._revoke_mapping(tun_ip)

    def try_map(self, tun_ip, map_ip, map_cmds, clean_cmds) -> bool:
        with self.lock:
            if map_ip in self.ip_mappings:
                return False

            mapping = IPMapping(
                tun_ip=tun_ip,
                map_ip=map_ip,
                clean_cmds=clean_cmds,
            )
            tun_ip = str(IPv4Address(tun_ip))
            map_ip = str(IPv4Address(map_ip))

            success = False
            try:
                # If there was already a mapping, revoke it
                # Add new mapping at the same time
                self._revoke_mapping(tun_ip, remap_cmds=map_cmds)
                # Success
                self.ip_mappings[map_ip] = mapping
                self.ip_mappings_reverse[tun_ip] = mapping
                success = True
            except Exception as e:
                typer.echo(f"[-] Mapping {tun_ip} <-> {map_ip} failed: {e}")
                self._revoke_mapping(tun_ip)

            return success

    def next_assignment(self) -> Optional[IPAssignment]:
        # Maybe locks aren't required? async is weird

        assignment = None
        with self.lock:
            attempts = 0
            while assignment is None and attempts < self.network.num_addresses:
                ip = None
                try:
                    ip = str(next(self.ip_generator))
                except StopIteration:
                    self.ip_generator = self.network.hosts()
                    ip = str(next(self.ip_generator))
                attempts += 1
                assignment = self.try_reserve(ip)
        return assignment

    def validate_token(self, ip, token):
        try:
            ip = str(IPv4Address(ip))
        except Exception as e:
            raise HTTPException(status_code=400, detail=f"[-] Invalid ip: {e}")

        with self.lock:
            if ip not in self.assignments and not self.load_reservation(ip):
                # This isn't particularly sensitive info and may help troubleshooting
                raise HTTPException(status_code=404, detail=f"[-] {ip} is not reserved")
            if not token:
                raise HTTPException(status_code=403, detail=f"[-] token required")

            # compare_digest because why not
            return secrets.compare_digest(self.assignments[ip].token, token)


def ipam_api(
    *args,
    **kwargs,
) -> IpamApi:
    app = IpamApi(
        *args, title="autovnet-ipam-api", description="micro-API for IP reservations", version=__version__, **kwargs
    )
    app.include_router(router, prefix="/ipam", tags=["ipam"])
    return app
