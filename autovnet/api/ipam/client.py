import shutil
import tempfile
import tarfile
from pathlib import Path
from urllib.parse import urljoin
from ipaddress import IPv4Address

import yaml
import typer
import requests

from autovnet.models import IPAssignment
from autovnet.utils import (
    Cryptor,
    tar_extract,
    yaml_dump,
    wait_url_open,
)


class IpamApiClient:
    """Client for interacting with the IP reservation API

    It's not worth generating an API client from the OpenAPI spec;
    these micro-APIs are trivially simple.
    """

    def __init__(self, api_url, config_dir, codename=None, new=False):
        self.api_url = urljoin(api_url, "/ipam/")

        self.config_base_dir = Path(config_dir)
        self.config_base_dir.mkdir(parents=True, exist_ok=True)
        self.default_dir = self.config_base_dir / "default"

        self.config_dir = None
        self.config_file = None
        self.assignment = None

        if codename is None:
            self.config_file = self.default_dir / "assignment.yaml"
            if not new and self.config_file.is_file():
                self.config_dir = self.default_dir.resolve()
                self.assignment = IPAssignment.parse_obj(yaml.safe_load(self.config_file.read_text()))
        else:
            self.config_dir = self.config_base_dir / codename
            self.config_file = self.config_dir / "assignment.yaml"
            if not self.config_file.is_file():
                raise FileNotFoundError(f"[-] {self.config_file} is not found!")
            self.assignment = IPAssignment.parse_obj(yaml.safe_load(self.config_file.read_text()))

    def get_codename(self):
        if self.assignment is None:
            return None
        return self.assignment.codename

    def connect(self, assign=True):
        status_url = self.get_url("status")
        if not wait_url_open(status_url):
            raise Exception(f"[-] {status_url} not available")

        revoked = False
        to_delete = None
        if self.assignment is not None:
            if not self.token():
                typer.echo(f"[!] Assignment {self.assignment.ip} has been revoked by the server!")
                if self.config_dir == self.default_dir.resolve():
                    self.default_dir.unlink()
                to_delete = self.config_dir
                # shutil.rmtree(self.config_dir)
                self.config_dir = None
                self.assignment = None

        assigned = self.assignment is not None
        if assign and not assigned:
            self.assign()
            self.config()

        assigned = self.assignment is not None
        if to_delete is not None and assigned:
            # Previous assignment revoked, copy mount.yaml to new codename
            mnt_src = to_delete / "mount.yaml"
            mnt_dest = self.config_dir / "mount.yaml"
            if to_delete != self.config_dir:
                shutil.copyfile(mnt_src, mnt_dest)
                shutil.rmtree(to_delete)

    def require_token(self):
        if self.assignment is None or self.assignment.token is None:
            raise ValueError(f"[-] token not set but required")
        return {
            "token": self.assignment.token,
        }

    def get_url(self, *args):
        url = self.api_url
        for arg in args:
            if not url.endswith("/"):
                url += "/"
            url = urljoin(url, arg)
        return url

    def status(self):
        url = self.get_url("status")
        resp = requests.get(url, verify=False)
        return resp.json()

    def assign(self):
        url = self.get_url("assign")
        resp = requests.put(url, verify=False)
        self.assignment = IPAssignment.parse_obj(resp.json())
        self.config_dir = self.config_base_dir / self.assignment.codename
        self.config_dir.mkdir(parents=True, exist_ok=True)
        if not self.default_dir.is_dir():
            self.default_dir.symlink_to(self.config_dir.name, target_is_directory=True)

        config_file = self.config_dir / "assignment.yaml"
        config_file.write_text(yaml_dump(self.assignment.dict()))

        return self.assignment

    def token(self):

        data = self.require_token()
        url = self.get_url(self.assignment.ip, "token")
        resp = requests.get(url, params=data, verify=False)
        return resp.ok

    def config(self):
        data = self.require_token()
        url = self.get_url(self.assignment.ip, "config")

        with tempfile.NamedTemporaryFile() as enc:
            with requests.get(url, params=data, verify=False, stream=True) as resp:
                resp.raise_for_status()
                # 65536
                chunk_size = 1 << 16
                for chunk in resp.iter_content(chunk_size=chunk_size):
                    enc.write(chunk)
            enc.flush()
            with tempfile.NamedTemporaryFile() as dec:
                cryptor = Cryptor(password=self.assignment.token)
                cryptor.decrypt_file(enc.name, dec.name)
                with tarfile.open(dec.name, "r") as tar_obj:
                    tar_extract(tar_obj, self.config_dir)

    def map(self, map_ip):
        data = self.require_token()
        url = self.get_url(self.assignment.ip, "map", str(IPv4Address(map_ip)))
        resp = requests.put(url, params=data, verify=False)
        return resp

    def unassign(self):
        data = self.require_token()
        url = self.get_url(self.assignment.ip, "unassign")
        resp = requests.delete(url, params=data, verify=False)
        return resp

    def unmap(self):
        data = self.require_token()
        url = self.get_url(self.assignment.ip, "unmap")
        resp = requests.delete(url, params=data, verify=False)
        return resp
