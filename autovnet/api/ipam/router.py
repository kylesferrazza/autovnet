import os
import shlex
import shutil
import tempfile
from pathlib import Path
from ipaddress import IPv4Address

import typer

from fastapi import APIRouter, Request, HTTPException
from fastapi.responses import FileResponse

from autovnet.models import IPAssignment
from autovnet.utils import (
    compose_up_ctx,
    Cryptor,
)

router = APIRouter()


@router.get("/status")
async def status(
    request: Request,
) -> IPAssignment:
    """Check if server is up"""
    data = {
        "num_addresses": request.app.network.num_addresses,
        "num_assignments": len(request.app.assignments),
    }
    return data


@router.put("/assign")
async def assign(
    request: Request,
) -> IPAssignment:
    """Request an IP on the tun network

    The assignment includes an auth token used to prove you have reserved the IP in question.
    The token is passed to other API endpoints by the client, and used to encrypt the generated certificates.

    The threat model here is that everyone able to access this endpoint is a cooperative, semi-trusted player,
    not a party pooper.
    Don't intentionally spam this endpoint to exhaust the reservation pool - an admin can / will just revoke all reservations / certs.

    """

    assignment = request.app.next_assignment()
    if assignment is None:
        raise HTTPException(status_code=409, detail=f"[-] No addresses in {request.app.network} are available!")

    # Note: assignments must be returned successfully or released on error
    return assignment


@router.get("/{ip}/token")
async def token(
    ip: str,
    token: str,
    request: Request,
):
    """Verify that a previously issued token is still trusted by the server

    Since clients can drop in / drop out, and the server could have restarted / intentionally revoked
    previous reservations, clients should do a pre-flight check to verify their token
    (and by extension, the client certificates they have downloaded) are still valid.

    The tokens are not feasible to brute force over this endpoint, so don't try.
    """
    if not request.app.validate_token(ip, token):
        raise HTTPException(status_code=403, detail="[-] Invalid token")
    request.app.extend(ip)

    # 200 OK == token is good


@router.delete("/{ip}/unassign")
async def unassign(
    ip: str,
    token: str,
    request: Request,
):
    """Voluntarily release an assignment so it can be used by someone else"""
    if not request.app.validate_token(ip, token):
        raise HTTPException(status_code=403, detail="[-] Invalid token")
    request.app.revoke(ip)


@router.get("/{ip}/config")
async def config(
    ip: str,
    token: str,
    request: Request,
):
    """Request the nebula client config for tun IP assignment

    The assignment token (issued with the IP address) is required to download and decrypt the config.
    """
    if not request.app.validate_token(ip, token):
        raise HTTPException(status_code=403, detail="[-] Invalid token")

    # This should always be redundant,
    #   but in case authentication is ever broken, use trusted values
    token = request.app.assignments[ip].token

    codename = request.app.gen_codename(ip)

    encrypted_bundle = request.app.assignment_dir / f"{codename}.etgz"

    # Cert was already issued, resend it
    if encrypted_bundle.is_file():
        return FileResponse(encrypted_bundle)

    # Create a tempdir used to generate the client cert
    with tempfile.TemporaryDirectory() as payload_dir:
        certs_dir = Path(payload_dir) / codename / "certs"
        ca_dir = certs_dir / "ca"
        host_dir = certs_dir / "host"
        bundle_dir = Path(payload_dir) / codename / "bundle"
        config_dir = Path(payload_dir) / codename / "config"
        for _dir in [certs_dir, ca_dir, host_dir, bundle_dir, config_dir]:
            _dir.mkdir(parents=True, exist_ok=True)

        # Stage the required certs in the temp dir
        shutil.copyfile(request.app.ca_key, ca_dir / "ca.key")
        shutil.copyfile(request.app.ca_crt, ca_dir / "ca.crt")

        # Prepare compose
        project = f"nebula_{codename}"
        docker_path = request.app.avn.docker_path(f"plugins/tun/")

        assignment = f"{ip}/{request.app.network.prefixlen}"
        # network_mode: host
        # TODO: Allow user to opt-in to NEBULA_ALLOW_PIVOT / NEBULA_PIVOT_SUBNETS ?
        #   - must be opt-in, as it allows peers to pivot through your client
        #   - pedantically validate NEBULA_PIVOT_SUBNETS to avoid command injection
        env = {
            "NEBULA_MODE": "sign",
            "NEBULA_CODENAME": codename,
            "NEBULA_ASSIGNMENT": assignment,
            "NEBULA_LIGHTHOUSE_IP": str(request.app.lighthouse_ip),
            "NEBULA_ALLOW_PIVOT": str(0),
            "NEBULA_PIVOT_SUBNETS": "",
            "UID": str(os.geteuid()),
            "GID": str(os.getegid()),
            "NEBULA_CA_DIR": str(ca_dir),
            "NEBULA_HOST_DIR": str(host_dir),
            "NEBULA_BUNDLE_DIR": str(bundle_dir),
            "NEBULA_CONFIG_DIR": str(config_dir),
        }

        with compose_up_ctx(docker_path, env=env, project=project, quiet=True) as (
            compose_thread,
            compose_stop,
        ):
            compose_thread.join()
        raw_bundle = bundle_dir / f"{codename}.tar.gz"
        if not raw_bundle.is_file():
            raise FileNotFoundError(f"[-] Client bundle {raw_bundle} not generated!")

        cryptor = Cryptor(password=token)
        cryptor.encrypt_file(raw_bundle, out_path=encrypted_bundle)

    return FileResponse(encrypted_bundle)


@router.put("/{tun_ip}/map/{map_ip}")
async def map(
    tun_ip: str,
    map_ip: str,
    token: str,
    request: Request,
):
    """Map a tun_ip to a local ip (map_ip)

    ip should be assigned by /assign
        * it's an IP on the nebula subnet
    map_ip should be an arbitrary IP (randomly drawn) from the autovnet IP pool
    """
    if not request.app.validate_token(tun_ip, token):
        raise HTTPException(status_code=403, detail="[-] Invalid token")
    # WARNING: Scary input
    try:
        tun_ip = str(IPv4Address(tun_ip))
        map_ip = str(IPv4Address(map_ip))
    except Exception as e:
        raise HTTPException(status_code=400, detail=f"[-] Invalid ip: {e}")

    map_cmds = []
    map_cmds.append(f"iptables-nft -t nat -I NEBULA-POST -s {shlex.quote(tun_ip)} -j SNAT --to {shlex.quote(map_ip)}")
    map_cmds.append(f"iptables-nft -t nat -I NEBULA-PRE -d {shlex.quote(map_ip)} -j DNAT --to {shlex.quote(tun_ip)}")

    clean_cmds = []
    clean_cmds.append(f"iptables-nft -t nat -D NEBULA-POST -s {shlex.quote(tun_ip)} -j SNAT --to {shlex.quote(map_ip)}")
    clean_cmds.append(f"iptables-nft -t nat -D NEBULA-PRE -d {shlex.quote(map_ip)} -j DNAT --to {shlex.quote(tun_ip)}")
    if not request.app.try_map(tun_ip, map_ip, map_cmds, clean_cmds):
        raise HTTPException(status_code=409, detail=f"[-] {map_ip} is already mapped!")


@router.delete("/{tun_ip}/unmap")
async def unmap(
    tun_ip: str,
    token: str,
    request: Request,
):
    """Remove any iptables mapping associated with tun_ip"""
    if not request.app.validate_token(tun_ip, token):
        raise HTTPException(status_code=403, detail="[-] Invalid token")
    request.app.unmap(tun_ip)
