/***
 * Examples:
 *  
 * https://github.com/mayth/go-simple-upload-server/blob/master/simple_upload_server.go
 * https://gist.github.com/shivakar/cd52b5594d4912fbeb46
 */

package main

import (
    "os"
    "io"
    "log"
    "path"
    "time"
    "strconv"
    "math"
    "math/big"
    "net"
    "net/http"
    "net/url"
    "crypto/rand"
    "crypto/tls"
    "crypto/rsa"
    "crypto/x509"
    "crypto/x509/pkix"
)

var BindAddrHTTP string = "0.0.0.0:8888"
var BindAddrHTTPS string = "0.0.0.0:8443"

// 1MB
var MaxUploadMemory int64 = (1<<20)

type tcpKeepAliveListener struct {
    *net.TCPListener
}

var (
    suffixes [5]string
)

// https://hakk.dev/docs/golang-convert-file-size-human-readable/
func Round(val float64, roundOn float64, places int ) (newVal float64) {
    var round float64
    pow := math.Pow(10, float64(places))
    digit := pow * val
    _, div := math.Modf(digit)
    if div >= roundOn {
        round = math.Ceil(digit)
    } else {
        round = math.Floor(digit)
    }
    newVal = round / pow
    return
}

func HumanFileSize(size int64) string {
    suffixes[0] = "B"
    suffixes[1] = "KB"
    suffixes[2] = "MB"
    suffixes[3] = "GB"
    suffixes[4] = "TB"

    base := math.Log(float64(size))/math.Log(1024)
    getSize := Round(math.Pow(1024, base - math.Floor(base)), .5, 1)
    getSuffix := suffixes[int(math.Floor(base))]
    return strconv.FormatFloat(getSize, 'f', -1, 64)+" "+string(getSuffix)
}

func (ln tcpKeepAliveListener) Accept() (c net.Conn, err error) {
    tc, err := ln.AcceptTCP()
    if err != nil {
        return
    }
    tc.SetKeepAlive(true)
    tc.SetKeepAlivePeriod(1 * time.Minute)
    return tc, nil
}

func initTLS() (tls.Certificate, error) {
    now := time.Now()
    template := &x509.Certificate{
        SerialNumber: big.NewInt(now.Unix()),
        Subject: pkix.Name{
            CommonName:         "",
            Country:            []string{""},
            Organization:       []string{""},
            OrganizationalUnit: []string{""},
        },
        NotBefore:             now,
        NotAfter:              now.AddDate(1, 0, 0), // Valid for one year
        BasicConstraintsValid: true,
        IsCA:        true,
        ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
        KeyUsage: x509.KeyUsageKeyEncipherment |
            x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
    }

    priv, err := rsa.GenerateKey(rand.Reader, 2048)
    if err != nil {
        log.Printf("[FileDepot] [-] Key generation failed: %s\n", priv)
        return tls.Certificate{}, err
    }

    cert, err := x509.CreateCertificate(rand.Reader, template, template, priv.Public(), priv)

    if err != nil {
        log.Printf("[FileDepot] [-] Cert creation failed: %s\n", cert)
        return tls.Certificate{}, err
    }

    var outCert tls.Certificate
    outCert.Certificate = append(outCert.Certificate, cert)
    outCert.PrivateKey = priv

    return outCert, nil
}

type FileDepot struct {}

func (s *FileDepot) Run(proto string, addr string) error {
    var listener net.Listener

    ln, err := net.Listen("tcp", addr)
    if err != nil {
        return err
    }
    listener = tcpKeepAliveListener{ln.(*net.TCPListener)}
    switch proto {
        case "http":
        case "https":
            cert, err := initTLS()

            if err != nil {
                return err
            }

            tlsConfig := &tls.Config{
                Certificates: []tls.Certificate{cert},
            }

            listener = tls.NewListener(listener, tlsConfig)
        default:
            log.Printf("[-] Bad proto %s\n", proto)
            os.Exit(1)
    }

    server := &http.Server{
        Addr: addr,
        Handler: s,
    }

    _, err = os.Getwd()
    if err != nil {
        log.Printf("[-] Error: %s\n", err)
        return err
    }
    //log.Printf("[+] Serving %s at %s://%s/\n", wd, proto, addr)
    return server.Serve(listener)
}

func (s FileDepot) handleGet(w http.ResponseWriter, r *http.Request) {
    wd, err := os.Getwd()
    if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        return
    }
    http.FileServer(http.Dir(wd)).ServeHTTP(w, r)
}

func (s FileDepot) handlePost(w http.ResponseWriter, r *http.Request) {
    // Restrict max memory consumed per concurrent file upload
    err := r.ParseMultipartForm(MaxUploadMemory)
    if err != nil {
        log.Printf("[-] Failed to set maxMemory: %s\n", err)
    }

    srcFile, info, err := r.FormFile(".")
    if err != nil {
        log.Printf("[-] Failed to get file from request: %s\n", err)
        w.WriteHeader(http.StatusBadRequest)
        return
    }

    defer srcFile.Close()

    // https://dzx.cz/2021/04/02/go_path_traversal/
    // WARNING: Probably vulnerable
    filePath, err := url.PathUnescape(r.URL.Path)
    if err != nil {
        log.Printf("[-] Failed to unescape path: %s\n", err)
        w.WriteHeader(http.StatusBadRequest)
        return
    }
    // TODO Windows paths
    wd, err := os.Getwd()
    if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        return
    }
    dstPath := path.Join(wd, path.Join("/", filePath))
    log.Printf("[*] Receiving %s: %s -> %s\n", HumanFileSize(info.Size), filePath, dstPath)

    err = os.MkdirAll(path.Dir(dstPath), os.ModePerm)
    if err != nil {
        log.Printf("[-] Failed to create dirs: %s\n", err)
        w.WriteHeader(http.StatusInternalServerError)
        return
    }
    dstFile, err := os.Create(dstPath)
    if err != nil {
        log.Printf("[-] Failed to create %s: %s\n", dstPath, err)
        w.WriteHeader(http.StatusInternalServerError)
        return
    }
    defer dstFile.Close()
    written, err := io.Copy(dstFile, srcFile)

    if err != nil {
        log.Printf("[-] Failed to write %s\n", dstPath)
    }

    if written == info.Size {
        log.Printf("[+] Uploaded %s to %s\n", HumanFileSize(written), dstPath)
    } else {
        log.Printf("[!] Uploaded %s / %s to  %s\n", HumanFileSize(written), HumanFileSize(info.Size), dstPath)
    }
    w.WriteHeader(http.StatusOK)
}

func (s FileDepot) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    filePath, err := url.PathUnescape(r.URL.Path)
    if err != nil {
        log.Printf("[-] Failed to unescape path: %s\n", err)
        w.WriteHeader(http.StatusBadRequest)
        return
    }
    switch r.Method {
        case http.MethodGet, http.MethodHead:
            log.Printf("[+] %s | GET %s\n", r.RemoteAddr, filePath)
            s.handleGet(w, r)
        case http.MethodPost:
            log.Printf("[+] %s | POST %s\n", r.RemoteAddr, filePath)
            s.handlePost(w, r)
        default:
            w.WriteHeader(http.StatusMethodNotAllowed)
    }
}



func main() {

    httpDepot := &FileDepot{}
    httpsDepot := &FileDepot{}
    go httpDepot.Run("http", BindAddrHTTP)
    go httpsDepot.Run("https", BindAddrHTTPS)

    log.Printf("[+] Waiting for requests...\n")
    select {}
}
