#!/bin/sh
set -eu
CONFIG_TEMPLATE="/stunnel.conf"
CONFIG="/tmp/stunnel.conf"

# Resolve any environment variables in the config
eval "echo \"$(cat "${CONFIG_TEMPLATE}")\"" > "${CONFIG}"
chmod 0400 "${CONFIG}"
cat "${CONFIG}"
stunnel "${CONFIG}"
