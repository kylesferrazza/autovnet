#!/bin/bash

set -e

CA_NAME="rtfm"
NEBULA_GROUP="rtfm"
CA_DIR="/certs/ca"
CA_CRT="${CA_DIR}/ca.crt"
CA_KEY="${CA_DIR}/ca.key"

STAGE_DIR="/tmp/stage"
HOST_DIR="/certs/host"
HOST_CRT="${HOST_DIR}/host.crt"
HOST_KEY="${HOST_DIR}/host.key"
EXPORT_DIR="/certs/bundle/"

if [ "$#" != "1" ]; then
    echo "[-] Usage: $0 (sign|client|server)"
    exit 1
fi
NEBULA_MODE="${1}"
NEBULA_CONFIG_TEMPLATE="/etc/nebula/${NEBULA_MODE}.yaml"
NEBULA_CONFIG="/etc/nebula/config.yaml"

gen-ca()
{
    if [ ! -f "${CA_CRT}" ] || [ ! -f "${CA_KEY}" ] ; then
        rm -f "${CA_CRT}" "${CA_KEY}"
        nebula-cert ca -name "${CA_NAME}" -out-crt "${CA_CRT}" -out-key "${CA_KEY}"
        echo "[+] Generated CA cert"
    fi
}

gen-config()
{
    envsubst < "${NEBULA_CONFIG_TEMPLATE}" > "${NEBULA_CONFIG}"
    rm -f "${NEBULA_CONFIG_TEMPLATE}"
}

sign()
{
    BUNDLE_NAME="${NEBULA_CODENAME}.tar.gz"

    nebula_subnets=""
    if [ "${NEBULA_ALLOW_PIVOT}" != "0" ]; then
        # Currently not reachable from API
        # WARNING: if this is from remote user input, do paranoid validation
        echo "[!] Allow pivots to ${NEBULA_PIVOT_SUBNETS}"
        nebula_subnets="-subnets "$(echo "${NEBULA_PIVOT_SUBNETS}" | tr ' ' ',')""
    fi

    # codename derived from IP, so each peer gets a unique codename
    if [ ! -f "${HOST_CRT}" ] || [ ! -f "${HOST_KEY}" ] ; then
        rm -f "${HOST_CRT}" "${HOST_KEY}"
        nebula-cert sign \
            -name "${NEBULA_CODENAME}" \
            -ip "${NEBULA_ASSIGNMENT}" \
            -groups "${NEBULA_GROUP}" \
            -ca-crt "${CA_CRT}" \
            -ca-key "${CA_KEY}" \
            -out-crt "${HOST_CRT}" \
            -out-key "${HOST_KEY}" \
            ${nebula_subnets}

        echo "[+] Generated host cert"
    fi

   
    # Bundle
    mkdir -p "${STAGE_DIR}/ca"
    mkdir -p "${STAGE_DIR}/host"
    cp "${CA_CRT}" "${STAGE_DIR}/ca/"
    cp "${HOST_CRT}" "${STAGE_DIR}/host/"
    cp "${HOST_KEY}" "${STAGE_DIR}/host/"

    pushd "${STAGE_DIR}"
    tar czf "${BUNDLE_NAME}" * 
    popd
    mv "${STAGE_DIR}/${BUNDLE_NAME}" "${EXPORT_DIR}/${BUNDLE_NAME}"
    echo "[+] Saved ${BUNDLE_NAME}"
}

client()
{
    # HACK: patched nebula source to add create flag
    # If the user didn't give pivot routes, we are probably
    # in "listen" mode, and have to use ip rules to force callback replies
    # to return via the tun.
    # TL;DR we need to stop nebula from dropping outbound packets, this makes them all valid
    # (we carefully configure which packets get sent to the tun in the client)
    UNSAFE_ROUTES="
  unsafe_routes:
  - route: 0.0.0.0/0
    via: ${NEBULA_LIGHTHOUSE_IP}
    create: false
"
    if [ ! -z "${NEBULA_PIVOT_SUBNETS}" ]; then

        # Allow pivoting through this nebula server to any private IP
        # WARNING: super dangerous; could allow rogue players to pivot into your home network
        #   This exists to support pivoting through shared infrastructure

        for route in ${NEBULA_PIVOT_SUBNETS}; do
            UNSAFE_ROUTES+="
  - route: ${route}
    via: ${NEBULA_LIGHTHOUSE_IP}
"
        done
    fi

    export UNSAFE_ROUTES=${UNSAFE_ROUTES}
    # TODO
    echo "${UNSAFE_ROUTES}"
    gen-config

    # TODO
    cat /etc/nebula/config.yaml
    # Start client
    exec nebula -config /etc/nebula/config.yaml
}

server()
{
    # SERVER mode
    gen-ca

    nebula_subnets=""
    if [ "${NEBULA_ALLOW_PIVOT}" != "0" ]; then
        echo "[!] Allow pivots to ${NEBULA_PIVOT_SUBNETS}"
        nebula_subnets="-subnets "$(echo "${NEBULA_PIVOT_SUBNETS}" | tr ' ' ',')""
    fi

    if [ ! -f "${HOST_CRT}" ] || [ ! -f "${HOST_KEY}" ] ; then
        rm -f "${HOST_CRT}" "${HOST_KEY}"
        # codename derived from IP, so each peer gets a unique codename
        nebula-cert sign \
            -name "${NEBULA_CODENAME}" \
            -ip "${NEBULA_ASSIGNMENT}" \
            -groups "${NEBULA_GROUP}" \
            -ca-crt "${CA_CRT}" \
            -ca-key "${CA_KEY}" \
            -out-crt "${HOST_CRT}" \
            -out-key "${HOST_KEY}" \
            ${nebula_subnets}
    fi

    gen-config

    # Start lighthouse
    exec nebula -config /etc/nebula/config.yaml
}

if [ "${NEBULA_MODE}" = "sign" ]; then
    sign
elif [ "${NEBULA_MODE}" = "client" ]; then
    client
elif [ "${NEBULA_MODE}" = "server" ]; then
    server
else
    echo "[-] unknown mode: ${NEBULA_MODE}"
    exit 1
fi
