#!/bin/bash
set -eu

mkdir -p /run/sshd
for key in ssh_host_rsa_key ssh_host_ecdsa_key ssh_host_ed25519_key; do
    if [ ! -f "/home/x/.ssh/${key}" ]; then
        ssh-keygen -q -C '' -P '' -f "/home/x/.ssh/${key}"
    fi
done

cp /authorized_keys /home/x/.ssh/authorized_keys
chmod 0400 /home/x/.ssh/authorized_keys
usermod -u "${UID}" x
groupmod -g "${GID}" x

chown -R x:x /home/x/
chown root:root /home/x/
chown root:root /home/x/chroot

# https://github.com/openssh/openssh-portable/blob/master/platform.c#L89
# :/ Can't use CAP_SYS_CHROOT - must run as UID 0 if using chroot
exec /usr/sbin/sshd -D -e -f sshd_config
