import re
import os
import shlex
import shutil
import subprocess
from ipaddress import IPv4Network
from pathlib import Path
from typing import List
from contextlib import contextmanager

import typer
import yaml
import powerdns

from autovnet.models import Config, Listener, Mount
from autovnet.utils import (
    rand_ip,
    rand_ip_list,
    rand_port,
    gen_codename,
    wait_container_exit,
    wait_tcp_open,
    wait_url_open,
    open_url,
    dns_register,
    dns_unregister,
    make_dns_names,
)
from .listener import AutoVnetListener

# Avoid temporarily exposing player-only services during deconfliction
DECONFLICT_IP = "192.0.2.0"


class AutoVnet:
    def __init__(self):
        self.verbose = False
        self.config = None

    def init(self, config_file, verbose=False):
        self.verbose = verbose
        self.config_file = config_file

        if not Path(config_file).expanduser().is_file():
            raise FileNotFoundError(f"[-] Config file {config_file} not found")

        config_dict = yaml.safe_load(Path(config_file).expanduser().read_text())
        if config_dict is None:
            config_dict = {}
        self.config = Config.parse_obj(config_dict)

    @classmethod
    def config_path(cls, filename, expand_home=True):
        home = Path("~")
        if expand_home:
            home = Path.home()
        return str(home / f".{cls.__name__.lower()}" / filename)

    @classmethod
    def ssh_sock(cls, filename="rtfm.ssh.sock"):
        return cls.config_path(filename)

    def get_default_networks(self, category="any"):
        # 'svc' category does not use APT pool
        # 'any' category uses any IP in APT pool
        networks = []
        apt = os.getenv("AVN_APT")
        env_network = os.getenv("AVN_NET")
        if env_network:
            networks = [env_network]
        elif apt is not None and category != "svc":
            profile = self.profile_path(codename=apt)
            infra = yaml.safe_load(profile.read_text())
            if category != "any" and category in infra:
                networks = infra[category]
            else:
                for nets in infra.values():
                    for n in nets:
                        networks.append(n)
        else:
            networks = self.config.networks

        return networks

    def networks(self, networks, category="any"):
        if not networks:
            networks = self.get_default_networks(category=category)
        return [IPv4Network(net) for net in networks]

    def rand_ip(self):
        return rand_ip(self.networks(None))

    def is_server(self) -> bool:
        return self.config.server_mode

    def server_only(self):
        if not self.is_server():
            raise Exception("[-] Operation only permitted in server mode")

    def docker_path(self, name, require=True):
        docker_path = None
        if self.config is None:
            try:
                self.init(self.config_path("config.yaml"))
                docker_path = self.config.docker_path
            except Exception:
                pass
        else:
            docker_path = self.config.docker_path

        if docker_path is None:
            docker_path = Path(__file__).parent / "docker" / name
        if require and docker_path is None or not Path(docker_path).exists():
            raise FileNotFoundError(f"[-] Invalid docker_path({name}): {docker_path}")
        return docker_path

    def docs_path(self, require=True):
        docs_path = None
        if self.config is None:
            try:
                self.init(self.config_path("config.yaml"))
                docs_path = self.config.docs_path
            except Exception:
                pass
        else:
            docs_path = self.config.docs_path

        if docs_path is None:
            docs_path = Path(__file__).parent
        if require and docs_path is None or not Path(docs_path).exists():
            raise FileNotFoundError(f"[-] Invalid docs_path(): {docs_path}")
        return docs_path

    def data_path(self, name, make=False, require=False):
        data_path = "/opt/rtfm"
        if self.config is None:
            try:
                self.init(self.config_path("config.yaml"))
                data_path = self.config.data_path
            except Exception:
                pass
        else:
            data_path = self.config.data_path

        path = Path(data_path).expanduser() / name
        if require and not path.is_dir():
            raise FileNotFoundError(f"[-] Invalid data_path({name}): {path}")
        elif make:
            path.mkdir(parents=True, exist_ok=True)
        return path

    def profile_path(self, codename=None, strict=True):
        profile_path = Path(self.data_path("apt/profiles"))
        if codename is not None:
            profile_path = profile_path / codename / f"{codename}.yaml"
            if strict and not profile_path.is_file():
                raise FileNotFoundError(f"{profile_path} not found")
        return profile_path

    def profile_infra(self, codename=None, strict=True):
        profile_path = self.profile_path(codename=codename, strict=strict)
        return yaml.safe_load(profile_path.read_text())

    def listener(
        self,
        networks,
        remote_ports,
        dest_ip,
        dest_port=None,
        echo=True,
        codename=None,
        deconflict=True,
        register=None,
        random_register=0,
        suppress_register=False,
    ) -> AutoVnetListener:

        # Only do deconfliction when generating new listeners, not when loading them from disk
        if deconflict is None:
            deconflict = networks is None

        networks = self.networks(networks)
        if dest_port is None:
            dest_port = rand_port()

        lns = []
        # Require unique IPs for a single listener
        remote_ips = rand_ip_list(networks, len(remote_ports))
        remote_ip_set = None
        deconflict_attempts = 256
        any_ip_msg = False
        for remote_port, remote_ip in zip(remote_ports, remote_ips):
            ln = None
            attempts = deconflict_attempts
            while True:
                ln = Listener(
                    remote_ip=str(remote_ip),
                    remote_port=remote_port,
                    dest_ip=dest_ip,
                    dest_port=dest_port,
                )
                if deconflict:
                    ln.dest_ip = DECONFLICT_IP
                    avn_listener = AutoVnetListener(self, [ln], echo=False)
                    try:
                        with avn_listener.listen() as proc:
                            if self.config.rtfm_creds.reuse:
                                # ok, would have raised
                                avn_listener.mux_cancel()
                                break
                            try:
                                timeout = 0.5
                                proc.wait(timeout=timeout)
                                typer.echo(f"[-] Failed to allocate {ln.remote()}", err=True)
                            except subprocess.TimeoutExpired:
                                # ok, no conflict (for now)
                                proc.terminate()
                                proc.wait()
                                break
                    except subprocess.CalledProcessError:
                        # reuse is on, but forward failed
                        pass

                    # Conflicted, roll new remote_ip
                    remote_ip = rand_ip(networks)
                    if remote_ip_set is None:
                        remote_ip_set = set(remote_ips)

                    # This could spinlock, depending on math - don't ask for silly allocations
                    while remote_ip in remote_ip_set:
                        if attempts <= 0:
                            raise Exception(f"[-] Failed to find an available IP!")
                        if not any_ip_msg and deconflict_attempts - attempts > 16:
                            # Broaden search, ignore -n / --network if given
                            typer.echo(f"[!] Searching for any available IP", err=True)
                            networks = self.networks(None)
                            any_ip_msg = True
                        remote_ip = rand_ip(networks)
                        attempts -= 1
                else:
                    # No defconfliction, no check
                    break
            ln.dest_ip = dest_ip
            lns.append(ln)

        if register is None:
            register = []

        num_register = len(register) + random_register
        if num_register > 0:
            if num_register < len(lns):
                random_register = len(lns) - len(register)
            names = make_dns_names(register=register, random_register=random_register, tld=avn.config.dns.tld)
            for ln, name in zip(lns, names):
                ln.dns_names = [name]

        return AutoVnetListener(self, lns, echo=echo, codename=codename, suppress_register=suppress_register)

    def listener_from_models(
        self, lns: List[Listener], echo=True, codename=None, suppress_register=False
    ) -> AutoVnetListener:
        return AutoVnetListener(self, lns, echo=echo, codename=codename, suppress_register=suppress_register)

    def listener_from_yaml(self, yml, echo=True, codename=None) -> AutoVnetListener:
        spec = yaml.safe_load(yml)
        models = [Listener.parse_obj(ln) for ln in spec["listeners"]]
        return avn.listener_from_models(models, echo=echo, codename=codename)

    def listener_from_file(self, path, echo=True, codename=None) -> AutoVnetListener:
        return self.listener_from_yaml(Path(path).read_text(), echo=echo, codename=codename)

    def listener_from_codename(
        self, codename, echo=True, payload_dir=None, filename=None, suffix=""
    ) -> AutoVnetListener:
        if payload_dir is None:
            payload_dir = self.data_path("c2")
        if filename is None:
            if suffix:
                suffix = f".{suffix}"
            filename = f"{codename}{suffix}.yaml"
        c2_yaml = Path(payload_dir) / codename / filename
        return self.listener_from_file(c2_yaml, echo=echo, codename=codename)

    @contextmanager
    def _svc(
        self,
        networks=None,
        infra_category="svc",
        count=None,
        dest_ip=None,
        dest_port=None,
        remote_port=None,
        payload_dir=None,
        codename=None,
        echo=True,
        deconflict=True,
        suffix="",
    ) -> (subprocess.Popen, str):
        """Helper for svc()"""
        cmd = ["autovnet", "rtfm", "svc", "export"]
        if networks is not None:
            for net in networks:
                cmd.extend(["--network", str(net)])
        if infra_category is not None:
            cmd.extend(["--infra-category", infra_category])

        if count is not None:
            cmd.extend(["--count", str(count)])
        if dest_ip is not None:
            cmd.extend(["--dest-ip", str(dest_ip)])
        if dest_port is not None:
            cmd.extend(["--dest-port", str(dest_port)])
        if payload_dir is not None:
            cmd.extend(["--payload-dir", str(payload_dir)])
        if deconflict is False:
            # Probably an internal call, where caller is forcing an allocation
            cmd.append("--no-deconflict")
        if codename is None:
            codename = gen_codename()
        cmd.extend(["--set-codename", str(codename)])
        if suffix:
            cmd.extend(["--set-suffix", str(suffix)])
        if remote_port is not None:
            cmd.append(str(remote_port))

        stdout = None
        stderr = None
        if not echo:
            stdout = subprocess.DEVNULL
            stderr = subprocess.DEVNULL
        proc = None
        try:
            proc = subprocess.Popen(cmd, stdout=stdout, stderr=stderr)
            yield proc, codename
        finally:
            if proc is not None and proc.poll() is None:
                proc.terminate()
                proc.wait()

            # For some reason, the stunnel container can keep running for a few seconds,
            # even after we kill the svc export command.

            # This is a hack to verify that the container itself actually exited
            container_prefix = f"svc_export_{codename}"
            if not wait_container_exit(container_prefix):
                raise Exception(f"[-] Container for {container_prefix} is still running!")

    @contextmanager
    def svc(
        self,
        networks=None,
        dest_ip=None,
        dest_port=None,
        remote_port=None,
        payload_dir=None,
        codename=None,
        echo=True,
        suffix="",
    ) -> (subprocess.Popen, str, str):
        """Gross wrapper around a subprocess to `autovnet rtfm svc export`

        Probably not suitable for interactive foreground processes.
        Obviously bad idea.

        The goal is provide a usable interface to calling code that can ignore the terrible things we are about to do.

        Example use:
        with avn.svc(...) as (proc, codename, remote_addr):
            typer.echo(f"[+] mount: autovnet rtfm svc mount -C {codename} {remote_addr}")
            compose_up(...)
        """
        # 1. We need to pre-deconflict a public IP:PORT so we can return it to the caller
        # Thankfully, we can (usually) force a random port here, so conflicts are super unlikely
        if remote_port is None:
            remote_port = rand_port()
        ln = self.listener(networks, [remote_port], dest_ip, dest_port=dest_port, codename=codename)
        # ln is deconflicted and has a codename
        codename = ln.codename
        # Simplifying assumptions - count not supported, exactly 1 listener
        remote_addr = ln.lns[0].remote()
        remote_ip = ln.lns[0].remote_ip

        # Force the svc export to allocate the public_addr and codename that we know
        # (IPC hard)
        with self._svc(
            networks=[remote_ip],
            remote_port=remote_port,
            dest_ip=dest_ip,
            dest_port=dest_port,
            payload_dir=payload_dir,
            codename=codename,
            echo=echo,
            deconflict=False,
            suffix=suffix,
        ) as (export_proc, codename):
            yield export_proc, codename, remote_addr

    @contextmanager
    def svc_from_codename(self, codename, echo=True, payload_dir=None, suffix=""):
        cmd = ["autovnet", "rtfm", "svc", "export"]
        if payload_dir is not None:
            cmd.extend(["--payload-dir", str(payload_dir)])
        if suffix:
            cmd.extend(["--set-suffix", suffix])
        cmd.append(str(codename))

        # Peek at the listener so we can figure out the remote_addr
        ln = self.listener_from_codename(codename, payload_dir=payload_dir, suffix=suffix)
        remote_addr = ln.lns[0].remote()

        stdout = None
        stderr = None
        if not echo:
            stdout = subprocess.DEVNULL
            stderr = subprocess.DEVNULL
        proc = None
        try:
            proc = subprocess.Popen(cmd, stdout=stdout, stderr=stderr)
            yield proc, codename, remote_addr
        finally:
            if proc is not None and proc.poll() is None:
                proc.terminate()
                proc.wait()

    @contextmanager
    def svc_mount(
        self,
        codename=None,
        opt=None,
        dest_ip="127.0.0.1",
        dest_port=None,
        mount_type="svc",
        echo=False,
        echo_ready=True,
        echo_mount=True,
        payload_dir=None,
        xdg_open=None,
        wait_tcp=False,
        suffix="",
        custom_mount_args=None,
        url_path="",
        keep_mount=True,
    ):
        if dest_port is None:
            dest_port = rand_port()

        if dest_ip is None:
            dest_ip = "127.0.0.1"

        set_suffix = suffix
        cmd = ["autovnet", "rtfm", "svc", "mount", "--mount-type", mount_type]

        codename_pattern = re.compile("^[a-z_]+$")
        if codename is not None:
            # Check for existing dir
            if suffix:
                suffix = f".{suffix}"
            old_config = Path(payload_dir) / codename / "mount{suffix}.yaml"
            if old_config.is_file():
                typer.echo(f"[!] {old_config} already exists! Loading previous config..")
                opt = codename
                codename = None

        if opt is not None and codename_pattern.match(opt):
            codename = opt
            # Loading a codename, peek at the mount config to get the local addr
            mnt = Mount.load(payload_dir, codename, suffix=set_suffix)
            dest_ip = mnt.svc_host
            dest_port = mnt.svc_port
        else:
            # Pass thru any svc options
            if dest_ip != "127.0.0.1":
                cmd.extend(["-d", dest_ip])
            if dest_port is not None:
                cmd.extend(["-p", str(dest_port)])

            if codename is not None:
                cmd.extend(["-C", codename])

        if set_suffix:
            cmd.extend(["--set-suffix", set_suffix])

        cmd.extend(["-D", str(payload_dir)])
        if opt is not None:
            cmd.append(opt)

        if codename is not None:
            cmd_str = f"autovnet rtfm {mount_type} mount {codename}"
            if set_suffix:
                cmd_str += f" --set-suffix {set_suffix}"
            if custom_mount_args:
                cmd_str += custom_mount_args
            if echo_mount:
                typer.echo(f"[+] {cmd_str}")

        proc = None
        stdout = None
        stderr = None
        if not echo:
            stdout = subprocess.DEVNULL
            stderr = subprocess.DEVNULL

        # No service check
        ok = None
        if wait_tcp or xdg_open is not None:
            # Doing a service check
            ok = False
            # Helpful especially on the first run,
            #   when we pull the image and it takes a while
            if echo_ready:
                typer.echo(f"[*] Waiting for {mount_type} ...")

        try:
            proc = subprocess.Popen(cmd, stdout=stdout, stderr=stderr)

            if wait_tcp:
                ok = wait_tcp_open(dest_ip, dest_port, proc=proc)
                if echo and not ok:
                    typer.echo(f"[!] ran: {shlex.join(cmd)}")

            url = f"{xdg_open}://{dest_ip}:{dest_port}{url_path}"
            if xdg_open is not None:
                typer.echo(f"[+] connect: xdg-open {url}")
                ok = wait_url_open(url, proc=proc)

            if echo_ready:
                if ok:
                    typer.echo(f"[+] {mount_type} is ready!")
                elif ok is False:
                    typer.echo(f"[!] {mount_type} may still be initializing")

            if xdg_open is not None:
                open_url(url)
            # Caller must wait
            yield proc
        finally:
            if proc is not None and proc.poll() is None:
                if echo:
                    typer.echo(f"[+] Shutting down mount (please wait)...")
                while proc.poll() is None:
                    proc.terminate()
                    try:
                        proc.wait(timeout=5)
                    except subprocess.TimeoutExpired:
                        pass
            if codename and not keep_mount:
                rm_dir = Path(payload_dir) / codename
                shutil.rmtree(rm_dir)

    @contextmanager
    def dns_record(
        self,
        dest_ip="127.0.0.1",
        dest_port=None,
        register=None,
        networks=None,
        opt=None,
        codename=None,
        payload_dir=None,
        echo=False,
        register_echo=False,
        unregister=True,
        unregister_only=False,
        keep_mount=False,
    ):
        """Create a DNS record"""

        if payload_dir is None:
            payload_dir = Path(self.data_path("tmp"))

        if dest_port is None:
            dest_port = rand_port()

        if opt is None:
            opt = f"{self.config.dns.host}:{self.config.dns.port}"

        url = f"http://{dest_ip}:{dest_port}"
        api_key = self.config.rtfm_creds.password
        api_url = f"{url}/api/v1"
        api_client = powerdns.PDNSApiClient(
            api_endpoint=api_url,
            api_key=api_key,
        )
        api = powerdns.PDNSEndpoint(api_client)

        tld = self.config.dns.tld
        networks = avn.networks(networks)

        if echo:
            typer.echo(f"[+] Connecting...")

        if not keep_mount and codename is None:
            # Need to pre-compute codename so we can delete the dir
            codename = gen_codename()

        cancelled = False
        with avn.svc_mount(
            codename=codename,
            opt=opt,
            dest_ip=dest_ip,
            dest_port=dest_port,
            payload_dir=payload_dir,
            mount_type="dns",
            echo_mount=False,
            keep_mount=keep_mount,
        ) as proc:
            ok = wait_url_open(url, proc=proc)
            if echo:
                typer.echo(f"[+] dns ready!")

            if not ok:
                raise Exception(f"[-] DNS not ready")

            echo_func = None
            if register_echo:
                echo_func = typer.echo
            registered = []
            try:
                if unregister_only:
                    dns_unregister(register, api, tld=tld, echo_func=echo_func)
                    yield [], proc
                else:
                    registered, cancelled = dns_register(register, api, tld=tld, networks=networks, echo_func=echo_func)
                    if cancelled:
                        # Interrupted
                        raise KeyboardInterrupt
                    else:
                        yield registered, proc
            except Exception as e:
                typer.echo(f"[-] dns error: {e}")
                raise
            finally:
                if unregister and not unregister_only:
                    dns_unregister(registered, api, tld=tld, echo_func=echo_func)


global avn
avn = AutoVnet()
