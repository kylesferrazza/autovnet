import os
import shutil
import subprocess
from typing import Optional
from pathlib import Path

import typer

from ..autovnet import avn
from autovnet.utils import gen_codename

MOUNT_TYPE = "tmp"

app = typer.Typer()


@app.command()
def dir(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to create tmp dir in."
    ),
):
    """Create and print an autovnet tmp / scratch directory"""

    path = Path(payload_dir) / gen_codename()
    # Error out if we somehow conflict
    path.mkdir(parents=True, exist_ok=False)
    typer.echo(f"{path}")


@app.command()
def clean(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to create tmp dir in."
    ),
):
    """Delete all autovnet tmp directories"""

    path = Path(payload_dir)
    if path.is_dir():
        confirm = typer.confirm(f"Recursively delete {path} ?")
        if confirm:
            shutil.rmtree(path)


@app.command()
def shell(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to create tmp dir in."
    ),
    shell: Optional[str] = typer.Option(None, "-s", "--shell", help="Alternate shell to use."),
    keep: Optional[bool] = typer.Option(False, "-k", "--keep", help="Keep the tmp dir after the shell exits."),
):
    """Start a shell in a clean tmp directory"""

    path = Path(payload_dir) / gen_codename()
    # Error out if we somehow conflict
    path.mkdir(parents=True, exist_ok=False)
    typer.echo(f"[+] {path}")
    if shell is None:
        shell = os.getenv("SHELL")
    if shell is None:
        raise ValueError(f"SHELL not found!")
    subprocess.run(shell, cwd=path)
    if not keep:
        shutil.rmtree(path)
