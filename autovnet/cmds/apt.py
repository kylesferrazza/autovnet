import os
import string
import random
import shutil
import subprocess
from pathlib import Path
from typing import Optional, List
from ipaddress import IPv4Address, IPv4Network

# 3.7
import importlib.resources

import yaml
import typer
import pyfiglet

from autovnet import __version__
from autovnet.utils import (
    rand_ip_list,
    gen_adjective,
    yaml_dump,
    complete_codename,
    complete_apt_dns,
    complete_apt_geo,
)
from autovnet.models import APTSize
from ..autovnet import AutoVnet, avn

app = typer.Typer()

MOUNT_TYPE = "apt"

APT_ENV_VAR = "AVN_APT"

DEFAULT_FONT = "cybermedium"
DEFAULT_REGION_CIDR = 20
SMALL_REGION_CIDR = 28
SMALL_REGION_THRESHOLD = 16

DEFAULT_GEO_PERCENT = 90
#  Use most of the IP pool for possible use by players
DEFAULT_FAMILY_PERCENT = 80

UNKNOWN_NAME = "UNKNOWN"


@app.command()
def init(
    networks: Optional[List[str]] = typer.Option(
        None,
        "-n",
        "--network",
        help="Simulated network to draw a random IP from.",
    ),
    new_prefix: Optional[int] = typer.Option(
        None,
        "-p",
        "--new-prefix",
        help="",
    ),
    regions_file: Optional[str] = typer.Option(
        None,
        "-r",
        "--regions-file",
        help="Read regions from a file instead of using the defaults",
    ),
    families_file: Optional[str] = typer.Option(
        None,
        "-f",
        "--families-file",
        help="Read APT regions from a file instead of using the defaults",
    ),
    geo_percent: Optional[int] = typer.Option(
        90,
        "-G",
        "--geo-percent",
        help="Percent of regions that are associated with a particular region",
    ),
    family_percent: Optional[int] = typer.Option(
        80,
        "-F",
        "--family-percent",
        help="Percent of regions that are associated with a particular APT family",
    ),
    out_dir: Optional[str] = typer.Option(
        avn.config_path(MOUNT_TYPE),
        "-D",
        "--out-dir",
        help="Directory to save output.",
    ),
):
    """Paritiion the autovnet network space into regions and APT families


    * A "region" is an arbitrary partion that simulates the concept of a geolocatable region (like a country)

    * An "APT Family" is an arbitrary partiion that simulates the concept of a logical Advanced Persisent Threat actor / sponsor
        - E.g. similar to the "Suspected attribution" from https://www.mandiant.com/resources/apt-groups
        - an APT Family is associated with a set of infrastructure, which could span many regions

    * An "APT" or "APT Profile" is a single, named APT that is logically responsible for a set of activities
        - an APT has a set of infrastructure that could overlap with other APTs within the same APT family
        - an APT could run multiple "campaigns" / attack chains with unique TTPs and IoCs

    From the perspective of the defender, correct attribution / clustering is intentionally difficult,
        and depending on the amount of data availalbe, may not be possible.

    This is realistic, see the concept of UNC groups: https://www.mandiant.com/resources/unc-groups
    """

    avn.server_only()

    out_dir = Path(out_dir)
    regions_dir = out_dir / "regions"
    families_dir = out_dir / "families"

    regions_dir.mkdir(parents=True, exist_ok=True)
    families_dir.mkdir(parents=True, exist_ok=True)

    region_names, family_names = init_apt_names(out_dir, regions_file=regions_file, families_file=families_file)

    region_files = {}
    family_files = {}

    region_counts = {r: 0 for r in region_names}
    family_counts = {f: 0 for f in family_names}

    region_hosts = {r: 0 for r in region_names}
    family_hosts = {f: 0 for f in family_names}

    for d in region_counts, family_counts, region_hosts, family_hosts:
        d[UNKNOWN_NAME] = 0

    try:
        for region in region_names:
            path = regions_dir / f"{region}.list"
            region_files[region] = Path(path).open("w")
        path = regions_dir / f"{UNKNOWN_NAME}.list"
        region_files[UNKNOWN_NAME] = Path(path).open("w")

        for family in family_names:
            path = families_dir / f"{family}.list"
            family_files[family] = Path(path).open("w")
        path = families_dir / f"{UNKNOWN_NAME}.list"
        family_files[UNKNOWN_NAME] = Path(path).open("w")

        networks = avn.networks(networks)
        for network in networks:
            diff = DEFAULT_REGION_CIDR - network.prefixlen
            if network.prefixlen > SMALL_REGION_THRESHOLD:
                diff = SMALL_REGION_CIDR - network.prefixlen
            kwargs = {}
            if new_prefix is not None:
                kwargs["new_prefix"] = new_prefix
            else:
                kwargs["prefixlen_diff"] = diff

            for subnet in network.subnets(**kwargs):

                region = UNKNOWN_NAME
                if random.random() < geo_percent / 100:
                    region = random.choice(region_names)
                f = region_files[region]
                f.write(f"{subnet}\n")
                region_counts[region] += 1
                region_hosts[region] += subnet.num_addresses

                family = UNKNOWN_NAME
                if random.random() < family_percent / 100:
                    family = random.choice(family_names)
                f = family_files[family]
                f.write(f"{subnet}\n")
                family_counts[family] += 1
                family_hosts[family] += subnet.num_addresses

    finally:
        for key, path in region_files.items():
            path.close()
        for key, path in family_files.items():
            path.close()

    typer.echo("\n")
    for region, count in region_counts.items():
        typer.echo(f"{region}: {region_counts[region]} networks / {region_hosts[region]} hosts")

    for family, count in family_counts.items():
        typer.echo(f"{family}: {family_counts[family]} networks / {family_hosts[family]} hosts")


@app.command()
def new(
    families_file: Optional[str] = typer.Option(
        avn.config_path("apt/families.list"),
        "-f",
        "--families-file",
        help="Read APT regions from a file instead of using the defaults",
    ),
    regions_file: Optional[str] = typer.Option(
        None,
        "-r",
        "--regions-file",
        help="Read regions from a file instead of using the defaults",
    ),
    templates_file: Optional[str] = typer.Option(
        avn.config_path("apt/templates.yaml"),
        "-t",
        "--templates-file",
        help="Read APT profile templates from this file",
    ),
    families_dir: Optional[str] = typer.Option(
        avn.config_path("apt/families"),
        "-d",
        "--families-dir",
        help="Directory containing FAMILY.list files",
    ),
    family: Optional[str] = typer.Option(
        None,
        "-F",
        "--family",
        help="Specify the APT family to use",
    ),
    out_path: Optional[str] = typer.Option(
        None,
        "-o",
        "--out-file",
        help="Path to save generated profile",
    ),
    size: Optional[APTSize] = typer.Option(
        "default",
        "-s",
        "--size",
        help="",
    ),
    no_dns: Optional[bool] = typer.Option(
        False,
        "-N",
        "--no-dns",
        help="Do not pre-register DNS names, even if template has dns: true.",
    ),
    no_prompt: Optional[bool] = typer.Option(
        False,
        "--no-prompt",
        help="Do not attempt to update shell prompt when this profile is activated.",
    ),
    out_dir: Optional[str] = typer.Option(
        avn.config_path(MOUNT_TYPE),
        "-D",
        "--out-dir",
        help="Directory to save output.",
    ),
):
    """Generate a new APT profile


    * Logically, an APT Profile is a single actor responsible for a set of campaigns or attacks

    * In practice, an APT Profile is a collection of infrastructure randomly sampled
        from the pool of infrastructure (e.g. IP addresses) associated with the APT family

    * When you use autovnet with an APT Profile active, autovnet will draw random IPs from your APT's infrastrucutre

        - This means that your IPs are categorized and more likely to be reused, making it possible for a defender to attribute a
            set of related actions and multiple IoCs to a single, logical actor

    * You are free to share sufficiently large APT Profiles between players,
        or each player can generate their own profile, depending on your goals and game dynamics

    """

    out_dir = Path(out_dir)
    region_names, family_names = init_apt_names(out_dir, regions_file=regions_file, families_file=families_file)
    if family is None:
        family = random.choice(family_names)

    typer.echo(f"[+] APT Family: {family.upper()}")

    src = Path(families_dir) / f"{family}.list"
    networks = src.read_text().splitlines()
    networks = avn.networks(networks)

    templates_file = Path(templates_file)

    templates = yaml.safe_load(templates_file.read_text())

    template = templates[size]
    dns = template.pop("dns", False)
    if no_dns:
        dns = False
    geo = template.pop("geo", False)
    # this logic only works if templates do not ask for a larger block than the block used by families

    typer.echo(f"[+] Assigning infrastructure (size={size})...")
    # figure out how many "blocks" we need
    blocks = 0
    hosts = {}
    for category, data in template.items():
        for cidr, count in data.items():
            blocks += count
            hosts.setdefault(category, 0)
            hosts[category] += count * (2 ** (32 - cidr))

    # first pass: get unique IP addresses (pretend everything is /32)
    assignments = rand_ip_list(networks, blocks)

    infra = {}
    # dns records
    register = []
    if dns:
        infra["dns"] = {}

    if geo:
        infra["geo"] = {}
        geomap = load_geomap(region_names)

    i = 0
    # gross
    for category, data in template.items():
        for cidr, count in data.items():
            for _ in range(count):
                net = f"{assignments[i]}/{cidr}"
                net = str(IPv4Network(net, strict=False).network_address)
                if cidr != 32:
                    net += f"/{cidr}"
                i += 1
                infra.setdefault(category, [])
                infra[category].append(net)
                if dns:
                    register.extend(str(ip) for ip in IPv4Network(net))
                if geo:
                    for ip in IPv4Network(net):
                        ip = str(ip)
                        infra["geo"][ip] = geolocate_ip(geomap, ip)

    adj = gen_adjective()
    codename = f"{adj}_{family}"
    art = pyfiglet.figlet_format(codename.replace("_", " "), font=DEFAULT_FONT)
    typer.echo(art)
    typer.echo(f"[+] name: {codename}")
    total_hosts = 0
    for category, host_addrs in hosts.items():
        typer.echo(f"[+] {category}: {host_addrs}")
        total_hosts += host_addrs

    typer.echo(f"[+] total: {total_hosts}")

    if register:
        typer.echo(f"[*] Creating {len(register)} DNS records...")
        with avn.dns_record(
            register=register,
            codename=codename,
            echo=False,
            register_echo=False,
            unregister=False,
        ) as (registered, proc):
            for record in registered:
                name, host = record.split(":", maxsplit=1)
                infra["dns"][host] = name
        typer.echo(f"[+] dns: {len(register)} records created")
    infra_str = yaml_dump(infra)

    apt_dir = avn.data_path(f"{MOUNT_TYPE}/profiles/{codename}")
    apt_dir.mkdir(parents=True, exist_ok=True)

    if out_path is None:
        out_path = apt_dir / f"{codename}.yaml"

    out_path = Path(out_path)
    out_path.write_text(infra_str)

    typer.echo(f"[+] profile: {out_path}")
    typer.echo(f"[+] activate: autovnet apt shell {codename}")

    if not no_prompt:
        # Hack update prompt
        # Only tested on Ubuntu (bash / zsh) and Kali (zsh)
        ps1 = os.getenv("PS1")
        prompt = os.getenv("PROMPT")
        shell = os.getenv("SHELL")
        if prompt is None and ps1 is None and shell is not None:
            # $PROMPT works weird, at least in zsh + Kali
            prompt = subprocess.check_output([os.getenv("SHELL"), "-i", "-c", 'echo -n "${PROMPT}"']).decode()

        venv = os.getenv("VIRTUAL_ENV_PROMPT")
        rc_test = ""
        if prompt is not None:
            # assume kali default zsh prompt
            venv = "${VIRTUAL_ENV:+"
            if venv in prompt:
                prompt = prompt.replace(venv, "${AVN_APT_PROMPT:-", 1)
            else:
                prompt = f"({codename}) " + prompt

            rc_text = """
export AVN_APT_PROMPT="(${AVN_APT})─"
"""
            rc_text += f"""
export PROMPT="{prompt}"
"""
        elif ps1 is not None:
            if venv in ps1:
                ps1 = ps1.replace(venv, f"({codename}) ")
            else:
                ps1 = f"({codename}) " + ps1

            rc_text = f"""
export PS1="{ps1}"
"""
        rc_file = apt_dir / "activate"
        rc_file.write_text(rc_text)


@app.command()
def shell(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(f"{MOUNT_TYPE}/profiles"), "-D", "--payload-dir", help="Directory to create tmp dir in."
    ),
    shell: Optional[str] = typer.Option(None, "-s", "--shell", help="Alternate shell to use."),
    no_rc: Optional[bool] = typer.Option(
        False, "--no-rc", help="Do not attempt to modmodify the shell's rc file, needed to update shell prompt"
    ),
    codename: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="Codename of the APT to switch to",
    ),
):
    """Activate an APT profile in the current shell"""
    payload_dir = Path(payload_dir)
    apt_dir = payload_dir / codename
    if not apt_dir.is_dir():
        raise FileNotFoundError(f"[!] {apt_dir} not found")

    if shell is None:
        shell = os.getenv("SHELL")
    if shell is None:
        raise ValueError(f"[!] SHELL not found!")

    profile_rc = f"""
"""

    activate_path = payload_dir / ("${" + APT_ENV_VAR + "}") / "activate"
    # lol
    profile_rc += (
        """
# added by autovnet """
        + __version__
        + """
if [ ! -z "${"""
        + APT_ENV_VAR
        + '''}" ]; then
    if [ -f "'''
        + str(activate_path)
        + '''" ]; then
        . "'''
        + str(activate_path)
        + """"
    fi
fi

"""
    )
    if not no_rc:
        shell_name = Path(shell).name
        shell_rc = Path(f"~/.{shell_name}rc").expanduser()
        shell_rc_text = shell_rc.read_text()
        if APT_ENV_VAR not in shell_rc_text:
            with shell_rc.open("a") as f:
                f.write(profile_rc)

    os.environ[APT_ENV_VAR] = codename

    subprocess.run(shell, env=os.environ)


@app.command()
def show(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(f"{MOUNT_TYPE}/profiles"), "-D", "--payload-dir", help="Directory to create tmp dir in."
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Codename of the APT to switch to.",
    ),
):
    """Show the current APT profile"""

    if codename is None:
        codename = os.getenv(APT_ENV_VAR)

    if codename is None:
        typer.echo(f"[!] No active profile ({APT_ENV_VAR}) not set")
        return

    hosts = {}

    profile_path = avn.profile_path(codename=codename)
    infra = yaml.safe_load(profile_path.read_text())

    for category, networks in infra.items():
        count = sum([IPv4Network(net).num_addresses for net in infra[category]])
        hosts[category] = count

    art = pyfiglet.figlet_format(codename.replace("_", " "), font=DEFAULT_FONT)
    typer.echo(art)
    typer.echo(f"[+] name: {codename}")

    total_hosts = 0
    hosts.pop("dns")
    hosts.pop("geo")
    for category, host_addrs in hosts.items():
        typer.echo(f"[+] {category}: {host_addrs}")
        total_hosts += host_addrs

    typer.echo(f"[+] total: {total_hosts}")
    typer.echo(f"[+] profile: {profile_path}")


@app.command()
def dns(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(f"{MOUNT_TYPE}/profiles"), "-D", "--payload-dir", help="Directory for APT profiles."
    ),
    codename: Optional[str] = typer.Option(
        None,
        "-p",
        "--profile",
        autocompletion=complete_codename,
        help="Codename of the APT profile.",
    ),
    ip: str = typer.Argument(
        ...,
        autocompletion=complete_apt_dns,
        help="IP to look up DNS name for.",
    ),
):
    """Look up a DNS name registered for an IP in the current APT profile"""

    if codename is None:
        codename = os.getenv(APT_ENV_VAR)

    if codename is None:
        raise ValueError(f"[!] No active profile ({APT_ENV_VAR}) not set")

    infra = avn.profile_infra(codename=codename)
    dns = infra.get("dns", {})

    # value error on bad input
    try:
        ip = str(IPv4Address(ip))
    except Exception as e:
        raise ValueError(f"[!] Failed parsing IP ({ip}): {e}")

    if ip not in dns:
        raise ValueError(f"[!] {codename} has no dns entry for {ip}")

    typer.echo(dns[ip])


@app.command()
def geo(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(f"{MOUNT_TYPE}/profiles"), "-D", "--payload-dir", help="Directory for APT profiles."
    ),
    codename: Optional[str] = typer.Option(
        None,
        "-p",
        "--profile",
        autocompletion=complete_codename,
        help="Codename of the APT profile.",
    ),
    ip: str = typer.Argument(
        ...,
        autocompletion=complete_apt_geo,
        help="IP to look up geo region for.",
    ),
):
    """Look up a region associated with an IP in the current APT profile"""

    if codename is None:
        codename = os.getenv(APT_ENV_VAR)

    if codename is None:
        raise ValueError(f"[!] No active profile ({APT_ENV_VAR}) not set")

    infra = avn.profile_infra(codename=codename)
    geo = infra.get("geo", {})

    # value error on bad input
    try:
        ip = str(IPv4Address(ip))
    except Exception as e:
        raise ValueError(f"[!] Failed parsing IP ({ip}): {e}")

    if ip not in geo:
        raise ValueError(f"[!] {codename} has no dns entry for {ip}")

    typer.echo(geo[ip])


@app.command()
def register(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(f"{MOUNT_TYPE}/profiles"), "-D", "--payload-dir", help="Directory for APT profiles."
    ),
    codename: Optional[str] = typer.Option(
        None,
        "-p",
        "--profile",
        autocompletion=complete_codename,
        help="Codename of the APT profile.",
    ),
):
    """Register DNS names for APT infrastructure"""

    if codename is None:
        codename = os.getenv(APT_ENV_VAR)

    if codename is None:
        raise ValueError(f"[!] No active profile ({APT_ENV_VAR}) not set")

    infra = avn.profile_infra(codename=codename)
    dns = infra.get("dns", {})
    register = [f"{name}:{ip}" for ip, name in dns.items()]

    typer.echo(f"[*] Creating {len(register)} DNS records...")
    with avn.dns_record(
        register=register,
        codename=codename,
        echo=False,
        register_echo=False,
        unregister=False,
    ) as (registered, proc):
        typer.echo(f"[+] dns: {len(registered)} records created")


@app.command()
def unregister(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(f"{MOUNT_TYPE}/profiles"), "-D", "--payload-dir", help="Directory for APT profiles."
    ),
    codename: Optional[str] = typer.Option(
        None,
        "-p",
        "--profile",
        autocompletion=complete_codename,
        help="Codename of the APT profile.",
    ),
):
    """Unregister DNS records for current APT profile"""

    if codename is None:
        codename = os.getenv(APT_ENV_VAR)

    if codename is None:
        raise ValueError(f"[!] No active profile ({APT_ENV_VAR}) not set")

    infra = avn.profile_infra(codename=codename)
    dns = infra.get("dns", {})
    register = [f"{name}:{ip}" for ip, name in dns.items()]

    typer.echo(f"[*] Unregister {len(register)} DNS records...")
    with avn.dns_record(
        register=register,
        codename=codename,
        echo=False,
        register_echo=False,
        unregister_only=True,
    ) as (registered, proc):
        pass

    typer.echo(f"[+] dns: {len(register)} records deleted")


################
# helpers


def load_geomap(regions):
    """Load region -> List[IPv4Network] mapping from disk

    Because we use big network blocks, even a /4 sim net is only 1MB
    """

    geomap = {}
    for region in regions:
        nets = Path(avn.config_path(f"apt/regions/{region}.list")).read_text().splitlines()
        geomap[region] = [IPv4Network(net) for net in nets]

    return geomap


def geolocate_ip(geomap, ip):
    """Return the region associated with IP, according to geomap"""
    ip = IPv4Address(ip)
    # shuffle keys so our runtime averages out, regardless of region
    regions = list(geomap.keys())
    random.shuffle(regions)
    for region in regions:
        for net in geomap[region]:
            if ip in net:
                return region
    return "UNKNOWN"


def init_apt_names(out_dir, regions_file=None, families_file=None):
    region_names = None
    family_names = None

    if regions_file is None:
        src = importlib.resources.files("autovnet.files") / "apt" / "regions.list"
        region_names = src.read_text().splitlines()

        regions_file = out_dir / "regions.list"
        if not regions_file.is_file():
            shutil.copyfile(src, regions_file)
    else:
        region_names = Path(regions_file).read_text().splitlines()

    if families_file is None:
        src = importlib.resources.files("autovnet.files") / "apt" / "families.list"
        family_names = src.read_text().splitlines()

        families_file = out_dir / "families.list"
        if not families_file.is_file():
            shutil.copyfile(src, families_file)
    else:
        family_names = Path(families_file).read_text().splitlines()

    templates_file = out_dir / "templates.yaml"
    if not templates_file.is_file():
        src = importlib.resources.files("autovnet.files") / "apt" / "templates.yaml"
        shutil.copyfile(src, templates_file)

    return region_names, family_names
