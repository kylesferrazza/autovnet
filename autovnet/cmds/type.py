from pathlib import Path
from typing import Optional, List

import typer

from autovnet.utils import (
    ghost_type_ctx,
)
from ..autovnet import AutoVnet, avn

app = typer.Typer()


@app.command()
def it(
    files: Optional[List[str]] = typer.Option(None, "-f", "--file", help="Files to type into the "),
    args: List[str] = typer.Argument(..., help="Command to invoke. (Use -- my -c ommand --with flags.)"),
):
    """Execute cmd interactively, but ask a ghost to type text into STDIN.

    Prints a special file path that you can write to (e.g. with echo / cat).

        * any text you write will be typed by the ghost

    Unix only (lol)
    WARNING: gigahax
    """
    text = "\n".join(Path(p).read_text() for p in files)
    with ghost_type_ctx(args, text) as (proc, tty_path):
        typer.echo(f"[+] control: {tty_path}")
        proc.wait()
        typer.echo(f"[+] type it exited: {proc.poll()}")
