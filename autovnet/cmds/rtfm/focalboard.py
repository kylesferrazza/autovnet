import os
import gzip
import shutil
import shlex
import subprocess
from pathlib import Path
from typing import Optional, List

import typer

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount
from autovnet.utils import (
    compose_up_ctx,
    complete_codename,
    read_env_file,
    rand_port,
    wait_url_open,
    open_url,
    ensure_cert,
    pg_dumper,
)

app = typer.Typer()

MOUNT_TYPE = "focalboard"


@app.command()
def export(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).",
    ),
    dest_port: Optional[int] = typer.Option(
        None, "-p", "--dest-port", help="Local bind port for the server. (default: random)."
    ),
    https: Optional[bool] = typer.Option(
        False, "-s", "--https", help="Use HTTPs instead of HTTP (not really more secure here)."
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    env_file: Optional[str] = typer.Option(
        avn.config_path("rtfm.env"),
        "-e",
        "--env-file",
        help="[ADVANCED] Environment file to pass to docker-compose",
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    init_sql: Optional[str] = typer.Option(
        None,
        "--init-sql",
        help="[ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.",
    ),
    force_init_sql: Optional[bool] = typer.Option(
        False,
        "--force-init-sql",
        help="[ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.",
    ),
    backup_interval: Optional[int] = typer.Option(
        300, "-i", "--backup-interval", help="Seconds between automatic backups (database exports)."
    ),
    backup_retain: Optional[int] = typer.Option(
        16,
        "-r",
        "--backup-retain",
        help="Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.",
    ),
    backup_quiet: Optional[bool] = typer.Option(
        False, "-Q", "--backup-quiet", help="If set, do not print backup confirmation messages."
    ),
    no_backup: Optional[bool] = typer.Option(
        False, "-B", "--no-backup", help="If set, do not make automated backups of the server contents."
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load",
    ),
):
    """Run a self-hosted instance of focalboard

    Focalboard is an open source, self-hosted alternative to Trello, Notion, and Asana.
    You can make kanban boards, tasks, to-do lists, etc. to keep your team organized!
    (The "Project Tasks" board type is the best for simple task tracking.)

    The first user to register (hopefully you, the export-er) is the admin,
    and must generate an invite link so other players can register.

    1. Register with my_handle@x.localhost and a random, unique password
    2. Click the Focalboard logo
    3. Invite Users
    4. Share the mount instructions and invite link with other players

    """
    confirmed = True

    if dest_port is None:
        dest_port = rand_port()

    env = read_env_file(env_file)

    ctx = None
    new_svc = False

    if codename is None:
        # New svc
        ctx = lambda: avn.svc(
            networks=networks, dest_ip=dest_ip, dest_port=dest_port, payload_dir=payload_dir, echo=False
        )
        new_svc = True
    else:
        # Loaded svc
        ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False)
        # Peek at listener config to grab the values we need
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir)
        model = ln.lns[0]
        dest_ip = model.next_dest_ip
        dest_port = model.next_dest_port

        if init_sql is not None and not force_init_sql:
            # Confirm so that we don't accidentally delete a database if the user re-executes a command in their history
            confirmed = typer.confirm("[!] --init-sql provided but database may already exist! Import anyway?")
            if not confirmed:
                typer.echo("[-] Ignoring --init-sql")

    # Prepare compose
    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    proto = "http"
    focalboard_ssl = "false"
    if https:
        proto = "https"
        focalboard_ssl = "true"

    focalboard_bind = f"{dest_ip}:{dest_port}"
    url = f"{proto}://{focalboard_bind}"

    tmp_env = {
        "FOCALBOARD_BIND": focalboard_bind,
        "FOCALBOARD_PORT": f"{dest_port}",
        "VIRTUAL_PROTO": proto,
        "FOCALBOARD_SSL": focalboard_ssl,
        "FOCALBOARD_CONFIG_TEMPLATE": str(avn.docker_path(f"plugins/{MOUNT_TYPE}/config.json")),
    }

    with ctx() as (proc, codename, remote_addr):

        # Prepare codename directory, now that we know the codename
        data_dir = Path(payload_dir) / codename / "data"
        backups_dir = Path(payload_dir) / codename / "backups"
        certs_dir = Path(payload_dir) / codename / "certs"
        for _dir in [data_dir, backups_dir, certs_dir]:
            _dir.mkdir(parents=True, exist_ok=True)

        # focalboard silently reverts to http if you don't put certs in the magic hardcoded path (:
        # (This is not documented at https://www.focalboard.com/guide/admin/)
        # https://github.com/mattermost/focalboard/blob/bf23bd0d25714a7a9fe3450a81d7cf2874fc7087/server/web/webserver.go#L106
        cert_path = certs_dir / "cert.pem"
        key_path = certs_dir / "key.pem"

        if https:
            ensure_cert(cert_path, key_path)

        tmp_env["FOCALBOARD_DATA"] = str(data_dir)
        tmp_env["FOCALBOARD_CERTS"] = str(certs_dir)
        tmp_env.update(env)
        env = tmp_env
        env["UID"] = str(os.geteuid())
        env["GID"] = str(os.getegid())

        project = f"{MOUNT_TYPE}_{codename}"
        # dest_port included because invite link will contain it
        mount_cmd = f"autovnet rtfm {MOUNT_TYPE} mount -C {codename} {remote_addr} -p {dest_port}"
        export_cmd = f"autovnet rtfm {MOUNT_TYPE} export {codename}"

        if https:
            mount_cmd += " -s"
            export_cmd += " -s"

        typer.echo(f"[+] {export_cmd}")
        typer.echo(f"[+] connect : xdg-open {url}")
        typer.echo(f"[+] mount   : {mount_cmd}")

        with compose_up_ctx(docker_path, env=env, project=project, quiet=True) as (
            compose_thread,
            compose_stop,
        ):
            typer.echo(f"[*] Waiting for {MOUNT_TYPE} ...")
            ok = wait_url_open(url, proc=proc, thread=compose_thread)
            if ok:
                typer.echo(f"[+] {MOUNT_TYPE} is ready!")
            elif ok is False:
                typer.echo(f"[!] {MOUNT_TYPE} may still be initializing")

            if not no_browser:
                open_url(url)

            if new_svc:
                typer.echo(
                    f"[+] Register (my_handle@x.localhost) > Click Logo > Invite Users > Distribute mount cmd + invite link"
                )

            # Leverage the fact we can pre-compute the database's fully qualified, unique container name
            db_container = f"{MOUNT_TYPE}_{codename}_db_1"

            if init_sql is not None and confirmed:
                # User confirmed they want to do this
                import_cmd = ["docker", "exec", "-i", db_container, "psql", "boards", "-U", "boardsuser"]
                typer.echo(f"[+] Importing {init_sql} ...")
                proc = subprocess.Popen(import_cmd, stdin=subprocess.PIPE, stdout=subprocess.DEVNULL)
                file_ctx = None
                if init_sql.endswith(".gz"):
                    file_ctx = lambda: gzip.GzipFile(init_sql, "rb")
                else:
                    file_ctx = lambda: open(init_sql, "rb")

                with file_ctx() as in_file:
                    shutil.copyfileobj(in_file, proc.stdin)
                proc.stdin.close()
                proc.wait()
                if proc.returncode == 0:
                    typer.echo(f"[+] Imported {init_sql}")
                else:
                    typer.echo(f"[!] Import {init_sql} failed: '{shlex.join(import_cmd)}' returned {proc.returncode}")

            if no_backup:
                compose_thread.join()
            else:
                backup_cmd = [
                    "docker",
                    "exec",
                    db_container,
                    "pg_dump",
                    "boards",
                    "-U",
                    "boardsuser",
                    "--clean",
                    "--if-exists",
                ]
                with pg_dumper(
                    backup_cmd, backups_dir, interval=backup_interval, retain=backup_retain, echo=(not backup_quiet)
                ) as (thr, stop):
                    # Dumper thread has already started
                    try:
                        compose_thread.join()
                    finally:
                        # Compose has exited, stop and join the background thread
                        stop.set()


@app.command()
def mount(
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!",
    ),
    dest_port: Optional[int] = typer.Option(
        None,
        "-p",
        "--dest-port",
        help="[ADVANCED] Destination port you will use to access the service (default: random).",
    ),
    https: Optional[bool] = typer.Option(
        False, "-s", "--https", help="Use HTTPs instead of HTTP (not really more secure here)."
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: Optional[str] = typer.Option(None, "-C", "--set-codename", help="Set the codename for the mount."),
    opt: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="IP:PORT or codename to mount.",
    ),
):
    """Mount and connect to another player's focalboard service"""

    # Nothing fancy here, a standard mount
    custom_mount_args = None
    proto = "http"
    if https:
        proto = "https"
        custom_mount_args = " -s"

    if no_browser:
        proto = None

    with avn.svc_mount(
        codename=codename,
        opt=opt,
        dest_ip=dest_ip,
        dest_port=dest_port,
        payload_dir=payload_dir,
        xdg_open=proto,
        mount_type=MOUNT_TYPE,
        custom_mount_args=custom_mount_args,
    ) as proc:
        if codename is not None:
            # For UX, guess that user would appreciate the first-time connect help if they used the -C codename syntax
            typer.echo(f"[+] Get invite link from host > Register (my_handle@x.localhost)")
        proc.wait()
