import os
import re
import time
import shutil
import socket
import shutil
import shlex
import gzip
import tempfile
import subprocess
from pathlib import Path
from typing import Optional, List

import yaml
import typer

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount
from autovnet.utils import (
    read_env_file,
    compose_up,
    compose_up_ctx,
    rand_port,
    complete_codename,
    pg_dumper,
    gen_password,
    gen_codename,
    yaml_dump,
    split_host_port,
)

app = typer.Typer()

# Fields expected in /usr/share/metasploit-framework/config/database.yml
# Since we generate the file for users mounting msfdb, at least warn someone (the host)
# That msfdb has changed the their fields and autovnet needs to be updated
MSFDB_FIELDS = ["adapter", "database", "username", "password", "host", "port", "pool", "timeout"]


@app.command()
def export(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).",
    ),
    dest_port: Optional[int] = typer.Option(5432, "-p", "--dest-port", help="Local bind port for the server."),
    random_dest_port: Optional[bool] = typer.Option(
        False, "-r", "--random-dest-port", help="If set, ignore dest_port and use a random port instead."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("msfdb"), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    env_file: Optional[str] = typer.Option(
        avn.config_path("rtfm.env"),
        "-e",
        "--env-file",
        help="[ADVANCED] Environment file to pass to docker-compose",
    ),
    backup_interval: Optional[int] = typer.Option(
        300, "-i", "--backup-interval", help="Seconds between automatic backups (database exports)."
    ),
    backup_retain: Optional[int] = typer.Option(
        16,
        "-r",
        "--backup-retain",
        help="Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.",
    ),
    backup_quiet: Optional[bool] = typer.Option(
        False, "-Q", "--backup-quiet", help="If set, do not print backup confirmation messages."
    ),
    no_backup: Optional[bool] = typer.Option(
        False, "-B", "--no-backup", help="If set, do not make automated backups of the server contents."
    ),
    force: Optional[bool] = typer.Option(
        False, "-f", "--force", help="If set, overwrite existing database config, if present."
    ),
    postgres_socks: Optional[str] = typer.Option(
        "/var/run/postgresql", "-S", "--postgres-socks", help="[ADVANCED] Directory where postgres stores its sockets."
    ),
    no_disable_postgres: Optional[bool] = typer.Option(
        False,
        "--no-disable-postgres",
        help="[ADVANCED] No not disable the local postgresql service. By default, it is disabled to project you from yourself.",
    ),
    no_msfdb_init: Optional[bool] = typer.Option(
        False,
        "--no-msfdb-init",
        help="[ADVANCED] No not call msfdb init. Useful if you just want to export an arbitrary postgres server.",
    ),
    init_sql: Optional[str] = typer.Option(
        None,
        "--init-sql",
        help="[ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.",
    ),
    no_init_sql_user: Optional[bool] = typer.Option(
        False,
        "--no-init-sql-user",
        help="[ADVANCED] No not create an 'msf' user before running --init-sql",
    ),
    db_config: Optional[str] = typer.Option(
        "/usr/share/metasploit-framework/config/database.yml",
        "--db-config",
        help="[ADVANCED] Path where msfdb init will save the configuration.",
    ),
    db_prefs: Optional[str] = typer.Option(
        "~/.msf4/database.yml",
        "--db-prefs",
        help="[ADVANCED] Path to save database startup preference. See https://docs.rapid7.com/metasploit/automatically-connect-the-database/.",
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load",
    ),
):
    """Initialize and export an msfdb (Metasploit-Framework) Database service

    You should probably only run at most one export or mount per machine, unless you super know what you are doing.
    Use workspaces if you think you need multiple concurrent databases.

    WARNING: postgres should *not* be running, and you should *not* have an msfdb instance that is not managed by autovnet.

    WARNING: msfdb export is Kali specific and will not work with manual metasploit-framework installs
    * See --no-msfdb-init and --init-sql for a workaround

    * Your existing database configuration or data could be lost!

    See also https://www.offensive-security.com/metasploit-unleashed/using-databases/
    """

    # Do automated backups by default, since notes are important data
    if backup_interval <= 0:
        raise ValueError(f"[-] --backup-interval must be positive! Use --no-backup to disable backups.")

    if init_sql is not None and not Path(init_sql).is_file():
        raise FileNotFoundError(f"[-] {init_sql} does not exist!")

    if Path(db_config).is_file():
        if force:
            typer.echo(f"[!] (sudo) Deleting {db_config}")
            rm_cmd = ["sudo", "rm", "-f", str(db_config)]
            subprocess.run(rm_cmd, check=True)
        else:
            raise FileExistsError(f"[-] {db_config} already exists! (Use --force to delete it and continue)")
    db_prefs = Path(db_prefs).expanduser()
    db_prefs.parent.mkdir(parents=True, exist_ok=True)

    docker_path = avn.docker_path("plugins/msfdb")

    if random_dest_port:
        dest_port = rand_port()

    env = read_env_file(env_file)

    ctx = None
    need_init = False
    postgres_data = None
    if codename is None:
        # New svc
        ctx = lambda: avn.svc(
            networks=networks,
            dest_ip=dest_ip,
            dest_port=dest_port,
            payload_dir=payload_dir,
            echo=False,
        )
        need_init = True
    else:
        # Loaded svc
        ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False)
        # Peek at listener config to grab the values we need
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir)
        model = ln.lns[0]
        dest_ip = model.next_dest_ip
        dest_port = model.next_dest_port
        postgres_data = Path(payload_dir) / codename / "data"
        if not postgres_data.is_dir():
            need_init = True
            # User can delete the data directory to force a re-init

    # Because of permission issues, (see Arbitrary --user notes here: https://hub.docker.com/_/postgres)
    # We need to start postgres as root, so it can call initdb successfully, then fix up the socket directory and restart as UID:GID,
    # so that the socket we pass from container -> host is owned by UID:GID, and not whatever UID 999 happens to be.

    # docker run -it --rm -v "/tmp/pg_socks:/var/run/postgresql/" -e PGPORT=1234 -e POSTGRES_PASSWORD=password -p 1234:1234 --user "${UID}:${GID}" postgres:latest
    tmp_env = {
        "POSTGRES_BIND": f"{dest_ip}:{dest_port}",
        "POSTGRES_PORT": f"{dest_port}",
        "INITDB_ONLY_SH": docker_path / "initdb_only.sh",
        "MSF_DB_PASSWORD": gen_password(secure=True),
    }
    # Note: use a random, secure root password (not saved), since we won't use it

    # Allow hardcore users to do what they want by crafting an env file
    tmp_env.update(env)
    env = tmp_env

    tmp_data = None
    # Begin the indentation madness
    with tempfile.TemporaryDirectory(prefix="rtfm_msfdb_") as tmp_socks:

        if not no_disable_postgres and Path("/lib/systemd/system/postgresql.service").is_file():
            # Host has postgres installed; make sure we use our nice, containerized database instead
            mask = Path("/etc/systemd/system/postgresql.service")
            if not (mask.is_symlink() and mask.readlink() == Path("/dev/null")):
                typer.echo(f"[+] (sudo) Disabling postgresql service")
                mask_cmd = ["sudo", "systemctl", "mask", "--now", "postgresql"]
                subprocess.run(mask_cmd, check=True)

        # This is technically a backdoor control socket for the database
        # We only need it for the call to 'msfdb init',
        # but it's easier to leave it because https://github.com/docker/compose/issues/3979

        env["POSTGRES_SOCKS"] = str(tmp_socks)

        if need_init:
            # We need to initialize the database secure-ishly before we expose it to the world

            # Cause the container to exit after initialization
            env["INITDB_ONLY"] = "1"

            # We probably don't know the codename yet, so we need a temporary directory to hold
            # the initialized database, so we can move it later.
            tmp_data = tempfile.TemporaryDirectory(prefix="rtfm_msfdb_")
            env["POSTGRES_DATA"] = str(tmp_data.name)
            # Leave POSTGRES_SOCKS, UID, and GID blank during initialization

            # Use a random, tmp project name, doesn't matter as long as it's unique
            project = Path(tmp_data.name).name
            compose_up(docker_path, env=env, project=project, quiet=True)
            # Container exits after initialization

            # postgres should now be intialized, but msfdb is not yet

            # Gross, postgres really wants to be UID 999, which is probably systemd-coredump or something nasty
            # Note: We must run this even with --no-msfdb-init, because postgres will actually take ownership of tmp_socks,
            # preventing us from deleting it.
            # And we can't easily disable passing tmp_socks, because https://github.com/docker/compose/issues/3979
            typer.echo(f"[+] (sudo) Fixing database permissions...")
            chown_cmd = ["sudo", "chown", "-R", f"{os.getuid()}:{os.getegid()}", str(tmp_socks), str(tmp_data.name)]
            subprocess.run(chown_cmd, check=True)

        with ctx() as (svc_proc, codename, remote_addr):

            # Can't finish the docker config until we have the codename
            postgres_data = Path(payload_dir) / codename / "data"
            postgres_backups = Path(payload_dir) / codename / "backups"

            if tmp_data is not None:
                # Preserve the intialized database so that initdb is skipped, avoiding permissionTruees
                shutil.move(tmp_data.name, postgres_data)

            for d in [postgres_data, postgres_backups]:
                d.mkdir(parents=True, exist_ok=True)

            project = f"msfdb_{codename}"
            docker_path = avn.docker_path("plugins/msfdb/")

            typer.echo(f"[+] autovnet rtfm msfdb {codename}")

            # Runtime values for env - running for real this time
            env["POSTGRES_DATA"] = str(postgres_data)
            env["UID"] = str(os.geteuid())
            env["GID"] = str(os.getegid())
            env["INITDB_ONLY"] = ""

            sock_dest = None
            # The msfdb script that ships with kali is extremely limited and does not accept connection URLs.
            # So, we make our containerized postgres look as much like the default postgres service as possible
            # so that msfdb "just works".

            # Basically, we pass the trusted control socket from the guest container to the host, then use
            # our sudo privs to link the socket into the hosts /var/run/postgresql directory, which is where
            # the host's postgres service would place it
            if need_init and not no_msfdb_init:
                sock_name = f".s.PGSQL.{dest_port}"
                sock_src = str(Path(tmp_socks) / sock_name)
                sock_dest = str(Path(postgres_socks) / sock_name)
                ln_cmd = ["sudo", "ln", "-s", sock_src, sock_dest]
                try:
                    subprocess.run(ln_cmd, check=True)
                    # After this point, we _must_ unlink sock_dest on exit
                except Exception as e:
                    typer.echo(
                        f"[-] Failed to link {sock_src} -> {sock_dest}! Is a conflicting postgres instance running?"
                    )

            try:
                with compose_up_ctx(docker_path, env=env, project=project, quiet=True) as (
                    compose_thread,
                    compose_stop,
                ):
                    # compose_thread is running

                    # Give container time to start before continuing
                    # Because we already did the intialization, this should be pretty fast, and the postgres image should be cached
                    typer.echo("[+] Waiting for database...")

                    attempts = 300
                    ok = False
                    while attempts > 0:
                        if not compose_thread.is_alive():
                            raise RuntimeError(f"[-] docker-compose exited early!")
                        try:
                            with socket.create_connection((dest_ip, dest_port), timeout=1):
                                ok = True
                                break
                        except Exception:
                            attempts -= 1
                            time.sleep(1)

                    if not ok:
                        raise RuntimeError(f"[-] Could not connect to database on {dest_ip}:{dest_port}")

                    saved_config = Path(payload_dir) / codename / Path(db_config).name
                    if need_init:
                        if no_msfdb_init:
                            typer.echo("[+] Skipping msfdb init...")

                            if not saved_config.is_file():
                                typer.echo(f"[!] Saving placeholder ({saved_config})")

                                # WARNING: If msf changes their file format, this will break and users will have to manually fix until
                                # autovnet is updated
                                generated_password = gen_password(secure=True)
                                cfg = {
                                    "production": {
                                        "adapter": "postgresql",
                                        "database": "msf",
                                        "username": "msf",
                                        "password": gen_password(secure=True),
                                        "host": dest_ip,
                                        "port": dest_port,
                                        "pool": 5,
                                        "timeout": 5,
                                    },
                                }
                                saved_config.write_text(yaml_dump(cfg))
                        else:
                            typer.echo("[+] (sudo) Intializing msfdb...")
                            init_cmd = ["sudo", "msfdb", "init"]
                            subprocess.run(init_cmd, check=True, env={"PGPORT": str(dest_port)})

                            if not Path(db_config).is_file():
                                raise FileNotFoundError(f"[-] 'msfdb init' did not create expected file: {db_config}")

                            # Save the database config for later
                            shutil.copyfile(db_config, saved_config)
                    else:
                        if not saved_config.is_file():
                            raise FileNotFoundError(f"[-] Saved database config not found: {saved_config}")

                    # 'activate' a previously saved database config, in case the user is maintaining multiple msfdb's for some reason
                    typer.echo(f"[+] Setting {db_prefs}")
                    shutil.copyfile(saved_config, db_prefs)

                    msfdb_config = yaml.safe_load(saved_config.read_text())

                    # Try to detect msf format changes
                    # Just warn on error, because users might be able to salvage this by manually fixing config files
                    for key in ["production"]:
                        if key not in msfdb_config:
                            typer.echo(f"[!] {db_config} missing expected key: {key}")

                    prod_config = msfdb_config.get("production", {})
                    for key in MSFDB_FIELDS:
                        if key not in prod_config:
                            typer.echo(f"[!] {db_config} (production) missing expected key: {key}")

                    if "password" not in prod_config:
                        typer.echo(
                            f"[!] Cannot find the database password in {db_config}! "
                            + "If this is a new msf change, open an autovnet issue. "
                            + "For now, you may be able to manually fix generated commands and files."
                        )

                    password = prod_config.get("password", "FIX_ME")
                    # Even if the file format changes, these defaults probably won't
                    username = prod_config.get("username", "msf")
                    host = prod_config.get("host", "localhost")
                    port = prod_config.get("port", dest_port)
                    database = prod_config.get("database", "msf")

                    confirmed = True
                    if init_sql is not None and not need_init:
                        # Confirm so that we don't accidentally delete a database if the user re-executes a command in their history
                        confirmed = typer.confirm(
                            "[!] --init-sql provided but database may already exist! Import anyway?"
                        )
                        if not confirmed:
                            typer.echo("[-] Ignoring --init-sql")

                    if init_sql is not None and confirmed:
                        # User confirmed they want to do this
                        db_container = f"msfdb_{codename}_database_1"
                        # msf database / user may not exist - authenticate locally as the default postgres user
                        import_cmd = ["docker", "exec", "-i", db_container, "psql", "-U", "postgres"]
                        proc = subprocess.Popen(import_cmd, stdin=subprocess.PIPE, stdout=subprocess.DEVNULL)
                        file_ctx = None
                        if init_sql.endswith(".gz"):
                            file_ctx = lambda: gzip.GzipFile(init_sql, "rb")
                        else:
                            file_ctx = lambda: open(init_sql, "rb")

                        if not no_init_sql_user:
                            # Doing this by default allows --init-sql my_msfdb_pg_dump.tar.gz to work correctly
                            typer.echo(f"[+] Creating 'msf' user manually")
                            # WARNING: SQL injection, but it requires you to modify the database.yml file on disk
                            # If you could do that, you could also modify whatever file we are about to --init-sql
                            proc.stdin.write(f"CREATE USER msf WITH PASSWORD '{password}';".encode())

                        with file_ctx() as in_file:
                            shutil.copyfileobj(in_file, proc.stdin)
                        proc.stdin.close()
                        proc.wait()
                        if proc.returncode == 0:
                            typer.echo(f"[+] Imported {init_sql}")
                        else:
                            typer.echo(
                                f"[!] Import {init_sql} failed: '{shlex.join(import_cmd)}' returned {proc.returncode}"
                            )

                    typer.echo(
                        f"[+] mount : autovnet rtfm msfdb mount -C {codename} --db-connect {username}:{password}@{host}:{port}/{database} {remote_addr}"
                    )

                    if no_backup:
                        compose_thread.join()
                    else:
                        # Leverage the fact we can pre-compute the database's fully qualified, unique container name
                        db_container = f"msfdb_{codename}_database_1"
                        backup_cmd = [
                            "docker",
                            "exec",
                            db_container,
                            "pg_dump",
                            database,
                            "-U",
                            username,
                            "--clean",
                            "--if-exists",
                        ]
                        with pg_dumper(
                            backup_cmd,
                            postgres_backups,
                            interval=backup_interval,
                            retain=backup_retain,
                            echo=(not backup_quiet),
                            stop=compose_stop,
                        ) as (backup_thread, stop):
                            # Both threads are running
                            # Wait for compose to exit
                            compose_thread.join()

            except Exception as e:
                typer.echo(f"[-] Got exception: {e}")
                raise
            finally:
                if sock_dest is not None:
                    # Clean up dangling sock link
                    typer.echo(f"[+] (sudo) Cleaning up symbolic link...")
                    ln_cmd = ["sudo", "rm", "-f", sock_dest]
                    subprocess.run(ln_cmd, check=True)

                if Path(db_config).is_file():
                    # We don't actually need this file, and it causes an extra sudo prompt
                    # Basically, if this file is missing, assume autovnet is controlling msfdb
                    typer.echo(f"[+] (sudo) Cleaning up {db_config}...")
                    rm_cmd = ["sudo", "rm", "-f", str(db_config)]
                    subprocess.run(rm_cmd, check=True)


@app.command()
def mount(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("msfdb"), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: Optional[str] = typer.Option(None, "-C", "--set-codename", help="Set the codename for the mount."),
    db_config: Optional[str] = typer.Option(
        "/usr/share/metasploit-framework/config/database.yml",
        "--db-config",
        help="[ADVANCED] Path where msfdb init will save the configuration.",
    ),
    db_prefs: Optional[str] = typer.Option(
        "~/.msf4/database.yml",
        "--db-prefs",
        help="[ADVANCED] Path to save database startup preference. See https://docs.rapid7.com/metasploit/automatically-connect-the-database/.",
    ),
    force: Optional[bool] = typer.Option(
        False, "-f", "--force", help="If set, overwrite existing database config, if present."
    ),
    db_connect: Optional[str] = typer.Option(
        None,
        "-c",
        "--db-connect",
        help="Database URL from the msfdb export. Required if opt is not a previous codename.",
    ),
    backup_interval: Optional[int] = typer.Option(
        300, "-i", "--backup-interval", help="Seconds between automatic backups (database exports)."
    ),
    backup_retain: Optional[int] = typer.Option(
        16,
        "-r",
        "--backup-retain",
        help="Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.",
    ),
    backup_quiet: Optional[bool] = typer.Option(
        False, "-Q", "--backup-quiet", help="If set, do not print backup confirmation messages."
    ),
    no_backup: Optional[bool] = typer.Option(
        False, "-B", "--no-backup", help="If set, do not make automated backups of the server contents."
    ),
    no_disable_postgres: Optional[bool] = typer.Option(
        False,
        "--no-disable-postgres",
        help="[ADVANCED] No not disable the local postgresql service. By default, it is disabled to project you from yourself.",
    ),
    opt: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="IP:PORT or previous codename to mount.",
    ),
):
    """Mount another player's msfdb instance.

    Sharing a metasploit-framework database allows for multiplayer metasploit-ing!

    See also https://www.offensive-security.com/metasploit-unleashed/using-databases/
    """

    db_prefs = Path(db_prefs).expanduser()
    for path in [db_prefs]:
        if path.is_file():
            if force:
                typer.echo(f"[!] Deleting {path}")
                path.unlink()
            else:
                raise FileExistsError(f"[-] {path} already exists! (Use --force to delete it and continue)")
    db_prefs.parent.mkdir(parents=True, exist_ok=True)
    # Example:
    # msf:JnTew1oMyE+9Rwx7OHw2yHYsxuqqK5IhwXXW63sz7wg=@localhost:5432/msf
    # Tokens:
    # 1: A sequence of not-colon (username)
    # 2: A sequence of not-at (password)
    # 3: A sequence of not-colon / not-slash (host)
    # 4: Optional :PORT
    # 5: If :PORT exists, just the PORT part
    # 6. The remainder of the string (database)
    connect_pattern = re.compile("([^:]+):([^@]+)@([^:/]+)(:([0-9]+))?/(.*)")

    dest_ip = "127.0.0.1"
    dest_port = 5432

    ctx = None
    username = None
    password = None
    database = None

    remote_addr = None

    codename_pattern = re.compile("^[a-z_]+$")
    if codename_pattern.match(opt):
        # codename
        codename = opt
        mnt = Mount.load(payload_dir, codename)
        dest_ip = mnt.svc_host
        dest_port = mnt.svc_port
        remote_addr = f"{mnt.host}:{mnt.port}"
    else:
        if db_connect is None:
            raise ValueError(f"[-] --db-connect DB_URL is missing!")
        # assert that opt is an IP:PORT (the autovnet IP:PORT remote_addr)
        host, port = split_host_port(opt)
        remote_addr = opt

        match = connect_pattern.match(db_connect)
        if match is None:
            raise ValueError(f"[-] db_connect URL {db_connect} invalid!")

        if codename is None:
            codename = gen_codename(opt + db_connect)

        # Parse URL
        username, password, host, _, port, database = match.groups()
        if port is not None:
            dest_port = int(port)
        if host != "localhost":
            dest_ip = host

    typer.echo(f"[+] autovnet rtfm msfdb mount {codename}")

    if not no_disable_postgres and Path("/lib/systemd/system/postgresql.service").is_file():
        # Host has postgres installed; make sure we use our nice, containerized database instead
        mask = Path("/etc/systemd/system/postgresql.service")
        if not (mask.is_symlink() and mask.readlink() == Path("/dev/null")):
            typer.echo(f"[+] (sudo) Disabling postgresql service")
            mask_cmd = ["sudo", "systemctl", "mask", "--now", "postgresql"]
            subprocess.run(mask_cmd, check=True)

    # Activate the database config
    out_dir = Path(payload_dir) / codename
    out_dir.mkdir(parents=True, exist_ok=True)
    postgres_backups = out_dir / "backups"
    saved_config = out_dir / Path(db_config).name
    if not saved_config.is_file():
        typer.echo(f"[+] Saving {saved_config}")

        # WARNING: If msf changes their file format, this will break and users will have to manually fix until
        # autovnet is updated
        cfg = {
            "production": {
                "adapter": "postgresql",
                "database": database,
                "username": username,
                "password": password,
                "host": dest_ip,
                "port": dest_port,
                "pool": 5,
                "timeout": 5,
            },
        }
        saved_config.write_text(yaml_dump(cfg))
    else:
        # Load connection info so we can print an optional db_connect command
        cfg = yaml.safe_load(saved_config.read_text())
        prod = cfg["production"]
        username = prod["username"]
        password = prod["password"]
        database = prod["database"]
        # ignore host and port, because the canonical values are in mount.yaml

    typer.echo(f"[+] Updating {db_prefs}")
    shutil.copyfile(saved_config, db_prefs)

    # It doesn't look like this is required
    # typer.echo(f"[+] (sudo) Updating {db_config}")
    # cp_cmd = ["sudo", "cp", str(saved_config), str(db_config)]
    # subprocess.run(cp_cmd, check=True)

    # Mount the service
    cmd = ["autovnet", "rtfm", "svc", "mount", "--mount-type", "msfdb"]
    # Pass thru any svc options
    if dest_ip != "127.0.0.1":
        cmd.extend(["-d", dest_ip])
    if dest_port is not None:
        cmd.extend(["-p", str(dest_port)])

    if codename is not None:
        cmd.extend(["-C", codename])

    cmd.extend(["-D", str(payload_dir)])
    cmd.append(opt)

    typer.echo(f"[+] mounted locally   : {dest_ip}:{dest_port}")
    typer.echo(
        f"[+] manual db_connect : db_connect --name {codename} {username}:{password}@{dest_ip}:{dest_port}/{database}"
    )
    proc = subprocess.Popen(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    if no_backup:
        try:
            proc.wait()
        finally:
            if proc.poll() is None:
                proc.terminate()
                proc.wait()
        return

    # Do automated backups by default, since notes are important data
    if backup_interval <= 0:
        raise ValueError(f"[-] --backup-interval must be positive! Use --no-backup to disable backups.")

    # Pass password via env-file instead of on the command line, not that it really matters
    env_file = out_dir / "pg_dump.env"
    env_file.write_text(
        f"""PGPORT={dest_port}
PGPASSWORD={password}"""
    )

    # Need to use postgres:lastest, because pg_dump version must match the database
    backup_cmd = [
        "docker",
        "run",
        "-it",
        "--rm",
        "--net=host",
        f"--env-file={env_file}",
        "postgres:latest",
        "pg_dump",
        database,
        "-U",
        username,
        "-h",
        dest_ip,
        "--clean",
    ]
    with pg_dumper(
        backup_cmd,
        postgres_backups,
        interval=backup_interval,
        retain=backup_retain,
        echo=(not backup_quiet),
    ) as (backup_thread, stop):
        # Both threads are running

        # Wait for mount to exit
        try:
            proc.wait()
        finally:
            # mount has exited, stop and join the background thread
            stop.set()
            if proc.poll() is None:
                proc.terminate()
                proc.wait()
            backup_thread.join()
