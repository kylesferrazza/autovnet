import os
from pathlib import Path
from typing import Optional, List

import typer
import pyfiglet

from ...autovnet import AutoVnet, avn
from autovnet.utils import (
    compose_run,
    gen_codename,
    complete_codename,
    complete_files,
    read_env_file,
)

app = typer.Typer()

MOUNT_TYPE = "kali"
CONTAINER_KALI_DIR = f"/opt/{MOUNT_TYPE}"
DEFAULT_FONT = "cyberlarge"


@app.command()
def shell(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        help="Directory to save command metadata.",
    ),
    mount_dir: Optional[str] = typer.Option(
        avn.data_path("mnt"),
        "-M",
        "--mount-dir",
        help="Host directory to mount in container at the same path.",
    ),
    container_mount_dir: Optional[str] = typer.Option(
        avn.data_path("mnt"),
        "--container-mount-dir",
        help="Container directory to mount --mount-dir.",
    ),
    quiet: Optional[bool] = typer.Option(
        False,
        "-q",
        "--quiet",
        help="Hide build output.",
    ),
    root: Optional[bool] = typer.Option(
        False,
        "-r",
        "--root",
        help="Run container as root.",
    ),
    env_file: Optional[str] = typer.Option(
        avn.config_path("rtfm.env"),
        "-e",
        "--env-file",
        help="[ADVANCED] Environment file to pass to docker-compose",
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load.",
    ),
):
    """Run a shell based on Kali

    Uses https://hub.docker.com/r/kalilinux/kali-rolling + kali-linux-headless

    WARNING: the first build will be veeeeeeerry slow
    """

    if codename is None:
        codename = gen_codename()

    kali_dir = payload_dir / codename
    kali_dir.mkdir(parents=True, exist_ok=True)

    # Prepare compose
    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    env = read_env_file(env_file)
    tmp_env = {
        "KALI_DATA": str(kali_dir),
        "MOUNT_DIR": str(mount_dir),
        "CONTAINER_MOUNT_DIR": str(container_mount_dir),
        "UID": str(os.geteuid()),
        "GID": str(os.getegid()),
    }
    tmp_env.update(env)
    env = tmp_env

    if root:
        env["UID"] = str(0)
        env["GID"] = str(0)

    if not quiet:
        typer.echo("=" * 80)
        art = pyfiglet.figlet_format(MOUNT_TYPE, font=DEFAULT_FONT)
        art = "\n".join(line for line in art.splitlines() if line.strip())
        typer.echo(art)

    # This is important, print it anyway
    typer.echo("\n" + "=" * 80)
    typer.echo(f"[+] autovnet rtfm kali {codename}")
    typer.echo("=" * 80)
    typer.echo(f"[+] mnt: {mount_dir} => {container_mount_dir}")
    typer.echo(f"[+] mnt: {kali_dir} => {CONTAINER_KALI_DIR}")
    typer.echo("=" * 80)

    project = f"{MOUNT_TYPE}_{codename}"
    compose_run(
        docker_path,
        MOUNT_TYPE,
        env=env,
        project=project,
        build=True,
        verbose_build=(not quiet),
    )
