import re
import os
import sys
import shutil
import shlex
import subprocess
import random
import tempfile
import secrets
from urllib.parse import urlparse
from pathlib import Path
from typing import Optional, List
from getpass import getpass
from ipaddress import IPv4Network, IPv4Address

import typer
import requests
from codenamize import codenamize

from sliver import SliverClient, SliverClientConfig

from ...autovnet import avn
from autovnet.utils import (
    rand_ip,
    rand_port,
    yaml_dump,
    get_password,
    gen_password,
    gen_codename,
    compose_up,
    compose_down,
    compose_build,
    complete_codename,
    complete_msf_codename,
    complete_opt_codename,
    Cryptor,
    as_port,
)

from autovnet.listener import AutoVnetListener

from . import server
from . import proxychains
from . import msf
from . import fs
from . import svc
from . import hedgedoc
from . import vnc
from . import msfdb
from . import drawio
from . import gitea
from . import focalboard
from . import matrix
from . import cyberchef
from . import nextcloud
from . import owncast
from . import tun
from . import dns
from . import merlin
from . import kali
from . import sliver

app = typer.Typer()

app.add_typer(
    server.app,
    name="server",
    help="[Advanced] Manage the autovrtfm server. (You probably don't need to run most of these commands manually)",
)

app.add_typer(proxychains.app, name="proxychains", help="Use proxychains to (obfuscate source IP).")

app.add_typer(msf.app, name="msf", help="Use metasploit-framework.")

app.add_typer(fs.app, name="fs", help="Share or mount file systems between players.")

app.add_typer(svc.app, name="svc", help="Share or mount services between players.")

app.add_typer(hedgedoc.app, name="hedgedoc", help="Securely share a hedgedoc collaborative notes server.")

app.add_typer(vnc.app, name="vnc", help="Securely screenshare between players.")

app.add_typer(msfdb.app, name="msfdb", help="Securely share msfdb between players.")

app.add_typer(drawio.app, name="drawio", help="Securely share a drawio server.")

app.add_typer(gitea.app, name="gitea", help="Securely share a gitea server.")

app.add_typer(focalboard.app, name="focalboard", help="Securely share a focalboard server.")

app.add_typer(matrix.app, name="matrix", help="Securely share a matrix server.")

app.add_typer(cyberchef.app, name="cyberchef", help="Securely share a cyberchef instance.")

app.add_typer(nextcloud.app, name="nextcloud", help="Securely share a nextcloud instance.")

app.add_typer(owncast.app, name="owncast", help="Securely share an owncast instance.")

app.add_typer(tun.app, name="tun", help="Secure, virtual overlay networking.")

app.add_typer(dns.app, name="dns", help="Secure, dns control.")

app.add_typer(merlin.app, name="merlin", help="Use merlin (https://github.com/Ne0nd0g/merlin).")

app.add_typer(kali.app, name="kali", help="Run a shell based on Kali Linux")

app.add_typer(sliver.app, name="sliver", help="Use sliver (https://github.com/BishopFox/sliver).")


@app.command()
def listen(
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1", "-d", "--dest-ip", help="Destination IP for tunnelled traffic (default: 127.0.0.1)."
    ),
    dest_port: Optional[int] = typer.Option(
        None, "-p", "--dest-port", help="Destination port for tunnelled traffic (default: random)."
    ),
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    count: Optional[int] = typer.Option(1, "-c", "--count", help="The number of remote IP addresses to allocate."),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("c2"),
        "-D",
        "--dir",
        is_eager=True,
        help="Base directory to store build output. A new directory will be created inside.",
    ),
    build_only: Optional[bool] = typer.Option(
        False, "-b", "--build-only", help="Only build the listener, do not start it."
    ),
    out_path: Optional[str] = typer.Option(
        None, "-o", "--out-path", help="Save listener config to this path, ignoring -D."
    ),
    in_path: Optional[str] = typer.Option(None, "-i", "--in-path", help="Load listener config from this path."),
    set_codename: Optional[str] = typer.Option(
        "", "-C", "--set-codename", help="[Advanced] Set a specific codename instead of a random one."
    ),
    random_register: Optional[int] = typer.Option(
        0,
        "-R",
        "--register-random",
        count=True,
        help="Register a random DNS name (repeatable).",
    ),
    register: Optional[List[str]] = typer.Option(
        None,
        "-r",
        "--register",
        help="Register a DNS name (repeatable).",
    ),
    opt: Optional[str] = typer.Argument(
        "443", autocompletion=complete_codename, help="Remote port to listen on, or codename for a previous listener"
    ),
):
    """Start a remote listener on the RTFM server

    You thought it was provisioning cloud infrastructure, but it was me,
        SSH port forwarding
    """
    listener = None
    remote_port = None

    try:
        remote_port = as_port(opt)
    except ValueError as e:
        # Looks like a codename
        codename = opt
        c2_yaml = out_path
        if c2_yaml is None:
            out_dir = Path(payload_dir) / codename
            if not out_dir.is_dir():
                raise FileNotFoundError(f"[-] Listener {out_dir} not found!")
            c2_yaml = out_dir / f"{codename}.yaml"

        if not c2_yaml.is_file():
            raise FileNotFoundError(f"[-] Listener {c2_yaml} not found!")
        listener = avn.listener_from_file(c2_yaml, codename=codename)
    else:
        if in_path:
            c2_yaml = Path(in_path)
            if not c2_yaml.is_file():
                raise FileNotFoundError(f"[-] Listener {c2_yaml} not found!")
            listener = avn.listener_from_file(c2_yaml)

        if listener is None:
            # Looks like a port
            networks = avn.networks(networks, category="listen")
            remote_ports = [remote_port] * count
            listener = avn.listener(
                networks,
                remote_ports,
                dest_ip,
                dest_port=dest_port,
                codename=set_codename,
                register=register,
                random_register=random_register,
            )

        if out_path is None:
            listener.save(payload_dir)
            typer.echo(f"[+] autovnet rtfm listen {listener.codename}")
        else:
            listener.save(out_path, is_file=True)
            typer.echo(f"[+] autovnet rtfm listen -i {out_path}")

    if build_only:
        return

    with listener.listener() as ln:
        ln.proc.wait()


@app.command()
def mux_disconnect():
    """Force mux disconnect from the autovnet server

    Only applicable if SSH connection sharing is on (reuse=True).
    Should not be needed, debug only.
    Will invalidate all listeners, but may not stop autovnet processes
    """
    AutoVnetListener(avn).mux_disconnect()


@app.command()
def mux_connect():
    """Force mux connect the autovnet server

    Only applicable if SSH connection sharing is on (reuse=True).
    Should not be needed, debug only.
    """
    AutoVnetListener(avn).mux_connect()


@app.command()
def pwncat(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    count: Optional[int] = typer.Option(1, "-c", "--count", help="The number of remote IP addresses to allocate."),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1", "-d", "--dest-ip", help="Listen IP for local pwncat (default: 127.0.0.1)."
    ),
    dest_port: Optional[int] = typer.Option(
        None, "-p", "--dest-port", help="Listen port for local pwncat (default: random)."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("c2"),
        "-D",
        "--dir",
        help="Base directory to store build output. A new directory will be created inside.",
    ),
    opt: Optional[str] = typer.Argument(
        "80", autocompletion=complete_codename, help="The remote port to accept inbound traffic, or previous codename."
    ),
):
    """Start a 'remote' pwncat-cs listener

    pwncat-cs actually runs locally, and we set up remote TCP listener to grab the traffic

    pwncat-cs is used here as a "smart" handler for simple TCP reverse shells.
    """
    ln = None
    remote_port = None
    try:
        remote_port = int(opt)
        # port
    except ValueError as e:
        pass
    if remote_port is None:
        # codename
        ln = avn.listener_from_codename(opt, payload_dir=payload_dir)
    else:
        remote_ports = [remote_port] * count
        ln = avn.listener(networks, remote_ports, dest_ip, dest_port=dest_port)
        ln.save(payload_dir)

    # Force all listeners to point to the same destination, taken from first listener
    dest_ip = ln.lns[0].dest_ip
    dest_port = ln.lns[0].dest_port
    for model in ln.lns:
        model.dest_ip = dest_ip
        model.dest_port = dest_port

    exe = shutil.which("pwncat-cs")
    if not exe:
        raise FileNotFoundError(f"[-] pwncat-cs not found. Try 'pip3 install pwncat-cs' ?")

    typer.echo(f"[+] autovnet rtfm pwncat {ln.codename}")
    fg = ["pwncat-cs", "-l", str(dest_ip), str(dest_port)]
    ln.background(fg)


@app.command()
def socat(args: List[str] = typer.Argument(..., help="socat args. (Use -- if your command has flags)")):
    """[Experimental] Run an arbitrary socat command on the RTFM server

    Example syntax:

    (`127.4.4.4:80` => `127.5.5.5:8000`)

    `autovnet rtfm socat TCP4-LISTEN:80,bind=127.4.4.4,fork,reuseaddr TCP4:127.5.5.5:8000`

    (`socat -h`)
    `autovnet rtfm cmd socat -- -h`
    """
    ssh_dest = f"{avn.config.rtfm_creds.user}@{avn.config.rtfm_creds.host}"
    port = str(avn.config.rtfm_creds.ports.get("ssh", 2222))

    cmd = [
        "ssh",
        "-q",
        "-o",
        "StrictHostKeyChecking=no",
        "-o",
        "UserKnownHostsFile=/dev/null",
        "-o",
        "IdentitiesOnly=yes",
        "-o",
        "ServerAliveInterval=30",
        "-o",
        "ServerAliveCountMax=5",
        "-o",
        "IdentitiesOnly=yes",
        "-o",
        "PasswordAuthentication=no",
        "-i",
        avn.config.rtfm_creds.key,
        "-p",
        port,
        "-t",
        ssh_dest,
        "socat",
    ]
    cmd.extend(args)
    subprocess.run(cmd, check=False)


@app.command()
def share(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    count: Optional[int] = typer.Option(1, "-c", "--count", help="The number of remote IP addresses to allocate."),
    msf: Optional[str] = typer.Option(
        None,
        "--msf",
        autocompletion=complete_msf_codename,
        help="Codename. Share the payload from a previous 'autovnet rtfm msf' command.",
    ),
    msf_dir: Optional[str] = typer.Option(
        avn.data_path("msf"), "--msf-dir", help="The directory to look in when using --msf."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("c2"), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    force: Optional[bool] = typer.Option(
        False, "-f", "--force", help="Do not prompt for confirmation before deleting temp dirs."
    ),
    encrypt: Optional[bool] = typer.Option(
        False, "-e", "--encrypt", help="If sharing a single file, encrypt it for other players."
    ),
    prompt: Optional[bool] = typer.Option(False, "-P", "--prompt", help="If encrypting, prompt for the password."),
    password: Optional[str] = typer.Option(None, "-p", "--password", help="If encrypting, use this password."),
    secure: Optional[bool] = typer.Option(False, "-s", "--secure", help="If encrypting, generate a secure password."),
    no_dns: Optional[bool] = typer.Option(False, "--no-dns", help="Do not generate DNS names by default"),
    random_register: Optional[int] = typer.Option(
        0,
        "-R",
        "--register-random",
        count=True,
        help="Register a random DNS name (repeatable).",
    ),
    register: Optional[List[str]] = typer.Option(
        None,
        "-r",
        "--register",
        help="Register a DNS name (repeatable).",
    ),
    codename: Optional[bool] = typer.Option(
        False, "-C", "--codename", help="Use codename completion instead of file completion."
    ),
    opt: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_opt_codename,
        help="Directory, file, or codename to share / serve. Default: Create an emphemeral tempdir.",
    ),
):
    """Share a directory over HTTP and HTTPS

    The first time you run a share command, it will take a long time to initialize.
    Subsequent share commands will be much faster.
    """
    # AAAAAAAAAAAAAAAAAAAAAAAAAAH

    ln = None
    dest_ip = "127.1.33.7"
    port_map = {}
    loaded_previous = False
    is_path = False
    # Best effort, use ./ or abs path to prevent matching codename regex
    codename_pattern = re.compile("^[a-z_]+$")
    if opt is not None:
        is_path = Path(opt).is_absolute() or len(Path(opt).parts) != 1 or not codename_pattern.match(opt)
        # Must expand to absolute path now, or docker-compose will share the wrong dir (:
        if is_path:
            opt = Path(opt).absolute()
            if not opt.exists():
                raise FileNotFoundError(f"[-] {opt} not found!")

    # If codename
    if opt is not None and not is_path and (Path(payload_dir) / opt).is_dir():
        # Share codename
        ln = avn.listener_from_codename(opt, payload_dir=payload_dir, echo=False)
        # We need to re-populate port_map with the dest ports for HTTP and 443 traffic
        for ln_model in ln.lns:
            if len(port_map) == 2:
                # Found both ports
                break
            port_map[ln_model.remote_port] = ln_model.dest_port
            # In case the user pulled a sneaky on us
            # Use the last dest_ip from the spec
            dest_ip = ln_model.dest_ip
        loaded_previous = True
    else:
        # Not codename
        port_map = {80: rand_port(), 443: rand_port()}

        if (not no_dns) and (not register):
            random_register = count

        if register and not len(register) == 2 * count:
            # Since we techincally have 2 listeners, user must provide twice as many DNS names
            raise ValueError(f"[-] With count={count}, register requires {2*count} names")

        # Silly workaround since we need 2 dest ports
        ln_80 = avn.listener(
            networks,
            [80] * count,
            dest_ip,
            dest_port=port_map[80],
            random_register=random_register,
            register=register[:count],
        )
        ln_443 = avn.listener(
            networks,
            [443] * count,
            dest_ip,
            dest_port=port_map[443],
            random_register=random_register,
            register=register[count:],
        )
        ln = avn.listener_from_models(ln_80.lns + ln_443.lns, echo=False)
        ln.save(payload_dir)
        typer.echo(f"[+] autovnet rtfm share {ln.codename}")

    default_url = None
    for ln_model in ln.lns:
        if ln_model.remote_port == 443:
            default_url = f"https://{ln_model.remote_ip}"

    project = f"rtfm_share_{port_map[80]}_{port_map[443]}"
    docker_path = avn.docker_path("plugins/share")
    procs = []
    env = {
        "SHARE_DIR": "/nonexistent",
        "UID": str(os.geteuid()),
        "GID": str(os.getegid()),
        "BIND_HTTP": f"{dest_ip}:{port_map[80]}",
        "BIND_HTTPS": f"{dest_ip}:{port_map[443]}",
    }
    compose_build(docker_path, env=env, project=project)
    tempdir = None
    try:
        # TODO: Yikes
        shared = None
        directory = None
        if opt is not None and Path(opt).is_file():
            # Share file
            shared = Path(opt)
        if opt is not None and Path(opt).is_dir():
            # Share drectory
            directory = opt
        if directory is None:
            tempdir = tempfile.TemporaryDirectory(prefix="rtfm_share_")
            directory = tempdir.name
            if shared is None:
                shared = Path(directory)

        env["SHARE_DIR"] = directory

        typer.echo(f"[+] =======================================")
        typer.echo(f"[+] Sharing: {directory}")

        # Allow user to share a single file, which we copy to a tempdir
        if shared is not None and shared.is_file():
            out_path = Path(directory) / shared.name
            shutil.copyfile(shared, out_path)

            download_cmd = ["autovnet", "rtfm", "download"]
            # Encrypt the copy, if enabled
            if encrypt:
                password = get_password(password=password, prompt=prompt, secure=secure)
                if not prompt:
                    typer.echo(f"[+] Password: {password}")
                cryptor = Cryptor(password=password)
                cryptor.encrypt_file(out_path)
                download_cmd.extend(["--password", password])
            download_cmd.append(f"{default_url}/{out_path.name}")

            # Save the (unencrypted) file being shared for later
            if not loaded_previous:
                save_dir = Path(payload_dir) / ln.codename
                shutil.copyfile(shared, save_dir / shared.name)
                if encrypt:
                    # Save the encryption password because no one is going to remember it
                    password_file = Path(save_dir) / "password"
                    password_file.write_text(password)
                ln.add_file(shared.name)

            typer.echo(f"   [*] staged   : /{out_path.name} ( {shared} )")
            if default_url is not None:
                typer.echo(f"   [*] download : {shlex.join(download_cmd)}")

        payload = None
        codename = None
        password = None
        if msf:
            payload = Path(msf_dir) / msf
            codename = msf
        elif loaded_previous:
            payload = Path(payload_dir) / ln.codename
            password_file = payload / "password"
            # If there was a password, remind the user what it was
            if password_file.is_file():
                password = password_file.read_text()
                typer.echo(f"[+] Password: {password}")
            codename = ln.codename
            for file_name in ln.shared():
                file_path = Path(payload) / file_name
                out_path = Path(directory) / file_path.name
                shutil.copyfile(file_path, out_path)
                # If the file is supposed to be encrypted, re-encrypt it
                download_cmd = ["autovnet", "rtfm", "download"]
                if password is not None:
                    cryptor = Cryptor(password=password)
                    cryptor.encrypt_file(out_path)
                    download_cmd.extend(["--password", password])
                download_cmd.append(f"{default_url}/{file_path.name}")
                typer.echo(f"   [*] staged   : /{file_path.name} ( {file_path} )")
                if default_url is not None:
                    typer.echo(f"   [*] download : {shlex.join(download_cmd)}")

        if payload is not None:
            # Payload sharing, e.g. msf
            if not payload.is_dir():
                typer.echo(f"[-] payload directory {payload} not found")
                return

            # All files that start with the codename
            for file_path in payload.glob(f"{codename}*"):
                if file_path.name == f"{codename}.yaml":
                    continue
                out_path = Path(directory) / file_path.name
                shutil.copyfile(file_path, out_path)

                download_cmd = ["autovnet", "rtfm", "download"]
                if password is not None:
                    cryptor = Cryptor(password=password)
                    cryptor.encrypt_file(out_path)
                    download_cmd.extend(["--password", password])
                download_cmd.append(f"{default_url}/{file_path.name}")
                typer.echo(f"   [*] staged   : /{file_path.name} ( {file_path} )")
                if default_url is not None:
                    typer.echo(f"   [+] download : {shlex.join(download_cmd)}")

                # Save any shared files for later
                if not loaded_previous:
                    shutil.copyfile(file_path, Path(payload_dir) / ln.codename / file_path.name)
                    ln.add_file(file_path.name)

        # If we shared any files, re-save the listener spec so we can re-share then next time
        if ln.shared:
            ln.save(payload_dir, exist_ok=True)

        # TODO: Pretty tables
        # Naming is hard
        last_proto = ""
        for ln_model in ln.lns:
            # TODO: Bad
            proto = "http"
            if ln_model.remote_port != 80:
                proto = "https"
            if last_proto != proto:
                typer.echo(f"[+] =======================================")
                last_proto = proto
            typer.echo(f"[+] {proto}://{ln_model.remote_ip}")

        typer.echo(f"[+] =======================================")

        # Run listener until docker-compose exits, then tear down
        with ln.listener():
            compose_up(docker_path, detach=False, build=False, env=env, project=project)

    finally:
        if tempdir is not None:
            confirm = force
            # tempdir is deleted when the object is destructed, so we can't stop the cleanup,
            # even if we wanted to
            while not confirm:
                confirm = typer.confirm(f"[!] {directory} will be deleted! Are you ready?")
            tempdir.cleanup()


@app.command()
def codename(
    count: Optional[int] = typer.Option(1, "-c", "--count", help="Number of codenames to generate."),
    tokens: Optional[int] = typer.Option(2, "-t", "--tokens", help="Number of tokens to include in the codename."),
    join: Optional[str] = typer.Option("_", "-j", "--join", help="Delimiter for joining tokens"),
):
    """Generate random codenames

    Use for fun, to help with naming, or even for human-friendly temporary passwords
    """
    for i in range(count):
        typer.echo(gen_codename(tokens=tokens, join=join))


@app.command()
def password(
    count: Optional[int] = typer.Option(1, "-c", "--count", help="Number of codenames to generate."),
    tokens: Optional[int] = typer.Option(3, "-t", "--tokens", help="Number of tokens to include in the codename."),
    join: Optional[str] = typer.Option("-", "-j", "--join", help="Delimiter for joining tokens."),
    digits: Optional[int] = typer.Option(
        3, "-d", "--digits", help="Ensure some digits in the password for complexity requirements (not real security)."
    ),
    secure: Optional[bool] = typer.Option(False, "-s", "--secure", help="Generate a secure, hard to type password."),
):
    """Generate random passwords

    Like the codename generator, but for passwords
    """
    for i in range(count):
        seed = secrets.token_bytes(256)
        password = None
        if secure:
            password = gen_password(secure=True)
        else:
            password = codenamize(seed, tokens - 1, join=join, capitalize=True)
        if digits > 0:
            # This is an obvious pattern, but ease of typing >> security for game passwords
            suffix = "".join([str(random.randint(0, 9)) for i in range(digits)])
            password += f"{join}{suffix}"
        typer.echo(password)


@app.command()
def encrypt(
    prompt: Optional[bool] = typer.Option(False, "-P", "--prompt", help="Prompt for password to use for encryption."),
    password: Optional[str] = typer.Option(
        None, "-p", "--password", help="Password to use for encryption. Default: generate one"
    ),
    secure: Optional[bool] = typer.Option(
        False, "-s", "--secure", help="If generating a password, make it secure. Default: easy to type"
    ),
    paths: List[str] = typer.Argument(..., help="Files to encrypt."),
    outs: Optional[List[str]] = typer.Option(
        None, "-o", "--out", help="[Repeatable] Path to save encrypted file. Default: overwrite source."
    ),
):
    """Encrypt a file for other players"""
    if outs and len(outs) != len(paths):
        raise ValueError(f"[-] Got {len(paths)} input paths, and {len(outs)} output paths")
    if not outs:
        outs = [None] * len(paths)
    # This is fast enough that concurrent encryption is not worth the effort
    if prompt:
        password = getpass("Encryption Password: ")
    cryptor = Cryptor(password=password, secure=secure, generate=True)
    if not prompt:
        typer.echo(f"[+] Password: {cryptor.password}")
    for in_path, out_path in zip(paths, outs):
        try:
            ok_path = out_path or in_path
            if out_path:
                typer.echo(f"[+] Encrypting {in_path} => {out_path} ...")
            else:
                typer.echo(f"[+] Encrypting {in_path} ...")
            cryptor.encrypt_file(in_path, out_path=out_path)
            typer.echo(f"[+] {ok_path} OK")
        except Exception as e:
            typer.echo(f"[-] Failed to encrypt {in_path}: {e}")


@app.command()
def decrypt(
    prompt: Optional[bool] = typer.Option(False, "-P", "--prompt", help="Prompt for password to use for decryption."),
    password: Optional[str] = typer.Option(None, "-p", "--password", help="Password to use for decryption"),
    paths: List[str] = typer.Argument(..., help="Files to decrypt."),
    outs: Optional[List[str]] = typer.Option(
        None, "-o", "--out", help="[Repeatable] Path to save decrypted file. Default: overwrite source."
    ),
):
    """Decrypt a file from another player"""
    if outs and len(outs) != len(paths):
        raise ValueError(f"[-] Got {len(paths)} input paths, and {len(outs)} output paths")
    if not outs:
        outs = [None] * len(paths)
    # This is fast enough that concurrent encryption is not worth the effort
    if prompt:
        password = getpass("Decryption Password: ")
    cryptor = Cryptor(password=password)
    for in_path, out_path in zip(paths, outs):
        try:
            ok_path = out_path or in_path
            if out_path:
                typer.echo(f"[+] Decrypting {in_path} => {out_path} ...")
            else:
                typer.echo(f"[+] Decrypting {in_path} ...")
            cryptor.decrypt_file(in_path, out_path=out_path)
            typer.echo(f"[+] {ok_path} OK")
        except Exception as e:
            typer.echo(f"[-] Failed to decrypt {in_path}: {e}")


@app.command()
def download(
    prompt: Optional[bool] = typer.Option(False, "-P", "--prompt", help="Prompt for password to use for decryption."),
    password: Optional[str] = typer.Option(None, "-p", "--password", help="Password to use for decryption."),
    url: str = typer.Argument(..., help="URL to download."),
    out: Optional[str] = typer.Option(None, "-o", "--out", help="Path to save download."),
):
    """Download a file

    Handy for downloading and decrypting files between players
    """
    if out is None:
        out = Path(urlparse(url).path).name
    out = Path(out)
    with requests.get(url, verify=False, stream=True) as resp:
        resp.raise_for_status()
        with out.open("wb") as f:
            # 65536
            chunk_size = 1 << 16
            for chunk in resp.iter_content(chunk_size=chunk_size):
                f.write(chunk)

    password = get_password(password=password, prompt=prompt, generate=False)
    if password is not None:
        cryptor = Cryptor(password=password)
        cryptor.decrypt_file(out, password=password)
