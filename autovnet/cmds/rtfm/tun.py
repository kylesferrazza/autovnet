import re
import os
import sys
import gzip
import shlex
import shutil
import time
import subprocess
from threading import Lock, Event
from pathlib import Path
from typing import Optional, List
from ipaddress import IPv4Address, IPv4Network

import yaml
import typer

from autovnet.autovnet import AutoVnet, avn
from autovnet.api.ipam import ipam_api, IpamApiClient
from autovnet.models import Mount, IPAssignment
from autovnet.utils import (
    compose_up,
    compose_up_ctx,
    complete_codename,
    gen_codename,
    rand_port,
    run_app,
    get_ip,
    ip_gen,
    as_port,
    tun_name,
    Timer,
    TimerQueue,
    random_dns,
)

app = typer.Typer()

MOUNT_TYPE = "tun"

# UDP
DEFAULT_NEBULA_PORT = 4242
# TCP
DEFAULT_API_PORT = 4242
# DEFAULT_API_MOUNT_PORT = 42420

RESERVATION_REFRESH_INTERVAL = 60 * 5

# The nebula server must be reachable from players
# This reduces extra layers of encryption,
# plus we can't tunnel / redirect UDP using ssh or stunnel

DEFAULT_NEBULA_BIND = get_ip()

IPAM_CONFIG = str(Path("ipam") / "config")

# No, client rate limiting isn't real rate limiting.
# This is just to reduce hammering iptables commands too aggressively
# This minimum is extremely conservative, assuming that many players will
# use the minimum (roll as often as possible)
# Since .proxy should be your first attempt, a 1 minute minimum roll interval seems like
# a reasonable compromise, and should be low-resource enough for many players to leave running
ROLL_MIN_INTERVAL = 60


@app.command()
def server(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    nebula_network: Optional[str] = typer.Option(None, "-N", "--nebula-network", help="Nebula virtual network to use."),
    clean: Optional[bool] = typer.Option(False, "-c", "--clean", help="Delete any previous IP assignments."),
    nebula_bind_ip: Optional[str] = typer.Option(
        None,
        "-b",
        "--nebula-bind-ip",
        help="Bind address for nebula server.",
    ),
    nebula_bind_port: Optional[int] = typer.Option(
        None,
        "-P",
        "--nebula-bind-port",
        help="Bind port (UDP) for nebula server.",
    ),
    nebula_remote_port: Optional[int] = typer.Option(
        None,
        "-R",
        "--nebula-remote-port",
        help="[ADVANCED] Remote bind port for tun API. Not intended to be set by humans.",
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).",
    ),
    dest_port: Optional[int] = typer.Option(None, "-p", "--dest-port", help="[ADVANCED] Local port for nebula data"),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    enable_pivot: Optional[bool] = typer.Option(
        False,
        "-e",
        "--enable-pivot",
        help="[!DANGER!] Allow other players to pivot through your tun server to IPs given by --allow-subnet (private IPs).",
    ),
    enable_pivot_avn: Optional[bool] = typer.Option(
        False,
        "-E",
        "--enable-pivot-avn",
        help="[!DANGER!] Allow other players to pivot through your tun server to IPs controlled by autovnet",
    ),
    allow_subnet: Optional[List[str]] = typer.Option(
        (), "-a", "--allow-subnet", help="[ADVANCED] Network to allow for --enable-pivot."
    ),
    verbose: Optional[bool] = typer.Option(
        False,
        "-v",
        "--verbose",
        help="Enable verbose output during container invocations",
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load",
    ),
):
    """Start an RTFM tun server

    This is intended to run on the autovnet server.
    Only run on a player machine if you trust other players.
    (They could pivot through your tun server to you local network!)
    """

    if nebula_remote_port is None:
        nebula_remote_port = avn.config.tun.control_port or DEFAULT_API_PORT

    if nebula_bind_port is None:
        nebula_bind_port = avn.config.tun.data_port or DEFAULT_NEBULA_PORT

    if dest_port is None:
        dest_port = avn.config.tun.data_port or DEFAULT_NEBULA_PORT

    if nebula_bind_ip is None:
        nebula_bind_ip = avn.config.tun.host or DEFAULT_NEBULA_BIND

    if not networks:
        networks = [avn.config.tun.host or DEFAULT_NEBULA_BIND]

    ctx = None
    allow_subnet_str = ""
    if not allow_subnet:
        allow_subnet = []
        if enable_pivot:
            allow_subnet.extend(["10.0.0.0/8", "192.168.0.0/16", "172.16.0.0/12"])
        if enable_pivot_avn:
            # This can be a helpful hack if VPN configurations prevent correct routing
            # from Red -> autovnet simulated networks
            allow_subnet.extend(avn.config.networks)

        allow_subnet_str = " ".join(allow_subnet)

    if codename is None:
        # New svc
        codename = gen_codename()
        ctx = lambda: avn.svc(
            networks=networks,
            dest_ip=dest_ip,
            dest_port=dest_port,
            remote_port=nebula_remote_port,
            payload_dir=payload_dir,
            echo=False,
            codename=codename,
        )
    else:
        # Loaded svc
        ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False)
        # Peek at listener config to grab the values we need
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir)
        model = ln.lns[0]
        dest_ip = model.next_dest_ip
        dest_port = model.next_dest_port
        nebula_remote_port = model.remote_port

    if nebula_network is None:
        nebula_network = avn.config.tun.network

    nebula_network = IPv4Network(nebula_network)

    if nebula_network.num_addresses < 3:
        raise ValueError(f"[-] Network {nebula_network} is too small!")

    lighthouse_ip = nebula_network[1]
    assignment = f"{lighthouse_ip}/{nebula_network.prefixlen}"

    certs_dir = Path(payload_dir) / codename / "certs"
    ca_dir = certs_dir / "ca"
    host_dir = certs_dir / "host"
    bundle_dir = Path(payload_dir) / codename / "bundle"
    config_dir = Path(payload_dir) / codename / "config"
    for _dir in [certs_dir, ca_dir, host_dir, bundle_dir, config_dir]:
        _dir.mkdir(parents=True, exist_ok=True)

    # Prepare compose
    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    server_yaml = avn.docker_path(f"plugins/{MOUNT_TYPE}/server.yaml")
    shutil.copyfile(server_yaml, config_dir / "server.yaml")

    nebula_bind_ip = str(IPv4Address(nebula_bind_ip))
    nebula_bind_port = as_port(nebula_bind_port)
    iptables_docker_path = avn.docker_path(f"plugins/iptables/")

    with ctx() as (proc, codename, remote_addr):

        # https://unix.stackexchange.com/questions/451368/allowed-chars-in-linux-network-interface-names
        nebula_tun_name, nebula_tun_alias = tun_name(codename)

        # network_mode: host
        env = {
            "NEBULA_MODE": "server",
            "NEBULA_CODENAME": codename,
            "NEBULA_TUN_NAME": nebula_tun_name,
            "NEBULA_ASSIGNMENT": assignment,
            "NEBULA_LIGHTHOUSE_IP": str(lighthouse_ip),
            "NEBULA_ALLOW_PIVOT": str(1) if allow_subnet_str else str(0),
            "NEBULA_PIVOT_SUBNETS": allow_subnet_str,
            "NEBULA_BIND_IP": nebula_bind_ip,
            "NEBULA_BIND_PORT": str(nebula_bind_port),
            "NEBULA_CA_DIR": str(ca_dir),
            "NEBULA_HOST_DIR": str(host_dir),
            "NEBULA_BUNDLE_DIR": str(bundle_dir),
            "NEBULA_CONFIG_DIR": str(config_dir),
            "UID": str(os.geteuid()),
            "GID": str(os.getegid()),
        }
        project = f"{MOUNT_TYPE}_{codename}"
        # dest_port included because invite link will contain it
        mount_cmd = f"autovnet rtfm {MOUNT_TYPE} up {remote_addr}"
        export_cmd = f"autovnet rtfm {MOUNT_TYPE} server {codename}"

        typer.echo(f"[+] {export_cmd}")
        typer.echo(f"[+] mount      : {mount_cmd}")
        typer.echo(f"[+] lighthouse : {nebula_bind_ip}:{nebula_bind_port}")

        app = None
        clean_cmds = []
        try:
            with compose_up_ctx(docker_path, env=env, project=project, quiet=(not verbose)) as (
                compose_thread,
                compose_stop,
            ):
                try:
                    ca_key = ca_dir / "ca.key"
                    ca_crt = ca_dir / "ca.crt"

                    typer.echo(f"[*] Waiting for nebula to initialize...")
                    while not (ca_key.is_file() and ca_crt.is_file()):
                        # Getting stuck here is a probably a set up issue,
                        # but we can't really time out (maybe your build is slow) or safely continue
                        time.sleep(1)

                    typer.echo(f"[*] Waiting for {nebula_tun_name} / {nebula_tun_alias} ...")
                    while True:
                        cmd = ["ip", "link", "show", nebula_tun_name]
                        res = subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                        if res.returncode == 0:
                            break
                        time.sleep(1)

                    # WARNING: use extreme caution incorporating user input into these commands
                    # (we are basically using docker to privesc to NET_ADMIN so we don't have to sudo ;) )

                    # Add a cosmetic alias
                    ip_cmd = shlex.join(["ip", "link", "set", nebula_tun_name, "alias", nebula_tun_alias])
                    env = {
                        "IPTABLES_CMD": ip_cmd,
                    }
                    iptables_project = f"iptables_{codename}"
                    compose_up(iptables_docker_path, env=env, project=iptables_project, quiet=(not verbose))

                    iptables_cmds = [
                        # Keep PREROUTING rulse in NEBULA-PRE
                        ("iptables -t nat -N NEBULA-PRE", None, None),
                        (f"iptables -t nat {op} NEBULA-PRE -j RETURN" for op in ["-C", "-A", "-D"]),
                        (*(f"iptables -t nat {op} PREROUTING --jump NEBULA-PRE" for op in ["-C", "-I"]), None),
                        # Keep POSTROUTING rules in NEBULA-POST
                        ("iptables -t nat -N NEBULA-POST", None, None),
                        (*(f"iptables -t nat {op} NEBULA-POST -j RETURN" for op in ["-C", "-A"]), None),
                        (*(f"iptables -t nat {op} POSTROUTING --jump NEBULA-POST" for op in ["-C", "-I"]), None),
                    ]
                    # Scary command formatting
                    # Need to allow traffic to get forwarded to the tun interface
                    # (the postrouting will rewrite the dest ip to a tun address, and requires forward for delivery)
                    # iptables_cmds.append(f"iptables -I NEBULA-FORWARD -o {shlex.quote(nebula_tun_name)} -j ACCEPT")
                    for version in ["-nft", "-legacy"]:
                        for direction in ["-s", "-d"]:
                            check_cmd = (
                                f"iptables{version} -C FORWARD {direction} {shlex.quote(str(nebula_network))} -j ACCEPT"
                            )
                            ip_cmd = (
                                f"iptables{version} -I FORWARD {direction} {shlex.quote(str(nebula_network))} -j ACCEPT"
                            )
                            clean_cmd = (
                                f"iptables{version} -D FORWARD {direction} {shlex.quote(str(nebula_network))} -j ACCEPT"
                            )

                            cmds = [check_cmd, ip_cmd, clean_cmd]
                            iptables_cmds.append(cmds)

                    for check_cmd, ip_cmd, clean_cmd in iptables_cmds:
                        try:
                            # Skip if the rule already exists
                            check_env = {
                                "IPTABLES_CMD": check_cmd,
                            }
                            if verbose:
                                typer.echo(f"[+] check: {check_cmd}")
                            compose_up(
                                iptables_docker_path,
                                env=check_env,
                                project=iptables_project,
                                quiet=(not verbose),
                                exit_code_from="iptables",
                            )

                        except subprocess.SubprocessError:
                            # Rule did not exist
                            if ip_cmd is None:
                                continue

                            ip_env = {
                                "IPTABLES_CMD": ip_cmd,
                            }
                            try:
                                if verbose:
                                    typer.echo(f"[+] insert: {ip_cmd}")
                                compose_up(
                                    iptables_docker_path,
                                    env=ip_env,
                                    project=iptables_project,
                                    quiet=True,
                                    exit_code_from="iptables",
                                )
                                # Success, register cleanup if there is one
                                if clean_cmd is not None:
                                    clean_cmds.append(clean_cmd)
                            except subprocess.SubprocessError as e:
                                typer.echo(f"[-] Failed to add rule {ip_cmd}: {e}")

                    typer.echo(f"[*] Starting IP API...")
                    lighthouse_connect = f"{nebula_bind_ip}:{nebula_bind_port}"
                    app = ipam_api(
                        avn,
                        network=nebula_network,
                        ca_key=ca_key,
                        ca_crt=ca_crt,
                        clean=clean,
                        server_codename=codename,
                        lighthouse_connect=lighthouse_connect,
                    )
                    run_app(app, host=dest_ip, port=dest_port)
                    app.timer_queue.stop()
                    app.cleanup()
                except Exception as e:
                    typer.echo(f"[-] Failed to start API: {e}")
                    compose_stop.set()
                    raise

                compose_thread.join()
        finally:
            for clean_cmd in clean_cmds:
                ip_env = {
                    "IPTABLES_CMD": clean_cmd,
                }
                try:
                    if verbose:
                        typer.echo(f"[+] clean: {clean_cmd}")
                    compose_up(
                        iptables_docker_path,
                        env=ip_env,
                        project=iptables_project,
                        quiet=(not verbose),
                        exit_code_from="iptables",
                    )
                except subprocess.SubprocessError as e:
                    typer.echo(f"[-] Failed to clean up with {clean_cmd}: {e}")


@app.command()
def up(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!",
    ),
    new: Optional[bool] = typer.Option(
        False, "-N", "--new", help="Force a new IP assignment. Don't be greedy, only request what you need."
    ),
    dest_port: Optional[int] = typer.Option(
        None,
        "-p",
        "--dest-port",
        help="[ADVANCED] Destination port you will use to access the service (default: random).",
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(IPAM_CONFIG), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    routes: Optional[List[str]] = typer.Option(
        None,
        "-r",
        "--route",
        help="IPv4Network (X.X.X./Y) to route through the tun server. (Server must have pre-approved the network.)",
    ),
    interval: Optional[int] = typer.Option(
        None,
        "-i",
        "--interval",
        help="If set, roll new IP every --interval seconds.",
    ),
    tokens: Optional[int] = typer.Option(
        2,
        "-t",
        "--tokens",
        help="Number of random tokens, if using -R",
    ),
    ttl_inc: Optional[int] = typer.Option(
        1,
        "-T",
        "--ttl-inc",
        help="[ADVANCED] Increase the IP TTL of outbound tun packets by this amount. Generally, you should set to the number of intermediate hops between you and the autovnet server, as shown by traceroute.",
    ),
    random_register: Optional[bool] = typer.Option(
        False,
        "-R",
        "--register-random",
        help="Register a random DNS name on each roll.",
    ),
    register: Optional[str] = typer.Option(
        None,
        "-a",
        "--register",
        help="Re-register this DNS name for each roll. Can be combined with -R.",
    ),
    random_fixed: Optional[bool] = typer.Option(
        False,
        "-A",
        "--random-name",
        help="Like register, but generate a random name to use. Can be combined with -R.",
    ),
    server: Optional[str] = typer.Option(None, "-s", "--server", help="Override IP:PORT of nebula lighthouse (UDP)"),
    verbose_build: Optional[bool] = typer.Option(False, "--verbose-build", help="[ADVANCED] Show build output."),
    opt: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="IP:PORT or codename to mount.",
    ),
):
    """Mount and connect to another player's nebula service"""

    if random_fixed:
        register = random_dns(tld=avn.config.dns.tld, tokens=tokens)

    elif register and not register.endswith(avn.config.dns.tld):
        register = f"{register}.{avn.config.dns.tld}"

    if routes is None:
        routes = ""
    else:
        routes = " ".join([str(IPv4Network(r)) for r in routes])

    if interval is not None and interval < ROLL_MIN_INTERVAL:
        typer.echo(f"[!] --interval {interval} too small, using {ROLL_MIN_INTERVAL}")
        interval = ROLL_MIN_INTERVAL

    to_delete = None
    tmp_codename = False
    codename = None
    codename_pattern = re.compile("^[a-z_]+$")
    if dest_port is None:
        dest_port = rand_port()

    tmp_client = IpamApiClient(None, payload_dir, new=new)
    # Yikes
    if opt is None:
        codename = tmp_client.get_codename()
        tmp_codename = True

        # Yikes++
        if tmp_client.config_file is not None and tmp_client.config_file.is_file() and new:
            # If user wants the "default" server but is forcing a new assignment,
            # read out the connect IP:PORT from the default config file
            assignment = IPAssignment.parse_obj(yaml.safe_load(tmp_client.config_file.read_text()))
            opt = assignment.lighthouse_connect
        else:
            # The default default is rtfm_server:control_port
            opt = f"{avn.config.tun.host}:{avn.config.tun.control_port}"

    elif codename_pattern.match(opt):
        # codename
        codename = opt
    else:
        # IP:PORT
        tmp_codename = True

    if codename is not None:
        # Loaded default or given codename
        mnt = Mount.load(payload_dir, codename)
        dest_ip = mnt.svc_host
        dest_port = mnt.svc_port
        opt = codename
    elif tmp_codename:
        # The mount.yaml will get written to this codename,
        # because we don't know what the assigned IP will be
        while codename is None:
            codename = gen_codename()
            to_delete = Path(payload_dir) / codename
            if to_delete.is_dir():
                codename = None

    url = f"http://{dest_ip}:{dest_port}"
    iptables_project = f"iptables_{codename}"

    with avn.svc_mount(
        codename=codename,
        opt=opt,
        dest_ip=dest_ip,
        dest_port=dest_port,
        payload_dir=payload_dir,
        mount_type=MOUNT_TYPE,
        echo_mount=False,
    ) as proc:
        typer.echo(f"[*] Waiting for IPAM ...")
        client_codename = None
        if not tmp_codename:
            client_codename = codename

        c = IpamApiClient(url, payload_dir, codename=client_codename, new=new)
        networks = avn.networks(networks, category="tun")
        mapper = TunMapper(c, networks, fixed_name=register, random_name=random_register, tokens=tokens)
        try:
            c.connect()
        except Exception as e:
            typer.echo(f"[-] Error connecting to API: {e}")
            raise
        typer.echo(f"[+] IPAM is ready!\n")
        typer.echo(f"[+] autovnet rtfm {MOUNT_TYPE} up {c.assignment.codename}")
        typer.echo(f"[+] tun_ip: {c.assignment.ip}")

        if to_delete is not None:
            mnt_src = to_delete / "mount.yaml"
            mnt_dest = Path(payload_dir) / c.assignment.codename / "mount.yaml"
            shutil.copyfile(mnt_src, mnt_dest)
            shutil.rmtree(to_delete)

        codename = c.assignment.codename
        base_dir = Path(payload_dir) / codename
        ca_dir = base_dir / "ca"
        host_dir = base_dir / "host"
        config_dir = base_dir
        # Only needed to keep docker-compose happy
        bundle_dir = base_dir / "bundle"
        bundle_dir.mkdir(parents=True, exist_ok=True)

        client_yaml = avn.docker_path(f"plugins/{MOUNT_TYPE}/client.yaml")
        shutil.copyfile(client_yaml, config_dir / "client.yaml")

        if server is None:
            # Forcing --server might be useful if a goofy network requires UDP redirection to reach the nebula server
            server = c.assignment.lighthouse_connect

        # Prepare compose
        docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")
        project = f"{MOUNT_TYPE}_{codename}"
        nebula_tun_name, nebula_tun_alias = tun_name(codename)
        env = {
            "NEBULA_MODE": "client",
            "NEBULA_CODENAME": codename,
            "NEBULA_TUN_NAME": nebula_tun_name,
            "NEBULA_LIGHTHOUSE_IP": str(c.assignment.lighthouse_ip),
            "NEBULA_LIGHTHOUSE_CONNECT": str(server),
            "NEBULA_PIVOT_SUBNETS": routes,
            "NEBULA_CA_DIR": str(ca_dir),
            "NEBULA_HOST_DIR": str(host_dir),
            "NEBULA_CONFIG_DIR": str(config_dir),
            "NEBULA_BUNDLE_DIR": str(bundle_dir),
            "UID": str(os.geteuid()),
            "GID": str(os.getegid()),
        }
        typer.echo(f"\n[*] Starting nebula...")
        rt_tables_path = Path("/etc/iproute2/rt_tables")
        clean_cmds = []
        try:
            with compose_up_ctx(docker_path, env=env, project=project, quiet=(not verbose_build)) as (
                compose_thread,
                compose_stop,
            ):

                typer.echo(f"[*] Waiting for {nebula_tun_name} / {nebula_tun_alias} ...")
                while True:
                    cmd = ["ip", "link", "show", nebula_tun_name]
                    res = subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                    if res.returncode == 0:
                        break
                    time.sleep(1)
                # Add a cosmetic alias
                ip_cmd = shlex.join(["ip", "link", "set", nebula_tun_name, "alias", nebula_tun_alias])
                env = {
                    "IPTABLES_CMD": ip_cmd,
                }
                iptables_docker_path = avn.docker_path(f"plugins/iptables/")
                compose_up(iptables_docker_path, env=env, project=iptables_project, quiet=True)

                # https://unix.stackexchange.com/questions/4420/reply-on-same-interface-as-incoming
                rt_tables = rt_tables_path.read_text()

                num = 100
                while str(num) in rt_tables:
                    num += 1
                    if num > 255:
                        # In practice, this is very unlikely
                        raise Exception(
                            f"[!] rt_tables is full! Stop all tuns, then: sudo sed -i '/rtfm-/d' /etc/iproute2/rt_tables"
                        )

                line = f"{num} {nebula_tun_name}"
                if f"{nebula_tun_name}" not in rt_tables:
                    # Create custom table
                    append_cmd = f"echo {shlex.quote(line)} >> {shlex.quote(str(rt_tables_path))}"
                    iptables_cmd = f"/bin/bash -c {shlex.quote(append_cmd)}"
                    env = {
                        "IPTABLES_CMD": iptables_cmd,
                    }
                    compose_up(iptables_docker_path, env=env, project=iptables_project, quiet=True)

                res = subprocess.run(["ip", "rule", "show", "table", nebula_tun_name], capture_output=True)
                output = res.stdout.decode()

                if f"from {c.assignment.ip} lookup {nebula_tun_name}" not in output:
                    # Create lookup rule (if from tun_ip, punt to rtfm table)
                    iptables_cmd = f"ip rule add from {c.assignment.ip} table {nebula_tun_name} prio 1"
                    env = {
                        "IPTABLES_CMD": iptables_cmd,
                    }
                    compose_up(iptables_docker_path, env=env, project=iptables_project, quiet=True)

                res = subprocess.run(["ip", "route", "show", "table", nebula_tun_name], capture_output=True)
                output = res.stdout.decode()

                if f"dev {nebula_tun_name}" not in output:
                    # Divert traffic (if traffic hits rtfm table, disregard regular routes, force through tun / lighthouse ip)
                    # If target calls back via the tun / mapped remote IP, we need to reply through the tun so the server can SNAT the reply.
                    # We have to use rules, because unless user set --routes, the tun has no routes.
                    # We want to be able to reply to any connection / callback without knowing the target's source ip in advance
                    iptables_cmd = f"ip route add default via {c.assignment.lighthouse_ip} dev {nebula_tun_name} table {nebula_tun_name}"
                    env = {
                        "IPTABLES_CMD": iptables_cmd,
                    }
                    compose_up(iptables_docker_path, env=env, project=iptables_project, quiet=True)

                if ttl_inc > 0:
                    iptables_cmds = [
                        (
                            f"iptables -t mangle {op} POSTROUTING -o {nebula_tun_name} -j TTL --ttl-inc {ttl_inc}"
                            for op in ["-C", "-I", "-D"]
                        ),
                    ]
                    for check_cmd, ip_cmd, clean_cmd in iptables_cmds:
                        try:
                            # Skip if the rule already exists
                            check_env = {
                                "IPTABLES_CMD": check_cmd,
                            }
                            compose_up(
                                iptables_docker_path,
                                env=check_env,
                                project=iptables_project,
                                quiet=True,
                                exit_code_from="iptables",
                            )

                        except subprocess.SubprocessError:
                            # Rule did not exist
                            if ip_cmd is None:
                                continue

                            ip_env = {
                                "IPTABLES_CMD": ip_cmd,
                            }
                            try:
                                compose_up(
                                    iptables_docker_path,
                                    env=ip_env,
                                    project=iptables_project,
                                    quiet=True,
                                    exit_code_from="iptables",
                                )
                                # Success, register cleanup if there is one
                                if clean_cmd is not None:
                                    clean_cmds.append(clean_cmd)
                            except subprocess.SubprocessError as e:
                                typer.echo(f"[-] Failed to add rule {ip_cmd}: {e}")

                typer.echo(f"[+] tun is ready!\n")
                try:
                    mapper.map()
                    timer_queue = TimerQueue()
                    if interval is not None:
                        remap_timer = Timer(mapper.map, interval=interval)
                        timer_queue.add(remap_timer)

                    refresh_timer = Timer(lambda: c.token(), interval=RESERVATION_REFRESH_INTERVAL)
                    timer_queue.add(refresh_timer)
                    timer_queue.run()

                    # Hard timeout for bad people who 'yes | autovnet tun up' >:(
                    # (yes, it works, no, you shouldn't - it is extremely expensive for the autovnet server, compared to other features)
                    timeout = 1
                    while proc.poll() is None:
                        try:
                            proc.wait(timeout=timeout)
                            break
                        except subprocess.TimeoutExpired:
                            pass

                        if typer.confirm(f"[>] Roll new IP?\n"):
                            map_ip = mapper.map()

                except Exception as e:
                    typer.echo(f"[-] {e}")
                    raise

                timer_queue.stop()
                compose_stop.set()

                compose_thread.join()

        finally:
            c.unmap()
            for clean_cmd in clean_cmds:
                ip_env = {
                    "IPTABLES_CMD": clean_cmd,
                }
                try:
                    compose_up(
                        iptables_docker_path,
                        env=ip_env,
                        project=iptables_project,
                        quiet=True,
                        exit_code_from="iptables",
                    )
                except subprocess.SubprocessError as e:
                    typer.echo(f"[-] Failed to clean up with {clean_cmd}: {e}")
            with mapper.lock:
                mapper.unregister_dns(cleanup=True)


@app.command()
def redirect(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(IPAM_CONFIG), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    proto: Optional[str] = typer.Option("udp", "-P", "--proto", help="Protocol to match."),
    match_port: Optional[int] = typer.Option(
        ..., "-m", "--match-port", help="Destination port to match. (E.g. the port that matches your payload)"
    ),
    match_src: Optional[str] = typer.Option(
        None,
        "-s",
        "--match-src",
        help="Source IP (or CIDR) to match. Default: any",
    ),
    to_port: Optional[int] = typer.Option(
        ...,
        "-t",
        "--to-port",
        help="When a packet matches, rewrite the dest port to this port. (E.g. the port that matches your listener.)",
    ),
    background: Optional[bool] = typer.Option(
        False,
        "-b",
        "--background",
        help="Do not wait for CTRL+C, require manual --delete (or stopping the tun)",
    ),
    delete: Optional[bool] = typer.Option(
        False,
        "--delete",
        help="Delete this rule instead of creating it. (Rules still exist after a tun is torn down!)",
    ),
    verbose: Optional[bool] = typer.Option(
        False,
        "-v",
        "--verbose",
        help="Enable verbose output during container invocations",
    ),
    codename: Optional[str] = typer.Argument(
        "default",
        autocompletion=complete_codename,
        help="Codename of active tun.",
    ),
):
    """Set up a redirect rule on your local tun

    This allows you to match certain inbound packets and rewrite the destination port.

    This is useful when you have a payload configured to connect to a well known port
        that you cannot actually bind your listener to, due to a conflict (e.g. UDP:53)

    """

    tun_path = Path(payload_dir) / codename
    if not tun_path.is_dir():
        raise FileNotFoundError(f"[-] invalid tun {codename}: {tun_path} not found")
    config_path = tun_path / "config.yaml"
    if not config_path.is_file():
        raise FileNotFoundError(f"[-] {config_path} not found")

    nebula_config = yaml.safe_load(config_path.read_text())
    interface = nebula_config["tun"]["dev"]

    argv = list(sys.argv)
    delete_cmd = None
    if not delete:
        argv.append("--delete")
        delete_cmd = argv

    op = "-A"
    if delete:
        op = "-D"

    cmd = [
        "iptables",
        "-t",
        "nat",
        "-p",
        proto,
        op,
        "PREROUTING",
        "-i",
        interface,
        "--dport",
        str(match_port),
    ]
    if match_src is not None:
        cmd.extend(
            [
                "-s",
                match_src,
            ]
        )
    cmd.extend(
        [
            "-j",
            "REDIRECT",
            "--to-port",
            str(to_port),
        ]
    )
    cmd_str = shlex.join(cmd)
    if verbose:
        typer.echo(f"[~] {cmd_str}")
    env = {
        "IPTABLES_CMD": cmd_str,
    }
    iptables_project = f"iptables_redirect_{codename}"
    iptables_docker_path = avn.docker_path(f"plugins/iptables/")
    # raises on error
    compose_up(iptables_docker_path, env=env, project=iptables_project, exit_code_from="iptables", quiet=(not verbose))

    src = ""
    deleted = ""
    if delete:
        deleted = "(deleted) "
    if match_src is not None:
        src = f"(src={match_src}) "
    typer.echo(f"[+] {deleted}{src}:{match_port} => :{to_port}")

    if background and not delete:
        tmp = delete_cmd[0]
        delete_cmd[0] = Path(delete_cmd[0]).name
        typer.echo(f"[+] delete: {shlex.join(delete_cmd)}")
        delete_cmd[0] = tmp

    if delete or background:
        # all done
        return

    # added rule, not background
    # wait for CTRL+C
    try:
        Event().wait()
    finally:
        subprocess.run(delete_cmd, check=True)


# TODO: Move somwhere else
class TunMapper:
    def __init__(self, client, networks, fixed_name=None, random_name=False, tokens=2):
        self.client = client
        self.networks = networks
        self.lock = Lock()
        self.fixed_name = fixed_name
        self.random_name = random_name
        self.tokens = tokens
        self.registered = []

    def unregister_dns(self, cleanup=False):
        if not self.registered:
            return
        unregister_cmd = ["autovnet", "rtfm", "dns", "unregister", "-q"]
        if self.fixed_name and not cleanup:
            # Special case: if we have a fixed name (first),
            # don't unregister it unless all done
            self.registered.pop(0)

        unregister_cmd.extend(self.registered)
        self.registered = []
        try:
            subprocess.run(unregister_cmd, check=True)
        except Exception as e:
            typer.echo(f"[!] dns unregister failed: {e}")

    def register_dns(self, map_ip):
        to_register = []
        if self.fixed_name:
            to_register.append(f"{self.fixed_name}:{map_ip}")
        if self.random_name:
            name = random_dns(tld=avn.config.dns.tld, tokens=self.tokens)
            to_register.append(f"{name}:{map_ip}")
        if to_register:
            register_cmd = ["autovnet", "rtfm", "dns", "register", "-q", "-d"]
            register_cmd.extend(to_register)
            self.registered = to_register
            try:
                subprocess.run(register_cmd, check=True)
            except Exception as e:
                typer.echo(f"[!] dns register failed: {e}")

    def dns(self, map_ip):
        self.unregister_dns()
        self.register_dns(map_ip)

    def map(self):
        with self.lock:
            ip_generator = ip_gen(avn.networks(self.networks))
            map_ip = None
            while True:
                map_ip = None
                try:
                    map_ip = next(ip_generator)
                    resp = self.client.map(map_ip)
                    resp.raise_for_status()
                    typer.echo(f"[+] map_ip: {map_ip}")
                    break
                except Exception as e:
                    typer.echo(f"[-] Failed to map {self.client.assignment.ip} <-> {map_ip}")
                    time.sleep(ROLL_MIN_INTERVAL)
            self.dns(map_ip)
            return map_ip
