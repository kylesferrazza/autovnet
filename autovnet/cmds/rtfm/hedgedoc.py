import os
import re
import time
import gzip
import shutil
import shlex
import socket
import requests
import subprocess
from pathlib import Path

from typing import Optional, List
import typer

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount
from autovnet.utils import (
    compose_up,
    compose_up_ctx,
    compose_down,
    read_env_file,
    complete_codename,
    rand_port,
    as_port,
    split_host_port,
    gen_codename,
    gen_password,
    pg_dumper,
    wait_url_open,
    open_url,
    ensure_cert,
    gen_dhparam,
)

app = typer.Typer()

MOUNT_TYPE = "hedgedoc"
HEDGEDOC_PORT = 1337
HEDGEDOC_HOME = "home"


@app.command()
def export(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).",
    ),
    dest_port: Optional[int] = typer.Option(
        HEDGEDOC_PORT, "-p", "--dest-port", help="Local port that all players will use to access the hedgedoc server."
    ),
    random_dest_port: Optional[bool] = typer.Option(
        False, "-r", "--random-dest-port", help="If set, ignore dest_port and use a random port instead."
    ),
    quiet: Optional[bool] = typer.Option(
        False, "-q", "--quiet", help="If set, do not print the default help instructions."
    ),
    init_sql: Optional[str] = typer.Option(
        None,
        "--init-sql",
        help="[ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.",
    ),
    force_init_sql: Optional[bool] = typer.Option(
        False,
        "--force-init-sql",
        help="[ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.",
    ),
    env_file: Optional[str] = typer.Option(
        avn.config_path("rtfm.env"),
        "-e",
        "--env-file",
        help="[ADVANCED] Environment file to pass to docker-compose",
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    backup_interval: Optional[int] = typer.Option(
        300, "-i", "--backup-interval", help="Seconds between automatic backups (database exports)."
    ),
    backup_retain: Optional[int] = typer.Option(
        16,
        "-r",
        "--backup-retain",
        help="Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.",
    ),
    backup_quiet: Optional[bool] = typer.Option(
        False, "-Q", "--backup-quiet", help="If set, do not print backup confirmation messages."
    ),
    no_backup: Optional[bool] = typer.Option(
        False, "-B", "--no-backup", help="If set, do not make automated backups of the server contents."
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    https: Optional[bool] = typer.Option(
        False, "-s", "--https", help="Use HTTPs instead of HTTP (not really more secure here)."
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load.",
    ),
):
    """Run a hedgedoc server for note collaboration with other players.

    [HedgeDoc](https://hedgedoc.org/) is a real-time, collaborative markdown editor.

    This command uses `autovnet rtfm svc export` to securely share the server with other players.

    Unfortuately, other players cannot change the local port they will use to mount the service;
    it must match the port configured by the host, or hedgedoc's internal URLs will be broken.
    (The commands printed by this command are correct, just not customizable)

    The general recommendation is to use hedgedoc for all real-time document collaboration and note-taking,
    not to host "official" documentation, which should be in version control (e.g. git)
    """

    if not no_backup:
        # Do automated backups by default, since notes are important data
        if backup_interval <= 0:
            raise ValueError(f"[-] --backup-interval must be positive! Use --no-backup to disable backups.")

    if init_sql is not None and not Path(init_sql).is_file():
        raise FileNotFoundError(f"[-] {init_sql} does not exist!")

    if random_dest_port:
        dest_port = rand_port()

    env = read_env_file(env_file)

    proto = "http"
    if https:
        proto = "https"

    ctx = None
    need_init = False
    if codename is None:
        # New svc
        ctx = lambda: avn.svc(
            networks=networks, dest_ip=dest_ip, dest_port=dest_port, payload_dir=payload_dir, echo=False
        )
        need_init = True
    else:
        # Loaded svc
        ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False)
        # Peek at listener config to grab the values we need
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir)
        model = ln.lns[0]
        dest_ip = model.next_dest_ip
        dest_port = model.next_dest_port

    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    with ctx() as (svc_proc, codename, remote_addr):

        # Can't do the docker config until we have the codename
        doc_data = Path(payload_dir) / codename / "data"
        doc_uploads = Path(payload_dir) / codename / "uploads"
        doc_backups = Path(payload_dir) / codename / "backups"
        certs_dir = Path(payload_dir) / codename / "certs"

        for d in [doc_data, doc_uploads, doc_backups, certs_dir]:
            d.mkdir(parents=True, exist_ok=True)

        cert_path = certs_dir / "cert.pem"
        key_path = certs_dir / "key.pem"
        # See https://github.com/hedgedoc/hedgedoc/issues/1880
        dhparam_path = certs_dir / "dhparam.pem"
        if https:
            ensure_cert(cert_path, key_path)
            # Always rotating the dhparam is probably more secure
            gen_dhparam(dhparam_path)

        config_path = docker_path / f"{proto}.json"
        tmp_env = {
            "HEDGEDOC_BIND": f"{dest_ip}:{dest_port}",
            "HEDGEDOC_PORT": f"{dest_port}",
            "HEDGEDOC_DOMAIN": f"{dest_ip}",
            "HEDGEDOC_DATA": str(doc_data),
            "HEDGEDOC_UPLOADS": str(doc_uploads),
            "HEDGEDOC_CERTS": str(certs_dir),
            "HEDGEDOC_CONFIG": str(config_path),
        }

        # Allow hardcore users to do what they want by crafting an env file
        tmp_env.update(env)
        env = tmp_env

        env["UID"] = str(os.geteuid())
        env["GID"] = str(os.getegid())

        project = f"hedgedoc_{codename}"
        export_cmd = f"autovnet rtfm hedgedoc export {codename} -q"
        # Clients must use our dest_port because it is used to load JS / CSS etc.
        # The site will be broken if users don't mount on the same port that the server was configured for
        mount_cmd = f"autovnet rtfm hedgedoc mount -C {codename} {remote_addr}"
        if dest_port != HEDGEDOC_PORT:
            mount_cmd += f"-p {dest_port}"
        if dest_ip != "127.0.0.1":
            mount_cmd += f" -d {dest_ip}"
        if https:
            mount_cmd += " -s"
            export_cmd += " -s"

        home_url = f"{proto}://{dest_ip}:{dest_port}/{HEDGEDOC_HOME}"
        # Always give the TL;DR
        typer.echo(f"[+] {export_cmd}")
        typer.echo(f"[+] connect : xdg-open {home_url}")
        typer.echo(f"[+] mount   : {mount_cmd}")

        banner = "=" * 80
        alt_banner = "*" * 80
        login_instructions = f"""
    1. Click Logo > Sign In > your_handle_here@x.localhost + any password > Register
    2. Sign In > your_handle_here@x.localhost + your password > Sign In
"""
        intro = f"""
{banner}
(You can suppress this message with -q / --quiet)

You can use HedgeDoc without ever registering or signing in.
But if you want to ask users to create accounts:
{login_instructions}

1. Create a nice landing page at {home_url} for your friends

2. On your home page, give players examples of how to create pages and format good
    * See {avn.docker_path("plugins/hedgedoc/home.md")} for an example home page you can use

3. Give your friends these instructions:

{alt_banner}
Run this command to mount the hedgedoc server locally, to your {dest_ip}:{dest_port} :
```
{mount_cmd}
```
{banner}
"""
        if not quiet:
            typer.echo(intro)

        # The service export is already running, start the hedgedoc server in a thread
        with compose_up_ctx(docker_path, env=env, project=project, quiet=True) as (
            compose_thread,
            compose_stop,
        ):
            # Leverage the fact we can pre-compute the database's fully qualified, unique container name
            db_container = f"hedgedoc_{codename}_database_1"

            confirmed = True
            if init_sql is not None and not need_init and not force_init_sql:
                # Confirm so that we don't accidentally delete a database if the user re-executes a command in their history
                confirmed = typer.confirm("[!] --init-sql provided but database may already exist! Import anyway?")
                if not confirmed:
                    typer.echo("[-] Ignoring --init-sql")

            typer.echo("[+] Waiting for hedgedoc...")
            if wait_url_open(home_url, thread=compose_thread):
                typer.echo(f"[+] hedgedoc is ready!")
            else:
                typer.echo(f"[!] hedgedoc may still be initializing")

            if init_sql is not None and confirmed:
                # User confirmed they want to do this
                import_cmd = ["docker", "exec", "-i", db_container, "psql", "-U", "hedgedoc"]
                typer.echo(f"[+] Importing {init_sql} ...")
                proc = subprocess.Popen(import_cmd, stdin=subprocess.PIPE, stdout=subprocess.DEVNULL)
                file_ctx = None
                if init_sql.endswith(".gz"):
                    file_ctx = lambda: gzip.GzipFile(init_sql, "rb")
                else:
                    file_ctx = lambda: open(init_sql, "rb")

                with file_ctx() as in_file:
                    shutil.copyfileobj(in_file, proc.stdin)
                proc.stdin.close()
                proc.wait()
                if proc.returncode == 0:
                    typer.echo(f"[+] Imported {init_sql}")
                else:
                    typer.echo(f"[!] Import {init_sql} failed: '{shlex.join(import_cmd)}' returned {proc.returncode}")

            if not no_browser:
                open_url(home_url)

            # Note: all paths must call compose_thread.join() before exiting this context
            if no_backup:
                compose_thread.join()
            else:
                backup_cmd = [
                    "docker",
                    "exec",
                    db_container,
                    "pg_dump",
                    "hedgedoc",
                    "-U",
                    "hedgedoc",
                    "--clean",
                    "--if-exists",
                ]
                with pg_dumper(
                    backup_cmd, doc_backups, interval=backup_interval, retain=backup_retain, echo=(not backup_quiet)
                ) as (thr, stop):
                    # Dumper thread has already started
                    try:
                        compose_thread.join()
                    except KeyboardInterrupt as e:
                        raise
                    finally:
                        # Compose has exited, stop and join the background thread
                        stop.set()
                        compose_stop.set()


@app.command()
def mount(
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1", "-d", "--dest-ip", help="Destination IP you will use to access the service."
    ),
    dest_port: Optional[int] = typer.Option(
        HEDGEDOC_PORT,
        "-p",
        "--dest-port",
        help="[ADVANCED] Destination port you will use to access the service. Must match host's value.",
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: Optional[str] = typer.Option(None, "-C", "--set-codename", help="Set the codename for the mount."),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    https: Optional[bool] = typer.Option(
        False, "-s", "--https", help="Use HTTPs instead of HTTP (not really more secure here)."
    ),
    opt: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="IP:PORT or codename to mount.",
    ),
):

    """Mount another player's hedgedoc server locally."""

    custom_mount_args = None
    proto = "http"
    if https:
        proto += "s"
        custom_mount_args = " -s"

    with avn.svc_mount(
        codename=codename,
        opt=opt,
        dest_ip=dest_ip,
        dest_port=dest_port,
        payload_dir=payload_dir,
        xdg_open=proto,
        mount_type=MOUNT_TYPE,
        custom_mount_args=custom_mount_args,
        url_path="/home",
    ) as proc:
        proc.wait()
