import os
import gzip
import shutil
import shlex
from ipaddress import IPv4Address
import subprocess
import logging
from pathlib import Path
from typing import Optional, List

import typer
import powerdns

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount
from autovnet.utils import (
    compose_up_ctx,
    complete_codename,
    complete_files,
    read_env_file,
    rand_port,
    wait_url_open,
    pg_dumper,
    get_ip,
    rand_ip,
    rand_ip_list,
    gen_codename,
    random_dns,
)

app = typer.Typer()

MOUNT_TYPE = "dns"
# Don't log 404s
logging.getLogger("powerdns").setLevel(logging.CRITICAL)


DEFAULT_DNS_BIND = get_ip()
DEFAULT_DNS_PORT = 53
DEFAULT_API_REMOTE_PORT = 5353
DEFAULT_API_DEST_PORT = 15353
DEFAULT_CODENAME = "avn_dns"


@app.command()
def server(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    new: Optional[bool] = typer.Option(
        False, "-N", "--new", help="[ADVANCED] Force creating a new DNS server with a different codename."
    ),
    api_dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-A",
        "--api-dest-ip",
        help="[ADVANCED] Bind address for the API (anything but 127.X.X.X may allow anyone to access!).",
    ),
    api_dest_port: Optional[int] = typer.Option(None, "-P", "--api-dest-port", help="Local bind port for the API."),
    dest_ip: Optional[str] = typer.Option(
        None,
        "-d",
        "--dest-ip",
        help="[ADVANCED] Bind address for the DNS server.",
    ),
    dest_port: Optional[int] = typer.Option(53, "-p", "--dest-port", help="Local bind port for the server."),
    api_remote_port: Optional[int] = typer.Option(
        None,
        "-R",
        "--api-remote-port",
        help="[ADVANCED] Remote bind port for dns API. Not intended to be set by humans.",
    ),
    env_file: Optional[str] = typer.Option(
        avn.config_path("rtfm.env"),
        "-e",
        "--env-file",
        help="[ADVANCED] Environment file to pass to docker-compose",
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    init_sql: Optional[str] = typer.Option(
        None,
        "--init-sql",
        help="[ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.",
    ),
    force_init_sql: Optional[bool] = typer.Option(
        False,
        "--force-init-sql",
        help="[ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.",
    ),
    backup_interval: Optional[int] = typer.Option(
        300, "-i", "--backup-interval", help="Seconds between automatic backups (database exports)."
    ),
    backup_retain: Optional[int] = typer.Option(
        16,
        "-r",
        "--backup-retain",
        help="Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.",
    ),
    backup_quiet: Optional[bool] = typer.Option(
        False, "-Q", "--backup-quiet", help="If set, do not print backup confirmation messages."
    ),
    no_backup: Optional[bool] = typer.Option(
        False, "-B", "--no-backup", help="If set, do not make automated backups of the server contents."
    ),
    clean: Optional[bool] = typer.Option(
        False,
        "--clean",
        help="[ADVANCED] Do a clean start, with a clean database.",
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load",
    ),
):
    """Run a DNS server"""
    confirmed = True

    if api_dest_port is None:
        api_dest_port = DEFAULT_API_DEST_PORT

    if api_remote_port is None:
        api_remote_port = avn.config.dns.port or DEFAULT_API_REMOTE_PORT

    if dest_ip is None:
        dest_ip = avn.config.dns.host or DEFAULT_DNS_BIND

    if dest_port is None:
        dest_port = DEFAULT_DNS_PORT

    if not networks:
        networks = [avn.config.dns.host]

    env = read_env_file(env_file)
    ctx = None

    default_dir = Path(payload_dir) / DEFAULT_CODENAME
    if codename is None and default_dir.is_dir() and not new:
        codename = DEFAULT_CODENAME

    if codename is None:
        # New svc
        ctx = lambda: avn.svc(
            networks=networks,
            dest_ip=api_dest_ip,
            dest_port=api_dest_port,
            payload_dir=payload_dir,
            remote_port=api_remote_port,
            codename=DEFAULT_CODENAME,
            echo=False,
        )
    else:
        # Loaded svc
        ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False)
        # Peek at listener config to grab the values we need
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir)
        model = ln.lns[0]
        # TODO: Probably close enough
        dest_ip = model.remote_ip
        api_remote_ip = model.remote_ip
        api_remote_port = model.remote_port
        api_dest_port = model.next_dest_port

        if init_sql is not None and not force_init_sql:
            # Confirm so that we don't accidentally delete a database if the user re-executes a command in their history
            confirmed = typer.confirm("[!] --init-sql provided but database may already exist! Import anyway?")
            if not confirmed:
                typer.echo("[-] Ignoring --init-sql")

    # Prepare compose
    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    pdns_bind = f"{dest_ip}:{dest_port}"
    pdns_api_bind = f"{api_dest_ip}:{api_dest_port}"

    tld = avn.config.dns.tld
    if tld is None:
        tld = ""

    tmp_env = {
        "PDNS_BIND": pdns_bind,
        "PDNS_API_BIND": pdns_api_bind,
        "PDNS_AUTHORITY_CONF_TEMPLATE": str(avn.docker_path(f"plugins/{MOUNT_TYPE}/authority.conf")),
        "PDNS_RECURSOR_CONF_TEMPLATE": str(avn.docker_path(f"plugins/{MOUNT_TYPE}/recursor.conf")),
        "PDNS_INIT_SQL": str(avn.docker_path(f"plugins/{MOUNT_TYPE}/pdns.sql")),
        "AVN_TLD": tld,
        "DEFAULT_DNS": "127.0.0.11",
    }

    # TODO: Publicly expose stunnel side of API at MAIN_IP:5353
    with ctx() as (proc, codename, remote_addr):

        # Prepare codename directory, now that we know the codename
        data_dir = Path(payload_dir) / codename / "data"
        backups_dir = Path(payload_dir) / codename / "backups"
        certs_dir = Path(payload_dir) / codename / "certs"

        if clean and data_dir.is_dir():
            typer.echo(f"[!] Resetting existing database {data_dir}")
            shutil.rmtree(data_dir)

        for _dir in [data_dir, backups_dir, certs_dir]:
            _dir.mkdir(parents=True, exist_ok=True)

        tmp_env["PDNS_DATA"] = str(data_dir)
        tmp_env.update(env)
        env = tmp_env
        env["UID"] = str(os.geteuid())
        env["GID"] = str(os.getegid())

        project = f"{MOUNT_TYPE}_{codename}"
        # dest_port included because invite link will contain it
        mount_cmd = f"autovnet rtfm {MOUNT_TYPE} mount -C {codename} {remote_addr}"
        export_cmd = f"autovnet rtfm {MOUNT_TYPE} export {codename}"

        typer.echo(f"[+] {export_cmd}")
        typer.echo(f"[+] mount   : {mount_cmd}")

        with compose_up_ctx(docker_path, env=env, project=project, quiet=True) as (
            compose_thread,
            compose_stop,
        ):
            typer.echo(f"[*] Waiting for {MOUNT_TYPE} ...")
            url = f"http://{api_dest_ip}:{api_dest_port}"
            ok = wait_url_open(url, proc=proc, thread=compose_thread)
            if ok:
                typer.echo(f"[+] {MOUNT_TYPE} is ready!")
            elif ok is False:
                typer.echo(f"[!] {MOUNT_TYPE} may still be initializing")

            # Leverage the fact we can pre-compute the database's fully qualified, unique container name
            db_container = f"{MOUNT_TYPE}_{codename}_db_1"

            if init_sql is not None and confirmed:
                # User confirmed they want to do this
                import_cmd = ["docker", "exec", "-i", db_container, "psql", "pdns", "-U", "pdns"]
                typer.echo(f"[+] Importing {init_sql} ...")
                proc = subprocess.Popen(import_cmd, stdin=subprocess.PIPE, stdout=subprocess.DEVNULL)
                file_ctx = None
                if init_sql.endswith(".gz"):
                    file_ctx = lambda: gzip.GzipFile(init_sql, "rb")
                else:
                    file_ctx = lambda: open(init_sql, "rb")

                with file_ctx() as in_file:
                    shutil.copyfileobj(in_file, proc.stdin)
                proc.stdin.close()
                proc.wait()
                if proc.returncode == 0:
                    typer.echo(f"[+] Imported {init_sql}")
                else:
                    typer.echo(f"[!] Import {init_sql} failed: '{shlex.join(import_cmd)}' returned {proc.returncode}")

            if no_backup:
                compose_thread.join()
            else:
                backup_cmd = [
                    "docker",
                    "exec",
                    db_container,
                    "pg_dump",
                    "pdns",
                    "-U",
                    "pdns",
                    "--clean",
                    "--if-exists",
                ]
                with pg_dumper(
                    backup_cmd, backups_dir, interval=backup_interval, retain=backup_retain, echo=(not backup_quiet)
                ) as (thr, stop):
                    # Dumper thread has already started
                    try:
                        compose_thread.join()
                    finally:
                        # Compose has exited, stop and join the background thread
                        stop.set()


@app.command()
def register(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!",
    ),
    dest_port: Optional[int] = typer.Option(
        None,
        "-p",
        "--dest-port",
        help="[ADVANCED] Destination port you will use to access the service (default: random).",
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: Optional[str] = typer.Option(None, "-C", "--set-codename", help="Set the codename for the mount."),
    opt: Optional[str] = typer.Option(
        None,
        "-m",
        "-mount",
        autocompletion=complete_codename,
        help="[ADVANCED] IP:PORT or codename to mount.",
    ),
    register: Optional[List[str]] = typer.Argument(
        None,
        help="[DOMAIN:]IP[:IP...] mapping to register (repeatable)",
    ),
    count: Optional[int] = typer.Option(0, "-c", "--count", help="The number of remote IP addresses to allocate."),
    detach: Optional[bool] = typer.Option(
        False,
        "-d",
        "--detach",
        help="[ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.",
    ),
    quiet: Optional[bool] = typer.Option(
        False,
        "-q",
        "--quiet",
        help="Suppress most output.",
    ),
    keep_mount: Optional[bool] = typer.Option(
        False,
        "-k",
        "--keep-mount",
        help="[ADVANCED] Keep the generated mount dir instead of deleting it.",
    ),
    in_file: Optional[str] = typer.Option(
        None,
        "-i",
        "--in-file",
        autocompletion=complete_files,
        help="Read registrations from a file.",
    ),
    out_file: Optional[str] = typer.Option(
        None,
        "-o",
        "--out-file",
        autocompletion=complete_files,
        help="Write registrations to a file.",
    ),
):
    """Create DNS records

    Supported input format:
        * None / -c / --count => random domain, random IP
        * IP only => random domain
        * domain only => random ip
        * DOMAIN:IP[:IP][:IP] => create a record for DOMAIN that maps to one or more IPs

    The game configuration contains a forced TLD that all registrations will have,
    to prevent accidental (or intentional) hijacking of real domains during online games.
    """

    if in_file is not None:
        _register = Path(in_file).read_text().splitlines()
        if not quiet:
            typer.echo(f"[+] Loaded {len(_register)} registrations from {in_file}")

        if register:
            register.extend(_register)
        else:
            register = _register

    if not register and count == 0:
        # default to 1 random registration
        count = 1

    if count > 0:
        # generator is efficient for big -c
        register_gen = (str(ip) for ip in rand_ip_list(avn.networks(networks), count))
        if register:
            # ... but we have to enumerate if user gave multiple input types
            if not isinstance(register, list):
                register = list(register)
            register.extend(register_gen)
        else:
            register = register_gen

    with avn.dns_record(
        dest_ip=dest_ip,
        dest_port=dest_port,
        register=register,
        networks=networks,
        codename=codename,
        payload_dir=payload_dir,
        echo=(not quiet),
        register_echo=True,
        unregister=(not detach),
        keep_mount=keep_mount,
    ) as (registered, proc):
        if out_file:
            Path(out_file).write_text("\n".join(registered))
            if not quiet:
                typer.echo(f"[+] Created {out_file}")
        if not detach:
            proc.wait()


@app.command()
def unregister(
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!",
    ),
    dest_port: Optional[int] = typer.Option(
        None,
        "-p",
        "--dest-port",
        help="[ADVANCED] Destination port you will use to access the service (default: random).",
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: Optional[str] = typer.Option(None, "-C", "--set-codename", help="Set the codename for the mount."),
    opt: Optional[str] = typer.Option(
        None,
        "-m",
        "-mount",
        autocompletion=complete_codename,
        help="[ADVANCED] IP:PORT or codename to mount.",
    ),
    in_file: Optional[str] = typer.Option(
        None,
        "-i",
        "--in-file",
        autocompletion=complete_files,
        help="Read registrations from a file.",
    ),
    register: Optional[List[str]] = typer.Argument(
        None,
        help="[DOMAIN:]IP[:IP...] mapping to register (repeatable)",
    ),
    quiet: Optional[bool] = typer.Option(
        False,
        "-q",
        "--quiet",
        help="Suppress most output.",
    ),
    keep_mount: Optional[bool] = typer.Option(
        False,
        "-k",
        "--keep-mount",
        help="[ADVANCED] Keep the generated mount dir instead of deleting it.",
    ),
):
    """Uncreate DNS records"""

    if in_file is not None:
        _register = Path(in_file).read_text().splitlines()
        if register:
            register.extend(_register)
        else:
            register = _register

    with avn.dns_record(
        dest_ip=dest_ip,
        dest_port=dest_port,
        register=register,
        codename=codename,
        payload_dir=payload_dir,
        echo=(not quiet),
        unregister_only=True,
        keep_mount=keep_mount,
    ) as (registered, proc):
        pass


@app.command()
def name(
    count: Optional[int] = typer.Option(1, "-c", "--count", help="The number of dns names to generate."),
    tokens: Optional[int] = typer.Option(2, "-t", "--tokens", help="The number of tokens to generate."),
):
    for i in range(count):
        name = random_dns(tld=avn.config.dns.tld, tokens=tokens)
        typer.echo(name)
