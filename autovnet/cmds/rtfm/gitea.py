import os
import subprocess
import gzip
import shlex
import shutil
from typing import Optional, List
from pathlib import Path

import typer

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount
from autovnet.utils import (
    compose_up_ctx,
    complete_codename,
    rand_port,
    wait_url_open,
    open_url,
    read_env_file,
    ip_gen,
    gen_codename,
    split_host_port,
    pg_dumper,
    ensure_cert,
)


DEFAULT_HTTP_PORT = 3080
DEFAULT_SSH_PORT = 3022
MOUNT_TYPE = "gitea"

app = typer.Typer()


@app.command()
def export(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).",
    ),
    http_dest_port: Optional[int] = typer.Option(
        DEFAULT_HTTP_PORT, "-p", "--http-dest-port", help="Local bind port for the server."
    ),
    ssh_dest_port: Optional[int] = typer.Option(
        DEFAULT_SSH_PORT, "-P", "--ssh-dest-port", help="Local bind port for the server."
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    https: Optional[bool] = typer.Option(
        False, "-s", "--https", help="[TODO] If set, use https (self-signed) to make security nerds feel better."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    env_file: Optional[str] = typer.Option(
        avn.config_path("rtfm.env"),
        "-e",
        "--env-file",
        help="[ADVANCED] Environment file to pass to docker-compose",
    ),
    init_sql: Optional[str] = typer.Option(
        None,
        "--init-sql",
        help="[ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.",
    ),
    force_init_sql: Optional[bool] = typer.Option(
        False,
        "--force-init-sql",
        help="[ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.",
    ),
    backup_interval: Optional[int] = typer.Option(
        300, "-i", "--backup-interval", help="Seconds between automatic backups (database exports)."
    ),
    backup_retain: Optional[int] = typer.Option(
        16,
        "-r",
        "--backup-retain",
        help="Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.",
    ),
    backup_quiet: Optional[bool] = typer.Option(
        False, "-Q", "--backup-quiet", help="If set, do not print backup confirmation messages."
    ),
    no_backup: Optional[bool] = typer.Option(
        False, "-B", "--no-backup", help="If set, do not make automated backups of the server contents."
    ),
    app_ini: Optional[str] = typer.Option(
        avn.docker_path(f"plugins/{MOUNT_TYPE}/app.ini"),
        "--app-ini",
        help="[ADVANCED] Path to gitea app.ini template.",
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load",
    ),
):
    """Run a self-hosted instance of gitea"""

    confirmed = True
    if init_sql is not None and not Path(init_sql).is_file():
        raise FileNotFoundError(f"[-] {init_sql} does not exist!")

    http_dest_ip = dest_ip
    ssh_dest_ip = dest_ip

    env = read_env_file(env_file)
    proto = "http"
    if https:
        proto += "s"
    url = f"{proto}://{dest_ip}:{http_dest_port}"

    # Eww, gross, but none of the rest of the internals expect 2 ports
    # We also need 2 stunnel processes anyway, so this isn't _that_ unreasonable.
    # For now, any svc that needs multiple ports must have a finite number of with blocks
    http_ctx = None
    ssh_ctx = None

    # https
    cert_file = "cert.pem"
    key_file = "key.pem"

    if codename is None:
        # New svc
        # We need both remote IPs and codenames to match
        remote_ip = next(ip_gen(avn.networks(networks)))
        codename = gen_codename()
        # New svc
        http_ctx = lambda: avn.svc(
            networks=[remote_ip],
            dest_ip=dest_ip,
            dest_port=http_dest_port,
            payload_dir=payload_dir,
            echo=False,
            codename=codename,
            suffix="http",
        )
        ssh_ctx = lambda: avn.svc(
            networks=[remote_ip],
            dest_ip=dest_ip,
            dest_port=ssh_dest_port,
            payload_dir=payload_dir,
            echo=False,
            codename=codename,
            suffix="ssh",
        )

    else:
        # Loaded svc
        http_ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False, suffix="http")
        ssh_ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False, suffix="ssh")

        # Peek at listener config to grab the values we need
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir, suffix="http")
        model = ln.lns[0]
        http_dest_ip = model.next_dest_ip
        http_dest_port = model.next_dest_port

        ln = avn.listener_from_codename(codename, payload_dir=payload_dir, suffix="ssh")
        model = ln.lns[0]
        ssh_dest_ip = model.next_dest_ip
        ssh_dest_port = model.next_dest_port

        if init_sql is not None and not force_init_sql:
            # Confirm so that we don't accidentally delete a database if the user re-executes a command in their history
            confirmed = typer.confirm("[!] --init-sql provided but database may already exist! Import anyway?")
            if not confirmed:
                typer.echo("[-] Ignoring --init-sql")

    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    certs_dir = Path(payload_dir) / codename / "certs"
    data_dir = Path(payload_dir) / codename / "data"
    pg_dir = Path(payload_dir) / codename / "postgres"
    backups_dir = Path(payload_dir) / codename / "backups"
    for _dir in [certs_dir, data_dir, pg_dir, backups_dir]:
        _dir.mkdir(parents=True, exist_ok=True)

    cert_path = certs_dir / cert_file
    key_path = certs_dir / key_file
    mnt_path = Path("/certs")

    if https:
        ensure_cert(cert_path, key_path)
        # Fix paths to be container paths
        cert_path = mnt_path / cert_file
        key_path = mnt_path / key_file
    else:
        # not https
        cert_path = ""
        key_path = ""

    with http_ctx() as (http_proc, _, http_remote_addr):
        with ssh_ctx() as (ssh_proc, _, ssh_remote_addr):
            tmp_env = {
                "GITEA_HTTP_BIND": f"{http_dest_ip}:{http_dest_port}",
                "GITEA_SSH_BIND": f"{ssh_dest_ip}:{ssh_dest_port}",
                "HTTP_PORT": f"{http_dest_port}",
                "SSH_PORT": f"{ssh_dest_port}",
                "GITEA_APP_INI": str(app_ini),
                "GITEA_DATA": str(data_dir),
                "GITEA_POSTGRES_DATA": str(pg_dir),
                "GITEA_CERTS": str(certs_dir),
                "CERT_FILE": str(cert_path),
                "KEY_FILE": str(key_path),
                "PROTOCOL": proto,
            }
            tmp_env.update(env)
            env = tmp_env
            env["UID"] = str(os.geteuid())
            env["GID"] = str(os.getegid())

            project = f"{MOUNT_TYPE}_{codename}"
            remote_ip, http_remote_port = split_host_port(http_remote_addr)
            _, ssh_remote_port = split_host_port(ssh_remote_addr)

            mount_cmd = (
                f"autovnet rtfm {MOUNT_TYPE} mount -C {codename} {remote_ip} {http_remote_port} {ssh_remote_port}"
            )

            if http_dest_port != DEFAULT_HTTP_PORT:
                mount_cmd += f" -p {http_dest_port}"

            if ssh_dest_port != DEFAULT_SSH_PORT:
                mount_cmd += f" -P {ssh_dest_port}"

            export_cmd = f"autovnet rtfm {MOUNT_TYPE} export {codename}"
            if https:
                mount_cmd += " -s"
                export_cmd += " -s"

            typer.echo(f"[+] {export_cmd}")
            typer.echo(f"[+] connect : xdg-open {url}")
            typer.echo(f"[+] mount   : {mount_cmd}")
            with compose_up_ctx(docker_path, env=env, project=project, quiet=True) as (
                compose_thread,
                compose_stop,
            ):
                typer.echo(f"[*] Waiting for {MOUNT_TYPE} ...")
                ok = wait_url_open(url, proc=http_proc, thread=compose_thread)
                if ok:
                    typer.echo(f"[+] {MOUNT_TYPE} is ready!")
                elif ok is False:
                    typer.echo(f"[!] {MOUNT_TYPE} may still be initializing")

                if not no_browser:
                    open_url(url)

                # Leverage the fact we can pre-compute the database's fully qualified, unique container name
                db_container = f"gitea_{codename}_db_1"

                if init_sql is not None and confirmed:
                    # User confirmed they want to do this
                    import_cmd = ["docker", "exec", "-i", db_container, "psql", "-U", "gitea"]
                    typer.echo(f"[+] Importing {init_sql} ...")
                    proc = subprocess.Popen(import_cmd, stdin=subprocess.PIPE, stdout=subprocess.DEVNULL)
                    file_ctx = None
                    if init_sql.endswith(".gz"):
                        file_ctx = lambda: gzip.GzipFile(init_sql, "rb")
                    else:
                        file_ctx = lambda: open(init_sql, "rb")

                    with file_ctx() as in_file:
                        shutil.copyfileobj(in_file, proc.stdin)
                    proc.stdin.close()
                    proc.wait()
                    if proc.returncode == 0:
                        typer.echo(f"[+] Imported {init_sql}")
                    else:
                        typer.echo(
                            f"[!] Import {init_sql} failed: '{shlex.join(import_cmd)}' returned {proc.returncode}"
                        )

                if no_backup:
                    compose_thread.join()
                else:
                    backup_cmd = [
                        "docker",
                        "exec",
                        db_container,
                        "pg_dump",
                        "gitea",
                        "-U",
                        "gitea",
                        "--clean",
                        "--if-exists",
                    ]
                    with pg_dumper(
                        backup_cmd, backups_dir, interval=backup_interval, retain=backup_retain, echo=(not backup_quiet)
                    ) as (thr, stop):
                        # Dumper thread has already started
                        try:
                            compose_thread.join()
                        finally:
                            # Compose has exited, stop and join the background thread
                            stop.set()


@app.command()
def mount(
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!",
    ),
    http_dest_port: Optional[int] = typer.Option(
        DEFAULT_HTTP_PORT,
        "-p",
        "--http-dest-port",
        help="[ADVANCED] Destination port to mount http.",
    ),
    ssh_dest_port: Optional[int] = typer.Option(
        DEFAULT_SSH_PORT,
        "-P",
        "--ssh-dest-port",
        help="[ADVANCED] Destination port to mount ssh.",
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    https: Optional[bool] = typer.Option(
        False, "-s", "--https", help="If set, use https (self-signed) to make security nerds feel better."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: str = typer.Option(None, "-C", "--set-codename", help="Codename for the mount."),
    remote_ip: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Remote IP for service.",
    ),
    http_remote_port: Optional[int] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Remote PORT for http.",
    ),
    ssh_remote_port: Optional[int] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Remote PORT for ssh.",
    ),
):
    """Mount and connect to another player's gitea server"""

    http_remote = None
    ssh_remote = None

    if no_browser:
        proto = None

    if remote_ip is not None:
        if any(x is None for x in [http_remote_port, ssh_remote_port]):
            raise ValueError(f"[-] remote_ip requires HTTP and SSH ports!")
        http_remote = f"{remote_ip}:{http_remote_port}"
        ssh_remote = f"{remote_ip}:{ssh_remote_port}"

    # Massage our weird double-port shenanigans into 2 separate calls to our naive helper
    # Toggle codename "off" and opt "on" if this is a new svc (and vice-versa)
    http_codename = None
    http_opt = http_remote
    if http_remote is None:
        # Existing svc
        http_opt = codename
    else:
        # New svc, use --set-codename
        http_codename = codename

    ssh_codename = None
    ssh_opt = ssh_remote
    if ssh_remote is None:
        # Existing svc
        ssh_opt = codename
    else:
        # New svc, use --set-codename
        ssh_codename = codename

    proto = None
    if not no_browser:
        proto = "http"
        if https:
            proto += "s"

    cmd_str = f"autovnet rtfm {MOUNT_TYPE} mount -C {codename}"
    if https:
        cmd_str += " -s"

    typer.echo(f"[+] {cmd_str}")

    with avn.svc_mount(
        codename=ssh_codename,
        opt=ssh_opt,
        dest_ip=dest_ip,
        dest_port=ssh_dest_port,
        payload_dir=payload_dir,
        mount_type=MOUNT_TYPE,
        suffix="ssh",
        wait_tcp=True,
        echo=False,
        echo_ready=False,
        echo_mount=False,
    ) as ssh_proc:
        with avn.svc_mount(
            codename=http_codename,
            opt=http_opt,
            dest_ip=dest_ip,
            dest_port=http_dest_port,
            payload_dir=payload_dir,
            xdg_open=proto,
            mount_type=MOUNT_TYPE,
            suffix="http",
            echo=False,
            echo_mount=False,
        ) as http_proc:
            http_proc.wait()
            ssh_proc.wait()
