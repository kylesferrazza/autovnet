from typing import Optional, List

import typer

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount
from autovnet.utils import (
    compose_up_ctx,
    complete_codename,
    rand_port,
    wait_url_open,
    open_url,
)

app = typer.Typer()

MOUNT_TYPE = "drawio"
DRAWIO_HTTP_PORT = 8080
DRAWIO_HTTPS_PORT = 8443


@app.command()
def export(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).",
    ),
    dest_port: Optional[int] = typer.Option(
        None, "-p", "--dest-port", help="Local bind port for the server. (default: random)."
    ),
    https: Optional[bool] = typer.Option(
        False, "-s", "--https", help="Use HTTPs instead of HTTP (not really more secure here)."
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("drawio"), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load",
    ),
):
    """Run a self-hosted instance of drawio (a.k.a diagrams.net)

    You can make pretty diagrams / network maps in your browser, then upload them to hedgedoc as SVGs
    as part of your notes
    """

    if dest_port is None:
        dest_port = rand_port()

    ctx = None

    if codename is None:
        # New svc
        ctx = lambda: avn.svc(
            networks=networks, dest_ip=dest_ip, dest_port=dest_port, payload_dir=payload_dir, echo=False
        )
    else:
        # Loaded svc
        ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False)
        # Peek at listener config to grab the values we need
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir)
        model = ln.lns[0]
        dest_ip = model.next_dest_ip
        dest_port = model.next_dest_port

    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    drawio_port = DRAWIO_HTTP_PORT
    proto = "http"
    if https:
        drawio_port = DRAWIO_HTTPS_PORT
        proto = "https"
    drawio_bind = f"{dest_ip}:{dest_port}"
    url = f"{proto}://{drawio_bind}"

    env = {
        "DRAWIO_BIND": drawio_bind,
        "DRAWIO_PORT": f"{drawio_port}",
    }

    with ctx() as (proc, codename, remote_addr):
        project = f"{MOUNT_TYPE}_{codename}"
        mount_cmd = f"autovnet rtfm {MOUNT_TYPE} mount -C {codename} {remote_addr}"
        export_cmd = f"autovnet rtfm {MOUNT_TYPE} export {codename}"
        if https:
            mount_cmd += " -s"
            export_cmd += " -s"

        typer.echo(f"[+] {export_cmd}")
        typer.echo(f"[+] connect : xdg-open {url}")
        typer.echo(f"[+] mount   : {mount_cmd}")
        with compose_up_ctx(docker_path, env=env, project=project, quiet=True) as (
            compose_thread,
            compose_stop,
        ):
            typer.echo(f"[*] Waiting for {MOUNT_TYPE} ...")
            ok = wait_url_open(url, proc=proc, thread=compose_thread)
            if ok:
                typer.echo(f"[+] {MOUNT_TYPE} is ready!")
            elif ok is False:
                typer.echo(f"[!] {MOUNT_TYPE} may still be initializing")

            if not no_browser:
                open_url(url)

            compose_thread.join()


@app.command()
def mount(
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!",
    ),
    dest_port: Optional[int] = typer.Option(
        None,
        "-p",
        "--dest-port",
        help="[ADVANCED] Destination port you will use to access the service (default: random).",
    ),
    https: Optional[bool] = typer.Option(
        False, "-s", "--https", help="Use HTTPs instead of HTTP (not really more secure here)."
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: Optional[str] = typer.Option(None, "-C", "--set-codename", help="Set the codename for the mount."),
    opt: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="IP:PORT or codename to mount.",
    ),
):
    """Mount and connect to another player's drawio service"""

    # Nothing fancy here, a standard mount

    proto = "http"
    custom_mount_args = None
    if https:
        proto = "https"
        custom_mount_args = " -s"

    if no_browser:
        proto = None

    with avn.svc_mount(
        codename=codename,
        opt=opt,
        dest_ip=dest_ip,
        dest_port=dest_port,
        payload_dir=payload_dir,
        xdg_open=proto,
        mount_type=MOUNT_TYPE,
        custom_mount_args=custom_mount_args,
    ) as proc:
        proc.wait()
