import os
import re
import json
import shutil
import shlex
import subprocess
import secrets
import tempfile
import tarfile
import gzip
import time
from pathlib import Path
from typing import Optional, List
import asyncio
from threading import Event

import requests
import typer
import pyfiglet
from sliver import SliverClient, SliverClientConfig

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount, Listener
from autovnet.api.sliver import sliver_api
from autovnet.utils import (
    compose_run,
    gen_codename,
    gen_password,
    complete_codename,
    complete_opt_codename,
    complete_files,
    rand_port,
    split_host_port,
    read_env_file,
    compose_up_ctx,
    ghost_type_ctx,
    run_app,
    yield_none,
    rand_ip,
    tar_extract,
    pg_dumper,
    wait_tcp_open,
)
from autovnet.utils.svc import peek_mount
from autovnet.utils.sliver import *

app = typer.Typer()

MOUNT_TYPE = "sliver"
CONTAINER_SLIVER_DIR = f"/opt/{MOUNT_TYPE}"
DEFAULT_FONT = "cyberlarge"


@app.command()
def armorize(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        help="Directory to save command metadata.",
    ),
    url: Optional[str] = typer.Option(
        "https://raw.githubusercontent.com/sliverarmory/armory/master/armory.json",
        "-u",
        "--url",
        help="Armory JSON.",
    ),
    file: Optional[str] = typer.Option(
        None,
        "-f",
        "--file",
        help="[ADVANCED] Manual armory JSON file to use instead of URL.",
    ),
    codename: str = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load.",
    ),
):
    """Download the sliver armory from https://github.com/BishopFox/sliver/wiki/Armory

    The armory is distributed to clients when they import the sliver config
    """

    armory_json = None
    if file is not None:
        armory_json = Path(file).read_text()
    else:
        armory_json = requests.get(url).text

    armory = json.loads(armory_json)
    cmd = ["autovnet", "rtfm", "sliver", "client", "-D", str(payload_dir), codename]

    install = []
    for d in armory["bundles"]:
        install.append(d["name"])

    for d in armory["aliases"]:
        install.append(d["command_name"])

    for d in armory["extensions"]:
        install.append(d["command_name"])

    text = "\n".join([f"armory install {i}" for i in install]) + "\n"
    text += "\n".join(["armory update", "exit"])

    with ghost_type_ctx(cmd, text=text) as (proc, path):
        proc.wait()


@app.command()
def rc(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        help="Directory to save command metadata.",
    ),
    codename: str = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load.",
    ),
    files: List[str] = typer.Argument(
        ...,
        autocompletion=complete_files,
        help="Previous codename to load",
    ),
):
    """Run sliver commands from a file

    Similar to metasploit-style rc files
    """

    text = ""
    for f in files:
        text += Path(f).read_text()

    cmd = ["autovnet", "rtfm", "sliver", "client", "-D", str(payload_dir), codename]
    with ghost_type_ctx(cmd, text=text) as (proc, path):
        proc.wait()


@app.command()
def server(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        help="Directory to save command metadata.",
    ),
    mount_dir: Optional[str] = typer.Option(
        avn.data_path("mnt"),
        "-M",
        "--mount-dir",
        help="Host directory to mount in container at the same path.",
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "--dest-ip",
        help="[ADVANCED] Multiplayer host",
    ),
    dest_port: Optional[int] = typer.Option(
        None,
        "--dest-port",
        help="[ADVANCED] Multiplayer port",
    ),
    container_mount_dir: Optional[str] = typer.Option(
        avn.data_path("mnt"),
        "--container-mount-dir",
        help="Container directory to mount --mount-dir.",
    ),
    verbose: Optional[bool] = typer.Option(
        False,
        "-v",
        "--verbose",
        help="Show verbose output.",
    ),
    env_file: Optional[str] = typer.Option(
        avn.config_path("rtfm.env"),
        "-e",
        "--env-file",
        help="[ADVANCED] Environment file to pass to docker-compose",
    ),
    init_sql: Optional[str] = typer.Option(
        None,
        "--init-sql",
        help="[ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.",
    ),
    force_init_sql: Optional[bool] = typer.Option(
        False,
        "--force-init-sql",
        help="[ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.",
    ),
    backup_interval: Optional[int] = typer.Option(
        300, "-i", "--backup-interval", help="Seconds between automatic backups (database exports)."
    ),
    backup_retain: Optional[int] = typer.Option(
        16,
        "-r",
        "--backup-retain",
        help="Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.",
    ),
    backup_quiet: Optional[bool] = typer.Option(
        False, "-Q", "--backup-quiet", help="If set, do not print backup confirmation messages."
    ),
    no_backup: Optional[bool] = typer.Option(
        True, "-B", "--no-backup", help="If set, do not make automated backups of the server contents."
    ),
    no_armory: Optional[bool] = typer.Option(
        False,
        "-A",
        "--no-armory",
        help="If set, do not pre-download the sliver armory (https://github.com/sliverarmory/armory).",
    ),
    no_generate: Optional[bool] = typer.Option(
        False,
        "-G",
        "--no-generate",
        help="If set, do not pre-generate payloads.",
    ),
    no_dns: Optional[bool] = typer.Option(
        False,
        "--no-dns",
        help="Disable automatic dns registration",
    ),
    count: Optional[int] = typer.Option(4, "-c", "--count", help="The number of remote IP addresses to allocate."),
    random_register: Optional[int] = typer.Option(
        0,
        "-R",
        "--register-random",
        count=True,
        help="Register a random DNS name (repeatable).",
    ),
    register: Optional[List[str]] = typer.Option(
        None,
        "-r",
        "--register",
        help="Register a DNS name (repeatable).",
    ),
    profiles_path: Optional[str] = typer.Option(
        None,
        "-P",
        "--profiles-path",
        autocompletion=complete_files,
        help="Path to profiles yaml.",
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load.",
    ),
):
    """Start a sliver server

    See https://github.com/BishopFox/sliver/wiki for sliver help

    Note: all db options are disabled.
    sqlite is used until/unless sliver actually supports postgres
    """
    # TODO:
    # - svc for multiplayer / scripting
    # - generate config bundle for other players to import (client config + svc mount location)
    # - init_sql / postgres backup

    env = read_env_file(env_file)

    confirmed = True
    ctx = None

    if dest_port is None:
        dest_port = rand_port()

    default_ln = None
    default_ln_path = None

    # This svc is the actual sliver multiplayer api
    if codename is None:
        # New svc
        codename = gen_codename()
        ctx = lambda: avn.svc(
            networks=networks,
            codename=codename,
            dest_ip=dest_ip,
            dest_port=dest_port,
            payload_dir=payload_dir,
            echo=False,
        )

        networks = avn.networks(networks)
        # Generate svc listener and mount config for the export svc (the thing that generates the config bundle)
        # This isn't user configurable, but it should not really matter
        # If it does for you, just edit the file
        remote_ip = rand_ip(networks)
        remote_port = rand_port()
        local_ip = "127.0.0.1"
        local_port = rand_port()

        ln = avn.listener(
            [remote_ip],
            [remote_port],
            "127.1.33.7",
            dest_port=rand_port(),
            echo=True,
            codename=codename,
            deconflict=False,
        )
        for model in ln.lns:
            model.next_dest_ip = local_ip
            model.next_dest_port = local_port
        ln.save(payload_dir, suffix="export")

        mnt = Mount(
            host=str(remote_ip),
            port=remote_port,
            svc_host="127.0.0.1",
            svc_port=rand_port(),
        )
        mnt.save(payload_dir, codename=codename, exist_ok=False, cmd=MOUNT_TYPE, suffix="export")

        if not (register or random_register or no_dns):
            random_register = count

        # Set up default listeners
        default_ln = avn.listener(
            networks,
            [443] * count,
            "127.0.0.1",
            dest_port=rand_port(),
            echo=False,
            codename="default",
            register=register,
            random_register=random_register,
        )

    else:
        # Loaded svc
        ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False)
        # Peek at listener config to grab the values we need
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir, echo=False)
        model = ln.lns[0]
        dest_ip = model.next_dest_ip
        dest_port = model.next_dest_port
        no_armory = True
        if init_sql is not None and not force_init_sql:
            # Confirm so that we don't accidentally delete a database if the user re-executes a command in their history
            confirmed = typer.confirm("[!] --init-sql provided but database may already exist! Import anyway?")
            if not confirmed:
                typer.echo("[-] Ignoring --init-sql")

    # Prepare compose
    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    with ctx() as (proc, codename, remote_addr):

        sliver_dir = Path(payload_dir) / codename
        sliver_db = sliver_dir / "db"
        sliver_backups = sliver_dir / "backups"
        scripts = sliver_dir / "scripts"
        generated = sliver_dir / "generated"
        listeners_dir = sliver_dir / "listeners"
        mount_yaml = sliver_dir / "mount.yaml"

        for d in [sliver_dir, sliver_db, sliver_backups, generated, listeners_dir, scripts]:
            d.mkdir(parents=True, exist_ok=True)

        if profiles_path is None:
            profiles_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/profiles.yaml")
        profiles_dest = generated / "profiles.yaml"
        shutil.copyfile(profiles_path, profiles_dest)
        profiles_path = profiles_dest

        # The actual sliver API svc
        host, port = split_host_port(remote_addr)

        mnt = Mount(
            host=host,
            port=port,
            svc_host=dest_ip,
            svc_port=dest_port,
        )
        mnt.save(payload_dir, codename=codename, exist_ok=True, cmd=MOUNT_TYPE)

        if default_ln is not None:
            default_ln_path = listeners_dir / "default.yaml"
            default_ln.save(default_ln_path, is_file=True)

        tmp_env = {
            "SLIVER_DATA": str(sliver_dir),
            "SLIVER_DB_DATA": str(sliver_db),
            "MULTIPLAYER_HOST": str(dest_ip),
            "MULTIPLAYER_PORT": str(dest_port),
            "SLIVER_USER": "default",
            "POSTGRES_DB": "sliver",
            "POSTGRES_USER": "sliver",
            "POSTGRES_HOST": "127.0.0.1",
            "POSTGRES_PORT": str(rand_port()),  # random because network_mode: host
            "MOUNT_DIR": str(mount_dir),
            "CONTAINER_MOUNT_DIR": str(container_mount_dir),
        }
        tmp_env.update(env)
        env = tmp_env
        env["UID"] = str(os.geteuid())
        env["GID"] = str(os.getegid())

        typer.echo("=" * 80)
        art = pyfiglet.figlet_format(MOUNT_TYPE, font=DEFAULT_FONT)
        art = "\n".join(line for line in art.splitlines() if line.strip())
        typer.echo(art)

        # This is important, print it anyway
        typer.echo("\n" + "=" * 80)
        typer.echo(f"[+] autovnet rtfm sliver server {codename}")
        typer.echo(f"[+] client (local):        autovnet rtfm sliver client {codename}")
        typer.echo(f"[+] export (multiplayer):  autovnet rtfm sliver export {codename}")
        typer.echo(f"[+] listener:              autovnet rtfm sliver listener {codename}")
        typer.echo(f"[+] generate:              autovnet rtfm sliver generate {codename}")
        typer.echo("=" * 80)
        typer.echo(f"[+] mnt: {mount_dir} => {container_mount_dir}")
        typer.echo(f"[+] mnt: {sliver_dir} => {CONTAINER_SLIVER_DIR}")
        typer.echo("=" * 80)

        project = f"{MOUNT_TYPE}_{codename}"
        with compose_up_ctx(docker_path, env=env, project=project, quiet=(not verbose)) as (
            compose_thread,
            compose_stop,
        ):
            typer.echo(f"[~] setting up your server (this may take a while)...")
            if verbose:
                typer.echo(f"[+] waiting for {MOUNT_TYPE}...")
            ok = wait_tcp_open(env["MULTIPLAYER_HOST"], env["MULTIPLAYER_PORT"], thread=compose_thread)
            if ok:
                if verbose:
                    typer.echo(f"[+] sliver is running...")
            else:
                typer.echo(f"[+] {MOUNT_TYPE} still not ready! Trying to continue anyway...")
                time.sleep(10)

            # set up persistent default listeners on first start
            if default_ln is not None:
                if verbose:
                    typer.echo(f"[+] creating default listeners...")
                loop = asyncio.new_event_loop()
                config_path = sliver_dir / ".sliver-client" / "configs" / "default.json"
                config = SliverClientConfig.parse_config_file(config_path)
                client = SliverClient(config)

                loop.run_until_complete(client.connect())
                started = set()
                for model in default_ln.lns:
                    try:
                        key = f"{model.dest_ip}:{model.dest_port}"
                        if key in started:
                            continue
                        started.add(key)
                        sliver_ln = loop.run_until_complete(
                            client.start_mtls_listener(model.dest_ip, model.dest_port, persistent=True)
                        )
                        if sliver_ln is not None and verbose:
                            typer.echo(f"[+] started: {sliver_ln}")
                    except Exception as e:
                        if verbose:
                            typer.echo(f"[!] {e}")

            # prepare any autovnet listeners associated with this server
            # hack: merge any saved listeners into a single mega-listener object
            models = []
            for path in Path(listeners_dir).glob("*.yaml"):
                # load these now to avoid conflicts
                # (e.g. if user runs 'sliver listener' manually while we are generating default payloads)
                ln = avn.listener_from_file(path)
                models.extend(ln.lns)
            ln = avn.listener_from_models(models, codename=codename, echo=False, suppress_register=True)

            if default_ln is not None:
                # continue first time setup
                if not no_armory:
                    cmd = ["autovnet", "rtfm", "sliver", "armorize", codename]
                    stdout = None
                    stderr = None
                    if not verbose:
                        stdout = subprocess.DEVNULL
                        stderr = subprocess.DEVNULL
                    if verbose:
                        typer.echo("[+] downloading armory...")
                    subprocess.run(cmd, stdout=stdout, stderr=stderr)
                    if verbose:
                        typer.echo("[+] armory downloaded!")

                if not no_generate:
                    cmd = [
                        "autovnet",
                        "rtfm",
                        "sliver",
                        "generate",
                        str(codename),
                        "mtls",
                        "-L",
                        "default",
                    ]
                    if verbose:
                        typer.echo("[+] generating payloads...")
                    stdout = subprocess.DEVNULL
                    stderr = subprocess.DEVNULL
                    if verbose:
                        stdout = None
                        stderr = None
                    subprocess.run(cmd, stdout=stdout, stderr=stderr)
                    typer.echo(f"[+] generated payloads: {generated}")

            if verbose:
                typer.echo(f"[+] starting autovnet listeners...")
            with ln.listener():
                typer.echo(f"[+] sliver is ready!")
                # Note: all paths must call compose_thread.join() before exiting this context
                if no_backup:
                    compose_thread.join()
                else:
                    # Leverage the fact we can pre-compute the database's fully qualified, unique container name
                    db_container = f"sliver_{codename}_db_1"

                    if init_sql is not None and confirmed:
                        # User confirmed they want to do this
                        import_cmd = ["docker", "exec", "-i", db_container, "psql", "-U", env["POSTGRES_USER"]]
                        typer.echo(f"[+] Importing {init_sql} ...")
                        init_proc = subprocess.Popen(import_cmd, stdin=subprocess.PIPE, stdout=subprocess.DEVNULL)
                        file_ctx = None
                        if init_sql.endswith(".gz"):
                            file_ctx = lambda: gzip.GzipFile(init_sql, "rb")
                        else:
                            file_ctx = lambda: open(init_sql, "rb")

                        with file_ctx() as in_file:
                            shutil.copyfileobj(in_file, proc.stdin)
                        init_proc.stdin.close()
                        init_proc.wait()
                        if init_proc.returncode == 0:
                            typer.echo(f"[+] Imported {init_sql}")
                        else:
                            typer.echo(
                                f"[!] Import {init_sql} failed: '{shlex.join(import_cmd)}' returned {proc.returncode}"
                            )

                    backup_cmd = [
                        "docker",
                        "exec",
                        db_container,
                        "pg_dump",
                        "-p",
                        str(env["POSTGRES_PORT"]),
                        env["POSTGRES_DB"],
                        "-U",
                        env["POSTGRES_USER"],
                        "--clean",
                        "--if-exists",
                    ]
                    with pg_dumper(
                        backup_cmd,
                        sliver_backups,
                        interval=backup_interval,
                        retain=backup_retain,
                        echo=(not backup_quiet),
                    ) as (thr, stop):
                        # Dumper thread has already started
                        try:
                            compose_thread.join()
                        finally:
                            # Compose has exited, stop and join the background thread
                            stop.set()


@app.command()
def client(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        autocompletion=complete_files,
        help="Directory to save command metadata.",
    ),
    mount_dir: Optional[str] = typer.Option(
        avn.data_path("mnt"),
        "-M",
        "--mount-dir",
        help="Host directory to mount in container at the same path.",
    ),
    container_mount_dir: Optional[str] = typer.Option(
        avn.data_path("mnt"),
        "--container-mount-dir",
        autocompletion=complete_files,
        help="Container directory to mount --mount-dir.",
    ),
    verbose: Optional[bool] = typer.Option(
        False,
        "-v",
        "--verbose",
        help="Hide build output.",
    ),
    env_file: Optional[str] = typer.Option(
        avn.config_path("rtfm.env"),
        "-e",
        "--env-file",
        help="[ADVANCED] Environment file to pass to docker-compose",
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!",
    ),
    dest_port: Optional[int] = typer.Option(
        None,
        "-p",
        "--dest-port",
        help="[ADVANCED] Destination port you will use to access the service.",
    ),
    codename: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="Codename of sliver server, or sliver multiplayer config url.",
    ),
):
    """Sliver Client

    Connect to the sliver server so you can use sliver.

    If you started the sliver server, you just need the codename.
    If another player started the sliver server, they need to export it,
    and you need to import it.

    Then, you can connect by codename
    """

    env = read_env_file(env_file)

    # Prepare compose
    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")
    mnt, mnt_codename = peek_mount(codename, payload_dir)
    if mnt is None:
        raise FileNotFoundError(f"[-] Failed to find mount in {payload_dir}/{codename}")
    dest_ip = mnt.svc_host
    dest_port = mnt.svc_port

    sliver_dir = Path(payload_dir) / codename
    ln_path = sliver_dir / f"{codename}.yaml"

    is_server = False
    ctx = None

    if ln_path.is_file():
        # Special case: client is on the same system as the server
        is_server = True
        ctx = lambda: yield_none()
    else:
        ctx = lambda: avn.svc_mount(
            opt=codename,
            codename=codename,
            dest_ip=dest_ip,
            dest_port=dest_port,
            payload_dir=payload_dir,
            mount_type=MOUNT_TYPE,
            wait_tcp=True,
            echo_mount=False,
            echo=False,
        )

    tmp_env = {
        "SLIVER_DATA": str(sliver_dir),
        "MULTIPLAYER_HOST": str(dest_ip),
        "MULTIPLAYER_PORT": str(dest_port),
        "MOUNT_DIR": str(mount_dir),
        "CONTAINER_MOUNT_DIR": str(container_mount_dir),
    }

    tmp_env.update(env)
    env = tmp_env
    env["UID"] = str(os.geteuid())
    env["GID"] = str(os.getegid())

    with ctx() as proc:

        typer.echo("=" * 80)
        art = pyfiglet.figlet_format(MOUNT_TYPE, font=DEFAULT_FONT)
        art = "\n".join(line for line in art.splitlines() if line.strip())
        typer.echo(art)

        # This is important, print it anyway
        typer.echo("\n" + "=" * 80)
        typer.echo(f"[+] autovnet rtfm sliver client {codename}")
        typer.echo("=" * 80)
        typer.echo(f"[+] mnt: {mount_dir} => {container_mount_dir}")
        typer.echo(f"[+] mnt: {sliver_dir} => {CONTAINER_SLIVER_DIR}")
        typer.echo("=" * 80)

        project = f"{MOUNT_TYPE}_client_{codename}"
        try:
            compose_run(
                docker_path,
                "client",
                compose_file=f"docker-compose.client.yml",
                env=env,
                project=project,
                build=True,
                verbose_build=verbose,
            )
        except subprocess.SubprocessError as e:
            pass


@app.command()
def export(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        autocompletion=complete_files,
        help="Directory to save command metadata.",
    ),
    env_file: Optional[str] = typer.Option(
        avn.config_path("rtfm.env"),
        "-e",
        "--env-file",
        autocompletion=complete_files,
        help="[ADVANCED] Environment file to pass to docker-compose.",
    ),
    save_configs: Optional[bool] = typer.Option(
        False,
        "-s",
        "--save-configs",
        help="[ADVANCED] Save generated configs in the unlikely event a client needs to re-download.",
    ),
    codename: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="Previous codename to load.",
    ),
):
    """Share a sliver server with fellow players

    After the player imports the config, they can connect as a client
    to your server
    """
    server_dir = Path(payload_dir) / codename
    if not server_dir.is_dir():
        raise FileNotFoundError(f"[-] {server_dir} not found!")

    with avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False, suffix="export") as (
        proc,
        codename,
        remote_addr,
    ):
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir, suffix="export")
        model = ln.lns[0]
        export_ip = model.next_dest_ip
        export_port = model.next_dest_port

        ln = avn.listener_from_codename(codename, payload_dir=payload_dir)
        model = ln.lns[0]
        multiplayer_ip = model.next_dest_ip
        multiplayer_port = model.next_dest_port

        token = gen_password(secure=True)
        app = sliver_api(
            avn,
            payload_dir=payload_dir,
            server_codename=codename,
            server_dir=server_dir,
            multiplayer_host=multiplayer_ip,
            multiplayer_port=multiplayer_port,
            token=token,
            save_configs=save_configs,
        )

        typer.echo(f"[+] autovnet rtfm sliver import -t {token} {remote_addr}")
        # For some reason, the unicorn logging doesn't work here
        run_app(app, host=export_ip, port=export_port, log_level="debug", log_config=None)


@app.command("import")
def do_import(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        autocompletion=complete_files,
        help="Directory to save command metadata.",
    ),
    quiet: Optional[bool] = typer.Option(
        False,
        "-q",
        "--quiet",
        help="Hide build output.",
    ),
    token: Optional[str] = typer.Option(
        None,
        "-t",
        "--token",
        help="Sliver registration token from export.",
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED]",
    ),
    dest_port: Optional[int] = typer.Option(
        None,
        "-p",
        "--dest-port",
        help="[ADVANCED]",
    ),
    remote_addr: str = typer.Argument(
        ...,
        help="Remote address of sliver export.",
    ),
):
    """Import another player's sliver server configuration

    This allows connecting as a client to the player's server
    """

    if dest_port is None:
        dest_port = rand_port()

    player = avn.config.player
    if player is None:
        player = gen_codename()

    with avn.svc_mount(
        codename=gen_codename(),
        opt=remote_addr,
        dest_ip=dest_ip,
        dest_port=dest_port,
        payload_dir=payload_dir,
        wait_tcp=True,
        mount_type=MOUNT_TYPE,
        echo=False,
        echo_mount=False,
        keep_mount=False,
    ) as proc:
        url = f"http://{dest_ip}:{dest_port}/sliver/{player}/config"
        cmd = ["autovnet", "rtfm", "download"]
        if token:
            cmd.extend(["-p", token])
        else:
            cmd.append("-P")
        cmd.append(url)

        with tempfile.TemporaryDirectory() as tmp:
            tmp = Path(tmp)
            typer.echo(f"[+] downloading config...")
            subprocess.run(cmd, cwd=tmp)
            archive = tmp / "config"
            if not archive.is_file():
                raise FileNotFoundError(f"[-] Failed to download {archive.name}")

            with tarfile.open(name=archive) as tar_obj:
                tar_extract(tar_obj, tmp)

            for p in tmp.iterdir():
                if p.name == archive.name:
                    continue
                codename = p.name
                break

            server_dir = Path(payload_dir) / codename
            if server_dir.is_dir():
                raise FileExistsError(f"[-] {server_dir} already exists!")
            shutil.move(tmp / codename, server_dir)

            typer.echo(f"[+] imported: {server_dir}")
            typer.echo(f"\n[+] run: autovnet rtfm sliver client {codename}")


@app.command()
def generate(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        autocompletion=complete_files,
        help="Directory to save command metadata.",
    ),
    mount_dir: Optional[str] = typer.Option(
        avn.data_path("mnt"),
        "-M",
        "--mount-dir",
        autocompletion=complete_files,
        help="Host directory to mount in container at the same path.",
    ),
    container_mount_dir: Optional[str] = typer.Option(
        avn.data_path("mnt"),
        "--container-mount-dir",
        autocompletion=complete_files,
        help="Container directory to mount --mount-dir.",
    ),
    quiet: Optional[bool] = typer.Option(
        False,
        "-q",
        "--quiet",
        help="Hide build output.",
    ),
    verbose: Optional[bool] = typer.Option(
        False,
        "-v",
        "--verbose",
        help="Print more output.",
    ),
    env_file: Optional[str] = typer.Option(
        avn.config_path("rtfm.env"),
        "-e",
        "--env-file",
        autocompletion=complete_files,
        help="[ADVANCED] Environment file to pass to docker-compose",
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!",
    ),
    dest_port: Optional[int] = typer.Option(
        None,
        "-p",
        "--dest-port",
        help="[ADVANCED] Destination port you will use to access the service.",
    ),
    no_start_listener: Optional[bool] = typer.Option(
        False,
        "-N",
        "--no-start-listener",
        help="Do not attempt to start a listener on the sliver server.",
    ),
    no_persist_listener: Optional[bool] = typer.Option(
        False,
        "-M",
        "--no-persist-listener",
        help="Do make listeners on the sliver server persistent.",
    ),
    profiles_path: Optional[str] = typer.Option(
        None,
        "-P",
        "--profiles-path",
        autocompletion=complete_files,
        help="Path to profiles yaml.",
    ),
    listener_name: Optional[str] = typer.Option(
        None,
        "-L",
        "--listener-name",
        help="Listener codename from a previous sliver listener command.",
    ),
    listener_path: Optional[str] = typer.Option(
        None,
        "--listener-path",
        autocompletion=complete_files,
        help="[ADVANCED] Full path to listener yaml.",
    ),
    tun_ip: Optional[str] = typer.Option(
        None,
        "-T",
        "--tun-map-ip",
        help="autovnet map_ip to use for listener (as from autovnet rtfm tun up). Required for UDP. Set instead of --listener_path.",
    ),
    codename: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="Codename of sliver server, or sliver multiplayer config url.",
    ),
    proto: Optional[SliverProto] = typer.Argument(
        SliverProto.mtls,
        autocompletion=complete_codename,
        help="Protocol to use for payload.",
    ),
):
    """Generate a sliver binary

    Note: dns not fully supported for now
    """

    env = read_env_file(env_file)

    if dest_port is None:
        dest_port = proto_to_port(proto)

    # Prepare compose
    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")
    mnt, mnt_codename = peek_mount(codename, payload_dir)
    if mnt is None:
        raise FileNotFoundError(f"[-] Failed to find mount in {payload_dir}/{codename}")

    multiplayer_ip = mnt.svc_host
    multiplayer_port = mnt.svc_port

    if multiplayer_port is None:
        multiplayer_port = rand_port()

    sliver_dir = Path(payload_dir) / codename
    generated = sliver_dir / "generated"

    if listener_path is None and listener_name is not None:
        listener_path = sliver_dir / "listeners" / f"{listener_name}.yaml"

    if listener_name is None and listener_path is not None:
        listener_name = str(listener_path.stem)

    server_ln_path = sliver_dir / f"{codename}.yaml"

    is_server = False
    ctx = None

    if server_ln_path.is_file():
        # Special case: client is on the same system as the server
        is_server = True
        ctx = lambda: yield_none()
    else:
        ctx = lambda: avn.svc_mount(
            opt=codename,
            codename=codename,
            dest_ip=multiplayer_ip,
            dest_port=multiplayer_port,
            payload_dir=payload_dir,
            mount_type=MOUNT_TYPE,
            wait_tcp=True,
            echo_mount=False,
            echo=False,
        )

    tmp_env = {
        "SLIVER_DATA": str(sliver_dir),
        "MULTIPLAYER_HOST": str(multiplayer_ip),
        "MULTIPLAYER_PORT": str(multiplayer_port),
        "MOUNT_DIR": str(mount_dir),
        "CONTAINER_MOUNT_DIR": str(container_mount_dir),
    }

    tmp_env.update(env)
    env = tmp_env
    env["UID"] = str(os.geteuid())
    env["GID"] = str(os.getegid())

    config_path = None
    config_dir = sliver_dir / ".sliver-client" / "configs"
    host_config = config_dir / "default.json"
    player_config = config_dir / "sliver.json"
    if host_config.is_file():
        config_path = host_config
    elif player_config.is_file():
        config_path = player_config
    else:
        raise FileNotFoundError(f"[-] Failed to find sliver json config in {config_dir}")
    config = SliverClientConfig.parse_config_file(config_path)
    client = SliverClient(config)

    if profiles_path is None:
        profiles_path = generated / "profiles.yaml"
        if not profiles_path.is_file():
            profiles_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/profiles.yaml")

    ln = None
    if tun_ip is not None:
        model = Listener.construct(
            remote_ip=tun_ip,
            remote_port=dest_port,
            dest_ip=dest_ip,
            dest_port=dest_port,
        )
        ln = avn.listener_from_models([model])
        # probably contains ., so this might break scripts that rely on splitting paths
        listener_name = str(tun_ip)
    else:
        if listener_path is None:
            raise ValueError(
                f"[-] --listener-path must be set for TCP listeners. --tun-map-ip must be set for UDP listeners."
            )
        ln = avn.listener_from_file(listener_path)

    profiles = load_sliver_profiles(proto, profiles_path, listener_name=listener_name)
    sliver_c2s = listener_to_sliver_c2(proto, ln)

    loop = asyncio.new_event_loop()
    with ctx() as proc:
        loop.run_until_complete(client.connect())
        started = set()
        for model in ln.lns:
            try:
                key = f"{model.dest_ip}:{model.dest_port}"
                if key in started:
                    continue
                started.add(key)
                sliver_ln = loop.run_until_complete(
                    start_listener(client, proto, model, persistent=(not no_persist_listener))
                )
                if sliver_ln is not None:
                    typer.echo(f"[+] listener ({proto}://{model.dest_ip}:{model.dest_port}): {sliver_ln}")
            except Exception as e:
                if verbose:
                    typer.echo(f"[!] {e}")

        for p in profiles:
            is_wg = False
            for c2 in sliver_c2s:
                if c2.URL.startswith("wg://"):
                    is_wg = True
                    break
            if is_wg:
                wg_ip = loop.run_until_complete(client.generate_unique_ip())
                p.Config.WGPeerTunIP = wg_ip.IP
                p.Config.WGKeyExchangePort = 1337
                p.Config.WGTcpCommsPort = 8888
                p.Config.WGPeerTunIP = wg_ip.IP
            p.Config.C2.extend(sliver_c2s)
            try:
                loop.run_until_complete(client.save_implant_profile(p))
            except Exception as e:
                if verbose:
                    typer.echo(f"[!] {e}")

            if verbose:
                typer.echo(f"[+] {p}")

            typer.echo(f"[*] generating {p.Name} ...")
            res = loop.run_until_complete(client.generate(p.Config))
            out_path = generated / f"{p.Name}.{res.File.Name}"
            out_path.write_bytes(res.File.Data)
            typer.echo(f"[+] generated: {out_path}\n")


@app.command()
def listener(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        autocompletion=complete_files,
        help="Directory to save command metadata.",
    ),
    mount_dir: Optional[str] = typer.Option(
        avn.data_path("mnt"),
        "-M",
        "--mount-dir",
        autocompletion=complete_files,
        help="Host directory to mount in container at the same path.",
    ),
    container_mount_dir: Optional[str] = typer.Option(
        avn.data_path("mnt"),
        "--container-mount-dir",
        autocompletion=complete_files,
        help="Container directory to mount --mount-dir.",
    ),
    verbose: Optional[bool] = typer.Option(
        False,
        "-v",
        "--verbose",
        help="Print more output.",
    ),
    env_file: Optional[str] = typer.Option(
        avn.config_path("rtfm.env"),
        "-e",
        "--env-file",
        autocompletion=complete_files,
        help="[ADVANCED] Environment file to pass to docker-compose",
    ),
    name: Optional[str] = typer.Option(None, "-N", "--name", help="Listener name."),
    port: Optional[int] = typer.Option(None, "-p", "--port", help="Remote port to listen on."),
    dest_ip: Optional[str] = typer.Option(
        None, "-d", "--dest-ip", help="Destination IP for tunnelled traffic (default: 127.0.0.1)."
    ),
    dest_port: Optional[int] = typer.Option(
        None, "--dest-port", help="Destination port for tunnelled traffic (default: random)."
    ),
    no_auto_load: Optional[bool] = typer.Option(
        False,
        "--no-auto-load",
        help="Do not save this listener to run on next startup.",
    ),
    only_auto_load: Optional[bool] = typer.Option(
        False,
        "-A",
        "--only-auto-load",
        help="Run this listener on startup, but do not run it now.",
    ),
    no_dns: Optional[bool] = typer.Option(
        False,
        "--no-dns",
        help="Disable automatic dns registration",
    ),
    no_persist_listener: Optional[bool] = typer.Option(
        False,
        "-M",
        "--no-persist-listener",
        help="Do make listeners on the sliver server persistent.",
    ),
    force: Optional[bool] = typer.Option(
        False,
        "--force",
        help="Allow overwriting existing listener of the same naem.",
    ),
    count: Optional[int] = typer.Option(
        1,
        "-c",
        "--count",
        count=True,
        help="Number of IPs to allocate to listener.",
    ),
    random_register: Optional[int] = typer.Option(
        0,
        "-R",
        "--register-random",
        count=True,
        help="Register a random DNS name (repeatable).",
    ),
    register: Optional[List[str]] = typer.Option(
        None,
        "-r",
        "--register",
        help="Register a DNS name (repeatable).",
    ),
    codename: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="Codename of sliver server, or sliver multiplayer config url.",
    ),
    proto: Optional[SliverProto] = typer.Argument(
        SliverProto.mtls,
        autocompletion=complete_codename,
        help="Protocol to use for payload.",
    ),
):
    """Start a sliver listener + autovnet listener"""

    if name is None:
        name = gen_codename()

    networks = avn.networks(networks)

    if port is None:
        port = proto_to_port(proto)

    mnt, mnt_codename = peek_mount(codename, payload_dir)
    if mnt is None:
        raise FileNotFoundError(f"[-] Failed to find mount in {payload_dir}/{codename}")

    sliver_dir = Path(payload_dir) / codename
    server_ln_path = sliver_dir / f"{codename}.yaml"

    is_server = False
    ctx = None
    ln = None

    if server_ln_path.is_file():
        # Special case: client is on the same system as the server
        is_server = True
        ctx = lambda: yield_none()
    else:
        ctx = lambda: avn.svc_mount(
            opt=codename,
            codename=codename,
            payload_dir=payload_dir,
            mount_type=MOUNT_TYPE,
            wait_tcp=True,
            echo_mount=False,
            echo=False,
        )

    config_path = None
    config_dir = sliver_dir / ".sliver-client" / "configs"
    generated = sliver_dir / "generated"
    profiles_path = generated / "profiles.yaml"
    host_config = config_dir / "default.json"
    player_config = config_dir / "sliver.json"
    if host_config.is_file():
        config_path = host_config
    elif player_config.is_file():
        config_path = player_config
    else:
        raise FileNotFoundError(f"[-] Failed to find sliver json config in {config_dir}")

    listeners_dir = payload_dir / codename / "listeners"
    listener_path = listeners_dir / f"{name}.yaml"
    if listener_path.is_file() and not force:
        raise FileExistsError(f"{listener_path} already exists!")

    model = None
    is_udp = False
    if proto in [SliverProto.wg, SliverProto.dns]:
        is_udp = True

        if dest_ip is None:
            dest_ip = "0.0.0.0"

        if dest_port is None:
            dest_port = port

        typer.echo(f"[!] {proto} is UDP - binding listener to {dest_ip}")
        #  Make a fake listener
        ln = avn.listener_from_models([])
        model = Listener(
            remote_ip=dest_ip,
            remote_port=dest_port,
            dest_ip=dest_ip,
            dest_port=dest_port,
        )

    if dest_ip is None:
        dest_ip = "127.0.0.1"

    if dest_port is None:
        dest_port = rand_port()

    if ln is None:

        remote_ports = [port] * count
        if not (register or random_register or no_dns):
            random_register = count

        ln = avn.listener(
            networks,
            remote_ports,
            dest_ip,
            dest_port=dest_port,
            codename=name,
            register=register,
            random_register=random_register,
            echo=False,
            suppress_register=True,
        )
        model = ln.lns[0]

    if verbose:
        typer.echo("=" * 80)
        art = pyfiglet.figlet_format(MOUNT_TYPE, font=DEFAULT_FONT)
        art = "\n".join(line for line in art.splitlines() if line.strip())
        typer.echo(art)
        typer.echo("=" * 80)
        typer.echo(f"[+] {codename}.{name}")

    if not no_auto_load:
        if ln.lns:
            ln.save(listener_path, is_file=True)
            typer.echo(f"[+] ln: {listener_path}")

    with ctx() as proc:

        loop = asyncio.new_event_loop()
        config = SliverClientConfig.parse_config_file(config_path)
        client = SliverClient(config)

        loop.run_until_complete(client.connect())
        if verbose:
            typer.echo("[+] connected to sliver")

        try:
            if verbose:
                typer.echo(f"[~] listener ({proto}://{model.dest_ip}:{model.dest_port}) ...")
            sliver_ln = loop.run_until_complete(
                start_listener(client, proto, model, persistent=(not no_persist_listener))
            )
            typer.echo(f"[+] listener ({proto}://{model.dest_ip}:{model.dest_port}): {sliver_ln}")
        except Exception as e:
            typer.echo(f"[-] Failed to start listener: {e}")

        cmd = f"autovnet rtfm sliver generate {codename} {proto} -L {name}"
        if is_udp:
            cmd += " -T MAP_IP"
        typer.echo(f"[+] {cmd}")
        if only_auto_load:
            return

        if verbose:
            typer.echo("=" * 80)

        if not ln.lns:
            # Fake listener (http3) - no need to hold the terminal
            return

        with ln.listener():
            Event().wait()
