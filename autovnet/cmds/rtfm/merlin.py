import os
import shlex
import shutil
from pathlib import Path
from typing import Optional, List
from threading import Event

import typer
import pyfiglet

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount, ListenProto
from autovnet.utils import (
    compose_run,
    gen_codename,
    complete_codename,
    complete_files,
    ghost_type_ctx,
    rand_port,
)

app = typer.Typer()

MOUNT_TYPE = "merlin"
CONTAINER_MERLIN_DIR = "/opt/merlin"
DEFAULT_FONT = "cyberlarge"


@app.command()
def server(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        help="Directory to save command metadata.",
    ),
    mount_dir: Optional[str] = typer.Option(
        avn.data_path("mnt"),
        "-M",
        "--mount-dir",
        help="Host directory to mount in container at the same path",
    ),
    container_mount_dir: Optional[str] = typer.Option(
        avn.data_path("mnt"),
        "--container-mount-dir",
        help="Container directory to mount --mount-dir",
    ),
    version: Optional[str] = typer.Option(
        "latest",
        "-V",
        "--version",
        help="Merlin version to use. See https://github.com/Ne0nd0g/merlin/releases",
    ),
    verbose_build: Optional[bool] = typer.Option(
        False,
        "--verbose-build",
        help="[ADVANCED] Show build output",
    ),
    no_rc: Optional[bool] = typer.Option(
        False,
        "-R",
        "--no-rc",
        help="Do not load rc files from rc.d",
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load",
    ),
):
    """Run merlin server

    Merlin is a cross-platform post-exploitation Command & Control server and agent written in Go.
    See https://github.com/Ne0nd0g/merlin for official merlin usage

    Use `autonet rtfm merlin payload` to help configure listeners that use autovnet

    Last tested version: v1.4.1
    """

    ctx = None
    new_svc = False

    if codename is None:
        codename = gen_codename()

    merlin_dir = payload_dir / codename
    merlin_dir.mkdir(parents=True, exist_ok=True)
    mount_dir.mkdir(parents=True, exist_ok=True)
    rc_dir = merlin_dir / "rc.d"
    rc_dir.mkdir(parents=True, exist_ok=True)
    type_link = merlin_dir / "type"

    # Prepare compose
    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    env = {
        "MERLIN_VERSION": version,
        "MERLIN_DATA": str(merlin_dir),
        "MOUNT_DIR": str(mount_dir),
        "CONTAINER_MOUNT_DIR": str(container_mount_dir),
        "UID": str(os.geteuid()),
        "GID": str(os.getegid()),
    }

    rc_files = []
    if not no_rc:
        rc_files = sorted(rc_dir.glob("*.rc"))
    text = "\n".join(rc.read_text() for rc in rc_files)

    # hack: merge any saved listeners into a single mega-listener object
    models = []
    for path in Path(merlin_dir).glob("*.yaml"):
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir, filename=path.name)
        models.extend(ln.lns)
    ln = avn.listener_from_models(models, codename=codename)

    # Start autovnet listeners
    with ln.listener():

        # Set up merlin listeners
        with ghost_type_ctx(text=text) as (_, type_path):
            # header
            type_link.unlink(missing_ok=True)
            type_link.symlink_to(type_path)
            try:
                typer.echo("=" * 80)
                art = pyfiglet.figlet_format(MOUNT_TYPE, font=DEFAULT_FONT)
                art = "\n".join(line for line in art.splitlines() if line.strip())
                typer.echo(art)
                typer.echo("\n" + "=" * 80)
                typer.echo(f"[+] autovnet rtfm merlin {codename}")
                typer.echo(f"[+] listen: autovnet rtfm merlin listener {codename}")
                typer.echo(f"[+] script: autovnet rtfm merlin script {codename}")
                typer.echo("=" * 80)
                typer.echo(f"[+] mnt: {mount_dir} => {container_mount_dir}")
                typer.echo(f"[+] mnt: {merlin_dir} => {CONTAINER_MERLIN_DIR}")
                typer.echo("=" * 80)
                typer.echo(f"[+] type: {type_link}")
                typer.echo(f"[+] rc.d: {rc_dir}")
                for rc_file in rc_files:
                    typer.echo(f"        [+] {rc_file}")
                typer.echo("=" * 80)

                project = f"{MOUNT_TYPE}_{codename}"
                compose_run(
                    docker_path,
                    MOUNT_TYPE,
                    env=env,
                    project=project,
                    build=True,
                    verbose_build=verbose_build,
                )
            finally:
                type_link.unlink(missing_ok=True)


@app.command()
def script(
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        help="Directory to save command metadata.",
    ),
    auto_load: Optional[bool] = typer.Option(
        False,
        "-a",
        "--auto-load",
        help="Also run this script on startup, the next time the merlin server starts.",
    ),
    only_auto_load: Optional[bool] = typer.Option(
        False,
        "-A",
        "--only-auto-load",
        help="Run this script on startup, but do not run it now",
    ),
    force: Optional[bool] = typer.Option(
        False,
        "-f",
        "--force",
        help="Do not prompt for confirmation before overwriting an existing file.",
    ),
    codename: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="Previous codename to load",
    ),
    files: List[str] = typer.Argument(
        ...,
        autocompletion=complete_files,
        help="Previous codename to load",
    ),
):
    """Execute merlin server commands from a file

    Uses the `autovnet type` technique, and is subject to the same limitations.
    """

    merlin_dir = payload_dir / codename
    rc_dir = merlin_dir / "rc.d"
    type_link = merlin_dir / "type"

    if not merlin_dir.is_dir():
        raise FileNotFoundError(f"[-] {merlin_dir} not found!")

    if (not only_auto_load) and (not type_link.is_symlink()):
        # User wants to run the script now / expects the server to be up
        # But the type_link isn't there
        raise FileNotFoundError(f"[-] {type_link} not found!")

    save = auto_load or only_auto_load
    for file in files:
        file = Path(file)
        text = file.read_text()

        if not only_auto_load:
            typer.echo(f"[+] type: {file} -> {type_link}")
            type_link.write_text(text)

        if save:
            save_path = rc_dir / file.name
            if save_path.is_file() and not force:
                confirmed = typer.confirm(f"[!] {save_path} already exists! Overwrite it?")
                if not confirmed:
                    continue
            save_path.write_text(text)
            typer.echo(f"[+] rc: {save_path}")


@app.command()
def listener(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    port: Optional[int] = typer.Option(None, "-p", "--port", help="Remote port to listen on."),
    dest_ip: Optional[str] = typer.Option(
        None, "-d", "--dest-ip", help="Destination IP for tunnelled traffic (default: 127.0.0.1)."
    ),
    dest_port: Optional[int] = typer.Option(
        None, "--dest-port", help="Destination port for tunnelled traffic (default: random)."
    ),
    count: Optional[int] = typer.Option(1, "-c", "--count", help="The number of remote IP addresses to allocate."),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE),
        "-D",
        "--payload-dir",
        help="Directory to save command metadata.",
    ),
    force: Optional[bool] = typer.Option(
        False,
        "-f",
        "--force",
        help="Do not prompt for confirmation before overwriting an existing file.",
    ),
    name: Optional[str] = typer.Option(None, "-N", "--name", help="Listener name."),
    description: Optional[str] = typer.Option(None, "-X", "--description", help="Listener description."),
    psk: Optional[str] = typer.Option(None, "-k", "--psk", help="Pre-shared key for merlin. Agent must match server."),
    urls: Optional[str] = typer.Option(
        None, "-U", "--urls", help="URLS option in whatever format merlin wants (not documented)."
    ),
    x509_key: Optional[str] = typer.Option(None, "-K", "--key", help="Custom x509 key."),
    x509_cert: Optional[str] = typer.Option(None, "-C", "--cert", help="Custom x509 cert."),
    proto: Optional[ListenProto] = typer.Option(ListenProto.http2, "-P", "--proto", help="Listener protocol."),
    no_auto_load: Optional[bool] = typer.Option(
        False,
        "-T",
        "--no-auto-load",
        help="Do not save this listener to run on next startup.",
    ),
    only_auto_load: Optional[bool] = typer.Option(
        False,
        "-A",
        "--only-auto-load",
        help="Run this listener on startup, but do not run it now",
    ),
    no_dns: Optional[bool] = typer.Option(
        False,
        "--no-dns",
        help="Disable automatic dns registration",
    ),
    random_register: Optional[int] = typer.Option(
        0,
        "-R",
        "--register-random",
        count=True,
        help="Register a random DNS name (repeatable).",
    ),
    register: Optional[List[str]] = typer.Option(
        None,
        "-r",
        "--register",
        help="Register a DNS name (repeatable).",
    ),
    codename: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="Codename of merlin server",
    ),
):
    """Create an autovnet-assisted merlin listener"""

    merlin_dir = payload_dir / codename
    rc_dir = merlin_dir / "rc.d"
    type_link = merlin_dir / "type"

    if name is None:
        name = gen_codename()

    url_proto = "https"
    if proto in [ListenProto.http, ListenProto.h2c]:
        url_proto = "http"

    agent_proto = proto
    if agent_proto == ListenProto.http2:
        agent_proto = "h2"

    port_suffix = ""
    if port is None:
        if url_proto == "http":
            port = 80
        else:
            port = 443
    else:
        port_suffix = f":{port}"

    agent_cmd = None
    ln = None
    user_msg = None
    if proto == ListenProto.http3:
        # http3 is udp, which means it requires an external
        # 'autovnet rtfm tun up' and is not compatible with avn.listen*
        if dest_ip is None:
            dest_ip = "0.0.0.0"

        if dest_port is None:
            dest_port = port
        user_msg = "[!] http3 is UDP - expose: autovnet rtfm tun up"
        #  Make a fake listener
        ln = avn.listener_from_models([])
        agent_cmd = ["agent", "-proto", f"http3", "-url", f"https://MAP_IP{port_suffix}"]
        user_msg += f"\n[+] {shlex.join(agent_cmd)}"

    if dest_ip is None:
        dest_ip = "127.0.0.1"
    if dest_port is None:
        dest_port = rand_port()

    listener_path = payload_dir / codename / f"{codename}.{name}.yaml"
    if listener_path.is_file() and not force:
        raise FileExistsError(f"{listener_path} already exists!")

    type_link = payload_dir / codename / "type"
    if (not type_link.is_symlink()) and (not only_auto_load):
        raise FileNotFoundError(f"[-] {type_link} not found - is merlin {codename} running?")

    remote_ports = [port] * count
    if (not register) and (not random_register) and (not no_dns):
        random_register = count

    if ln is None:
        ln = avn.listener(
            networks,
            remote_ports,
            dest_ip,
            dest_port=dest_port,
            codename=codename,
            register=register,
            random_register=random_register,
        )

    typer.echo("\n" + "=" * 80)
    typer.echo(f"[+] {codename}.{name}")
    if user_msg:
        typer.echo(user_msg)

    for model in ln.lns:

        agent_cmd_ip = ["agent", "-proto", f"{agent_proto}", "-url", f"{url_proto}://{model.remote_ip}{port_suffix}"]

        if agent_cmd is None:
            agent_cmd = list(agent_cmd_ip)

        typer.echo(f"[+] {shlex.join(agent_cmd_ip)}")

        names = []
        if model.dns_names is not None:
            names = model.dns_names
        for dns in names:
            agent_cmd_dns = ["agent", "-proto", f"{agent_proto}", "-url", f"{url_proto}://{dns}{port_suffix}"]
            typer.echo(f"[+] {shlex.join(agent_cmd_dns)}")
        typer.echo("=" * 80)

    if description is None:
        description = shlex.join(agent_cmd)

    rc = f"""
main
listeners
use {proto}
set Name {name}
set Interface {dest_ip}
set Port {dest_port}
set Description {description}
"""
    if psk is not None:
        rc += f"""
set PSK {psk}
"""
    if urls is not None:
        rc += f"""
set URLS {urls}
"""
    rc += """
info
start
main
"""
    if x509_cert is not None:
        src = Path(x509_cert)
        if not src.is_file():
            raise FileNotFoundError(f"[-] {src} not found")
        dest = merlin_dir / "data" / f"{name}.crt"
        typer.echo(f"[+] crt: {src} -> {dest}")
        shutil.copyfile(src, dest)
        rc += f"""
set X509Cert {x509_cert}
"""

    if x509_key is not None:
        dest = Path(x509_key)
        if not dest.is_file():
            raise FileNotFoundError(f"[-] {dest} not found")
        dest = merlin_dir / "data" / f"{name}.key"
        typer.echo(f"[+] key: {src} -> {dest}")
        shutil.copyfile(src, dest)
        rc += f"""
set X509Key {x509_key}
"""

    if not no_auto_load:
        if ln.lns:
            ln.save(payload_dir, suffix=name)
            typer.echo(f"[+] ln: {listener_path}")

        save_path = rc_dir / f"{name}.rc"
        confirmed = True
        if save_path.is_file() and not force:
            confirmed = typer.confirm(f"[!] {save_path} already exists! Overwrite it?")
        if confirmed:
            save_path.write_text(rc)
            typer.echo(f"[+] rc: {save_path}")

    if only_auto_load:
        return

    type_link.write_text(rc)

    typer.echo("=" * 80)
    if not ln.lns:
        # Fake listener (http3) - no need to hold the terminal
        return

    with ln.listener():
        Event().wait()
