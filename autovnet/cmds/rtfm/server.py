import os
import sys
import shutil
import subprocess
import tempfile
from pathlib import Path
from typing import Optional

import typer

from ...autovnet import AutoVnet, avn
from autovnet.utils import (
    compose_up_ctx,
    compose_down,
    read_env_file,
    wait_tcp_open,
    as_port,
)

app = typer.Typer()


@app.command()
def up(
    detach: Optional[bool] = typer.Option(False, "-d", "--detach", help="Run docker-compose is the background"),
    env_file: Optional[str] = typer.Option(
        AutoVnet.config_path("rtfm.env"), "-e", "--env-file", help="Environment file to pass to docker-compose"
    ),
):
    """Start the core autovrtfm services

    The Red Team Force Multipler (RTFM) server contains:
        * autovrtfm.sshd (handles remote listeners)
        * autovrtfm.sockd (handles forward proxying)
        * autovrtfm.dns (TODO)
        * autovrtfm.ca (TODO)
    """
    avn.server_only()

    docker_path = avn.docker_path("server")
    env = read_env_file(env_file)
    env["AUTOVRTFM_ENV"] = env_file

    ssh_port = as_port(avn.config.rtfm_creds.ports.get("ssh", 2222))
    socks_port = as_port(avn.config.rtfm_creds.ports.get("socks", 1080))
    avn_host = avn.config.rtfm_creds.host

    typer.echo(f"[*] Starting autovnet server...")

    # In the context of a systemd service, autovnet isn't in our path
    autovnet_abs = sys.argv[0]

    # When restarting the server, it's probably a good idea to make sure there isn't any leftover sockets
    disconnect_cmd = [autovnet_abs, "rtfm", "mux-disconnect"]
    subprocess.run(disconnect_cmd)

    with compose_up_ctx(docker_path, detach=detach, env=env) as (compose_thread, compose_stop):
        procs = []

        # Wait for autovnet to be ready
        wait_tcp_open(avn_host, ssh_port, attempts=None)
        wait_tcp_open(avn_host, socks_port, attempts=None)

        if avn.config.rtfm_creds.reuse:
            connect_cmd = [autovnet_abs, "rtfm", "mux-connect"]
            subprocess.run(connect_cmd)

        cmd = [autovnet_abs, "rtfm", "tun", "server", "-e", "-E", "--verbose"]
        procs.append(subprocess.Popen(cmd))

        # Use --clean, because if the config changes, the database auth witl fail.
        # Plus, this is an easy way to purge all DNS records, if they get out of hand.
        cmd = [autovnet_abs, "rtfm", "dns", "server", "--clean"]
        procs.append(subprocess.Popen(cmd))

        if wait_tcp_open(avn.config.dns.host, avn.config.dns.port):
            cmd = [autovnet_abs, "rtfm", "dns", "register", "-d"]
            names = avn.config.dns.server_names or []
            for name in names:
                register = f"{name}:{avn.config.dns.host}"
                cmd.append(register)
            procs.append(subprocess.Popen(cmd))
        else:
            typer.echo(f"[!] Timeout waiting for DNS server")

        # Lie
        typer.echo(f"[+] autovnet server is ready!")

        try:
            compose_thread.join()
        finally:
            for proc in procs:
                if proc.poll() is None:
                    proc.terminate()
                    proc.wait()


@app.command()
def down(
    env_file: Optional[str] = typer.Option(
        AutoVnet.config_path("rtfm.env"), "-e", "--env-file", help="Environment file to pass to docker-compose"
    ),
):
    """Stop the core autovrtfm services"""
    avn.server_only()

    docker_path = avn.docker_path("server")
    env = read_env_file(env_file)
    env["AUTOVRTFM_ENV"] = env_file
    compose_down(docker_path, env=env)


@app.command()
def restart(
    detach: Optional[bool] = typer.Option(False, "-d", "--detach", help="Run docker-compose is the background"),
    env_file: Optional[str] = typer.Option(
        AutoVnet.config_path("rtfm.env"), "-e", "--env-file", help="Environment file to pass to docker-compose"
    ),
):
    """Restart the core autovrtfm services

    Calls down, then up
    """
    avn.server_only()

    try:
        down(env_file=env_file)
    except Exception as e:
        pass
    up(detach=detach, env_file=env_file)


@app.command()
def install(
    install_dir: Optional[str] = typer.Option(
        "/etc/systemd/system", "-d", "--install-dir", help="Directory to install systemd service"
    ),
):
    """Install a systemd service that runs the autovnet server"""
    avn.server_only()

    autovnet_abs = shutil.which("autovnet")

    if "USER" not in os.environ:
        raise Exception(f"[-] Env var USER not set!")
    user = os.environ["USER"]
    service = f"""
[Unit]
Description=Service to run the autovnet rtfm server
After=network.target

[Service]
Type=simple
User={user}
PermissionsStartOnly=true
Environment=PATH={os.environ["PATH"]}
"""
    for net in avn.config.networks:
        service += f"ExecStartPre=-ip route add local {net} dev lo"

    service += f"""
ExecStart={autovnet_abs} rtfm server restart
ExecStop={autovnet_abs} rtfm server down

[Install]
WantedBy=multi-user.target
"""
    out_dir = Path(install_dir)
    if not out_dir.is_dir():
        raise FileNotFoundError(f"[-] Install directory {install_dir} not found!")

    out_path = out_dir / "autovnet.service"

    # Race condition and possible privesc, but ez
    with tempfile.NamedTemporaryFile() as temp:
        Path(temp.name).write_text(service)
        subprocess.run(["sudo", "cp", str(temp.name), str(out_path)], check=True)

    subprocess.run(["sudo", "systemctl", "daemon-reload"], check=True)
    subprocess.run(["sudo", "systemctl", "enable", "autovnet"], check=True)
    subprocess.run(["sudo", "systemctl", "start", "autovnet"], check=True)


@app.command()
def uninstall(
    install_dir: Optional[str] = typer.Option(
        "/etc/systemd/system", "-d", "--install-dir", help="Directory used to install the systemd service"
    ),
):
    """Uninstall a previously installed systemd service"""

    avn.server_only()

    out_dir = Path(install_dir)
    if not out_dir.is_dir():
        raise FileNotFoundError(f"[-] Install directory {install_dir} not found!")

    out_path = out_dir / "autovnet.service"
    if not out_path.is_file():
        raise FileNotFoundError(f"[-] Service file {out_path} not found!")

    subprocess.run(["sudo", "systemctl", "disable", "autovnet"], check=True)
    subprocess.run(["sudo", "systemctl", "stop", "autovnet"], check=True)
    subprocess.run(["sudo", "rm", str(out_path)], check=True)
    subprocess.run(["sudo", "systemctl", "daemon-reload"], check=True)
