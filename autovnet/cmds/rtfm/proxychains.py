import os
import tempfile
from pathlib import Path
from typing import Optional, List

import typer

from ...autovnet import avn
from autovnet.utils import rand_ip_list

app = typer.Typer()


@app.command()
def config(
    networks: Optional[List[str]] = typer.Option(None, "-n", "--network", help="A network block to draw from."),
    count: Optional[int] = typer.Option(
        1, "-c", "--count", help="The number of unique IPs to generate. 0 returns all IPs"
    ),
    chain_len: Optional[int] = typer.Option(
        1, "-l", "--chain-len", help="The length of the chain to build. (For fun / stress testing, not more sneaky)"
    ),
    out: Optional[str] = typer.Option(None, "-o", "--out", help="Write config to out file."),
):
    """Generate a proxychains config you can edit

    Generally, you only need this if you need to do lower-level proxychains stuff that autovnet doesn't make easy yet
    """

    networks = avn.networks(networks)
    port = avn.config.rtfm_creds.ports["socks"]

    config = f"""
random_chain
chain_len = {chain_len}
proxy_dns
remote_dns_subnet 224
tcp_read_time_out 15000
tcp_connect_time_out 8000

[ProxyList]
"""
    for ip in rand_ip_list(networks, count, allow_zero=True, strict_count=False):
        config += f"socks5 {ip} {port} {avn.config.rtfm_creds.user} {avn.config.rtfm_creds.password}\n"

    if out:
        Path(out).write_text(config)
        # typer.echo(f"[+] Wrote config to {out}")
    else:
        typer.echo(config)


@app.command()
def run(
    networks: Optional[List[str]] = typer.Option(None, "-n", "--network", help="A network block to draw from."),
    count: Optional[int] = typer.Option(
        1, "-c", "--count", help="The number of unique IPs to generate. 0 returns all IPs"
    ),
    chain_len: Optional[int] = typer.Option(
        1, "-l", "--chain-len", help="The length of the chain to build. (For fun / stress testing, not more sneaky)"
    ),
    args: List[str] = typer.Argument(
        ..., help="Command to invoke under proxychains. (Use -- my -c ommand --with flags.)"
    ),
):
    """Run a command under proxychains4

    Assuming the command is compatible with proxychains4, the source IP of any TCP connections will be obfuscated by autovnet
    """
    networks = avn.networks(networks, category="proxy")
    with tempfile.NamedTemporaryFile() as temp_config:
        config(networks=networks, count=count, chain_len=chain_len, out=temp_config.name)
        cmd = ["proxychains4", "-q", "-f", temp_config.name]
        cmd.extend(args)

        # Do a real exec so underlying tools can handle CTRL+C / CTRL+Z
        os.execvp(cmd[0], cmd)
