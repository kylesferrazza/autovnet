import os
import gzip
import shutil
import shlex
import secrets
import subprocess
from pathlib import Path
from typing import Optional, List

import typer

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount
from autovnet.utils import (
    compose_up_ctx,
    complete_codename,
    read_env_file,
    rand_port,
    wait_url_open,
    wait_container_start,
    open_url,
    pg_dumper,
)

app = typer.Typer()

MOUNT_TYPE = "matrix"

MATRIX_HTTPS_PORT = 8448


@app.command()
def export(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).",
    ),
    dest_port: Optional[int] = typer.Option(
        MATRIX_HTTPS_PORT, "-p", "--dest-port", help="Local bind port for the server. (default: random)."
    ),
    no_browser: Optional[bool] = typer.Option(False, "-N", "--no-browser", help="If set, do not open element."),
    env_file: Optional[str] = typer.Option(
        avn.config_path("rtfm.env"),
        "-e",
        "--env-file",
        help="[ADVANCED] Environment file to pass to docker-compose",
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    init_sql: Optional[str] = typer.Option(
        None,
        "--init-sql",
        help="[ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.",
    ),
    force_init_sql: Optional[bool] = typer.Option(
        False,
        "--force-init-sql",
        help="[ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.",
    ),
    backup_interval: Optional[int] = typer.Option(
        300, "-i", "--backup-interval", help="Seconds between automatic backups (database exports)."
    ),
    backup_retain: Optional[int] = typer.Option(
        16,
        "-r",
        "--backup-retain",
        help="Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.",
    ),
    backup_quiet: Optional[bool] = typer.Option(
        False, "-Q", "--backup-quiet", help="If set, do not print backup confirmation messages."
    ),
    no_backup: Optional[bool] = typer.Option(
        False, "-B", "--no-backup", help="If set, do not make automated backups of the server contents."
    ),
    init_sql_sleep: Optional[int] = typer.Option(
        15,
        "--init-sql-sleep",
        help="[ADVANCED] Startup delay when using --init-sql.",
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load",
    ),
):
    """Run a self-hosted instance of matrix

    Matrix (https://matrix.org/) is a lot of things, but here we pretend it's
    a self-hosted chat server, similar to Slack, Discord, Mattermost, or Rocketchat.

    Matrix provides (techically, _is_) a set of nice APIs for chat servers to implement:
    (https://spec.matrix.org/latest/)

    Because the APIs are well defined, it's easy to interact with programatically with scripts or bots.
    * https://matrix.org/bridges/
    * https://matrix.org/sdks/

    """
    confirmed = True

    env = read_env_file(env_file)

    ctx = None
    new_svc = False

    if codename is None:
        # New svc
        ctx = lambda: avn.svc(
            networks=networks, dest_ip=dest_ip, dest_port=dest_port, payload_dir=payload_dir, echo=False
        )
        new_svc = True
    else:
        # Loaded svc
        ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False)
        # Peek at listener config to grab the values we need
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir)
        model = ln.lns[0]
        dest_ip = model.next_dest_ip
        dest_port = model.next_dest_port

        if init_sql is not None and not force_init_sql:
            # Confirm so that we don't accidentally delete a database if the user re-executes a command in their history
            confirmed = typer.confirm("[!] --init-sql provided but database may already exist! Import anyway?")
            if not confirmed:
                typer.echo("[-] Ignoring --init-sql")

    # Prepare compose
    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    proto = "https"

    matrix_bind = f"{dest_ip}:{dest_port}"
    url = f"{proto}://{matrix_bind}"

    tmp_env = {
        "MATRIX_BIND": matrix_bind,
        "INIT_DB_SH": str(avn.docker_path(f"plugins/{MOUNT_TYPE}/create_db.sh")),
    }

    with ctx() as (proc, codename, remote_addr):

        # Prepare codename directory, now that we know the codename
        data_dir = Path(payload_dir) / codename / "data"
        backups_dir = Path(payload_dir) / codename / "backups"
        matrix_dir = Path(payload_dir) / codename / "matrix"
        certs_dir = matrix_dir / "certs"
        config_dir = matrix_dir / "config"
        log_dir = matrix_dir / "log"
        media_dir = matrix_dir / "media"
        jetstream_dir = matrix_dir / "jetstream"

        for _dir in [data_dir, backups_dir, matrix_dir, certs_dir, config_dir, log_dir, media_dir, jetstream_dir]:
            _dir.mkdir(parents=True, exist_ok=True)

        # Certs get generated by the entrypoint, if needed
        # envsubst is also done in the container
        shutil.copyfile(avn.docker_path(f"plugins/{MOUNT_TYPE}/dendrite.yaml"), config_dir / "dendrite.yaml.template")

        tmp_env["MATRIX_DATA"] = str(matrix_dir)
        tmp_env["MATRIX_POSTGRES_DATA"] = str(data_dir)
        if init_sql is not None and confirmed:
            env["INIT_SQL_SLEEP"] = str(init_sql_sleep)
        else:
            env["INIT_SQL_SLEEP"] = "0"

        tmp_env.update(env)
        env = tmp_env
        env["UID"] = str(os.geteuid())
        env["GID"] = str(os.getegid())

        project = f"{MOUNT_TYPE}_{codename}"
        mount_cmd = f"autovnet rtfm {MOUNT_TYPE} mount -C {codename} {remote_addr}"
        if dest_port != MATRIX_HTTPS_PORT:
            mount_cmd += f" -p {MATRIX_HTTPS_PORT}"
        export_cmd = f"autovnet rtfm {MOUNT_TYPE} export {codename}"

        typer.echo(f"[+] {export_cmd}")
        typer.echo(f"[+] connect : .element")
        typer.echo(f"[+] mount   : {mount_cmd}")

        with compose_up_ctx(docker_path, env=env, project=project, quiet=True) as (
            compose_thread,
            compose_stop,
        ):
            # Leverage the fact we can pre-compute the database's fully qualified, unique container name
            db_container = f"{MOUNT_TYPE}_{codename}_db_1"
            wait_container_start(db_container)

            if init_sql is not None and confirmed:
                # User confirmed they want to do this
                tmp_user = f"tmp_{secrets.token_hex(2)}"
                db_cmd = ["docker", "exec", "-i", db_container, "psql", "-U", "dendrite", "postgres"]

                # Because we had to use pg_dumpall, we have to work around "can't drop current user"
                # So, we create a tmp user to do the restore
                db_proc = subprocess.run(
                    db_cmd,
                    input=f"CREATE USER {tmp_user} LOGIN SUPERUSER".encode(),
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                )

                db_cmd = ["docker", "exec", "-i", db_container, "psql", "-U", tmp_user, "postgres"]
                typer.echo(f"[+] Importing {init_sql} ...")
                db_proc = subprocess.Popen(
                    db_cmd, stdin=subprocess.PIPE, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
                )

                file_ctx = None
                if init_sql.endswith(".gz"):
                    file_ctx = lambda: gzip.GzipFile(init_sql, "rb")
                else:
                    file_ctx = lambda: open(init_sql, "rb")

                with file_ctx() as in_file:
                    shutil.copyfileobj(in_file, db_proc.stdin)
                db_proc.stdin.close()
                db_proc.wait()
                if db_proc.returncode == 0:
                    typer.echo(f"[+] Imported {init_sql}")
                else:
                    typer.echo(f"[!] Import {init_sql} failed: '{shlex.join(db_cmd)}' returned {db_proc.returncode}")

                db_cmd = ["docker", "exec", "-i", db_container, "psql", "-U", "dendrite"]
                db_proc = subprocess.run(
                    db_cmd,
                    input=f"DROP USER {tmp_user};".encode(),
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                )

            typer.echo(f"[*] Waiting for {MOUNT_TYPE} ...")
            ok = wait_url_open(url, status_code=404, thread=compose_thread, proc=proc)
            if ok:
                typer.echo(f"[+] {MOUNT_TYPE} is ready!")
            elif ok is False:
                typer.echo(f"[!] {MOUNT_TYPE} may still be initializing")

            if not no_browser:
                _start_element(url)

            if no_backup:
                compose_thread.join()
            else:
                backup_cmd = [
                    "docker",
                    "exec",
                    db_container,
                    "pg_dumpall",
                    "-U",
                    "dendrite",
                    "--clean",
                    "--if-exists",
                ]
                with pg_dumper(
                    backup_cmd, backups_dir, interval=backup_interval, retain=backup_retain, echo=(not backup_quiet)
                ) as (thr, stop):
                    # Dumper thread has already started
                    try:
                        compose_thread.join()
                    finally:
                        # Compose has exited, stop and join the background thread
                        stop.set()


@app.command()
def mount(
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!",
    ),
    dest_port: Optional[int] = typer.Option(
        MATRIX_HTTPS_PORT,
        "-p",
        "--dest-port",
        help="[ADVANCED] Destination port you will use to access the service.",
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: Optional[str] = typer.Option(None, "-C", "--set-codename", help="Set the codename for the mount."),
    opt: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="IP:PORT or codename to mount.",
    ),
):
    """Mount and connect to another player's matrix service"""

    with avn.svc_mount(
        codename=codename,
        opt=opt,
        dest_ip=dest_ip,
        dest_port=dest_port,
        payload_dir=payload_dir,
        mount_type=MOUNT_TYPE,
        wait_tcp=True,
    ) as proc:
        typer.echo(f"[+] connect: .element")
        if not no_browser:
            _start_element(f"https://{dest_ip}:{dest_port}")
        proc.wait()


def _start_element(url):
    element_config = Path("~/.config/Element/config.json").expanduser()
    if not element_config.is_file():
        element_config.parent.mkdir(parents=True, exist_ok=True)
        config_text = avn.docker_path(f"plugins/{MOUNT_TYPE}/element.json").read_text().replace("{{MATRIX_URL}}", url)
        element_config.write_text(config_text)
    subprocess.run(".element")
