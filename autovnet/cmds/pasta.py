import os
import shutil
import subprocess
from pathlib import Path
from typing import Optional, List

from jinja2 import Environment, FileSystemLoader, meta
import typer

from autovnet.utils import (
    rand_ip,
    rand_port,
    gen_codename,
    read_env_file,
    shell_exec,
)
from ..autovnet import AutoVnet, avn

app = typer.Typer()

LISTEN_NETWORKS = None
PROXY_NETWORKS = None
TUN_NETWORKS = None


@app.command()
def serve(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("tmp"), "-D", "--payload-dir", help="Directory to save temporary pasta."
    ),
    site_path: Optional[str] = typer.Option(
        None, "-o", "--site-path", help="[ADVANCED] Final directory for saving rendered pasta."
    ),
    debug: Optional[bool] = typer.Option(
        False,
        "-d",
        "--debug",
        help="Enable more debug output.",
    ),
    no_jinja: Optional[bool] = typer.Option(
        False,
        "-J",
        "--no_jinja",
        help="Skip jinja rendering.",
    ),
    only_build: Optional[bool] = typer.Option(
        False,
        "-B",
        "--only_build",
        help="[ADVANCED] Only build, do not serve.",
    ),
    pasta_path: Optional[str] = typer.Argument(
        ...,
        help="Pasta directory to render",
    ),
):
    """Render a directory of (copy)pasta.

    It must be in autovnet's pasta format.

    WARNING: Do no render pasta from untrusted sources.
    This provides shell_exec as a feature, and is not secure against malicious input.

    Do not run this command in a CI / CD pipeline that untrusted users can trigger.

    (Be nice.)
    """
    global LISTEN_NETWORKS, PROXY_NETWORKS, TUN_NETWORKS
    if not no_jinja:
        LISTEN_NETWORKS = avn.networks(networks, category="listen")
        PROXY_NETWORKS = avn.networks(networks, category="proxy")
        TUN_NETWORKS = avn.networks(networks, category="tun")

    pasta_path = Path(pasta_path).resolve()
    if not pasta_path.is_dir():
        raise FileNotFoundError(f"[-] {pasta_path} not found")
    mkdocs_path = pasta_path / "mkdocs.yml"
    if not mkdocs_path.is_file():
        raise FileNotFoundError(f"[-] {mkdocs_path} not found")

    tmp_path = Path(payload_dir) / gen_codename()
    if site_path is None:
        site_path = f"{tmp_path}.pasta"
    site_path = Path(site_path)
    # Error out if we somehow conflict
    tmp_path.mkdir(parents=True, exist_ok=True)
    site_path.mkdir(parents=True, exist_ok=True)
    shutil.copytree(pasta_path, tmp_path, dirs_exist_ok=True)
    try:
        if not no_jinja:
            # WARNING: if this is wrong, it throws super unhelpful TemplateNotFound errors
            # Use AUTOVNET_STACK_TRACE=1 to see them
            env = Environment(loader=FileSystemLoader(tmp_path))
            env.globals.update(shell_exec=shell_exec)

            render_dir(env, tmp_path, tmp_path, debug=debug)

        cmd = None
        if only_build:
            cmd = ["autovnet", "docs", "build", "--force", "--src-dir", str(tmp_path), str(site_path)]
        else:
            cmd = [
                "autovnet",
                "docs",
                "serve",
                "--build",
                "-p",
                str(rand_port()),
                "--src-dir",
                str(tmp_path),
                str(site_path),
            ]
        subprocess.run(cmd)
    finally:
        shutil.rmtree(tmp_path)
        if only_build:
            typer.echo(f"[+] Rendered {site_path}")
        else:
            shutil.rmtree(site_path)


###############
# Helpers
###############

# TODO: refactor into helper class
def render_dir(
    env, base_dir, current_dir, pasta_vars=None, debug=False, ignore_prefix=["."], ignore_suffix=[".swp", ".env"]
):

    if pasta_vars is None:
        pasta_vars = {}

    pasta_vars_bk = pasta_vars

    # Recurse into all directories (depth first)
    if debug:
        typer.echo(f"\n[*] Processing {current_dir}")
    for d in current_dir.iterdir():
        if not d.is_dir():
            continue

        if any(d.name.startswith(s) for s in ignore_prefix) or any(d.name.endswith(s) for s in ignore_suffix):
            # Allow ignoring special directories, like .git
            if debug:
                typer.echo(f"[!] Ignoring dir {d}")
            continue

        if (d / ".pastaignore").is_file():
            if debug:
                typer.echo(f"[!] Ignoring dir {d}")
            continue

        # Always start with the pasta_vars we got from our caller
        pasta_vars = pasta_vars_bk.copy()
        pasta_env_path = d / "pasta.env"
        pasta_env_path_rel = pasta_env_path.relative_to(base_dir)

        # Round 1: load defaults from pasta.env, if it exists
        # Try to read vars from pasta.env in the same dir as the file

        # Because we always attempt this first, and pass the same pasta_vars
        # to mutliple calls to render_file, this should mean that variables set in
        # pasta_vars are availble to all files within a directory.

        # It may behave strangely with subdirectories and peer directories.
        # (e.g. it may be possible to define a variable in bar/pasta.env and reference it in foo/README.md,
        # if the foo directory is visited after the bar directory by list_templates)
        # Use caution if relying on ordering side effects.
        if pasta_env_path.is_file():
            if debug:
                typer.echo(f"[*] Rendering pasta_vars ({pasta_env_path})")
            # Render the .env as jinja, so it can shell_exec if needed
            render_file(env, pasta_vars, base_dir, pasta_env_path, debug=debug)
            _pasta_vars = read_env_file(pasta_env_path)
            pasta_vars.update(_pasta_vars)

        # pasta_vars is now set to the correct "defaults" for current_dir an all subdirs
        # subdirs can override some values, so recurse with a copy of the dict
        render_dir(
            env,
            base_dir,
            d,
            pasta_vars=pasta_vars.copy(),
            debug=debug,
            ignore_prefix=ignore_prefix,
            ignore_suffix=ignore_suffix,
        )

    # Render all files in this directory
    if debug:
        typer.echo(f"\n[*] Rendering files in {current_dir}")
    for f in current_dir.iterdir():
        if not f.is_file():
            continue
        if any(f.name.startswith(s) for s in ignore_prefix) or any(f.name.endswith(s) for s in ignore_suffix):
            # Ignore any binary files that can't be rendered
            if debug:
                typer.echo(f"[!] Ignoring file {f}")
            continue

        render_file(env, pasta_vars, base_dir, f, debug=debug)


def render_file(env, pasta_vars, base_dir, current, debug=False):

    current_dir = current.parent
    if pasta_vars is None:
        pasta_vars = {}

    # Round 2: try to load overrides from foo.md.env
    pasta_env_path = Path(f"{current}.env")
    pasta_env_path_rel = pasta_env_path.relative_to(base_dir)

    if pasta_env_path.is_file():
        if debug:
            typer.echo(f"[*] Rendering pasta_vars ({pasta_env_path})")
        render_file(env, pasta_vars, base_dir, pasta_env_path)
        _pasta_vars = read_env_file(pasta_env_path)
        pasta_vars.update(_pasta_vars)

    # Copy here to prevent magic from polluting pasta_vars
    v = pasta_vars.copy()

    current_rel = str(current.relative_to(base_dir))
    src = env.loader.get_source(env, current_rel)
    parsed = env.parse(src)
    undefined = meta.find_undeclared_variables(parsed)
    found_undefined = False
    for u in undefined:
        # Some magic prefixes / variables are handled here
        if u not in v:
            if u.startswith("LHOST"):
                v[u] = str(rand_ip(LISTEN_NETWORKS))
            elif u.startswith("LPORT"):
                # If you don't want LPORT to be random, don't use the LPORT placeholder
                v[u] = rand_port()
            elif u.startswith("PHOST"):
                v[u] = str(rand_ip(PROXY_NETWORKS))
            elif u.startswith("THOST"):
                v[u] = str(rand_ip(TUN_NETWORKS))
            elif u.startswith("CODENAME"):
                v[u] = gen_codename()
            elif u == "CURRENT":
                v[u] = str(current)
            elif u == "CURRENT_DIR":
                v[u] = str(current_dir)
            else:
                typer.echo(f"[!] {current}: {u} not set")
                found_undefined = True

    if found_undefined:
        raise ValueError(
            f"[-] {current} contains undefined jinja variables. Set them in {pasta_env_path} or in pasta.env ."
        )

    template = env.get_template(current_rel)
    if debug:
        typer.echo(f"[*] Rendering {current_rel}")
    rendered = template.render(v)
    Path(template.filename).write_text(rendered)
