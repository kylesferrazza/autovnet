import os
import shutil
import subprocess
from pathlib import Path
from typing import Optional

import typer

from ..autovnet import avn
from autovnet.utils import (
    gen_codename,
    compose_up_ctx,
    wait_url_open,
    open_url,
)

app = typer.Typer()

MOUNT_TYPE = "docs"
DEFAULT_DOCS_PORT = 4280
DEFAULT_DOCS_DIR = "/opt/rtfm/docs"


@app.command()
def build(
    force: Optional[bool] = typer.Option(
        False,
        "-f",
        "--force",
        help="Delete the docs dir and rebuild it.",
    ),
    src_dir: Optional[str] = typer.Option(
        None,
        "-s",
        "--src-dir",
        help="Source dir to render with mkdocs.",
    ),
    docs_dir: Optional[str] = typer.Argument(
        DEFAULT_DOCS_DIR,
        help="Directory to save static site files.",
    ),
):
    """Create the sacred texts

    Build the offline autovnet documents
    """
    docs_dir = Path(docs_dir).resolve()

    if docs_dir.is_dir():
        if force:
            shutil.rmtree(docs_dir)
        else:
            typer.echo(f"[-] {docs_dir} already exists! Use --force to force.")
            raise typer.Exit(1)

    cmd = ["mkdocs", "build", "--strict", "-d", str(docs_dir)]

    if src_dir is None:
        src_dir = avn.docs_path()

    if not Path(src_dir).is_dir():
        raise FileNotFoundError(f"[-] src_dir {src_dir} not found!")

    subprocess.run(cmd, cwd=src_dir, check=True)
    serve_cmd = "autovnet docs serve"

    if docs_dir != Path(DEFAULT_DOCS_DIR):
        serve_cmd += f" {docs_dir}"

    typer.echo(f"[+] serve: {serve_cmd}")


@app.command()
def serve(
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="Bind IP for webserver.",
    ),
    dest_port: Optional[int] = typer.Option(
        DEFAULT_DOCS_PORT,
        "-p",
        "--dest-port",
        help="Bind IP for webserver.",
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    build: Optional[bool] = typer.Option(False, "-b", "--build", help="If set, force a rebuild first."),
    src_dir: Optional[str] = typer.Option(
        None,
        "-s",
        "--src-dir",
        help="Source dir to render with mkdocs.",
    ),
    docs_dir: Optional[str] = typer.Argument(
        "/opt/rtfm/docs",
        help="Directory to serve.",
    ),
):
    """Read the sacred texts

    View the offline autovnet documents (building if needed)
    """
    docs_dir = Path(docs_dir).resolve()

    if not docs_dir.is_dir():
        typer.echo(f"[+] Creating {docs_dir}")
        docs_dir.mkdir(parents=True, exist_ok=True)
        build = True

    if build:
        cmd = ["autovnet", "docs", "build", "--force", str(docs_dir)]
        if src_dir is not None:
            cmd.extend(["--src-dir", str(src_dir)])
        subprocess.run(cmd, check=True)

    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    proto = "http"
    docs_bind = f"{dest_ip}:{dest_port}"
    url = f"{proto}://{docs_bind}"
    env = {
        "DOCS_BIND": docs_bind,
        "DOCS_DIR": str(docs_dir),
        "UID": str(os.geteuid()),
        "GID": str(os.getegid()),
    }
    codename = gen_codename()
    project = f"{MOUNT_TYPE}_{codename}"
    with compose_up_ctx(docker_path, env=env, project=project, quiet=True) as (
        compose_thread,
        compose_stop,
    ):
        typer.echo(f"[*] Waiting for {MOUNT_TYPE} ({url}) ...")
        ok = wait_url_open(url, thread=compose_thread)
        if ok:
            typer.echo(f"[+] {MOUNT_TYPE} are ready!")
        elif ok is False:
            typer.echo(f"[!] {MOUNT_TYPE} may still be initializing")

        if not no_browser:
            open_url(url)

        compose_thread.join()

    subprocess.run(["docker", "network", "ls"])
