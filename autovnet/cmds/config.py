import io
import json
import shutil
import secrets
import tarfile
import tempfile
import subprocess
from getpass import getpass
from pathlib import Path
from typing import Optional, List

import yaml

import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

import typer

from ..autovnet import AutoVnet, avn
from autovnet.utils import (
    rand_ip,
    get_ip,
    is_url,
    yaml_dump,
    gen_password,
    gen_codename,
    encrypt_data,
    decrypt_data,
    gen_cert,
    tar_extract,
)
from autovnet.models import Config, Creds, Tun

DEFAULT_AVN_NETWORK = "240.0.0.0/4"
app = typer.Typer()


@app.command()
def init(
    rtfm_user: Optional[str] = typer.Option("x", "-u", "--rtfm-user", help="SSH user for port forwards."),
    rtfm_host: Optional[str] = typer.Option(None, "-s", "--rtfm-host", help="SSH host for port forwards."),
    rtfm_key: Optional[str] = typer.Option(
        AutoVnet.config_path("id_rsa", expand_home=False), "-k", "--rtfm-key", help="SSH key for port forwards."
    ),
    rtfm_pubkey: Optional[str] = typer.Option(
        AutoVnet.config_path("id_rsa.pub", expand_home=False),
        "-a",
        "--rtfm-pubkey",
        help="SSH pubkey for port forwards.",
    ),
    ssh_port: Optional[str] = typer.Option(2222, "-p", "--ssh-port", help="Port for ssh server."),
    socks_port: Optional[str] = typer.Option(1080, "-P", "--socks-port", help="Port for socks server."),
    rtfm_password: Optional[str] = typer.Option(None, "-q", "--password", help="Password for socks5 proxy."),
    networks: Optional[List[str]] = typer.Option(
        [DEFAULT_AVN_NETWORK], "-n", "--network", help="A network block to simulate."
    ),
    out_dir: Optional[str] = typer.Option(
        AutoVnet.config_path(""), "-o", "--out", help="Initalize the config directory at an alternate path."
    ),
    password_bytes: Optional[int] = typer.Option(8, "-l", "--length", help="Length of generated password(s) in bytes."),
    force: Optional[bool] = typer.Option(
        False, "-f", "--force", help="Delete the output config directory if it already exists."
    ),
):
    """Intialize the autovnet config

    Run once on the autovnet server, then export the config for clients to import
    """
    if Path(out_dir).is_dir():
        if not force:
            raise Exception(f"[-] Config directory {out_dir} already exists! (Use -f to force)")
        shutil.rmtree(out_dir)
        typer.echo(f"[*] Deleted old config directory {out_dir}")

    # Ensure directory structure
    Path(out_dir).mkdir(parents=True, exist_ok=True)

    # Generate key if missing
    rtfm_key_abs = Path(rtfm_key).expanduser()
    if not rtfm_key_abs.is_file():
        typer.echo(f"[+] Generating {rtfm_key_abs}")
        subprocess.run(["ssh-keygen", "-C", "", "-P", "", "-f", rtfm_key_abs], check=True)

    # Default host: guess
    if rtfm_host is None:
        rtfm_host = get_ip()

    # Default password: random
    if rtfm_password is None:
        rtfm_password = secrets.token_urlsafe(password_bytes)

    ports = {
        "ssh": ssh_port,
        "socks": socks_port,
    }
    # Build config
    rtfm_creds = Creds(
        user=rtfm_user,
        key=rtfm_key,
        pubkey=rtfm_pubkey,
        password=rtfm_password,
        host=rtfm_host,
        ports=ports,
    )
    # TODO: Could make these options settable
    tun = Tun(
        host=rtfm_host,
    )
    config = Config(
        rtfm_creds=rtfm_creds,
        networks=networks,
        server_mode=True,
        tun=tun,
    )
    rtfm_env = "\n".join(
        [
            f"AUTOVRTFM_PASSWORD={rtfm_password}",
            f"AUTOVRTFM_PUBKEY={rtfm_pubkey}",
            f"AUTOVRTFM_SSH_PORT={ssh_port}",
            f"AUTOVRTFM_SOCKS_PORT={socks_port}",
            # These are technically experimental, so this might be a bad idea
            "COMPOSE_DOCKER_CLI_BUILD=1",
            "DOCKER_BUILDKIT=1",
            "",
        ]
    )

    (Path(out_dir) / "rtfm.env").write_text(rtfm_env)

    cert_path = Path(out_dir) / "rtfm.cert"
    key_path = Path(out_dir) / "rtfm.key"
    gen_cert(cert_path, key_path)

    config_str = yaml_dump(config.dict())
    (Path(out_dir) / "config.yaml").write_text(config_str)

    typer.echo(f"[+] Created {out_dir}")

    # Generate APT files, if missing
    path = Path(avn.config_path("apt/templates.yaml"))
    if not path.is_file():
        typer.echo(f"[+] Generating APT files")
        out = subprocess.DEVNULL
        subprocess.run(["autovnet", "apt", "init"], stdout=out, stderr=out, check=True)

    typer.echo(f"[+] Initialized config")


@app.command()
def show():
    """Display the current autovnet config"""

    if avn.config is None:
        if not Path(avn.config_file).is_file():
            raise FileNotFoundError(f"[-] Config file {avn.config_file} not found")
        else:
            raise Exception(f"[-] Failed to load config from {avn.config_file}")

    config_str = yaml_dump(avn.config.dict())
    typer.echo(f"# {avn.config_file}")
    typer.echo(config_str)


@app.command()
def export(
    in_dir: Optional[str] = typer.Option(avn.config_path(""), "-i", "--in", help="Path to the config directory."),
    secure: Optional[bool] = typer.Option(False, "-s", "--secure", help="Use a secure (but hard to type) password."),
    share: Optional[bool] = typer.Option(
        False,
        "-S",
        "--share",
        help="Share the config file after exporting. (The first share is slow to start, be patient.)",
    ),
    out: Optional[str] = typer.Option(
        "autovnet-config.etgz", "-o", "--out", help="Path to store the encrypted config blob."
    ),
):
    """Export the current autovnet config

    The result is an encrypted blob that can be imported by an autovnet client
    """
    if not Path(in_dir).is_dir():
        raise FileNotFoundError(f"[-] Config directory {in_dir} not found")

    f = io.BytesIO()
    with tarfile.open(fileobj=f, mode="w:gz") as tar:
        tar.add(in_dir, arcname=Path(in_dir).name)

    password = gen_password(secure=secure)
    encrypted = encrypt_data(f.getvalue(), password)
    Path(out).write_bytes(encrypted)
    typer.echo(f"[+] Exported {out} | Password: {password}")
    if share:
        ip = str(avn.rand_ip())
        url = f"https://{ip}/{out}"
        typer.echo(f"[+] import: autovnet config import {url}")

        typer.echo(f"\n[*] Starting the share (please wait)...")
        # This is bad
        # https://github.com/tiangolo/typer/issues/106
        cmd = ["autovnet", "rtfm", "share", "-n", ip, out]
        subprocess.run(cmd, check=True)


@app.command("import")
def do_import(
    out: Optional[str] = typer.Option(
        str(Path(avn.config_path("")).parent), "-o", "--out", help="Parent directory to unpack the config directory in."
    ),
    force: Optional[bool] = typer.Option(
        False, "-f", "--force", help="Delete the output config directory if it already exists."
    ),
    in_path: Optional[str] = typer.Argument("autovnet-config.etgz", help="Path to the config blob. Can be a url."),
):
    """Import an autovnet config from the autovnet server

    Generally, the game administrator running the autovnet server will provide the command you should run.
    """

    # Reconstruct the final path based on naming convention
    save_path = Path(out) / Path(avn.config_path("")).name

    encrypted = b""
    if is_url(in_path):
        resp = requests.get(in_path, verify=False)
        resp.raise_for_status()
        encrypted = resp.content
    else:
        in_path = Path(in_path)
        if not in_path.is_file():
            raise FileNotFoundError(f"[-] Config blob {in_path} not found")

        if not Path(out).is_dir():
            raise FileNotFoundError(f"[-] Parent directory {out} not found")

        encrypted = Path(in_path).read_bytes()

    if Path(save_path).is_dir():
        if not force:
            raise Exception(f"[-] Config directory {save_path} already exists! (Use -f to force)")
        shutil.rmtree(save_path)
        typer.echo(f"[*] Deleted old config directory {save_path}")
    password = getpass("Config Password: ")
    decrypted = decrypt_data(encrypted, password)

    f = io.BytesIO(decrypted)
    typer.echo("[*] Unpacking config...")
    with tarfile.open(fileobj=f) as tar_obj:
        tar_extract(tar_obj, out)

    if not Path(save_path).is_dir():
        raise FileNotFoundError(f"[-] Extraction failed: {save_path} not found")

    # Validate and turn off server mode
    config_path = Path(save_path) / "config.yaml"
    config_dict = yaml.safe_load(config_path.read_text())
    cfg = Config.parse_obj(config_dict)
    cfg.server_mode = False
    if cfg.player is None:
        cfg.player = gen_codename()
    config_path.write_text(yaml_dump(cfg.dict()))

    # Fix permission on private keys
    id_rsa = Path(save_path) / "id_rsa"
    id_rsa.chmod(0o600)

    typer.echo(f"[+] Imported config: {save_path}")


@app.command()
def docker(
    install: Optional[bool] = typer.Option(
        False, "-i", "--install", help="Install the daemon.json, unless it already exists."
    ),
    force: Optional[bool] = typer.Option(
        False, "-f", "--force", help="If installing, overwrite any existing daemon.json."
    ),
):
    """Suggest a reasonable docker daemon configuration

    With now options, displays a suggested config that you can tailor for your environment.
    To install it, use the --install flag, or manually place the config in `/etc/docker/daemon.json`
    and restart the docker service.

    If you already have a daemon.json, you probably need to manually merge the configs, or you may
    temporarily break your docker install.

    This configuration will prevent you from running more than ~16 containers in the same network
    namespace.

    This matters to almost no one, but if you have some crazy app with tons of containers,
    it's up to you to modify the config as appropriate for your use case.

    """

    # If you are using autovnet, you are almost certainly creating many, tiny networks
    # But docker-compose will default to allocating gigantic networks, making eventual
    # IP conflicts more likely

    # For max compatibility and least surprises, use the primary default network block
    # that docker uses (https://github.com/docker/docker.github.io/issues/8663)
    # but just change the allocation sizes to be much more reasonably sized
    # If you do need more than
    config = {
        "default-address-pools": [
            {
                "base": "172.17.0.0/12",
                "size": 28,
            }
        ]
    }

    config_json = json.dumps(config, indent=2)

    daemon_path = Path("/etc/docker/daemon.json")
    if not install:
        typer.echo(config_json)
        typer.echo(f"\n[+] Install the above config at {daemon_path}, or use --install")
        return

    # install
    if daemon_path.is_file():
        typer.echo(f"[!] {daemon_path} already exists!")
        if not force:
            return
        backup_path = Path(f"{daemon_path}.bak")
        if not backup_path.is_file():
            subprocess.run(["sudo", "cp", str(daemon_path), str(backup_path)], check=True)
            typer.echo(f"[+] Saved original config as {backup_path}")

    # Race condition and possible privesc, but ez
    with tempfile.NamedTemporaryFile() as temp:
        Path(temp.name).write_text(config_json)
        subprocess.run(["sudo", "cp", str(temp.name), str(daemon_path)], check=True)

    try:
        subprocess.run(["sudo", "systemctl", "restart", "docker"], check=True)
    except Exception as e:
        typer.echo(
            f"[-] docker failed to start! You should remove, restore, or fix {daemon_path} and 'sudo systemctl restart docker'"
        )
