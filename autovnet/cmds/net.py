import time
import socket
import secrets
import subprocess
from ipaddress import IPv4Network
from pathlib import Path
from typing import Optional, List

import yaml
import typer
import pyfiglet

from ..autovnet import avn
from autovnet.utils import get_ip, ip_gen, rand_port, net_list, rand_ip_list, format_url
from autovnet.models import Config

app = typer.Typer()


@app.command()
def up(
    networks: Optional[List[str]] = typer.Option(None, "-n", "--network", help="A network block to simulate."),
):
    """Start simulating a network"""
    avn.server_only()

    networks = avn.networks(networks)
    typer.echo(f"[+] Simulating: {net_list(networks)}")

    for net in networks:
        subprocess.run(["sudo", "ip", "route", "add", "local", str(net), "dev", "lo"], check=True)

    typer.echo(f"[+] Success. Stop simluation with: 'autovnet net down'")


@app.command()
def down(
    networks: Optional[List[str]] = typer.Option(None, "-n", "--network", help="A network block to simulate."),
):
    """Stop simulating a network"""
    avn.server_only()

    networks = avn.networks(networks)

    for net in networks:
        subprocess.run(["sudo", "ip", "route", "del", "local", str(net), "dev", "lo"], check=True)
    typer.echo(f"[+] Stopped simulating: {net_list(networks)}")


@app.command()
def ip(
    networks: Optional[List[str]] = typer.Option(None, "-n", "--network", help="A network block to draw from."),
    count: Optional[int] = typer.Option(
        1, "-c", "--count", help="The number of IPs to generate. 0 returns all (or infinite) IPs."
    ),
    unique: Optional[bool] = typer.Option(False, "-u", "--unique", help="Do not repeat IP addresses."),
    port: Optional[str] = typer.Option(None, "-p", "--port", help="Port for url"),
    proto: Optional[str] = typer.Option(None, "-P", "--proto", help="Protocol prefix for url"),
    path: Optional[str] = typer.Option(None, "-q", "--path", help="Path suffix for url"),
    auth: Optional[str] = typer.Option(None, "-a", "--auth", help="user[:password] string for url"),
    category: Optional[str] = typer.Option(
        None,
        "-C",
        "--category",
        help="Category of intrastructure to draw the IP from. Only relevant if using an APT profile.",
    ),
):
    """Generate randomzied IP addresses"""
    networks = avn.networks(networks, category=category)
    if unique:
        # Much less efficient
        for ip in rand_ip_list(networks, count, allow_zero=True):
            url = format_url(ip, port=port, proto=proto, path=path, auth=auth)
            typer.echo(url)
    else:
        # Super efficient
        for ip in ip_gen(networks, count=count):
            url = format_url(ip, port=port, proto=proto, path=path, auth=auth)
            typer.echo(url)


@app.command()
def socks(
    count: Optional[int] = typer.Option(
        1, "-c", "--count", help="The number of unique IPs to generate. 0 returns all IPs."
    ),
    unique: Optional[bool] = typer.Option(False, "-u", "--unique", help="Do not repeat IP addresses."),
):
    """Generate socks urls for rtfm server"""

    networks = avn.networks(None)
    auth = f"{avn.config.rtfm_creds.user}:{avn.config.rtfm_creds.password}"
    port = avn.config.rtfm_creds.ports["socks"]
    if unique:
        # Much less efficient
        for ip in rand_ip_list(networks, count, allow_zero=True):
            url = format_url(ip, port=port, proto="socks5", auth=auth)
            typer.echo(url)
    else:
        # Super efficient
        for ip in ip_gen(networks, count=count):
            url = format_url(ip, port=port, proto="socks5", auth=auth)
            typer.echo(url)


@app.command()
def test(
    networks: Optional[List[str]] = typer.Option(None, "-n", "--network", help="A network block to draw from."),
    count: Optional[int] = typer.Option(16, "-c", "--count", help="The number of tests to run."),
    size: Optional[int] = typer.Option(1, "-k", "--size", help="The number of IPs to allocate for each test."),
    remote_port: Optional[int] = typer.Option(443, "-p", "--port", help="The remote port to test."),
    timeout: Optional[int] = typer.Option(5, "-t", "--timeout", help="Timeout for socket operations."),
    streams: Optional[int] = typer.Option(
        1, "-s", "--streams", help="Number of sequential streams to create for each test."
    ),
    data_len: Optional[int] = typer.Option(
        256, "-d", "--data", max=1024 * 1024, help="Amount of data to send per stream."
    ),
    no_deconflict: Optional[bool] = typer.Option(
        None,
        "-D",
        "--no-deconflict",
        help="Disable IP deconfliction. Much faster test, but will fail if a conflict happens.",
    ),
):
    """Test autovnet network connectivity"""

    dest_ip = "127.1.33.7"
    if count <= 0:
        raise ValueError("[-] Count must be positive")
    art = pyfiglet.figlet_format("autovnet", font="cyberlarge")
    typer.echo(art)

    if no_deconflict is None:
        # Servers experience random timeouts when using deconfliction :shrug:
        no_deconflict = avn.is_server()
    deconflict = not no_deconflict

    timeout = 5
    with typer.progressbar(range(count), label="Hacking the planet...") as progress:
        for value in progress:
            attempts = 10
            while attempts > 0:
                try:
                    ln = avn.listener(networks, [remote_port] * size, dest_ip, echo=False, deconflict=deconflict)
                    with ln.listener():
                        for model in ln.lns:
                            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
                                server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                                server.bind((dest_ip, model.dest_port))
                                server.listen()
                                attempts = 50
                                client = None

                                for i in range(streams):
                                    while client is None:
                                        try:
                                            client = socket.create_connection((model.remote_ip, model.remote_port))
                                        except Exception as e:
                                            if attempts <= 0:
                                                raise
                                            attempts -= 1
                                            time.sleep(0.05)
                                    client.settimeout(timeout)
                                    server.settimeout(timeout)
                                    data = secrets.token_bytes(nbytes=data_len)
                                    client.sendall(data)
                                    s, peer = server.accept()
                                    received = b""
                                    while True:
                                        rem = data_len - len(received)
                                        if rem <= 0:
                                            break
                                        read = s.recv(rem)
                                        if not read:
                                            break
                                        received += read
                                    if data != received:
                                        raise Exception(f"[-] Data integrity check failed!")
                                    client.close()
                                    client = None
                    break
                except socket.timeout:
                    # Problably race condition on conflicted IP, retry
                    attempts -= 1
                    typer.echo("[!] socket timeout", err=True)

    typer.echo(f"[+] OK!")
