import os
import sys
import time
import signal
import shlex
import subprocess
import multiprocessing
from threading import Event
from pathlib import Path
from contextlib import contextmanager
from typing import List

import typer

from autovnet.models import Listener
from autovnet.utils import gen_codename, yaml_dump


class ListenProc:
    def __init__(self, cancel=None):
        self.cancel = cancel
        self.done = False

    def wait(self, timeout=None):
        if timeout is not None:
            raise ValueError(f"[!] wait timeout {timeout}, but ListenProc would block")
        if self.done:
            return 0

        try:
            Event().wait()
        except KeyboardInterrupt:
            self.terminate()

    def poll(self):
        if self.done:
            return 0
        else:
            return None

    def terminate(self):
        if self.cancel is not None:
            self.cancel()  # pylint: disable=not-callable
        self.done = True

    def kill(self):
        self.terminate()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.terminate()


class AutoVnetListener:
    def __init__(self, avn, lns=None, echo=True, suppress_register=False, codename=None):
        self.avn = avn
        self.proc = None
        self.lns = lns
        self.echo = echo
        self.suppress_register = suppress_register
        self.codename = codename

    def mux_disconnect(self):
        if not self.avn.config.rtfm_creds.reuse:
            if self.echo:
                typer.echo(f"[!] Connection sharing not enabled")
            return
        path = Path(self.avn.ssh_sock())
        if not path.is_socket():
            if self.echo:
                typer.echo(f"[!] {path} not connected")
            return
        ssh_cmd = self.get_ssh_cmd(reuse_shutdown=True)
        subprocess.run(ssh_cmd)
        if path.is_socket():
            typer.echo(f"[!] Delete hung socket")
            path.unlink()

    def mux_connect(self):
        if not self.avn.config.rtfm_creds.reuse:
            if self.echo:
                typer.echo(f"[!] Connection sharing not enabled")
            return
        self.ensure_socket()
        path = self.avn.ssh_sock()
        if Path(path).is_socket():
            if self.echo:
                typer.echo(f"[+] Created mux {path}")
        else:
            if self.echo:
                typer.echo(f"[!] Failed to create {path}")

    def mux_cancel(self):
        if not self.avn.config.rtfm_creds.reuse:
            return
        ssh_cmd = self.get_ssh_cmd(reuse_cancel=True)
        subprocess.run(ssh_cmd)

    def get_ssh_cmd(
        self,
        socket_only=False,
        reuse_cancel=False,
        reuse_shutdown=False,
        check_only=False,
    ) -> List[str]:
        ssh_dest = self.avn.config.rtfm_creds.ssh_dest()
        port = str(self.avn.config.rtfm_creds.ports.get("ssh", 2222))
        ssh_cmd = [
            "ssh",
            "-q",
            "-o",
            "StrictHostKeyChecking=no",
            "-o",
            "UserKnownHostsFile=/dev/null",
            "-o",
            "IdentitiesOnly=yes",
            "-o",
            "ExitOnForwardFailure=yes",
            "-o",
            "ServerAliveInterval=30",
            "-o",
            "ServerAliveCountMax=5",
            "-o",
            "PasswordAuthentication=no",
        ]
        socket_args = [
            "-o",
            "ControlPersist=yes",
            "-o",
            "ControlMaster=auto",
            "-S",
            self.avn.ssh_sock(),
        ]
        if self.avn.config.rtfm_creds.reuse:
            ssh_cmd.extend(socket_args)
            if reuse_cancel:
                ssh_cmd.append("-Ocancel")
            elif reuse_shutdown:
                ssh_cmd.append("-Oexit")
            elif check_only:
                ssh_cmd.append("-Ocheck")
            elif not socket_only:
                ssh_cmd.append("-Oforward")

        ssh_cmd.extend(
            [
                "-i",
                str(Path(self.avn.config.rtfm_creds.key).expanduser()),
                "-p",
                port,
                "-N",
            ]
        )
        if not (socket_only or reuse_shutdown or check_only):
            for ln in self.lns:
                ssh_cmd.extend(["-R", ln.r_forward()])
        ssh_cmd.append(ssh_dest)
        return ssh_cmd

    def get_msg(self):
        max_len = max(len(r) for r in [ln.remote() for ln in self.lns])
        prefix = "[+]"
        if self.codename is not None:
            prefix += f" {self.codename} |"
        return "\n".join([f"{prefix} {ln.remote().ljust(max_len)} => {ln.dest()}" for ln in self.lns])

    def msf_rc(self) -> str:
        """Return an msf rc file with helpful listener variables set"""
        if self.codename is None:
            self.codename = gen_codename()
        lines = []
        # Set variables for all the listeners
        prefix = f"set -g {self.codename.upper()}_"
        for i, ln in enumerate(self.lns):
            cmd = f"{prefix}{i:02d}_"
            lines.append(f"{cmd}LHOST {ln.remote_ip}")
            lines.append(f"{cmd}SRVHOST {ln.remote_ip}")
            lines.append(f"{cmd}SRVPORT {ln.remote_port}")
            lines.append(f"{cmd}ReverseListenerBindAddress {ln.dest_ip}")
            lines.append(f"{cmd}LPORT {ln.dest_port}")

        # Use the first IP as default
        ln = self.lns[0]
        lines.append(f"set -g LHOST {ln.remote_ip}")
        lines.append(f"set -g SRVHOST {ln.remote_ip}")
        lines.append(f"set -g SRVPORT {ln.remote_port}")
        lines.append(f"set -g ReverseListenerBindAddress {ln.dest_ip}")
        lines.append(f"set -g LPORT {ln.dest_port}")
        lines.append(f"set -g")

        return "\n".join(lines)

    def ensure_socket(self):
        if not self.avn.config.rtfm_creds.reuse:
            return None
        path = self.avn.ssh_sock()
        if Path(path).is_socket():
            check_cmd = self.get_ssh_cmd(check_only=True)
            ret = subprocess.run(check_cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            if ret.returncode == 0:
                return None
            else:
                # Bad
                self.mux_disconnect()
        ssh_cmd = self.get_ssh_cmd(socket_only=True)
        proc = subprocess.Popen(ssh_cmd)
        try:
            proc.wait(timeout=5)
        except subprocess.TimeoutExpired:
            pass
        return proc

    def _register(self):
        register = []
        for ln in self.lns:
            if ln.dns_names:
                for name in ln.dns_names:
                    register.append(f"{name}:{ln.remote_ip}")
        return register

    def dns_register(self):
        register = self._register()
        if not register:
            return
        dns_cmd = ["autovnet", "rtfm", "dns", "register", "-q", "-d"]
        dns_cmd.extend(register)
        stdout = None
        stderr = None
        if self.suppress_register:
            stdout = subprocess.DEVNULL
            stderr = subprocess.DEVNULL
        subprocess.run(dns_cmd, check=True, stdout=stdout, stderr=stderr)

    def dns_unregister(self):
        register = self._register()
        if not register:
            return
        dns_cmd = ["autovnet", "rtfm", "dns", "unregister", "-q"]
        dns_cmd.extend(register)
        subprocess.run(dns_cmd, check=True)

    def listen(self):
        socket_proc = self.ensure_socket()
        ssh_cmd = self.get_ssh_cmd()

        if socket_proc is not None and socket_proc.poll() is None:
            # Lost race and created an extra process we can kill
            socket_proc.terminate()
            socket_proc.wait()

        proc = None
        if self.avn.config.rtfm_creds.reuse:
            subprocess.run(ssh_cmd, check=True)
            proc = ListenProc(cancel=self.mux_cancel)
        else:
            proc = subprocess.Popen(ssh_cmd)

        if self.echo:
            typer.echo(self.get_msg())

        return proc

    @contextmanager
    def listener(self):
        if not self.lns:
            yield self
            return

        try:
            self.proc = self.listen()
            self.dns_register()
            yield self
        finally:
            if self.proc is not None and self.proc.poll() is None:
                # Process started but not exited
                self.proc.terminate()
                self.proc.wait()
            if self.avn.config.rtfm_creds.reuse:
                cancel_cmd = self.get_ssh_cmd(reuse_cancel=True)
                cancel_proc = subprocess.Popen(cancel_cmd)
                cancel_proc.wait()
            self.dns_unregister()
            self.proc = None

    def background(self, foreground: List[str]):
        """Exec foreground in the foreground, while listener runs in the background

        Listener exits if foreground exits, as detected by a change in PPID
        Dumb, non-portable, and bad
        """
        # This is a dumb idea, but we need to do a real exec for CTRL+C and CTRL+Z to play nice
        # So, fork a child to do the port forward, poll to see when our parent exits, then exit the forward

        lock = multiprocessing.Lock()
        lock.acquire()

        ppid = os.getpid()
        pid = os.fork()
        self.proc = None
        if pid == 0:
            signal.signal(signal.SIGINT, signal.SIG_IGN)
            signal.signal(signal.SIGTSTP, signal.SIG_IGN)

            try:
                self.proc = self.listen()
                lock.release()
                while self.proc.poll() is None and os.getppid() == ppid:
                    time.sleep(1)

            finally:
                if self.proc is not None and self.proc.poll() is None:
                    # Process started but not exited
                    self.proc.terminate()
                    self.proc.wait()
        else:
            lock.acquire()
            os.execvp(foreground[0], foreground)

    def save(self, save_path, exist_ok=True, is_file=False, suffix="") -> (str, str):
        spec = {"listeners": [ln.dict() for ln in self.lns]}
        yaml_spec = yaml_dump(spec)

        codename = self.codename or gen_codename(yaml_spec)
        self.codename = codename

        argv = list(sys.argv)
        # Drop absolute path to autovnet
        argv[0] = Path(argv[0]).name

        user_cmd = shlex.join(argv)
        handle_cmd = f"autovnet rtfm listen {codename}"
        yaml_spec = (
            f"""
# original : {user_cmd}
# handle   : {handle_cmd}
"""
            + yaml_spec
        )

        if suffix:
            suffix = f".{suffix}"

        out_dir = Path(save_path) / codename
        out_file = Path(save_path)

        if not is_file:
            try:
                out_dir.mkdir(parents=True, exist_ok=True)
            except Exception as e:
                typer.echo(f"[-] Output dir {out_dir} could not be created: {e}")
                raise typer.Exit(1)

            out_file = out_dir / f"{codename}{suffix}.yaml"

        if out_file.is_file() and not exist_ok:
            raise Exception(f"[-] Listener {codename} already exists! Use: autovnet rtfm listen {codename}")

        out_file.write_text(yaml_spec)

        if not is_file:
            out_file = out_dir / f"README"
            readme = " | ".join([ln.remote() for ln in self.lns])
            # Bad assumption
            ln = self.lns[0]
            readme = f"[=> {ln.dest()}] | " + readme
            readme += f" # {user_cmd}"
            out_file.write_text(readme)

        return yaml_spec, self.codename

    def shared(self) -> List[str]:
        # Eww
        s = set()
        for ln in self.lns:
            for f in ln.shared:
                s.add(f)
        return list(s)

    def add_file(self, name):
        for ln in self.lns:
            ln.shared.append(name)
