from typing import Optional, List, Dict

from pydantic import BaseModel  # pylint: disable=no-name-in-module


class Tun(BaseModel):
    enable: Optional[bool] = True
    control_port: Optional[int] = 4242
    data_port: Optional[int] = 4242
    host: Optional[str]
    network: Optional[str] = "169.254.0.0/22"
