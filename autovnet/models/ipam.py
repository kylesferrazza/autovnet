from ipaddress import IPv4Address, IPv4Network
from typing import Optional, List, Any

from pydantic import BaseModel, Field, validator  # pylint: disable=no-name-in-module

from autovnet.utils import Timer


class IPAssignment(BaseModel):
    codename: str
    ip: str
    network: str
    lighthouse_ip: str
    lighthouse_connect: str
    # token allows downloading client config
    token: str

    # Attributes
    # https://github.com/samuelcolvin/pydantic/issues/660
    # >= 1.9.0
    timer: Optional[Timer] = Field(None, exclude=True)

    class Config:
        arbitrary_types_allowed = True

    @validator("ip")
    def validate_ip(cls, ip):  # pylint: disable=no-self-argument
        if isinstance(ip, IPv4Address):
            return str(ip)
        else:
            return str(IPv4Address(ip))

    @validator("network")
    def validate_network(cls, network):  # pylint: disable=no-self-argument
        if isinstance(network, IPv4Network):
            return str(network)
        else:
            return str(IPv4Network(network))


class IPMapping(BaseModel):
    tun_ip: str
    map_ip: str
    clean_cmds: List[str]
