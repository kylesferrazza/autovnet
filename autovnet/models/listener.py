from ipaddress import IPv4Address
from typing import Optional, List

from pydantic import BaseModel, validator  # pylint: disable=no-name-in-module


class Listener(BaseModel):
    remote_ip: Optional[str]
    remote_port: Optional[int] = 443
    dns_names: Optional[List[str]]

    dest_ip: Optional[str] = "127.0.0.1"
    dest_port: Optional[int]

    # Used by stunnel
    next_dest_ip: Optional[str]
    next_dest_port: Optional[int]

    # used by autovnet rtfm share
    shared: Optional[List[str]] = []

    # used by autovnet rtfm fs export
    fs_export: Optional[str]

    @validator("remote_ip")
    def validate_remote_ip(cls, remote_ip):  # pylint: disable=no-self-argument
        if isinstance(remote_ip, IPv4Address):
            return str(remote_ip)
        else:
            return str(IPv4Address(remote_ip))

    def msg(self):
        """Return a user-friendly message for this listener"""
        return f"{self.remote()} => {self.dest()}"

    def r_forward(self):
        """Return a remote port forward string for ssh -R"""
        return f"{self.remote()}:{self.dest(logical=False)}"

    def dest(self, logical=True):
        if logical and self.next_dest_ip is not None and self.next_dest_port is not None:
            return f"{self.next_dest_ip}:{self.next_dest_port}"
        else:
            return f"{self.dest_ip}:{self.dest_port}"

    def remote(self):
        return f"{self.remote_ip}:{self.remote_port}"
