from typing import Optional, List, Dict

from pydantic import BaseModel  # pylint: disable=no-name-in-module


class Creds(BaseModel):
    user: Optional[str]
    password: Optional[str]
    key: Optional[str]
    pubkey: Optional[str]
    host: Optional[str]
    ports: Optional[Dict[str, int]] = {}
    reuse: Optional[bool] = True

    def ssh_dest(self):
        return f"{self.user}@{self.host}"
