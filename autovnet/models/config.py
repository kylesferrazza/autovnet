from pathlib import Path
from ipaddress import IPv4Network

from typing import Optional, List

from pydantic import BaseModel, validator  # pylint: disable=no-name-in-module

from .creds import Creds
from .tun import Tun
from .dns import Dns


class Config(BaseModel):
    player: Optional[str]
    networks: Optional[List[str]]
    rtfm_creds: Optional[Creds]
    docker_path: Optional[str]
    docs_path: Optional[str]
    data_path: Optional[str] = "/opt/rtfm"
    server_mode: Optional[bool]
    tun: Optional[Tun] = Tun()
    dns: Optional[Dns] = Dns()

    @validator("networks")
    def validate_networks(cls, networks):  # pylint: disable=no-self-argument
        for net in networks:
            IPv4Network(net)
        return networks

    @validator("docker_path")
    def validate_docker_path(cls, docker_path):  # pylint: disable=no-self-argument
        if docker_path is not None:
            if not Path(docker_path).expanduser().is_dir():
                raise FileNotFoundError(f"[-] Config error: Invalid docker_path: {docker_path}")
        return docker_path

    @validator("data_path")
    def validate_data_path(cls, data_path):  # pylint: disable=no-self-argument
        if data_path is not None:
            if not Path(data_path).expanduser().is_dir():
                raise FileNotFoundError(f"[-] Config error: Invalid data_path: {data_path}")
        return data_path
