from enum import Enum
from typing import Optional
from pathlib import Path

import yaml
from pydantic import BaseModel  # pylint: disable=no-name-in-module

from autovnet.utils import yaml_dump, gen_codename

from . import Listener


class StreamQuality(str, Enum):
    potato = "potato"
    low = "low"
    default = "default"
    mid = "mid"
    high = "high"
    ultra = "ultra"
    raw = "raw"


class Mount(BaseModel):
    host: str
    port: int

    # fs
    mountpoint: Optional[str]

    # svc
    svc_host: Optional[str] = "127.0.0.1"
    svc_port: Optional[int]

    # vnc
    vnc_quality: Optional[str] = "default"

    def mount_cmd(self, cmd):
        cmd = f"autovnet rtfm {cmd} mount {self.host}:{self.port}"
        if self.mountpoint:
            cmd += " {self.mountpoint}"
        return cmd

    def save(self, save_dir, codename=None, exist_ok=False, cmd="fs", suffix="") -> (str, str):
        yaml_spec = yaml_dump(self.dict())
        if codename is None:
            codename = gen_codename(yaml_spec)
        yaml_spec = f"""
# {self.mount_cmd(cmd)}
{yaml_spec}
"""
        dir_path = Path(save_dir).expanduser() / codename
        if suffix:
            suffix = f".{suffix}"
        config_path = dir_path / f"mount{suffix}.yaml"
        if not exist_ok and config_path.exists():
            raise FileExistsError(f"[-] {config_path} already exists!")
        dir_path.mkdir(parents=True, exist_ok=True)
        readme = dir_path / "README"

        if not readme.exists():
            text = f"{self.host}:{self.port}"
            if self.mountpoint is not None:
                text += f" @ {self.mountpoint}"
            readme.write_text(text)

        config_path.write_text(yaml_spec)

        return yaml_spec, codename

    @classmethod
    def load(cls, cfg_dir, codename, suffix=""):
        if suffix:
            suffix = f".{suffix}"
        config_path = Path(cfg_dir).expanduser() / codename / f"mount{suffix}.yaml"
        if not config_path.is_file():
            raise FileNotFoundError(f"[-] {config_path} not found!")

        spec = yaml.safe_load(config_path.read_text())
        return cls.parse_obj(spec)
