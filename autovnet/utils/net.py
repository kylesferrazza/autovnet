import time
import socket
import random
import hashlib
import subprocess
from urllib.parse import urlparse, urlsplit
from typing import Optional, List, Iterable
from ipaddress import IPv4Address, IPv4Network

import requests
import powerdns
from powerdns.exceptions import PDNSError

from .crypto import gen_codename

# This IP should be something you don't have a local route for
# (the goal is the detect the src IP of your default interface)
# https://en.wikipedia.org/wiki/Reserved_IP_addresses
GET_IP_DEST = "192.0.2.0"


def get_ip() -> str:
    # https://stackoverflow.com/questions/166506/finding-local-ip-addresses-using-pythons-stdlib
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    ip = "127.0.0.1"
    try:
        s.connect((GET_IP_DEST, 1))
        ip = s.getsockname()[0]
    except Exception:
        pass
    finally:
        s.close()
    return ip


def net_list(networks: List[IPv4Network]) -> str:
    res = " | ".join([str(net) for net in networks])
    return f"[{res}]"


def ip_gen(networks: List[IPv4Network], count: Optional[int] = None):
    """Generate random IPs from networks (with replacement)"""
    if networks is None or len(networks) == 0:
        raise ValueError("[-] ip_gen got no networks!")
    if count == 0:
        count = None

    bounds = []
    last = 0
    for net in networks:
        last += net.num_addresses
        bounds.append(last)

    ip = None
    while count is None or count > 0:
        r = random.randint(0, last)
        for i, b in enumerate(bounds):
            if r <= b:
                net = networks[i]
                ip = net[random.randint(0, net.num_addresses - 1)]
                break
        yield ip
        if count is not None:
            count -= 1


def rand_ip_list(
    networks: List[IPv4Network], count: Optional[int] = None, allow_zero=False, strict_count=True
) -> Iterable[IPv4Address]:
    """Generate randomized lists of unique IPs from networks

    Aims to be reasonbly efficient (CPU / RAM) for reasonable requests.
    Random sorting 0.0.0.0/0 requires more RAM than you probably have ;)
    Assumes there is no overlap between networks.
    """

    avail = 0
    for net in networks:
        avail += net.num_addresses

    if count > avail:
        if strict_count:
            raise ValueError(f"[-] {count} IPs requested, but only {avail} IPs are avaiable in {networks}")
        else:
            count = avail

    if count == 0:
        if not allow_zero:
            raise ValueError(f"[-] Count cannot be zero")
        count = None

    if count is None:
        # Technique 1: random sort all IPs
        ips = []
        for net in networks:
            ips.extend(net)
        random.shuffle(ips)
        return ips

    if count < avail / 4:
        # Technique 2: if we want a small (<25%) subset of the network, use random sampling by index
        # (most efficient)
        ip_list = set()
        for ip in ip_gen(networks):
            ip = str(ip)
            if ip not in ip_list:
                ip_list.add(ip)
                count -= 1
            if count <= 0:
                break
        return [IPv4Address(ip) for ip in ip_list]

    # Technique 3: if we want a large (>25%) subset of the network, sample from full network in memory
    # (Use more RAM to void CPU spinning trying to generate random numbers we haven't previously generated)
    ips = []
    for net in networks:
        ips.extend(net)
    # no replacement
    return random.sample(ips, count)


def rand_ip(networks: List[IPv4Network]) -> IPv4Address:
    res = rand_ip_list(networks, 1)
    # Return first item in iterable, or None
    for r in res:
        return r
    return None


def format_url(ip, port=None, proto=None, path="", auth=""):
    url = ""
    if proto is not None:
        url += f"{proto}://"
    if auth is not None:
        url += f"{auth}@"
    url += f"{ip}"
    if port is not None:
        url += f":{port}"
    if path is not None:
        url += f"{path}"
    return url


def rand_port() -> int:
    return random.randint(32768, 65535)


def is_url(url) -> bool:
    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc])
    except Exception:
        return False


def split_host_port(url):
    result = None
    try:
        result = urlsplit(url)
    except Exception:
        pass

    if result is None or (result is not None and result.hostname is None):
        result = urlsplit(f"//{url}")

    return result.hostname, result.port


def as_port(x, min_value=1, max_value=65535):
    try:
        if x is None:
            raise ValueError("not set")
        port = int(x)
        if port < min_value or port > max_value:
            raise ValueError(f"not in range [{min_value}, {max_value}]")
        return port
    except ValueError as e:
        raise ValueError(f"[-] {x} is not a valid port: {e}")


def wait_tcp_open(dest_ip, dest_port, attempts=600, delay=1, proc=None, thread=None):
    ok = False
    while not ok and (attempts is None or attempts > 0):
        if proc is not None and proc.poll() is not None:
            raise Exception(f"[-] process unexpectedly returned {proc.poll()}")
        if thread is not None and not thread.is_alive():
            raise Exception("[-] thread unexpectedly exited")
        try:
            with socket.create_connection((dest_ip, dest_port), timeout=1):
                ok = True
        except Exception:
            if attempts is not None:
                attempts -= 1
            time.sleep(delay)
    return ok


def wait_url_open(url, attempts=600, delay=1, status_code=None, proc=None, thread=None):
    ok = False
    while not ok and (attempts is None or attempts > 0):
        if proc is not None and proc.poll() is not None:
            raise Exception(f"[-] process unexpectedly returned {proc.poll()}")
        if thread is not None and not thread.is_alive():
            raise Exception("[-] thread unexpectedly exited")
        try:
            ok = requests.get(url, verify=False, timeout=5)
            if ok or (status_code is not None and ok.status_code == status_code):
                ok = True
        except Exception:
            if attempts is not None:
                attempts -= 1
            time.sleep(delay)
    return ok


def open_url(url):
    """Calls xdg-open to open a url

    Returns the process object without blocking
    """
    open_cmd = ["xdg-open", url]
    # WARNING: leaking a process, but we only attempt to open the service is up
    # we don't want to block for the browser to exit, and sometimes xdg-open does.
    return subprocess.Popen(open_cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def tun_name(codename):
    # https://unix.stackexchange.com/questions/451368/allowed-chars-in-linux-network-interface-names
    sha256 = hashlib.sha256()
    sha256.update(codename.encode())
    prefix = f"rtfm-"
    name = prefix + sha256.hexdigest()[: 15 - len(prefix)]
    alias = prefix + codename.replace("_", "-")
    return name, alias


def split_dns_register(r, tld=None, networks=None):
    fields = r.split(":")
    zone_name = fields.pop(0)
    try:
        # Just IP == random DNS name
        IPv4Address(zone_name)
    except Exception as e:
        pass
    else:
        # User gave IP
        fields.insert(0, zone_name)
        zone_name = random_dns(tld=tld)

    if not zone_name.endswith(tld):
        zone_name += f".{tld}"

    if len(fields) == 0:
        fields.append(str(rand_ip(networks)))

    ips = fields
    # ns_name = f"ns.{tld}."
    ns_name = f"{tld}."
    a_list = [(ip, False) for ip in ips]
    return f"{zone_name}.", ips, ns_name, a_list


def random_dns(zone_name=None, tld=None, tokens=2):
    if tokens <= 0:
        tokens = 1
    if zone_name is None:
        zone_name = f"{gen_codename(join='.', tokens=tokens)}"
    if not zone_name.endswith(tld):
        zone_name += f".{tld}"
    return zone_name


def dns_register(register, api, tld=None, networks=None, echo_func=None):
    registered = []
    cancelled = False
    try:
        for r in register:
            zone_name, ips, ns_name, a_list = split_dns_register(r, tld=tld, networks=networks)
            try:
                records = [
                    powerdns.RRSet(
                        zone_name,
                        "A",
                        a_list,
                        ttl=0,
                    )
                ]
                zone = None
                create_zone = lambda: api.servers[0].create_zone(
                    name=zone_name,
                    kind="Native",
                    rrsets=records,
                    nameservers=[ns_name],
                )
                try:
                    zone = create_zone()
                except PDNSError as e:
                    if e.status_code != 409:
                        raise
                    # Conflict: Zone already exists
                    # Unregister and retry
                    dns_unregister([f"{zone_name[:-1]}"], api, tld=tld, echo_func=None)
                    create_zone()

                created = f"{zone_name[:-1]}:{' '.join(ips)}"
                if echo_func is not None:
                    echo_func(created)
                registered.append(created)
            except Exception as e:
                raise Exception(f"[-] Failed to register {zone_name[:-1]}: {e}")

    except KeyboardInterrupt:
        # Must return registered list so caller can unregister on cleanup
        cancelled = True
        if echo_func is not None:
            echo_func(f"[-] dns registration interrupted: {len(registered)} registered")

    return registered, cancelled


def dns_unregister(register, api, tld=None, echo_func=None):

    for r in register:
        zone_name = f"{r.split(':')[0]}."
        try:
            api.servers[0].delete_zone(zone_name)
            if echo_func is not None:
                echo_func(f"[+] unregistered {zone_name[:-1]}")
        except PDNSError as e:
            if e.status_code != 404 and echo_func is not None:
                echo_func(f"[-] unregister error: {e}")


def make_dns_names(register=None, random_register=0, tld=None):
    names = []
    if register is not None:
        for r in register:
            names.append(random_dns(zone_name=r, tld=tld))

    for i in range(random_register):
        names.append(random_dns(tld=tld))

    return names
