import time
import threading
from queue import PriorityQueue
from functools import total_ordering


@total_ordering
class Timer:
    def __init__(self, func, interval=60, repeat=True):
        self.interval = interval
        self.repeat = repeat
        self.func = func
        self.deadline = None
        self.dirty = False
        self.cancelled = False
        self.update_deadline()

    def run(self):
        return self.func()

    def update_deadline(self, dirty=False):
        self.dirty = dirty
        self.deadline = time.time() + self.interval

    def ready(self):
        return time.time() > self.deadline

    def cancel(self):
        self.dirty = True
        self.cancelled = True

    def __eq__(self, other):
        d = other
        if isinstance(other, Timer):
            d = other.deadline
        return self.deadline == d

    def __lt__(self, other):
        d = other
        if isinstance(other, Timer):
            d = other.deadline
        return self.deadline < d


class TimerQueue:
    def __init__(self):
        self._stop = threading.Event()
        self.msg = threading.Event()
        self.q = PriorityQueue()

    def _run(self):
        while not self._stop.is_set():
            timer = self.q.get()
            self.msg.clear()

            if self._stop.is_set():
                break
            if timer.dirty:
                # Deadline updated after insert, another item may have passed it
                timer.dirty = False
                if not timer.cancelled:
                    self.add(timer)
                continue

            timeout = max(timer.deadline - time.time(), 0)
            if self.msg.wait(timeout=timeout):
                # If a timer with a million year timeout gets popped, we would wait for a million years,
                # even if urgent items got added to the queue
                # So, wake up on queue insert and just queue our current timer, if it isn't ready yet
                if not timer.ready():
                    if not timer.cancelled:
                        self.add(timer)
                    self.add(timer)
                    continue

            if timer.dirty:
                # Deadline updated while waiting, another item may have passed it
                timer.dirty = False
                if not timer.cancelled:
                    self.add(timer)
                continue
            timer.run()
            if timer.repeat and not timer.cancelled:
                self.add(timer)

    def add(self, timer):
        timer.update_deadline()
        self.q.put(timer)
        self.msg.set()

    def run(self):
        thread = threading.Thread(target=self._run, daemon=True)
        thread.start()

    def stop(self):
        self._stop.set()
        self.q.put(Timer(None, interval=0))
