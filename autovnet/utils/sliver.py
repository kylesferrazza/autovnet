import copy
from pathlib import Path
from enum import Enum

import yaml

from autovnet.utils import (
    rand_ip,
)

from sliver import client_pb2


class SliverProto(str, Enum):
    mtls = "mtls"
    http = "http"
    https = "https"
    # UDP
    wg = "wg"
    dns = "dns"


def fix_sliver_profile(p):
    p["Format"] = client_pb2.OutputFormat.Value(p["Format"])
    for k, v in p.items():
        if isinstance(v, int) and any([k.endswith(s) for s in ["Jitter", "Interval", "Timeout"]]):
            # Accept fields in seconds instead of nanoseconds lol
            p[k] = int(v * 1e9)


def listener_to_sliver_c2(proto, ln):
    res = []
    for model in ln.lns:
        if model.dns_names is not None:
            for dns in model.dns_names:
                c2 = client_pb2.ImplantC2(URL=f"{proto}://{dns}:{model.remote_port}")
                res.append(c2)
        else:
            c2 = client_pb2.ImplantC2(URL=f"{proto}://{model.remote_ip}:{model.remote_port}")
            res.append(c2)
    return res


def load_sliver_profiles(proto, profiles_yaml, listener_name="default"):
    profiles = yaml.safe_load(Path(profiles_yaml).read_text())
    defaults = profiles.pop("defaults", {})
    platforms = profiles.pop("platforms", {})
    formats = platforms.pop("Format", [])

    go_os_list = platforms.pop("GOOS", [])
    go_arch_list = platforms.pop("GOARCH", [])

    res = []
    _go_os_list = None
    _go_arch_list = None
    _formats = None
    for name, p in profiles["profiles"].items():
        go_os = p.pop("GOOS", None)
        go_arch = p.pop("GOARCH", None)
        _format = p.pop("Format", None)

        if go_os is None:
            _go_os_list = go_os_list
        else:
            _go_os_list = [go_os]

        if go_arch is None:
            _go_arch_list = go_arch_list
        else:
            _go_arch_list = [go_arch]

        if _format is None:
            _formats = formats
        else:
            _formats = [_format]

        saved_name = name
        for go_os in _go_os_list:
            p["GOOS"] = go_os
            for go_arch in _go_arch_list:
                p["GOARCH"] = go_arch
                for _format in _formats:
                    variant = None
                    if "." in _format:
                        _format, variant = _format.split(".")
                        # Currently used for SHARED_LIB RunAtLoad
                        p[variant] = True
                    p["Format"] = _format

                    tmp_p = copy.deepcopy(p)
                    # apply defaults
                    tmp_defaults = copy.deepcopy(defaults)
                    tmp_defaults.update(tmp_p)
                    tmp_p = tmp_defaults
                    fix_sliver_profile(tmp_p)

                    name = saved_name
                    name = f"{name}.{proto}.{listener_name}.{go_os}_{go_arch}.{_format}"
                    if variant is not None:
                        name += f".{variant}"
                    config = client_pb2.ImplantConfig(**tmp_p)
                    sliver_profile = client_pb2.ImplantProfile(Name=name, Config=config)
                    res.append(sliver_profile)
    return res


def start_listener(client, proto, model, persistent=True):
    if proto == SliverProto.mtls:
        return client.start_mtls_listener(model.dest_ip, model.dest_port, persistent=persistent)
    elif proto == SliverProto.http:
        return client.start_http_listener(
            domain="", host=model.dest_ip, port=model.dest_port, secure=False, website="", persistent=persistent
        )
    elif proto == SliverProto.https:
        return client.start_https_listener(
            domain="",
            host=model.dest_ip,
            port=model.dest_port,
            website="",
            cert=b"",
            key=b"",
            acme=False,
            persistent=persistent,
        )
    elif proto == SliverProto.wg:
        return client.start_wg_listener(port=model.dest_port, tun_ip=model.dest_ip, persistent=persistent)
    elif proto == SliverProto.dns:
        return client.start_dns_listener(
            domains=model.dns_names, canaries=False, host=model.dest_ip, port=model.dest_port, persistent=persistent
        )
    raise ValueError(f"[-] Unknown proto {proto}")


def proto_to_port(proto):
    d = {
        SliverProto.mtls: 443,
        SliverProto.http: 80,
        SliverProto.https: 443,
        SliverProto.wg: 123,
        # 53 is probably already bound
        # to use it, start a tun and a listener for some other port (e.g. 5553)
        # then: sudo iptables -t nat -A PREROUTING -i rtfm-XXXX -p UDP -d --dport 53 -j REDIRECT --to-port 5553
        SliverProto.dns: 53,
    }
    return d[proto]
