from contextlib import contextmanager


@contextmanager
def yield_none(items=1):
    yield (None,) * items
