import os
import time
import subprocess
import threading
import gzip
import shutil
import shlex
from pathlib import Path
from datetime import datetime
from contextlib import contextmanager

import click
import typer

from .log import timestamp


def read_env_file(path, parent=True) -> dict:
    return read_env_str(Path(path).read_text(), parent=parent)


def read_env_str(data, parent=True) -> dict:
    res = {}
    if parent:
        res = os.environ.copy()
    # WARNING: probably wrong
    for line in data.splitlines():
        stripped = line.strip()
        if not stripped or stripped.startswith("#"):
            # Ignore comments and blank lines
            continue
        key, val = line.split("=", maxsplit=1)
        res[key] = val
    return res


def write_env_file(d, path) -> str:
    # WARNING: probably wrong
    lines = []
    for key, val in d.items():
        lines.append(f"{key}={val}")
    text = "\n".join(lines)
    Path(path).write_text(text)
    return text


def compose_down(dir_path, env=None, project=None, quiet=False, rmi="local", compose_file=None):
    if env is not None:
        tmp = os.environ.copy()
        tmp.update(env)
        env = tmp
    cmd = ["docker-compose"]
    if project is not None:
        cmd.extend(["-p", project])
    if compose_file is not None:
        cmd.extend(["-f", compose_file])

    cmd.append("down")

    if rmi is not None:
        cmd.extend(["--remove-orphans", "--rmi", rmi])
    stdout = None
    stderr = None
    if quiet:
        stdout = subprocess.DEVNULL
        stderr = subprocess.DEVNULL
    return subprocess.run(cmd, cwd=dir_path, env=env, stdout=stdout, stderr=stderr, check=True)


def compose_build(dir_path, env=None, project=None, quiet=True, compose_file=None):
    if env is not None:
        tmp = os.environ.copy()
        tmp.update(env)
        env = tmp
    cmd = ["docker-compose"]
    if project is not None:
        cmd.extend(["-p", project])
    if compose_file is not None:
        cmd.extend(["-f", compose_file])

    cmd.append("build")
    stdout = None
    stderr = None
    if quiet:
        cmd.append("-q")
        # https://github.com/docker/compose/issues/8927
        stdout = subprocess.DEVNULL
        stderr = subprocess.DEVNULL
    return subprocess.run(cmd, cwd=dir_path, env=env, stdout=stdout, stderr=stderr, check=True)


def compose_up(
    dir_path,
    env=None,
    detach=False,
    build=False,
    project=None,
    quiet=False,
    stop=None,
    stop_interval=5,
    exit_code_from=None,
    compose_file=None,
):
    cmd = ["docker-compose"]
    if project is not None:
        cmd.extend(["-p", project])

    if compose_file is not None:
        cmd.extend(["-f", compose_file])

    if env is not None:
        tmp = os.environ.copy()
        tmp.update(env)
        env = tmp
    cmd.append("up")
    if build:
        cmd.append("--build")
    if detach:
        cmd.append("-d")
    if exit_code_from is not None:
        cmd.append(f"--exit-code-from={exit_code_from}")

    stdout = None
    stderr = None
    if quiet:
        stdout = subprocess.DEVNULL
        stderr = subprocess.DEVNULL
    if stop is not None and stop.is_set():
        # Last chance for the user to stop us before we commit to running docker-compose
        typer.echo("\n[+] Cancelled!")
        return

    ret = None
    proc = None
    try:
        proc = subprocess.Popen(cmd, cwd=dir_path, env=env, stdout=stdout, stderr=stderr)
        timeout = stop_interval
        if stop is None:
            timeout = None
        while stop is None or not stop.is_set():
            try:
                proc.wait(timeout=timeout)
                break
            except subprocess.TimeoutExpired:
                pass

        ret = proc.poll()
        if ret is None and stop is not None and stop.is_set():
            proc.terminate()
            proc.wait()

        if ret is not None and ret != 0:
            raise subprocess.SubprocessError(f"[-] docker-compose returned {ret}")

    except KeyboardInterrupt:
        # Some things are slow to shutdown (e.g. stunnel), so give immediate feedback
        typer.echo("\n[+] Shutting down (please wait)...")
        try:
            if proc is not None:
                proc.terminate()
                proc.wait()
        except Exception as e:
            typer.echo(f"[-] {e}")
    finally:
        # This seems to not always be called in a threaded context
        # So... we just call compose_down twice; it should silenty fail the second time
        compose_down(dir_path, env=env, project=project, quiet=quiet, compose_file=compose_file)


def compose_run(
    dir_path,
    service,
    env=None,
    build=True,
    project=None,
    verbose_build=False,
    compose_file=None,
):
    cmd = ["docker-compose"]
    if project is not None:
        cmd.extend(["-p", project])
    if compose_file is not None:
        cmd.extend(["-f", compose_file])
    cmd.extend(["run", "--rm", service])

    if env is not None:
        tmp = os.environ.copy()
        tmp.update(env)
        env = tmp

    if build:
        compose_build(dir_path, env=env, project=project, compose_file=compose_file, quiet=not verbose_build)
    try:
        return subprocess.run(cmd, cwd=dir_path, env=env, check=True)
    finally:
        compose_down(dir_path, env=env, project=project, compose_file=compose_file, quiet=not verbose_build)


@contextmanager
def _thread_ctx(*args, _target=None, cleanup=None, daemon=None, **kwargs):
    if _target is None:
        raise ValueError(f"[-] _thread_ctx missing _target!")
    kwargs.setdefault("stop", threading.Event())
    stop = kwargs["stop"]
    t = threading.Thread(target=_target, args=args, kwargs=kwargs, daemon=daemon)
    t.start()
    try:
        yield t, stop
        # Note: currently not joined, caller must join within ctx
    except Exception as e:
        # User aborts don't need an error message
        if not isinstance(e, click.exceptions.Abort):
            typer.echo(f"[!] Exception from thread_ctx: {e}")
        if stop is not None:
            stop.set()
        raise
    finally:
        typer.echo("\n[+] Shutting down helper thread (please wait)...")
        if stop is not None:
            stop.set()
        t.join()
        if cleanup is not None:
            cleanup()


def compose_up_ctx(dir_path, *args, **kwargs) -> (threading.Thread, threading.Event):
    """Run docker-compose up in a background thread"""

    cleanup_kwargs = {}
    for key in ["env", "project", "compose_file"]:
        if key in kwargs:
            cleanup_kwargs[key] = kwargs[key]
    cleanup = lambda: compose_down(dir_path, quiet=True, **cleanup_kwargs)
    return _thread_ctx(dir_path, *args, _target=compose_up, cleanup=cleanup, **kwargs)


def pg_dumper(*args, **kwargs) -> (threading.Thread, threading.Event):
    """Periodically run cmd in a different thread

    Intended to run something like:
    docker-compose exec database pg_dump my_database -U my_user

    then gzip and timestamp the output to backups.

    Yields the background thread (so you can join / synchronize with it)
    and an Event that will signal the thread to gracefully exit
    """
    return _thread_ctx(*args, _target=_pg_dumper, **kwargs)


def _pg_dumper(cmd, backups, stop=None, interval=300, retain=16, prefix="pg_dump.", echo=True):
    """Helper thread function to make pg_dump backups

    interval: seconds to wait before attempting the next backup.
              waits for previous jobs to finish, so no concurrent backups
    retain: number of backups to retain, old ones will be deleted.
    """
    backup_dir = Path(backups)
    if not backup_dir.is_dir():
        backup_dir.mkdir(parents=True, exist_ok=True)
    created = []
    # Sleep until signaled to stop, or it's time to backup
    while stop is None or not stop.wait(interval):
        # Time to backup
        ts = timestamp()
        try:
            backup_path = backup_dir / f"{prefix}{ts}.sql.gz"
            if backup_path.is_file():
                raise FileExistsError(f"backup_path {backup_path} already exists!")

            proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
            with gzip.GzipFile(filename=backup_path, mode="wb") as gz_file:
                shutil.copyfileobj(proc.stdout, gz_file)
            if proc.wait() != 0:
                raise Exception(f"'{shlex.join(cmd)}' returned {proc.returncode}")
        except Exception as e:
            # Error messages ignore echo
            typer.echo(f"[!] [{timestamp()}] Scheduled backup ({ts}) failed: {e}", err=True)
        else:
            if echo:
                typer.echo(f"[{timestamp()}] Saved backup: {backup_path}", err=True)

        if retain > 0:
            # There is a limit to retention
            created.append(backup_path)
            if len(created) > retain:
                oldest = created.pop(0)
                oldest.unlink(missing_ok=True)
                if echo:
                    typer.echo(f"[{timestamp()}] Deleted expired backup: {oldest}", err=True)

    # Signaled to stop, return so we can be joined
    if echo:
        typer.echo(f"[*] Helper thread stopping...", err=True)


def wait_container_exit(container_prefix, attempts=30, delay=1):
    running = True
    ps_cmd = ["docker", "ps", "-q", "-f", f"name={container_prefix}"]
    attempts = 30
    while running and attempts > 0:
        ps = subprocess.run(ps_cmd, capture_output=True, check=True)
        running = len(ps.stdout) > 0
        time.sleep(1)
        attempts -= 1

    return not running


def wait_container_start(container_prefix, attempts=30, delay=1):
    running = False
    ps_cmd = ["docker", "ps", "-q", "-f", f"name={container_prefix}"]
    attempts = 30
    while not running and attempts > 0:
        ps = subprocess.run(ps_cmd, capture_output=True, check=True)
        running = len(ps.stdout) > 0
        time.sleep(1)
        attempts -= 1

    return running
