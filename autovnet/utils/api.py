import uvicorn


def run_app(app, host="127.0.0.1", port=5000, log_level="info", log_config=uvicorn.config.LOGGING_CONFIG):
    uvicorn.run(app, host=host, port=port, log_level=log_level, log_config=log_config)
