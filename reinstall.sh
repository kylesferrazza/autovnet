#!/bin/echo Run: ${SHELL}

# Do not invoke directly!

# WARNING: stay POSIX-y, this could be zsh

# typer uses shellingham to detect current shell,
# and using a /bin/bash script confuses it

set -eu
PACKAGE=autovnet
if [ ! -f setup.py ]; then
    echo "setup.py not found"
    exit 1
fi
pip3 uninstall -y "${PACKAGE}"
rm -rf build
pip3 install --upgrade pip
./scripts/make_help.sh > /dev/null 2>&1 || true
pip3 install .
./scripts/make_help.sh > /dev/null 2>&1 || true

# Build the docs
autovnet docs build --force

SHELL_NAME="$(basename "${SHELL}")"
autovnet --install-completion

cat << EOF
# Run:
. ~/.${SHELL_NAME}rc
EOF
