# Usage

[TOC]

## Intro

Once you have installed and configured your `autovnet server` and `autovnet clients`, you are ready to enjoy unprecendented Red Team Force Muliplication!

This guide is for using `autovnet` on a client system (e.g. Red Kali system).

See [help.md](help.md) for the auto-generated help docs.
They are a helpful reference for the commands and options that exist.

See each feature section / document under [usage](../usage) for high-level descriptions and examples.

## Codenames and Interpreting Output

Many `autovnet` commands will generate a codename (e.g. `foo_bar`), and print an example command that contains it.

`autovnet` uses codenames to organize and save command metadata (by default, in `/opt/rtfm/*`).

By executing the corresponding codename command, you can re-create C2 infrastructure you had previously.

Because of this, it's a good idea to keep notes of any codenames you might want to reuse later. (e.g. copy the generated commands)

## Example Output

Here is some example command output:
```bash
$ autovnet rtfm listen
[+] autovnet rtfm listen caring_individual
[+] caring_individual | 10.194.76.42:443 => 127.0.0.1:51467
```

1. `autovnet rtfm listen` is the command we typed in the terminal

2. `autovnet rtfm listen caring_individual` is the command we should run if we want to later re-create this command
    - The randomly generated codename is `caring_individual`, and it is associated with this randomly generated IP
    - So, if you use `autovnet rtfm listen caring_individual` in the future, you will get the same IP that was generated this time
    - If you just ran `autovnet rtfm listen` again, you would get a new codename and new randomly generated IP

3. `10.194.76.42:443 => 127.0.0.1:51467` is in the form `PUBLIC_IP:PUBLIC_PORT => LOCAL_IP:LOCAL_PORT`

    - The left / _public_ side shows the _exposed_ side of the listener - this is where you should connect to
    - Any Blue or Red Team system (including yours) can access the public `IP:PORT`, if the network is correctly configured
    - The right / _local_ side shows the address your local service is / should be bound to
    - So, if you listen on `127.0.0.1:51467`, you will get packets that were sent to `10.194.76.42:443`
    - Blue Team will only see the left side - e.g. `RANDOM_IP:WELL_KNOWN_PORT`

## Offline / Local Docs

Are you viewing this document in the `gitlab` repo or a text editor?

You can view these docs in beautiful [mkdocs](https://www.mkdocs.org/) form [online](https://autovtools.gitlab.io/autovrtfm/autovnet-docs/usage/),
or offline (locally, in your browser) with

```bash
$ autovnet docs serve
```
