# Catching Callbacks (Blue -> Red Connections)

[TOC]

Often, you want to catch reverse shells, download payloads, or have a tool phone home to C2 infrasture you control.

All of these connections are Blue -> Red, meaning that the Blue Team system initiates the connection,
and the relevant firewall rules are the _egress_ firewall rules for the Blue Team, which are often more permissive than the _ingress_ rules.

## Manual

Manually creating a listener allows you to expose a local service on your Kali machine to the game network (Blue and Red players) via a _public_ / remote IP
(not _really_ public, but pretend public - it's in the IP block the _autovnet server_ is simulating).

The real-world analog would be something along the lines of:

1. Purchase and provision a VPS with public IP (or VPN that allows port forwarding)

2. Configure tunneling (or port forwarding) such that traffic destined for the public IP:PORT reaches your local machine
  - e.g. `ssh -R PUBLIC_IP:80:127.0.0.1:8000 my_vps`

The end result is the same: `PUBLIC_IP:80` is handled by `127.0.0.1:8000` (on your Kali machine), but with `autovnet`,
it's free, instant, and you can repeat as many times as you want for more IPs!

Turns out when you _are_ the internet (i.e. you can collude with the Cyber Range administrator to deploy `autovnet`),
you can actually skip the provisioning step entirely and skip straight to handing out a usable ip.

This is a big part of why `autovnet` scales so ridiculously well - the resource cost of creating a listener is almost zero.

You can use this technique to expose any TCP service (e.g. your favorite C2 server) through `autovnet`.

### Listener First

Let's assume we have not yet started our custom service,
and want `autovnet` to tell us how we should configure it.

```bash
$ autovnet rtfm listen
[+] autovnet rtfm listen caring_individual
[+] caring_individual | 10.194.76.42:443 => 127.0.0.1:51467
```

We use the right side (`127.0.0.1:51467`) as the `IP:PORT` we should listen on.
Since the default public port (what the Blue Team will see) is `443`, let's start an ssl server with `openssl` using `ncat`

```bash
$ ncat -nvl --ssl 127.0.0.1 51467
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: Generating a temporary 2048-bit RSA key. Use --ssl-key and --ssl-cert to use a permanent one.
Ncat: SHA-1 fingerprint: E495 0B1E EC2D 766C 482E AA98 005B 8E4C 663D 79B0
Ncat: Listening on 127.0.0.1:51467
```

We use the left side (`10.194.76.42:443`) as the destination we should connect to.
You can connect from any system, your local machine, a Blue Team system, or a fellow Red Team system.

Here is an example of connecting using `openssl`'s `s_client` from a different system:

```bash
$ openssl s_client  10.194.76.42:443
CONNECTED(00000003)
Can't use SSL_get_servername
depth=0 CN = 127.0.0.1
verify error:num=18:self signed certificate
verify return:1
depth=0 CN = 127.0.0.1
verify return:1
---
Certificate chain
 0 s:CN = 127.0.0.1
   i:CN = 127.0.0.1
---
Server certificate
-----BEGIN CERTIFICATE-----
# ... ommited
---
read R BLOCK
```

You can then type in your `ncat` and `openssl` windows and see the messages on the other side. Neat!

### Service First

What if you already have a custom service running that you want to expose through `autovnet`?

Don't worry, that's easy, too.

On Kali, let's set up a python HTTP file server:
```bash
$ mkdir -p /tmp/srv
$ cd /tmp/srv
$ python3 -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```

By default, it listens `:8000` locally.
But HTTP is usually `:80` in the real world, so we would prefer the Blue Team to see `:80`.

The great news is that you can use any ports you want when setting up your listeners,
so you can always ensure the Blue Team will see the best-looking traffic possible,
regardless on what ports your services are actually bound to locally.

```bash
$ autovnet rtfm listen 80 -p 8000
[+] autovnet rtfm listen paltry_pop
[+] paltry_pop | 10.202.98.172:80 => 127.0.0.1:8000
```
Here, the `80` is the remote port we would like to use, and `-p 8000` is the local port we would like to use.

Now everyone can access our local file server at the adress from the left side of the output:
```bash
$ curl 10.202.98.172:80
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
</ul>
<hr>
</body>
</html>
```

Be careful what you share - all Blue and Red players could access it :)
If you need to share a single, secret file, see [Sharing Secrets](#sharing-secrets).
If you need to collaborate securely with other Red players, see [Collaboration](#collaboration).

## Automatic / Easy

See [pwn integrations](../pwn/README.md) for the easy way to catch callbacks for popular tools!

