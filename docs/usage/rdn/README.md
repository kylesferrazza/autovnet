# RDN

[TOC]

## Intro

`RDN` stands for `RTFM Defined Networking`, which stands for `Red Team Force Multiplication - Defined Networking`.

A key insight / largely unique feature of `autovnet` is that _you_ the Red Teamer / player define (or have the power to define)
exactly how your traffic looks from the perspective of the Blue Team.

(Don't worry, you don't have to specify every detail - the defaults are good :) )

`autovnet` employs multiple techniques to allow you to control your traffic.
Each has its own use case, properties, and possibly limitations.

Use the technique that matches your needs.

## Techniques

### Listen

The `listen` primitive allows you to accept _inbound_ `TCP` traffic on a randomized "public" `IP`.

* You run a `TCP` service locally
* `listen` generates a random "public" `IP`
* The target connects to the random "public" `IP`
* The connection magically reaches your local `TCP` service
* You can expose the same service via multiple listeners, or create as many concurrent listeners as you want

If you just want to catch a `TCP`-based callback, this should be your first choice.

See [usage](listen.md) and [implementation details](../../rtfm/autovnet.md#listen-reverse-callback).

### Proxy

The `proxy` primitive allows you send _outbound_ `TCP` "from" an obfuscated "public" IP.

* You `.proxy` your connection to the target
* The target gets a connection from a random, obfuscated IP
* Each time you `.proxy`, your "public" IP is randomized
* You can run as many `.proxy` commands concurrently as you want
* (Not every program is compatible with `.proxy`, but many are)

If you are creating a `TCP` connection to a target or doing a network scan, `.proxy` it!

See [usage](proxy.md) and [implementation details](../../rtfm/autovnet.md#proxy-forward-proxy).

### Tun

The `tun` primitive allows you to create a full, `1:1` `IP` mapping / alias with a randomized "public" `IP`.

It's basically like a `VPN` connection, but the "public" IP assigned by the `VPN` is randomized,
and you can change it at any time without reconnecting.

See [usage](tun.md) and [implementation details](../../rtfm/autovnet.md#tun-11-ip-mapping).

There are 2 variants: `Listen Tun`s and `Connect Tun`s.

#### Listen Tun

A listen `tun` can only accept inbound connections from targets.

* You are assigned a randomized, "public" `map_ip` (that you can change at any time)
* The target connects to whatever `IP` is your current `map_ip`
* Your `map_ip` is mapped `1:1` to a local `tun_ip` on your machine
* All traffic to the `map_ip` reaches the `tun_ip`
* You can accept callbacks using any `IP` protocol / ports
* You can run multiple listen `tun`s concurrently

If you need to catch non-`TCP`-based callbacks, this is what you want.

#### Connect Tun

A connect `tun` can also accept inbound connections, but also changes the source IP a target (or target network) will see for all `IP` traffic you generate.

* You specify target `IP` / route(s) when you create the `tun`
* (This will change the routes packets to these destinations will take - breaking any pre-existing connections to those destinations)
* New connections (any `IP` protocol) to one of the specified destinations will come "from" your randomized `map_ip`
* You can change your `map_ip` at will, and your connections (created after `tun` was created should not break
* You can have at most `1` connect `tun` per route destination (conflicting routes are bad)

If you need to send non-`TCP` traffic to a target, this is what you want

