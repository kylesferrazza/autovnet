# Forward Proxy (Red -> Blue)

[TOC]

## Intro

When connecting to a Blue Team system, we generally want to use an ephemeral IP that the Blue Team will never see again.
The real-world analog would be using an anonymization service like TOR, open proxies, VPN providers, a single-use VPS, or a botnet service.
Or, perhaps each connection is from a separate actor - realistic analysis of the TTPs used could cluster and attribute the emphemeral IPs back to the "actors" being emulated.

Because every connection / log in a Blue Team sees is a different, random IP, and the scoring service is equally like to use the same IPs in the future,
we effectively disincentivize the Blue Team from blocking every IP that touches their system.

The forward proxy technique is really a wrapper around `proxychains-ng` (4),
so see the documentation for `proxychains` to understand what you can and cannot proxy.

The TL;DR is you can proxy TCP traffic sent by dynamically linked binaries (not `golang` binaries, not statically linked binaries, not a `docker run` command, etc.).

## Generic `.proxy`

```bash
# Start a local web server to be the target for scanning
$ python3 -m http.server 8000
```

When using the full command syntax (below), `--` is used as a special separator.
All options to the _left_ are `autovnet` options, and everything to the right is the command to run

```bash
# Attacker
# Scan the target through proxychains (128 random IPs)

# The command that will be executed is: 'nmap -sT -sV -sC -p8000 127.0.0.1'
$ autovnet rtfm proxychains run -c 128 -- nmap -sT -sV -sC -p8000 127.0.0.1

# Observe source IPs in server log
```

Alternatively, you can use the `.proxy` or `.proxy-nmap` helpers.

`.proxy` is a simple wrapper that can be used to proxy anything that `proxychains` can proxy.

```bash
# Example .proxy syntax
$ .proxy nmap -sT -sV -sC -p8000 127.0.0.1
```

## `.proxy-ssh`

`.proxy-ssh` is `ssh`, but proxied, and has some helpful default options.

As shown in this example, each time we log in, it's from a different, random source IP:
```bash
$ .proxy-ssh victim@target
$ w victim
15:22:55 up  5:34,  9 users,  load average: 0.22, 0.14, 0.14
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
victim   pts/5    10.233.72.18     15:22    6.00s  0.01s  0.00s w victim

exit

$ .proxy-ssh victim@target
$ w victim
15:24:00 up  5:35,  9 users,  load average: 0.11, 0.13, 0.14
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
victim   pts/5    10.204.194.8     15:23    0.00s  0.01s  0.00s w victim
```

## Integrations

`autovnet` includes integrations for popular Red Teaming / pen testing tools, with more coming soon!

A common use case for an integration is to use `autovnet` to set up your C2 infrastructure / IP addresses,
then configure a tool or framework to use those `autovnet` managed IPs.

Have a popular tool you'd love to see an integration for? Open an issue!

