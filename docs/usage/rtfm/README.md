# RTFM

[TOC]

## Intro

See [RTFM](../../rtfm/README.md) for background on `RTFM`.

This section of the documentation is for commands that don't fit well in other categories.

## Advanced Persistent Thread (APT) Emulation

`autovnet` allows you to roleplay as an [APT](https://csrc.nist.gov/topics/security-and-privacy/risk-management/threats/advanced-persistent-threats),
with a particular set of [TTPs](https://csrc.nist.gov/glossary/term/Tactics_Techniques_and_Procedures) and [IoCs](https://csrc.nist.gov/glossary/term/ioc) (IP addresses, `dns` names, etc.)

See [apt.md](apt.md) for more details.

## `pasta`

An easy, beautiful way to maintain and publish certain kinds of documentation, like TTP playbooks that are ready to execute.

See [pasta.md](pasta.md) for more details.
