# Pasta

[TOC]

## Intro

Do you have instructions, documentation, or attack plans / playbooks that describe your Red Team tradecraft?

`autovnet pasta` is a structured way you can manage and access these things.

The basic workflow is

1. Define your `pasta` in the correct format (collection of `.md` files, primarily), tracked in `git`

    - See [examples/pasta](../../../examples/pasta) for some examples

2. All players `clone` / `pull` the repo locally

3. All players run `autovnet pasta serve` on the directory, which will `render` the `pasta` locally in their browser


    - `pasta` is in the eye of the beholder: it can be customized at `render`-time to use random values (like `autovnet` IPs) or player-specific values.

    - everyone can `serve` the same `pasta`, and get a result that is personalized to their system, e.g. commmands that are 100% ready to copy / paste into a terminal, with no conflicts between players


## Security Warning

**DO NOT SERVE UNTRUSTED PASTA**

`pasta` provides **aribtrary code execution** _as a feature_.

  * Why? `pasta` can customize itself to be ready to copy-paste. This requires the ability to run shell commands, scripts, etc.

If a malicious actor provides `pasta` that you later `serve`, they will be able to execute code on your system.


## Templating

You don't have to use it, but files in a `pasta` directory are rendered as [jinja](https://jinja.palletsprojects.com/) templates.

This makes it possible to create placeholders for variables using `jinja` syntax (e.g. `{{FOO}}`)


## Static Site

The actual static website that you view in your browser is created with [mkdocs](https://www.mkdocs.org/).


## `pasta` Chef Guide

Creating some `pasta`?

Start with the [examples](../../../examples/pasta).

This is just a quick reference guide describing the current `pasta` rules (which are subject to change in future releases).

### Magic Variables

There are some magic `jinja` variables that are treated specially - i.e. you can refer to them in your `pasta`, and `autovnet` will provide values for you.

The values are generated once per render scope (usually within a particular file), or within a particular directory, if you invoke the variable within a `pasta.env` file.

So you can use a named variable like `LHOST_FOO` in multiple places within the same file, and they will all be set to the same, random value.

| Variable      | Description |
| -----------   | ----------- |
| `LHOST*`      | Random IP from `listen` pool, like `autovnet net ip -C listen`    |
| `PHOST*`      | Random IP from the `proxy` pool, like `autovnet net ip -C proxy`  |
| `THOST*`      | Random IP from the `tun` pool, like `autovnet net ip -C tun`      |
| `LPORT*`      | Random, ephemeral port. (If you want good-looking ports like :443, just use :443, not `LPORT`)    |
| `CODENAME*`   | Random codename (`foo_bar`), like `autovnet rtfm codename`        |
| `CURRENT`     | Path to the current file being rendered                           |
| `CURRENT_DIR` | Path to the directory containing the current file being rendered. (This helps locate helper scripts included in the `pasta`)  |


### Defining Variables

Magic variables can be used without being defined.

But you can define variables in your `pasta` to make it easier to make changes later, or allow variables to be dynamically computed in some way.

Variables can be defined in `.env` files.
A `.env` file looks something like:
```bash
VARIABLE_NAME_1=VALUE_1
VARIABLE_NAME_2=VALUE_2
```
and so on

There are currently 2 flavors:

* `foo.md.env`: when `foo.md` is rendered, any variables defined in `foo.md.env` will be available

* `foo/pasta.env`: when the `foo` directory is rendered, any variables defined in `foo/pasta.env` will be available

    - (experimental) variables propogate downward into subdirectories. More specific values (defined in a deeper subdirectory) have precidence

### `shell_exec`

`autovnet pasta` provides code execution **as a feature**. See [Security Warning!](#security-warning).

Use these feature sparingly, but it can be very helpful, especially for resolving system-specific values.

See [mini example](../../../examples/pasta/mini/) for some basic examples.


### Tips


#### Code Blocks

Code blocks will have a `copy` button that copies the entire text; very convenient.

So, when you are writing code blocks, include a code block that contains **only** the exact text that needs to be pasted (no `$` prompt, no example output, etc.)

You can provide example output in a different block, if desired.

#### `pasta.env`

Try to define all (non-magic) variables all all calls to `shell_exec` in a `pasta.env` file.

#### `shell_exec`

If there is a really complicated `shell_exec`, try to create a helper script instead (you can find it with `CURRENT_DIR`, and use `shell_exec` to invoke it.

`python3` is preferable for any scripts, since `autovnet` requires it, but `bash` can be used, if appropriate.

## Commands

### `autovnet pasta serve`

`autovnet` includes some example `pasta` ... `dish`es (?) that you can `serve`.

```bash
$ autovnet pasta serve examples/pasta/playbook
...
[*] Waiting for docs (http://127.0.0.1:35464) ...
[+] docs are ready!
```

Your browser will open, showing the rendered `pasta`.

Note: You get new random values each time you `serve`.

So, depending on your use case, you may need to take notes (using [hedgedoc](../collaboration/hedgedoc.md)),
to keep track of exactly what you did and when.

