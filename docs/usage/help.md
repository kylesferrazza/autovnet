# `autovnet`

**Usage**:

```console
$ autovnet [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `-v, --verbose`: Increase log verbosity.  [default: False]
* `--config TEXT`: Path to config file.  [default: ~/.autovnet/config.yaml]
* `--help`: Show this message and exit.

**Commands**:

* `apt`: Manage APT profiles
* `config`: Manage autovnet config
* `docs`: Manage autovnet offline docs
* `net`: Manage IP network simulation
* `pasta`: Render (copy)pasta for easy copy-paste
* `rtfm`: Unleash the power of RTFM (Red Team Force...
* `tmp`: Create tmp dirs
* `type`: Execute command, writing a payload to STDIN.

## `autovnet apt`

Manage APT profiles

**Usage**:

```console
$ autovnet apt [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `dns`: Look up a DNS name registered for an IP in...
* `geo`: Look up a region associated with an IP in the...
* `init`: Paritiion the autovnet network space into...
* `new`: Generate a new APT profile * Logically, an...
* `register`: Register DNS names for APT infrastructure
* `shell`: Activate an APT profile in the current shell
* `show`: Show the current APT profile
* `unregister`: Unregister DNS records for current APT...

### `autovnet apt dns`

Look up a DNS name registered for an IP in the current APT profile

**Usage**:

```console
$ autovnet apt dns [OPTIONS] IP
```

**Arguments**:

* `IP`: IP to look up DNS name for.  [required]

**Options**:

* `-D, --payload-dir TEXT`: Directory for APT profiles.  [default: /opt/rtfm/apt/profiles]
* `-p, --profile TEXT`: Codename of the APT profile.
* `--help`: Show this message and exit.

### `autovnet apt geo`

Look up a region associated with an IP in the current APT profile

**Usage**:

```console
$ autovnet apt geo [OPTIONS] IP
```

**Arguments**:

* `IP`: IP to look up geo region for.  [required]

**Options**:

* `-D, --payload-dir TEXT`: Directory for APT profiles.  [default: /opt/rtfm/apt/profiles]
* `-p, --profile TEXT`: Codename of the APT profile.
* `--help`: Show this message and exit.

### `autovnet apt init`

Paritiion the autovnet network space into regions and APT families


* A "region" is an arbitrary partion that simulates the concept of a geolocatable region (like a country)

* An "APT Family" is an arbitrary partiion that simulates the concept of a logical Advanced Persisent Threat actor / sponsor
    - E.g. similar to the "Suspected attribution" from https://www.mandiant.com/resources/apt-groups
    - an APT Family is associated with a set of infrastructure, which could span many regions

* An "APT" or "APT Profile" is a single, named APT that is logically responsible for a set of activities
    - an APT has a set of infrastructure that could overlap with other APTs within the same APT family
    - an APT could run multiple "campaigns" / attack chains with unique TTPs and IoCs

From the perspective of the defender, correct attribution / clustering is intentionally difficult,
    and depending on the amount of data availalbe, may not be possible.

This is realistic, see the concept of UNC groups: https://www.mandiant.com/resources/unc-groups

**Usage**:

```console
$ autovnet apt init [OPTIONS]
```

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-p, --new-prefix INTEGER`
* `-r, --regions-file TEXT`: Read regions from a file instead of using the defaults
* `-f, --families-file TEXT`: Read APT regions from a file instead of using the defaults
* `-G, --geo-percent INTEGER`: Percent of regions that are associated with a particular region  [default: 90]
* `-F, --family-percent INTEGER`: Percent of regions that are associated with a particular APT family  [default: 80]
* `-D, --out-dir TEXT`: Directory to save output.  [default: ~/.autovnet/apt]
* `--help`: Show this message and exit.

### `autovnet apt new`

Generate a new APT profile


* Logically, an APT Profile is a single actor responsible for a set of campaigns or attacks

* In practice, an APT Profile is a collection of infrastructure randomly sampled
    from the pool of infrastructure (e.g. IP addresses) associated with the APT family

* When you use autovnet with an APT Profile active, autovnet will draw random IPs from your APT's infrastrucutre

    - This means that your IPs are categorized and more likely to be reused, making it possible for a defender to attribute a
        set of related actions and multiple IoCs to a single, logical actor

* You are free to share sufficiently large APT Profiles between players,
    or each player can generate their own profile, depending on your goals and game dynamics

**Usage**:

```console
$ autovnet apt new [OPTIONS]
```

**Options**:

* `-f, --families-file TEXT`: Read APT regions from a file instead of using the defaults  [default: ~/.autovnet/apt/families.list]
* `-r, --regions-file TEXT`: Read regions from a file instead of using the defaults
* `-t, --templates-file TEXT`: Read APT profile templates from this file  [default: ~/.autovnet/apt/templates.yaml]
* `-d, --families-dir TEXT`: Directory containing FAMILY.list files  [default: ~/.autovnet/apt/families]
* `-F, --family TEXT`: Specify the APT family to use
* `-o, --out-file TEXT`: Path to save generated profile
* `-s, --size [one|min|tiny|small|default|medium|large|huge]`: [default: default]
* `-N, --no-dns`: Do not pre-register DNS names, even if template has dns: true.  [default: False]
* `--no-prompt`: Do not attempt to update shell prompt when this profile is activated.  [default: False]
* `-D, --out-dir TEXT`: Directory to save output.  [default: ~/.autovnet/apt]
* `--help`: Show this message and exit.

### `autovnet apt register`

Register DNS names for APT infrastructure

**Usage**:

```console
$ autovnet apt register [OPTIONS]
```

**Options**:

* `-D, --payload-dir TEXT`: Directory for APT profiles.  [default: /opt/rtfm/apt/profiles]
* `-p, --profile TEXT`: Codename of the APT profile.
* `--help`: Show this message and exit.

### `autovnet apt shell`

Activate an APT profile in the current shell

**Usage**:

```console
$ autovnet apt shell [OPTIONS] CODENAME
```

**Arguments**:

* `CODENAME`: Codename of the APT to switch to  [required]

**Options**:

* `-D, --payload-dir TEXT`: Directory to create tmp dir in.  [default: /opt/rtfm/apt/profiles]
* `-s, --shell TEXT`: Alternate shell to use.
* `--no-rc`: Do not attempt to modmodify the shell's rc file, needed to update shell prompt  [default: False]
* `--help`: Show this message and exit.

### `autovnet apt show`

Show the current APT profile

**Usage**:

```console
$ autovnet apt show [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Codename of the APT to switch to.

**Options**:

* `-D, --payload-dir TEXT`: Directory to create tmp dir in.  [default: /opt/rtfm/apt/profiles]
* `--help`: Show this message and exit.

### `autovnet apt unregister`

Unregister DNS records for current APT profile

**Usage**:

```console
$ autovnet apt unregister [OPTIONS]
```

**Options**:

* `-D, --payload-dir TEXT`: Directory for APT profiles.  [default: /opt/rtfm/apt/profiles]
* `-p, --profile TEXT`: Codename of the APT profile.
* `--help`: Show this message and exit.

## `autovnet config`

Manage autovnet config

**Usage**:

```console
$ autovnet config [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `docker`: Suggest a reasonable docker daemon...
* `export`: Export the current autovnet config The result...
* `import`: Import an autovnet config from the autovnet...
* `init`: Intialize the autovnet config Run once on the...
* `show`: Display the current autovnet config

### `autovnet config docker`

Suggest a reasonable docker daemon configuration

With now options, displays a suggested config that you can tailor for your environment.
To install it, use the --install flag, or manually place the config in `/etc/docker/daemon.json`
and restart the docker service.

If you already have a daemon.json, you probably need to manually merge the configs, or you may
temporarily break your docker install.

This configuration will prevent you from running more than ~16 containers in the same network
namespace.

This matters to almost no one, but if you have some crazy app with tons of containers,
it's up to you to modify the config as appropriate for your use case.

**Usage**:

```console
$ autovnet config docker [OPTIONS]
```

**Options**:

* `-i, --install`: Install the daemon.json, unless it already exists.  [default: False]
* `-f, --force`: If installing, overwrite any existing daemon.json.  [default: False]
* `--help`: Show this message and exit.

### `autovnet config export`

Export the current autovnet config

The result is an encrypted blob that can be imported by an autovnet client

**Usage**:

```console
$ autovnet config export [OPTIONS]
```

**Options**:

* `-i, --in TEXT`: Path to the config directory.  [default: ~/.autovnet]
* `-s, --secure`: Use a secure (but hard to type) password.  [default: False]
* `-S, --share`: Share the config file after exporting. (The first share is slow to start, be patient.)  [default: False]
* `-o, --out TEXT`: Path to store the encrypted config blob.  [default: autovnet-config.etgz]
* `--help`: Show this message and exit.

### `autovnet config import`

Import an autovnet config from the autovnet server

Generally, the game administrator running the autovnet server will provide the command you should run.

**Usage**:

```console
$ autovnet config import [OPTIONS] [IN_PATH]
```

**Arguments**:

* `[IN_PATH]`: Path to the config blob. Can be a url.  [default: autovnet-config.etgz]

**Options**:

* `-o, --out TEXT`: Parent directory to unpack the config directory in.  [default: ~]
* `-f, --force`: Delete the output config directory if it already exists.  [default: False]
* `--help`: Show this message and exit.

### `autovnet config init`

Intialize the autovnet config

Run once on the autovnet server, then export the config for clients to import

**Usage**:

```console
$ autovnet config init [OPTIONS]
```

**Options**:

* `-u, --rtfm-user TEXT`: SSH user for port forwards.  [default: x]
* `-s, --rtfm-host TEXT`: SSH host for port forwards.
* `-k, --rtfm-key TEXT`: SSH key for port forwards.  [default: ~/.autovnet/id_rsa]
* `-a, --rtfm-pubkey TEXT`: SSH pubkey for port forwards.  [default: ~/.autovnet/id_rsa.pub]
* `-p, --ssh-port TEXT`: Port for ssh server.  [default: 2222]
* `-P, --socks-port TEXT`: Port for socks server.  [default: 1080]
* `-q, --password TEXT`: Password for socks5 proxy.
* `-n, --network TEXT`: A network block to simulate.  [default: 240.0.0.0/4]
* `-o, --out TEXT`: Initalize the config directory at an alternate path.  [default: ~/.autovnet]
* `-l, --length INTEGER`: Length of generated password(s) in bytes.  [default: 8]
* `-f, --force`: Delete the output config directory if it already exists.  [default: False]
* `--help`: Show this message and exit.

### `autovnet config show`

Display the current autovnet config

**Usage**:

```console
$ autovnet config show [OPTIONS]
```

**Options**:

* `--help`: Show this message and exit.

## `autovnet docs`

Manage autovnet offline docs

**Usage**:

```console
$ autovnet docs [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `build`: Create the sacred texts Build the offline...
* `serve`: Read the sacred texts View the offline...

### `autovnet docs build`

Create the sacred texts

Build the offline autovnet documents

**Usage**:

```console
$ autovnet docs build [OPTIONS] [DOCS_DIR]
```

**Arguments**:

* `[DOCS_DIR]`: Directory to save static site files.  [default: /opt/rtfm/docs]

**Options**:

* `-f, --force`: Delete the docs dir and rebuild it.  [default: False]
* `-s, --src-dir TEXT`: Source dir to render with mkdocs.
* `--help`: Show this message and exit.

### `autovnet docs serve`

Read the sacred texts

View the offline autovnet documents (building if needed)

**Usage**:

```console
$ autovnet docs serve [OPTIONS] [DOCS_DIR]
```

**Arguments**:

* `[DOCS_DIR]`: Directory to serve.  [default: /opt/rtfm/docs]

**Options**:

* `-d, --dest-ip TEXT`: Bind IP for webserver.  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: Bind IP for webserver.  [default: 4280]
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-b, --build`: If set, force a rebuild first.  [default: False]
* `-s, --src-dir TEXT`: Source dir to render with mkdocs.
* `--help`: Show this message and exit.

## `autovnet net`

Manage IP network simulation

**Usage**:

```console
$ autovnet net [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `down`: Stop simulating a network
* `ip`: Generate randomzied IP addresses
* `socks`: Generate socks urls for rtfm server
* `test`: Test autovnet network connectivity
* `up`: Start simulating a network

### `autovnet net down`

Stop simulating a network

**Usage**:

```console
$ autovnet net down [OPTIONS]
```

**Options**:

* `-n, --network TEXT`: A network block to simulate.
* `--help`: Show this message and exit.

### `autovnet net ip`

Generate randomzied IP addresses

**Usage**:

```console
$ autovnet net ip [OPTIONS]
```

**Options**:

* `-n, --network TEXT`: A network block to draw from.
* `-c, --count INTEGER`: The number of IPs to generate. 0 returns all (or infinite) IPs.  [default: 1]
* `-u, --unique`: Do not repeat IP addresses.  [default: False]
* `-p, --port TEXT`: Port for url
* `-P, --proto TEXT`: Protocol prefix for url
* `-q, --path TEXT`: Path suffix for url
* `-a, --auth TEXT`: user[:password] string for url
* `-C, --category TEXT`: Category of intrastructure to draw the IP from. Only relevant if using an APT profile.
* `--help`: Show this message and exit.

### `autovnet net socks`

Generate socks urls for rtfm server

**Usage**:

```console
$ autovnet net socks [OPTIONS]
```

**Options**:

* `-c, --count INTEGER`: The number of unique IPs to generate. 0 returns all IPs.  [default: 1]
* `-u, --unique`: Do not repeat IP addresses.  [default: False]
* `--help`: Show this message and exit.

### `autovnet net test`

Test autovnet network connectivity

**Usage**:

```console
$ autovnet net test [OPTIONS]
```

**Options**:

* `-n, --network TEXT`: A network block to draw from.
* `-c, --count INTEGER`: The number of tests to run.  [default: 16]
* `-k, --size INTEGER`: The number of IPs to allocate for each test.  [default: 1]
* `-p, --port INTEGER`: The remote port to test.  [default: 443]
* `-t, --timeout INTEGER`: Timeout for socket operations.  [default: 5]
* `-s, --streams INTEGER`: Number of sequential streams to create for each test.  [default: 1]
* `-d, --data INTEGER RANGE`: Amount of data to send per stream.  [default: 256]
* `-D, --no-deconflict`: Disable IP deconfliction. Much faster test, but will fail if a conflict happens.
* `--help`: Show this message and exit.

### `autovnet net up`

Start simulating a network

**Usage**:

```console
$ autovnet net up [OPTIONS]
```

**Options**:

* `-n, --network TEXT`: A network block to simulate.
* `--help`: Show this message and exit.

## `autovnet pasta`

Render (copy)pasta for easy copy-paste

**Usage**:

```console
$ autovnet pasta [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `serve`: Render a directory of (copy)pasta.

### `autovnet pasta serve`

Render a directory of (copy)pasta.

It must be in autovnet's pasta format.

WARNING: Do no render pasta from untrusted sources.
This provides shell_exec as a feature, and is not secure against malicious input.

Do not run this command in a CI / CD pipeline that untrusted users can trigger.

(Be nice.)

**Usage**:

```console
$ autovnet pasta serve [OPTIONS] PASTA_PATH
```

**Arguments**:

* `PASTA_PATH`: Pasta directory to render  [required]

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-D, --payload-dir TEXT`: Directory to save temporary pasta.  [default: /opt/rtfm/tmp]
* `-o, --site-path TEXT`: [ADVANCED] Final directory for saving rendered pasta.
* `-d, --debug`: Enable more debug output.  [default: False]
* `-J, --no_jinja`: Skip jinja rendering.  [default: False]
* `-B, --only_build`: [ADVANCED] Only build, do not serve.  [default: False]
* `--help`: Show this message and exit.

## `autovnet rtfm`

Unleash the power of RTFM
(Red Team Force Multiplication)

**Usage**:

```console
$ autovnet rtfm [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `codename`: Generate random codenames Use for fun, to...
* `cyberchef`: Securely share a cyberchef instance.
* `decrypt`: Decrypt a file from another player
* `dns`: Secure, dns control.
* `download`: Download a file Handy for downloading and...
* `drawio`: Securely share a drawio server.
* `encrypt`: Encrypt a file for other players
* `focalboard`: Securely share a focalboard server.
* `fs`: Share or mount file systems between players.
* `gitea`: Securely share a gitea server.
* `hedgedoc`: Securely share a hedgedoc collaborative notes...
* `kali`: Run a shell based on Kali Linux
* `listen`: Start a remote listener on the RTFM server...
* `matrix`: Securely share a matrix server.
* `merlin`: Use merlin...
* `msf`: Use metasploit-framework.
* `msfdb`: Securely share msfdb between players.
* `mux-connect`: Force mux connect the autovnet server Only...
* `mux-disconnect`: Force mux disconnect from the autovnet server...
* `nextcloud`: Securely share a nextcloud instance.
* `owncast`: Securely share an owncast instance.
* `password`: Generate random passwords Like the codename...
* `proxychains`: Use proxychains to (obfuscate source IP).
* `pwncat`: Start a 'remote' pwncat-cs listener pwncat-cs...
* `server`: [Advanced] Manage the autovrtfm server.
* `share`: Share a directory over HTTP and HTTPS The...
* `sliver`: Use sliver...
* `socat`: [Experimental] Run an arbitrary socat command...
* `svc`: Share or mount services between players.
* `tun`: Secure, virtual overlay networking.
* `vnc`: Securely screenshare between players.

### `autovnet rtfm codename`

Generate random codenames

Use for fun, to help with naming, or even for human-friendly temporary passwords

**Usage**:

```console
$ autovnet rtfm codename [OPTIONS]
```

**Options**:

* `-c, --count INTEGER`: Number of codenames to generate.  [default: 1]
* `-t, --tokens INTEGER`: Number of tokens to include in the codename.  [default: 2]
* `-j, --join TEXT`: Delimiter for joining tokens  [default: _]
* `--help`: Show this message and exit.

### `autovnet rtfm cyberchef`

Securely share a cyberchef instance.

**Usage**:

```console
$ autovnet rtfm cyberchef [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `export`: Run a self-hosted instance of cyberchef...
* `mount`: Mount and connect to another player's...

#### `autovnet rtfm cyberchef export`

Run a self-hosted instance of cyberchef

CyberChef is a "Cyber Swiss Army Knife" and great for CTF-like encoding / decoding tasks.

**Usage**:

```console
$ autovnet rtfm cyberchef export [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-d, --dest-ip TEXT`: [ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: Local bind port for the server. (default: random).
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/cyberchef]
* `--help`: Show this message and exit.

#### `autovnet rtfm cyberchef mount`

Mount and connect to another player's cyberchef service

**Usage**:

```console
$ autovnet rtfm cyberchef mount [OPTIONS] OPT
```

**Arguments**:

* `OPT`: IP:PORT or codename to mount.  [required]

**Options**:

* `-d, --dest-ip TEXT`: [ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: [ADVANCED] Destination port you will use to access the service (default: random).
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/cyberchef]
* `-C, --set-codename TEXT`: Set the codename for the mount.
* `--help`: Show this message and exit.

### `autovnet rtfm decrypt`

Decrypt a file from another player

**Usage**:

```console
$ autovnet rtfm decrypt [OPTIONS] PATHS...
```

**Arguments**:

* `PATHS...`: Files to decrypt.  [required]

**Options**:

* `-P, --prompt`: Prompt for password to use for decryption.  [default: False]
* `-p, --password TEXT`: Password to use for decryption
* `-o, --out TEXT`: [Repeatable] Path to save decrypted file. Default: overwrite source.
* `--help`: Show this message and exit.

### `autovnet rtfm dns`

Secure, dns control.

**Usage**:

```console
$ autovnet rtfm dns [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `name`
* `register`: Create DNS records Supported input format: *...
* `server`: Run a DNS server
* `unregister`: Uncreate DNS records

#### `autovnet rtfm dns name`

**Usage**:

```console
$ autovnet rtfm dns name [OPTIONS]
```

**Options**:

* `-c, --count INTEGER`: The number of dns names to generate.  [default: 1]
* `-t, --tokens INTEGER`: The number of tokens to generate.  [default: 2]
* `--help`: Show this message and exit.

#### `autovnet rtfm dns register`

Create DNS records

Supported input format:
    * None / -c / --count => random domain, random IP
    * IP only => random domain
    * domain only => random ip
    * DOMAIN:IP[:IP][:IP] => create a record for DOMAIN that maps to one or more IPs

The game configuration contains a forced TLD that all registrations will have,
to prevent accidental (or intentional) hijacking of real domains during online games.

**Usage**:

```console
$ autovnet rtfm dns register [OPTIONS] [REGISTER]...
```

**Arguments**:

* `[REGISTER]...`: [DOMAIN:]IP[:IP...] mapping to register (repeatable)

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-d, --dest-ip TEXT`: [ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: [ADVANCED] Destination port you will use to access the service (default: random).
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/dns]
* `-C, --set-codename TEXT`: Set the codename for the mount.
* `-m, -mount TEXT`: [ADVANCED] IP:PORT or codename to mount.
* `-c, --count INTEGER`: The number of remote IP addresses to allocate.  [default: 0]
* `-d, --detach`: [ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.  [default: False]
* `-q, --quiet`: Suppress most output.  [default: False]
* `-k, --keep-mount`: [ADVANCED] Keep the generated mount dir instead of deleting it.  [default: False]
* `-i, --in-file TEXT`: Read registrations from a file.
* `-o, --out-file TEXT`: Write registrations to a file.
* `--help`: Show this message and exit.

#### `autovnet rtfm dns server`

Run a DNS server

**Usage**:

```console
$ autovnet rtfm dns server [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-N, --new`: [ADVANCED] Force creating a new DNS server with a different codename.  [default: False]
* `-A, --api-dest-ip TEXT`: [ADVANCED] Bind address for the API (anything but 127.X.X.X may allow anyone to access!).  [default: 127.0.0.1]
* `-P, --api-dest-port INTEGER`: Local bind port for the API.
* `-d, --dest-ip TEXT`: [ADVANCED] Bind address for the DNS server.
* `-p, --dest-port INTEGER`: Local bind port for the server.  [default: 53]
* `-R, --api-remote-port INTEGER`: [ADVANCED] Remote bind port for dns API. Not intended to be set by humans.
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/dns]
* `--init-sql TEXT`: [ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.
* `--force-init-sql`: [ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.  [default: False]
* `-i, --backup-interval INTEGER`: Seconds between automatic backups (database exports).  [default: 300]
* `-r, --backup-retain INTEGER`: Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.  [default: 16]
* `-Q, --backup-quiet`: If set, do not print backup confirmation messages.  [default: False]
* `-B, --no-backup`: If set, do not make automated backups of the server contents.  [default: False]
* `--clean`: [ADVANCED] Do a clean start, with a clean database.  [default: False]
* `--help`: Show this message and exit.

#### `autovnet rtfm dns unregister`

Uncreate DNS records

**Usage**:

```console
$ autovnet rtfm dns unregister [OPTIONS] [REGISTER]...
```

**Arguments**:

* `[REGISTER]...`: [DOMAIN:]IP[:IP...] mapping to register (repeatable)

**Options**:

* `-d, --dest-ip TEXT`: [ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: [ADVANCED] Destination port you will use to access the service (default: random).
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/dns]
* `-C, --set-codename TEXT`: Set the codename for the mount.
* `-m, -mount TEXT`: [ADVANCED] IP:PORT or codename to mount.
* `-i, --in-file TEXT`: Read registrations from a file.
* `-q, --quiet`: Suppress most output.  [default: False]
* `-k, --keep-mount`: [ADVANCED] Keep the generated mount dir instead of deleting it.  [default: False]
* `--help`: Show this message and exit.

### `autovnet rtfm download`

Download a file

Handy for downloading and decrypting files between players

**Usage**:

```console
$ autovnet rtfm download [OPTIONS] URL
```

**Arguments**:

* `URL`: URL to download.  [required]

**Options**:

* `-P, --prompt`: Prompt for password to use for decryption.  [default: False]
* `-p, --password TEXT`: Password to use for decryption.
* `-o, --out TEXT`: Path to save download.
* `--help`: Show this message and exit.

### `autovnet rtfm drawio`

Securely share a drawio server.

**Usage**:

```console
$ autovnet rtfm drawio [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `export`: Run a self-hosted instance of drawio (a.k.a...
* `mount`: Mount and connect to another player's drawio...

#### `autovnet rtfm drawio export`

Run a self-hosted instance of drawio (a.k.a diagrams.net)

You can make pretty diagrams / network maps in your browser, then upload them to hedgedoc as SVGs
as part of your notes

**Usage**:

```console
$ autovnet rtfm drawio export [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-d, --dest-ip TEXT`: [ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: Local bind port for the server. (default: random).
* `-s, --https`: Use HTTPs instead of HTTP (not really more secure here).  [default: False]
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/drawio]
* `--help`: Show this message and exit.

#### `autovnet rtfm drawio mount`

Mount and connect to another player's drawio service

**Usage**:

```console
$ autovnet rtfm drawio mount [OPTIONS] OPT
```

**Arguments**:

* `OPT`: IP:PORT or codename to mount.  [required]

**Options**:

* `-d, --dest-ip TEXT`: [ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: [ADVANCED] Destination port you will use to access the service (default: random).
* `-s, --https`: Use HTTPs instead of HTTP (not really more secure here).  [default: False]
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/drawio]
* `-C, --set-codename TEXT`: Set the codename for the mount.
* `--help`: Show this message and exit.

### `autovnet rtfm encrypt`

Encrypt a file for other players

**Usage**:

```console
$ autovnet rtfm encrypt [OPTIONS] PATHS...
```

**Arguments**:

* `PATHS...`: Files to encrypt.  [required]

**Options**:

* `-P, --prompt`: Prompt for password to use for encryption.  [default: False]
* `-p, --password TEXT`: Password to use for encryption. Default: generate one
* `-s, --secure`: If generating a password, make it secure. Default: easy to type  [default: False]
* `-o, --out TEXT`: [Repeatable] Path to save encrypted file. Default: overwrite source.
* `--help`: Show this message and exit.

### `autovnet rtfm focalboard`

Securely share a focalboard server.

**Usage**:

```console
$ autovnet rtfm focalboard [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `export`: Run a self-hosted instance of focalboard...
* `mount`: Mount and connect to another player's...

#### `autovnet rtfm focalboard export`

Run a self-hosted instance of focalboard

Focalboard is an open source, self-hosted alternative to Trello, Notion, and Asana.
You can make kanban boards, tasks, to-do lists, etc. to keep your team organized!
(The "Project Tasks" board type is the best for simple task tracking.)

The first user to register (hopefully you, the export-er) is the admin,
and must generate an invite link so other players can register.

1. Register with my_handle@x.localhost and a random, unique password
2. Click the Focalboard logo
3. Invite Users
4. Share the mount instructions and invite link with other players

**Usage**:

```console
$ autovnet rtfm focalboard export [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-d, --dest-ip TEXT`: [ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: Local bind port for the server. (default: random).
* `-s, --https`: Use HTTPs instead of HTTP (not really more secure here).  [default: False]
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/focalboard]
* `--init-sql TEXT`: [ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.
* `--force-init-sql`: [ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.  [default: False]
* `-i, --backup-interval INTEGER`: Seconds between automatic backups (database exports).  [default: 300]
* `-r, --backup-retain INTEGER`: Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.  [default: 16]
* `-Q, --backup-quiet`: If set, do not print backup confirmation messages.  [default: False]
* `-B, --no-backup`: If set, do not make automated backups of the server contents.  [default: False]
* `--help`: Show this message and exit.

#### `autovnet rtfm focalboard mount`

Mount and connect to another player's focalboard service

**Usage**:

```console
$ autovnet rtfm focalboard mount [OPTIONS] OPT
```

**Arguments**:

* `OPT`: IP:PORT or codename to mount.  [required]

**Options**:

* `-d, --dest-ip TEXT`: [ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: [ADVANCED] Destination port you will use to access the service (default: random).
* `-s, --https`: Use HTTPs instead of HTTP (not really more secure here).  [default: False]
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/focalboard]
* `-C, --set-codename TEXT`: Set the codename for the mount.
* `--help`: Show this message and exit.

### `autovnet rtfm fs`

Share or mount file systems between players.

**Usage**:

```console
$ autovnet rtfm fs [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `export`: Export a local directory for other players to...
* `mount`: Securely mount a network share exported by...

#### `autovnet rtfm fs export`

Export a local directory for other players to mount as a secure network share

Uses sshfs, secured by the autovnet key.
Red Teamers that mount this fs can see all exported files, edit, and delete them,
    but no one else can without access to the autovnet config secrets.
Data in transit is encrypted via ssh.

**Usage**:

```console
$ autovnet rtfm fs export [OPTIONS] [OPT]
```

**Arguments**:

* `[OPT]`: Directory, or codename to export. If not give, a new directory will be created by codename.

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-c, --count INTEGER`: The number of remote IP addresses to allocate.  [default: 1]
* `-p, --port INTEGER`: Remote port to use for sshfs.
* `-e, --env-file TEXT`: Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `-C, --codename`: Use codename completion instead of file completion.  [default: False]
* `-D, --payload-dir TEXT`: Directory to safe share output.  [default: /opt/rtfm/fs]
* `--help`: Show this message and exit.

#### `autovnet rtfm fs mount`

Securely mount a network share exported by another player

Generally, the creator of the share will provide you with the command you need to run.

**Usage**:

```console
$ autovnet rtfm fs mount [OPTIONS] OPT [MOUNTPOINT]
```

**Arguments**:

* `OPT`: IP:PORT or codename to mount.  [required]
* `[MOUNTPOINT]`: If given, custom location to mount the fs.

**Options**:

* `-C, --set-codename TEXT`: Set the codename for the mount.
* `-D, --payload-dir TEXT`: Directory to save metadata. Subdirectories will be created.  [default: /opt/rtfm/fs]
* `-m, --mirror`: Mirror the remote directory locally as a backup.  [default: False]
* `-M, --mirror-path TEXT`: Alternate path to store mirror. Do NOT make local changes in the mirror directory.
* `-i, --mirror-interval INTEGER`: Seconds between mirror updates.  [default: 60]
* `-k, --mirror-checksum`: Compute checksums to determine which files have changed (default uses modified times and size).  [default: False]
* `-q, --mirror-quiet`: Do not print confirmations after a successful mirror cycle  [default: False]
* `--help`: Show this message and exit.

### `autovnet rtfm gitea`

Securely share a gitea server.

**Usage**:

```console
$ autovnet rtfm gitea [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `export`: Run a self-hosted instance of gitea
* `mount`: Mount and connect to another player's gitea...

#### `autovnet rtfm gitea export`

Run a self-hosted instance of gitea

**Usage**:

```console
$ autovnet rtfm gitea export [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-d, --dest-ip TEXT`: [ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).  [default: 127.0.0.1]
* `-p, --http-dest-port INTEGER`: Local bind port for the server.  [default: 3080]
* `-P, --ssh-dest-port INTEGER`: Local bind port for the server.  [default: 3022]
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-s, --https`: [TODO] If set, use https (self-signed) to make security nerds feel better.  [default: False]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/gitea]
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `--init-sql TEXT`: [ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.
* `--force-init-sql`: [ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.  [default: False]
* `-i, --backup-interval INTEGER`: Seconds between automatic backups (database exports).  [default: 300]
* `-r, --backup-retain INTEGER`: Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.  [default: 16]
* `-Q, --backup-quiet`: If set, do not print backup confirmation messages.  [default: False]
* `-B, --no-backup`: If set, do not make automated backups of the server contents.  [default: False]
* `--app-ini TEXT`: [ADVANCED] Path to gitea app.ini template.  [default: ~/.venv/rtfm/lib/python3.10/site-packages/autovnet/docker/plugins/gitea/app.ini]
* `--help`: Show this message and exit.

#### `autovnet rtfm gitea mount`

Mount and connect to another player's gitea server

**Usage**:

```console
$ autovnet rtfm gitea mount [OPTIONS] [REMOTE_IP] [HTTP_REMOTE_PORT] [SSH_REMOTE_PORT]
```

**Arguments**:

* `[REMOTE_IP]`: Remote IP for service.
* `[HTTP_REMOTE_PORT]`: Remote PORT for http.
* `[SSH_REMOTE_PORT]`: Remote PORT for ssh.

**Options**:

* `-d, --dest-ip TEXT`: [ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!  [default: 127.0.0.1]
* `-p, --http-dest-port INTEGER`: [ADVANCED] Destination port to mount http.  [default: 3080]
* `-P, --ssh-dest-port INTEGER`: [ADVANCED] Destination port to mount ssh.  [default: 3022]
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-s, --https`: If set, use https (self-signed) to make security nerds feel better.  [default: False]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/gitea]
* `-C, --set-codename TEXT`: Codename for the mount.
* `--help`: Show this message and exit.

### `autovnet rtfm hedgedoc`

Securely share a hedgedoc collaborative notes server.

**Usage**:

```console
$ autovnet rtfm hedgedoc [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `export`: Run a hedgedoc server for note collaboration...
* `mount`: Mount another player's hedgedoc server...

#### `autovnet rtfm hedgedoc export`

Run a hedgedoc server for note collaboration with other players.

[HedgeDoc](https://hedgedoc.org/) is a real-time, collaborative markdown editor.

This command uses `autovnet rtfm svc export` to securely share the server with other players.

Unfortuately, other players cannot change the local port they will use to mount the service;
it must match the port configured by the host, or hedgedoc's internal URLs will be broken.
(The commands printed by this command are correct, just not customizable)

The general recommendation is to use hedgedoc for all real-time document collaboration and note-taking,
not to host "official" documentation, which should be in version control (e.g. git)

**Usage**:

```console
$ autovnet rtfm hedgedoc export [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load.

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-d, --dest-ip TEXT`: [ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: Local port that all players will use to access the hedgedoc server.  [default: 1337]
* `-r, --random-dest-port`: If set, ignore dest_port and use a random port instead.  [default: False]
* `-q, --quiet`: If set, do not print the default help instructions.  [default: False]
* `--init-sql TEXT`: [ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.
* `--force-init-sql`: [ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.  [default: False]
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/hedgedoc]
* `-i, --backup-interval INTEGER`: Seconds between automatic backups (database exports).  [default: 300]
* `-r, --backup-retain INTEGER`: Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.  [default: 16]
* `-Q, --backup-quiet`: If set, do not print backup confirmation messages.  [default: False]
* `-B, --no-backup`: If set, do not make automated backups of the server contents.  [default: False]
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-s, --https`: Use HTTPs instead of HTTP (not really more secure here).  [default: False]
* `--help`: Show this message and exit.

#### `autovnet rtfm hedgedoc mount`

Mount another player's hedgedoc server locally.

**Usage**:

```console
$ autovnet rtfm hedgedoc mount [OPTIONS] OPT
```

**Arguments**:

* `OPT`: IP:PORT or codename to mount.  [required]

**Options**:

* `-d, --dest-ip TEXT`: Destination IP you will use to access the service.  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: [ADVANCED] Destination port you will use to access the service. Must match host's value.  [default: 1337]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/hedgedoc]
* `-C, --set-codename TEXT`: Set the codename for the mount.
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-s, --https`: Use HTTPs instead of HTTP (not really more secure here).  [default: False]
* `--help`: Show this message and exit.

### `autovnet rtfm kali`

Run a shell based on Kali Linux

**Usage**:

```console
$ autovnet rtfm kali [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `shell`: Run a shell based on Kali Uses...

#### `autovnet rtfm kali shell`

Run a shell based on Kali

Uses https://hub.docker.com/r/kalilinux/kali-rolling + kali-linux-headless

WARNING: the first build will be veeeeeeerry slow

**Usage**:

```console
$ autovnet rtfm kali shell [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load.

**Options**:

* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/kali]
* `-M, --mount-dir TEXT`: Host directory to mount in container at the same path.  [default: /opt/rtfm/mnt]
* `--container-mount-dir TEXT`: Container directory to mount --mount-dir.  [default: /opt/rtfm/mnt]
* `-q, --quiet`: Hide build output.  [default: False]
* `-r, --root`: Run container as root.  [default: False]
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `--help`: Show this message and exit.

### `autovnet rtfm listen`

Start a remote listener on the RTFM server

You thought it was provisioning cloud infrastructure, but it was me,
    SSH port forwarding

**Usage**:

```console
$ autovnet rtfm listen [OPTIONS] [OPT]
```

**Arguments**:

* `[OPT]`: Remote port to listen on, or codename for a previous listener  [default: 443]

**Options**:

* `-d, --dest-ip TEXT`: Destination IP for tunnelled traffic (default: 127.0.0.1).  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: Destination port for tunnelled traffic (default: random).
* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-c, --count INTEGER`: The number of remote IP addresses to allocate.  [default: 1]
* `-D, --dir TEXT`: Base directory to store build output. A new directory will be created inside.  [default: /opt/rtfm/c2]
* `-b, --build-only`: Only build the listener, do not start it.  [default: False]
* `-o, --out-path TEXT`: Save listener config to this path, ignoring -D.
* `-i, --in-path TEXT`: Load listener config from this path.
* `-C, --set-codename TEXT`: [Advanced] Set a specific codename instead of a random one.  [default: ]
* `-R, --register-random`: Register a random DNS name (repeatable).  [default: 0]
* `-r, --register TEXT`: Register a DNS name (repeatable).
* `--help`: Show this message and exit.

### `autovnet rtfm matrix`

Securely share a matrix server.

**Usage**:

```console
$ autovnet rtfm matrix [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `export`: Run a self-hosted instance of matrix Matrix...
* `mount`: Mount and connect to another player's matrix...

#### `autovnet rtfm matrix export`

Run a self-hosted instance of matrix

Matrix (https://matrix.org/) is a lot of things, but here we pretend it's
a self-hosted chat server, similar to Slack, Discord, Mattermost, or Rocketchat.

Matrix provides (techically, _is_) a set of nice APIs for chat servers to implement:
(https://spec.matrix.org/latest/)

Because the APIs are well defined, it's easy to interact with programatically with scripts or bots.
* https://matrix.org/bridges/
* https://matrix.org/sdks/

**Usage**:

```console
$ autovnet rtfm matrix export [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-d, --dest-ip TEXT`: [ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: Local bind port for the server. (default: random).  [default: 8448]
* `-N, --no-browser`: If set, do not open element.  [default: False]
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/matrix]
* `--init-sql TEXT`: [ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.
* `--force-init-sql`: [ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.  [default: False]
* `-i, --backup-interval INTEGER`: Seconds between automatic backups (database exports).  [default: 300]
* `-r, --backup-retain INTEGER`: Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.  [default: 16]
* `-Q, --backup-quiet`: If set, do not print backup confirmation messages.  [default: False]
* `-B, --no-backup`: If set, do not make automated backups of the server contents.  [default: False]
* `--init-sql-sleep INTEGER`: [ADVANCED] Startup delay when using --init-sql.  [default: 15]
* `--help`: Show this message and exit.

#### `autovnet rtfm matrix mount`

Mount and connect to another player's matrix service

**Usage**:

```console
$ autovnet rtfm matrix mount [OPTIONS] OPT
```

**Arguments**:

* `OPT`: IP:PORT or codename to mount.  [required]

**Options**:

* `-d, --dest-ip TEXT`: [ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: [ADVANCED] Destination port you will use to access the service.  [default: 8448]
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/matrix]
* `-C, --set-codename TEXT`: Set the codename for the mount.
* `--help`: Show this message and exit.

### `autovnet rtfm merlin`

Use merlin (https://github.com/Ne0nd0g/merlin).

**Usage**:

```console
$ autovnet rtfm merlin [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `listener`: Create an autovnet-assisted merlin listener
* `script`: Execute merlin server commands from a file...
* `server`: Run merlin server Merlin is a cross-platform...

#### `autovnet rtfm merlin listener`

Create an autovnet-assisted merlin listener

**Usage**:

```console
$ autovnet rtfm merlin listener [OPTIONS] CODENAME
```

**Arguments**:

* `CODENAME`: Codename of merlin server  [required]

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-p, --port INTEGER`: Remote port to listen on.
* `-d, --dest-ip TEXT`: Destination IP for tunnelled traffic (default: 127.0.0.1).
* `--dest-port INTEGER`: Destination port for tunnelled traffic (default: random).
* `-c, --count INTEGER`: The number of remote IP addresses to allocate.  [default: 1]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/merlin]
* `-f, --force`: Do not prompt for confirmation before overwriting an existing file.  [default: False]
* `-N, --name TEXT`: Listener name.
* `-X, --description TEXT`: Listener description.
* `-k, --psk TEXT`: Pre-shared key for merlin. Agent must match server.
* `-U, --urls TEXT`: URLS option in whatever format merlin wants (not documented).
* `-K, --key TEXT`: Custom x509 key.
* `-C, --cert TEXT`: Custom x509 cert.
* `-P, --proto [http|https|http2|http3|h2c]`: Listener protocol.  [default: http2]
* `-T, --no-auto-load`: Do not save this listener to run on next startup.  [default: False]
* `-A, --only-auto-load`: Run this listener on startup, but do not run it now  [default: False]
* `--no-dns`: Disable automatic dns registration  [default: False]
* `-R, --register-random`: Register a random DNS name (repeatable).  [default: 0]
* `-r, --register TEXT`: Register a DNS name (repeatable).
* `--help`: Show this message and exit.

#### `autovnet rtfm merlin script`

Execute merlin server commands from a file

Uses the `autovnet type` technique, and is subject to the same limitations.

**Usage**:

```console
$ autovnet rtfm merlin script [OPTIONS] CODENAME FILES...
```

**Arguments**:

* `CODENAME`: Previous codename to load  [required]
* `FILES...`: Previous codename to load  [required]

**Options**:

* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/merlin]
* `-a, --auto-load`: Also run this script on startup, the next time the merlin server starts.  [default: False]
* `-A, --only-auto-load`: Run this script on startup, but do not run it now  [default: False]
* `-f, --force`: Do not prompt for confirmation before overwriting an existing file.  [default: False]
* `--help`: Show this message and exit.

#### `autovnet rtfm merlin server`

Run merlin server

Merlin is a cross-platform post-exploitation Command & Control server and agent written in Go.
See https://github.com/Ne0nd0g/merlin for official merlin usage

Use `autonet rtfm merlin payload` to help configure listeners that use autovnet

Last tested version: v1.4.1

**Usage**:

```console
$ autovnet rtfm merlin server [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/merlin]
* `-M, --mount-dir TEXT`: Host directory to mount in container at the same path  [default: /opt/rtfm/mnt]
* `--container-mount-dir TEXT`: Container directory to mount --mount-dir  [default: /opt/rtfm/mnt]
* `-V, --version TEXT`: Merlin version to use. See https://github.com/Ne0nd0g/merlin/releases  [default: latest]
* `--verbose-build`: [ADVANCED] Show build output  [default: False]
* `-R, --no-rc`: Do not load rc files from rc.d  [default: False]
* `--help`: Show this message and exit.

### `autovnet rtfm msf`

Use metasploit-framework.

**Usage**:

```console
$ autovnet rtfm msf [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `bind`: Build a bind payload with msfvenom Examples:...
* `console`: Run msfconsole, with listeners
* `handle`: Handle a previously generated msf payload, by...
* `reverse`: Build a reverse payload with msfvenom...

#### `autovnet rtfm msf bind`

Build a bind payload with msfvenom

Examples:

Good Windows bind payload

`autovnet rtfm msf bind -f exe -r X.X.X.X`

Good Linux bind payload

`autovnet rtfm msf bind -p linux/x64/meterpreter/bind_tcp -f elf -r X.X.X.X`

**Usage**:

```console
$ autovnet rtfm msf bind [OPTIONS] [OPTIONS]...
```

**Arguments**:

* `[OPTIONS]...`: (Advanced) Any additional payload options in the form KEY=VALUE.

**Options**:

* `-p, --payload TEXT`: An msfvenom (reverse) payload from 'msfvenom --list payloads'.  [default: windows/x64/meterpreter_bind_tcp]
* `-r, --rhost TEXT`: The remote host the handler should connect to.  [required]
* `-l, --lport INTEGER`: Port the target will listen on.  [default: 3398]
* `-f, --format TEXT`: Format for payload file. Default: use format from payload.
* `-D, --dir TEXT`: Base directory to store build output. A new directory will be created inside.  [default: /opt/rtfm/msf]
* `-b, --build-only`: Only generate artifacts, do no run the handler  [default: False]
* `--help`: Show this message and exit.

#### `autovnet rtfm msf console`

Run msfconsole, with listeners

**Usage**:

```console
$ autovnet rtfm msf console [OPTIONS]
```

**Options**:

* `-n, --network TEXT`: A network block to draw from.
* `-c, --count INTEGER`: The number of remote IP addresses to allocate.  [default: 3]
* `-D, --dir TEXT`: Base directory used for tracking.  [default: /opt/rtfm/msf]
* `--c2-dir TEXT`: Base directory used for listener tracking.  [default: /opt/rtfm/c2]
* `-P, --no-proxy`: Do not use proxychains to connect to the payload. Workaround for 'shell' channel bug for bind payloads.  [default: False]
* `-b, --build-only`: Only generate artifacts, do no run the handler  [default: False]
* `--help`: Show this message and exit.

#### `autovnet rtfm msf handle`

Handle a previously generated msf payload, by codename

Looks like there is some bug preventing a proxychain'ed msfconsole from opening shell channels
when connecting to meterpreter bind payloads.
Weird, but doesn't look like my fault, so use --no-proxy if you have issues and need 'shell'.

**Usage**:

```console
$ autovnet rtfm msf handle [OPTIONS] CODENAME
```

**Arguments**:

* `CODENAME`: The codename of the previously generated payload to handle  [required]

**Options**:

* `-D, --dir TEXT`: Base directory used previously to store build output.  [default: /opt/rtfm/msf]
* `--c2-dir TEXT`: Base directory used previously to store c2 output.  [default: /opt/rtfm/c2]
* `-P, --no-proxy`: Do not use proxychains to connect to the payload. Workaround for 'shell' channel bug for bind payloads.  [default: False]
* `--help`: Show this message and exit.

#### `autovnet rtfm msf reverse`

Build a reverse payload with msfvenom

Examples:

Good Windows reverse payload

`autovnet rtfm msf reverse -f exe`

Good Linux reverse payload

`autovnet rtfm msf reverse -p linux/x64/meterpreter_reverse_https -f elf`


Will this work for every payload?

* Probably not

Will this make the most common payloads super easy?

* Yup

The payload is configured with 'remote_port', the port the target will connect to.

We set up a port forward for

* REMOTE_IP:LPORT => 127.0.0.1:RANDOM_PORT

The payload gets configured (msfvenom) with the _left_ side
The msfconsole handler gets configured with the _right_ side

**Usage**:

```console
$ autovnet rtfm msf reverse [OPTIONS] [OPTIONS]...
```

**Arguments**:

* `[OPTIONS]...`: (Advanced) Any additional payload options in the form KEY=VALUE.

**Options**:

* `-n, --network TEXT`: A network block to draw from.
* `-p, --payload TEXT`: An msfvenom (bind) payload from 'msfvenom --list payloads'  [default: windows/x64/meterpreter_reverse_https]
* `-l, --remote-port INTEGER`: The port the target will connect out to, using autovnet.  [default: 443]
* `-c, --count INTEGER`: The number of remote IP addresses to allocate.  [default: 1]
* `-f, --format TEXT`: Format for payload file. Default: use format from payload
* `-D, --dir TEXT`: Base directory to store build output. A new directory will be created inside  [default: /opt/rtfm/msf]
* `--c2-dir TEXT`: Base directory to store c2 output. A new directory will be created inside  [default: /opt/rtfm/c2]
* `-b, --build-only`: Only generate artifacts, do no run the handler  [default: False]
* `--help`: Show this message and exit.

### `autovnet rtfm msfdb`

Securely share msfdb between players.

**Usage**:

```console
$ autovnet rtfm msfdb [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `export`: Initialize and export an msfdb...
* `mount`: Mount another player's msfdb instance.

#### `autovnet rtfm msfdb export`

Initialize and export an msfdb (Metasploit-Framework) Database service

You should probably only run at most one export or mount per machine, unless you super know what you are doing.
Use workspaces if you think you need multiple concurrent databases.

WARNING: postgres should *not* be running, and you should *not* have an msfdb instance that is not managed by autovnet.

WARNING: msfdb export is Kali specific and will not work with manual metasploit-framework installs
* See --no-msfdb-init and --init-sql for a workaround

* Your existing database configuration or data could be lost!

See also https://www.offensive-security.com/metasploit-unleashed/using-databases/

**Usage**:

```console
$ autovnet rtfm msfdb export [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-d, --dest-ip TEXT`: [ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: Local bind port for the server.  [default: 5432]
* `-r, --random-dest-port`: If set, ignore dest_port and use a random port instead.  [default: False]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/msfdb]
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `-i, --backup-interval INTEGER`: Seconds between automatic backups (database exports).  [default: 300]
* `-r, --backup-retain INTEGER`: Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.  [default: 16]
* `-Q, --backup-quiet`: If set, do not print backup confirmation messages.  [default: False]
* `-B, --no-backup`: If set, do not make automated backups of the server contents.  [default: False]
* `-f, --force`: If set, overwrite existing database config, if present.  [default: False]
* `-S, --postgres-socks TEXT`: [ADVANCED] Directory where postgres stores its sockets.  [default: /var/run/postgresql]
* `--no-disable-postgres`: [ADVANCED] No not disable the local postgresql service. By default, it is disabled to project you from yourself.  [default: False]
* `--no-msfdb-init`: [ADVANCED] No not call msfdb init. Useful if you just want to export an arbitrary postgres server.  [default: False]
* `--init-sql TEXT`: [ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.
* `--no-init-sql-user`: [ADVANCED] No not create an 'msf' user before running --init-sql  [default: False]
* `--db-config TEXT`: [ADVANCED] Path where msfdb init will save the configuration.  [default: /usr/share/metasploit-framework/config/database.yml]
* `--db-prefs TEXT`: [ADVANCED] Path to save database startup preference. See https://docs.rapid7.com/metasploit/automatically-connect-the-database/.  [default: ~/.msf4/database.yml]
* `--help`: Show this message and exit.

#### `autovnet rtfm msfdb mount`

Mount another player's msfdb instance.

Sharing a metasploit-framework database allows for multiplayer metasploit-ing!

See also https://www.offensive-security.com/metasploit-unleashed/using-databases/

**Usage**:

```console
$ autovnet rtfm msfdb mount [OPTIONS] OPT
```

**Arguments**:

* `OPT`: IP:PORT or previous codename to mount.  [required]

**Options**:

* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/msfdb]
* `-C, --set-codename TEXT`: Set the codename for the mount.
* `--db-config TEXT`: [ADVANCED] Path where msfdb init will save the configuration.  [default: /usr/share/metasploit-framework/config/database.yml]
* `--db-prefs TEXT`: [ADVANCED] Path to save database startup preference. See https://docs.rapid7.com/metasploit/automatically-connect-the-database/.  [default: ~/.msf4/database.yml]
* `-f, --force`: If set, overwrite existing database config, if present.  [default: False]
* `-c, --db-connect TEXT`: Database URL from the msfdb export. Required if opt is not a previous codename.
* `-i, --backup-interval INTEGER`: Seconds between automatic backups (database exports).  [default: 300]
* `-r, --backup-retain INTEGER`: Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.  [default: 16]
* `-Q, --backup-quiet`: If set, do not print backup confirmation messages.  [default: False]
* `-B, --no-backup`: If set, do not make automated backups of the server contents.  [default: False]
* `--no-disable-postgres`: [ADVANCED] No not disable the local postgresql service. By default, it is disabled to project you from yourself.  [default: False]
* `--help`: Show this message and exit.

### `autovnet rtfm mux-connect`

Force mux connect the autovnet server

Only applicable if SSH connection sharing is on (reuse=True).
Should not be needed, debug only.

**Usage**:

```console
$ autovnet rtfm mux-connect [OPTIONS]
```

**Options**:

* `--help`: Show this message and exit.

### `autovnet rtfm mux-disconnect`

Force mux disconnect from the autovnet server

Only applicable if SSH connection sharing is on (reuse=True).
Should not be needed, debug only.
Will invalidate all listeners, but may not stop autovnet processes

**Usage**:

```console
$ autovnet rtfm mux-disconnect [OPTIONS]
```

**Options**:

* `--help`: Show this message and exit.

### `autovnet rtfm nextcloud`

Securely share a nextcloud instance.

**Usage**:

```console
$ autovnet rtfm nextcloud [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `export`: Run a nexcloud server
* `mount`: Mount and connect to another player's...

#### `autovnet rtfm nextcloud export`

Run a nexcloud server

**Usage**:

```console
$ autovnet rtfm nextcloud export [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-d, --dest-ip TEXT`: [ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: Local bind port for the server. (default: random).
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/nextcloud]
* `--init-sql TEXT`: [ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.
* `--force-init-sql`: [ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.  [default: False]
* `-i, --backup-interval INTEGER`: Seconds between automatic backups (database exports).  [default: 300]
* `-r, --backup-retain INTEGER`: Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.  [default: 16]
* `-Q, --backup-quiet`: If set, do not print backup confirmation messages.  [default: False]
* `-B, --no-backup`: If set, do not make automated backups of the server contents.  [default: False]
* `--help`: Show this message and exit.

#### `autovnet rtfm nextcloud mount`

Mount and connect to another player's nextcloud service

**Usage**:

```console
$ autovnet rtfm nextcloud mount [OPTIONS] OPT
```

**Arguments**:

* `OPT`: IP:PORT or codename to mount.  [required]

**Options**:

* `-d, --dest-ip TEXT`: [ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: [ADVANCED] Destination port you will use to access the service (default: random).
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/nextcloud]
* `-C, --set-codename TEXT`: Set the codename for the mount.
* `--help`: Show this message and exit.

### `autovnet rtfm owncast`

Securely share an owncast instance.

**Usage**:

```console
$ autovnet rtfm owncast [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `export`: Run a self-hosted instance of owncast
* `mount`: Mount and connect to another player's owncast...

#### `autovnet rtfm owncast export`

Run a self-hosted instance of owncast

**Usage**:

```console
$ autovnet rtfm owncast export [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-d, --http-dest-ip TEXT`: [ADVANCED] Bind address for the server UI (for watching streams) (anything but 127.X.X.X may allow anyone to access!).  [default: 127.0.0.1]
* `-d, --rtmp-dest-ip TEXT`: [ADVANCED] Bind address for the server RTMP (for publishing a stream to owncast) (anything but 127.X.X.X may allow anyone to access!).  [default: 127.0.0.1]
* `-p, --http-dest-port INTEGER`: Local bind port for the server.  [default: 1980]
* `-P, --rtmp-dest-port INTEGER`: Local bind port for the server.  [default: 1935]
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/owncast]
* `--help`: Show this message and exit.

#### `autovnet rtfm owncast mount`

Mount and connect to another player's owncast server

**Usage**:

```console
$ autovnet rtfm owncast mount [OPTIONS] [REMOTE_IP] [HTTP_REMOTE_PORT] [RTMP_REMOTE_PORT]
```

**Arguments**:

* `[REMOTE_IP]`: Remote IP for service.
* `[HTTP_REMOTE_PORT]`: Remote PORT for http.
* `[RTMP_REMOTE_PORT]`: Remote PORT for rtmp.

**Options**:

* `-d, --dest-ip TEXT`: [ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!  [default: 127.0.0.1]
* `-p, --http-dest-port INTEGER`: [ADVANCED] Destination port to mount http.  [default: 1980]
* `-P, --rtmp-dest-port INTEGER`: [ADVANCED] Destination port to mount rtmp.  [default: 1935]
* `-N, --no-browser`: If set, do not open the default browser.  [default: False]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/owncast]
* `-C, --set-codename TEXT`: Codename for the mount.
* `--help`: Show this message and exit.

### `autovnet rtfm password`

Generate random passwords

Like the codename generator, but for passwords

**Usage**:

```console
$ autovnet rtfm password [OPTIONS]
```

**Options**:

* `-c, --count INTEGER`: Number of codenames to generate.  [default: 1]
* `-t, --tokens INTEGER`: Number of tokens to include in the codename.  [default: 3]
* `-j, --join TEXT`: Delimiter for joining tokens.  [default: -]
* `-d, --digits INTEGER`: Ensure some digits in the password for complexity requirements (not real security).  [default: 3]
* `-s, --secure`: Generate a secure, hard to type password.  [default: False]
* `--help`: Show this message and exit.

### `autovnet rtfm proxychains`

Use proxychains to (obfuscate source IP).

**Usage**:

```console
$ autovnet rtfm proxychains [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `config`: Generate a proxychains config you can edit...
* `run`: Run a command under proxychains4 Assuming the...

#### `autovnet rtfm proxychains config`

Generate a proxychains config you can edit

Generally, you only need this if you need to do lower-level proxychains stuff that autovnet doesn't make easy yet

**Usage**:

```console
$ autovnet rtfm proxychains config [OPTIONS]
```

**Options**:

* `-n, --network TEXT`: A network block to draw from.
* `-c, --count INTEGER`: The number of unique IPs to generate. 0 returns all IPs  [default: 1]
* `-l, --chain-len INTEGER`: The length of the chain to build. (For fun / stress testing, not more sneaky)  [default: 1]
* `-o, --out TEXT`: Write config to out file.
* `--help`: Show this message and exit.

#### `autovnet rtfm proxychains run`

Run a command under proxychains4

Assuming the command is compatible with proxychains4, the source IP of any TCP connections will be obfuscated by autovnet

**Usage**:

```console
$ autovnet rtfm proxychains run [OPTIONS] ARGS...
```

**Arguments**:

* `ARGS...`: Command to invoke under proxychains. (Use -- my -c ommand --with flags.)  [required]

**Options**:

* `-n, --network TEXT`: A network block to draw from.
* `-c, --count INTEGER`: The number of unique IPs to generate. 0 returns all IPs  [default: 1]
* `-l, --chain-len INTEGER`: The length of the chain to build. (For fun / stress testing, not more sneaky)  [default: 1]
* `--help`: Show this message and exit.

### `autovnet rtfm pwncat`

Start a 'remote' pwncat-cs listener

pwncat-cs actually runs locally, and we set up remote TCP listener to grab the traffic

pwncat-cs is used here as a "smart" handler for simple TCP reverse shells.

**Usage**:

```console
$ autovnet rtfm pwncat [OPTIONS] [OPT]
```

**Arguments**:

* `[OPT]`: The remote port to accept inbound traffic, or previous codename.  [default: 80]

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-c, --count INTEGER`: The number of remote IP addresses to allocate.  [default: 1]
* `-d, --dest-ip TEXT`: Listen IP for local pwncat (default: 127.0.0.1).  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: Listen port for local pwncat (default: random).
* `-D, --dir TEXT`: Base directory to store build output. A new directory will be created inside.  [default: /opt/rtfm/c2]
* `--help`: Show this message and exit.

### `autovnet rtfm server`

[Advanced] Manage the autovrtfm server. (You probably don't need to run most of these commands manually)

**Usage**:

```console
$ autovnet rtfm server [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `down`: Stop the core autovrtfm services
* `install`: Install a systemd service that runs the...
* `restart`: Restart the core autovrtfm services Calls...
* `uninstall`: Uninstall a previously installed systemd...
* `up`: Start the core autovrtfm services The Red...

#### `autovnet rtfm server down`

Stop the core autovrtfm services

**Usage**:

```console
$ autovnet rtfm server down [OPTIONS]
```

**Options**:

* `-e, --env-file TEXT`: Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `--help`: Show this message and exit.

#### `autovnet rtfm server install`

Install a systemd service that runs the autovnet server

**Usage**:

```console
$ autovnet rtfm server install [OPTIONS]
```

**Options**:

* `-d, --install-dir TEXT`: Directory to install systemd service  [default: /etc/systemd/system]
* `--help`: Show this message and exit.

#### `autovnet rtfm server restart`

Restart the core autovrtfm services

Calls down, then up

**Usage**:

```console
$ autovnet rtfm server restart [OPTIONS]
```

**Options**:

* `-d, --detach`: Run docker-compose is the background  [default: False]
* `-e, --env-file TEXT`: Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `--help`: Show this message and exit.

#### `autovnet rtfm server uninstall`

Uninstall a previously installed systemd service

**Usage**:

```console
$ autovnet rtfm server uninstall [OPTIONS]
```

**Options**:

* `-d, --install-dir TEXT`: Directory used to install the systemd service  [default: /etc/systemd/system]
* `--help`: Show this message and exit.

#### `autovnet rtfm server up`

Start the core autovrtfm services

The Red Team Force Multipler (RTFM) server contains:
    * autovrtfm.sshd (handles remote listeners)
    * autovrtfm.sockd (handles forward proxying)
    * autovrtfm.dns (TODO)
    * autovrtfm.ca (TODO)

**Usage**:

```console
$ autovnet rtfm server up [OPTIONS]
```

**Options**:

* `-d, --detach`: Run docker-compose is the background  [default: False]
* `-e, --env-file TEXT`: Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `--help`: Show this message and exit.

### `autovnet rtfm share`

Share a directory over HTTP and HTTPS

The first time you run a share command, it will take a long time to initialize.
Subsequent share commands will be much faster.

**Usage**:

```console
$ autovnet rtfm share [OPTIONS] [OPT]
```

**Arguments**:

* `[OPT]`: Directory, file, or codename to share / serve. Default: Create an emphemeral tempdir.

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-c, --count INTEGER`: The number of remote IP addresses to allocate.  [default: 1]
* `--msf TEXT`: Codename. Share the payload from a previous 'autovnet rtfm msf' command.
* `--msf-dir TEXT`: The directory to look in when using --msf.  [default: /opt/rtfm/msf]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/c2]
* `-f, --force`: Do not prompt for confirmation before deleting temp dirs.  [default: False]
* `-e, --encrypt`: If sharing a single file, encrypt it for other players.  [default: False]
* `-P, --prompt`: If encrypting, prompt for the password.  [default: False]
* `-p, --password TEXT`: If encrypting, use this password.
* `-s, --secure`: If encrypting, generate a secure password.  [default: False]
* `--no-dns`: Do not generate DNS names by default  [default: False]
* `-R, --register-random`: Register a random DNS name (repeatable).  [default: 0]
* `-r, --register TEXT`: Register a DNS name (repeatable).
* `-C, --codename`: Use codename completion instead of file completion.  [default: False]
* `--help`: Show this message and exit.

### `autovnet rtfm sliver`

Use sliver (https://github.com/BishopFox/sliver).

**Usage**:

```console
$ autovnet rtfm sliver [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `armorize`: Download the sliver armory from...
* `client`: Sliver Client Connect to the sliver server so...
* `export`: Share a sliver server with fellow players...
* `generate`: Generate a sliver binary Note: dns not fully...
* `import`: Import another player's sliver server...
* `listener`: Start a sliver listener + autovnet listener
* `rc`: Run sliver commands from a file Similar to...
* `server`: Start a sliver server See...

#### `autovnet rtfm sliver armorize`

Download the sliver armory from https://github.com/BishopFox/sliver/wiki/Armory

The armory is distributed to clients when they import the sliver config

**Usage**:

```console
$ autovnet rtfm sliver armorize [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load.

**Options**:

* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/sliver]
* `-u, --url TEXT`: Armory JSON.  [default: https://raw.githubusercontent.com/sliverarmory/armory/master/armory.json]
* `-f, --file TEXT`: [ADVANCED] Manual armory JSON file to use instead of URL.
* `--help`: Show this message and exit.

#### `autovnet rtfm sliver client`

Sliver Client

Connect to the sliver server so you can use sliver.

If you started the sliver server, you just need the codename.
If another player started the sliver server, they need to export it,
and you need to import it.

Then, you can connect by codename

**Usage**:

```console
$ autovnet rtfm sliver client [OPTIONS] CODENAME
```

**Arguments**:

* `CODENAME`: Codename of sliver server, or sliver multiplayer config url.  [required]

**Options**:

* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/sliver]
* `-M, --mount-dir TEXT`: Host directory to mount in container at the same path.  [default: /opt/rtfm/mnt]
* `--container-mount-dir TEXT`: Container directory to mount --mount-dir.  [default: /opt/rtfm/mnt]
* `-v, --verbose`: Hide build output.  [default: False]
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `-d, --dest-ip TEXT`: [ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: [ADVANCED] Destination port you will use to access the service.
* `--help`: Show this message and exit.

#### `autovnet rtfm sliver export`

Share a sliver server with fellow players

After the player imports the config, they can connect as a client
to your server

**Usage**:

```console
$ autovnet rtfm sliver export [OPTIONS] CODENAME
```

**Arguments**:

* `CODENAME`: Previous codename to load.  [required]

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/sliver]
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose.  [default: ~/.autovnet/rtfm.env]
* `-s, --save-configs`: [ADVANCED] Save generated configs in the unlikely event a client needs to re-download.  [default: False]
* `--help`: Show this message and exit.

#### `autovnet rtfm sliver generate`

Generate a sliver binary

Note: dns not fully supported for now

**Usage**:

```console
$ autovnet rtfm sliver generate [OPTIONS] CODENAME [PROTO]:[mtls|http|https|wg|dns]
```

**Arguments**:

* `CODENAME`: Codename of sliver server, or sliver multiplayer config url.  [required]
* `[PROTO]:[mtls|http|https|wg|dns]`: Protocol to use for payload.  [default: mtls]

**Options**:

* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/sliver]
* `-M, --mount-dir TEXT`: Host directory to mount in container at the same path.  [default: /opt/rtfm/mnt]
* `--container-mount-dir TEXT`: Container directory to mount --mount-dir.  [default: /opt/rtfm/mnt]
* `-q, --quiet`: Hide build output.  [default: False]
* `-v, --verbose`: Print more output.  [default: False]
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `-d, --dest-ip TEXT`: [ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: [ADVANCED] Destination port you will use to access the service.
* `-N, --no-start-listener`: Do not attempt to start a listener on the sliver server.  [default: False]
* `-M, --no-persist-listener`: Do make listeners on the sliver server persistent.  [default: False]
* `-P, --profiles-path TEXT`: Path to profiles yaml.
* `-L, --listener-name TEXT`: Listener codename from a previous sliver listener command.
* `--listener-path TEXT`: [ADVANCED] Full path to listener yaml.
* `-T, --tun-map-ip TEXT`: autovnet map_ip to use for listener (as from autovnet rtfm tun up). Required for UDP. Set instead of --listener_path.
* `--help`: Show this message and exit.

#### `autovnet rtfm sliver import`

Import another player's sliver server configuration

This allows connecting as a client to the player's server

**Usage**:

```console
$ autovnet rtfm sliver import [OPTIONS] REMOTE_ADDR
```

**Arguments**:

* `REMOTE_ADDR`: Remote address of sliver export.  [required]

**Options**:

* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/sliver]
* `-q, --quiet`: Hide build output.  [default: False]
* `-t, --token TEXT`: Sliver registration token from export.
* `-d, --dest-ip TEXT`: [ADVANCED]  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: [ADVANCED]
* `--help`: Show this message and exit.

#### `autovnet rtfm sliver listener`

Start a sliver listener + autovnet listener

**Usage**:

```console
$ autovnet rtfm sliver listener [OPTIONS] CODENAME [PROTO]:[mtls|http|https|wg|dns]
```

**Arguments**:

* `CODENAME`: Codename of sliver server, or sliver multiplayer config url.  [required]
* `[PROTO]:[mtls|http|https|wg|dns]`: Protocol to use for payload.  [default: mtls]

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/sliver]
* `-M, --mount-dir TEXT`: Host directory to mount in container at the same path.  [default: /opt/rtfm/mnt]
* `--container-mount-dir TEXT`: Container directory to mount --mount-dir.  [default: /opt/rtfm/mnt]
* `-v, --verbose`: Print more output.  [default: False]
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `-N, --name TEXT`: Listener name.
* `-p, --port INTEGER`: Remote port to listen on.
* `-d, --dest-ip TEXT`: Destination IP for tunnelled traffic (default: 127.0.0.1).
* `--dest-port INTEGER`: Destination port for tunnelled traffic (default: random).
* `--no-auto-load`: Do not save this listener to run on next startup.  [default: False]
* `-A, --only-auto-load`: Run this listener on startup, but do not run it now.  [default: False]
* `--no-dns`: Disable automatic dns registration  [default: False]
* `-M, --no-persist-listener`: Do make listeners on the sliver server persistent.  [default: False]
* `--force`: Allow overwriting existing listener of the same naem.  [default: False]
* `-c, --count`: Number of IPs to allocate to listener.  [default: 1]
* `-R, --register-random`: Register a random DNS name (repeatable).  [default: 0]
* `-r, --register TEXT`: Register a DNS name (repeatable).
* `--help`: Show this message and exit.

#### `autovnet rtfm sliver rc`

Run sliver commands from a file

Similar to metasploit-style rc files

**Usage**:

```console
$ autovnet rtfm sliver rc [OPTIONS] [CODENAME] FILES...
```

**Arguments**:

* `[CODENAME]`: Previous codename to load.
* `FILES...`: Previous codename to load  [required]

**Options**:

* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/sliver]
* `--help`: Show this message and exit.

#### `autovnet rtfm sliver server`

Start a sliver server

See https://github.com/BishopFox/sliver/wiki for sliver help

Note: all db options are disabled.
sqlite is used until/unless sliver actually supports postgres

**Usage**:

```console
$ autovnet rtfm sliver server [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load.

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/sliver]
* `-M, --mount-dir TEXT`: Host directory to mount in container at the same path.  [default: /opt/rtfm/mnt]
* `--dest-ip TEXT`: [ADVANCED] Multiplayer host  [default: 127.0.0.1]
* `--dest-port INTEGER`: [ADVANCED] Multiplayer port
* `--container-mount-dir TEXT`: Container directory to mount --mount-dir.  [default: /opt/rtfm/mnt]
* `-v, --verbose`: Show verbose output.  [default: False]
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.autovnet/rtfm.env]
* `--init-sql TEXT`: [ADVANCED] Arbitrary .sql[.gz] file to import (e.g. from pg_dump). WARNING: use with caution, could cause data loss.
* `--force-init-sql`: [ADVANCED] Do not prompt for --init-sql, even if it looks like we are going to delete an existing database.  [default: False]
* `-i, --backup-interval INTEGER`: Seconds between automatic backups (database exports).  [default: 300]
* `-r, --backup-retain INTEGER`: Number of backups to retain. 0 means unlimited. Any backups from a previous run are retained.  [default: 16]
* `-Q, --backup-quiet`: If set, do not print backup confirmation messages.  [default: False]
* `-B, --no-backup`: If set, do not make automated backups of the server contents.  [default: True]
* `-A, --no-armory`: If set, do not pre-download the sliver armory (https://github.com/sliverarmory/armory).  [default: False]
* `-G, --no-generate`: If set, do not pre-generate payloads.  [default: False]
* `--no-dns`: Disable automatic dns registration  [default: False]
* `-c, --count INTEGER`: The number of remote IP addresses to allocate.  [default: 4]
* `-R, --register-random`: Register a random DNS name (repeatable).  [default: 0]
* `-r, --register TEXT`: Register a DNS name (repeatable).
* `-P, --profiles-path TEXT`: Path to profiles yaml.
* `--help`: Show this message and exit.

### `autovnet rtfm socat`

[Experimental] Run an arbitrary socat command on the RTFM server

Example syntax:

(`127.4.4.4:80` => `127.5.5.5:8000`)

`autovnet rtfm socat TCP4-LISTEN:80,bind=127.4.4.4,fork,reuseaddr TCP4:127.5.5.5:8000`

(`socat -h`)
`autovnet rtfm cmd socat -- -h`

**Usage**:

```console
$ autovnet rtfm socat [OPTIONS] ARGS...
```

**Arguments**:

* `ARGS...`: socat args. (Use -- if your command has flags)  [required]

**Options**:

* `--help`: Show this message and exit.

### `autovnet rtfm svc`

Share or mount services between players.

**Usage**:

```console
$ autovnet rtfm svc [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `export`: Securely export (expose) a local service to...
* `mount`: Securely mount another player's shared...

#### `autovnet rtfm svc export`

Securely export (expose) a local service to other players

Only other autovnet players with the shared TLS cert can connect.

The SSH tunnel redirects traffic from autovnet to a local stunnel instance (random local port), which provides mTLS.
Then, the local stunnel instance then redirects the "raw" TCP traffic to the provided dest_ip / dest_port.

**Usage**:

```console
$ autovnet rtfm svc export [OPTIONS] [OPT]
```

**Arguments**:

* `[OPT]`: Codename or remote port to use.

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-I, --infra-category TEXT`: [ADVANCED] Category of infrastructure.  [default: svc]
* `-c, --count INTEGER`: The number of remote IP addresses to allocate.  [default: 1]
* `-d, --dest-ip TEXT`: Destination IP for tunnelled traffic.  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: Destination port for tunnelled traffic (default: random).
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.venv/rtfm/lib/python3.10/site-packages/autovnet/docker/plugins/stunnel/stunnel.env]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/svc]
* `-C, --set-codename TEXT`: [ADVANCED] Explictly set codename to this value.
* `--set-suffix TEXT`: [ADVANCED] Set internal codename suffix.  [default: ]
* `-N, --no-deconflict`: [ADVANCED] Do not deconflict public IP assignment.  [default: False]
* `--help`: Show this message and exit.

#### `autovnet rtfm svc mount`

Securely mount another player's shared service

Mounting a service means binding locally to a (localhost) port.
Packets you send to the local port will be
1. wrapped in TLS, using the rtfm.key
2. sent through the autovnet server
3. reach the other player's 'svc export' instance
4. be authenticated (by rtfm.key)
5. TLS unwrapped
6. forwared to the "local" service that the other player exported

So, you use the standard client for the service (e.g. a web browser) pointing at your "local" end of the tunnel,
and the "local" service of the other player will get your requests.

On the wire, it's encrypted with TLS, and more importantly, _only_ other autovnet players that have the rtfm.key
can ride the tunnel to reach the other player's service.

So, this is how you should securely expose any "autovnet player only" services, like tools for collaboration

**Usage**:

```console
$ autovnet rtfm svc mount [OPTIONS] OPT
```

**Arguments**:

* `OPT`: IP:PORT or codename to mount.  [required]

**Options**:

* `-d, --dest-ip TEXT`: Destination IP you will use to access the service.  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: Destination port you will use to access the service (default: random).
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose  [default: ~/.venv/rtfm/lib/python3.10/site-packages/autovnet/docker/plugins/stunnel/stunnel.env]
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/svc]
* `-C, --set-codename TEXT`: Set the codename for the mount.
* `-m, --mount-type TEXT`: [ADVANCED] Mount type.
* `--set-suffix TEXT`: [ADVANCED] Set internal codename suffix.  [default: ]
* `--help`: Show this message and exit.

### `autovnet rtfm tun`

Secure, virtual overlay networking.

**Usage**:

```console
$ autovnet rtfm tun [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `redirect`: Set up a redirect rule on your local tun This...
* `server`: Start an RTFM tun server This is intended to...
* `up`: Mount and connect to another player's nebula...

#### `autovnet rtfm tun redirect`

Set up a redirect rule on your local tun

This allows you to match certain inbound packets and rewrite the destination port.

This is useful when you have a payload configured to connect to a well known port
    that you cannot actually bind your listener to, due to a conflict (e.g. UDP:53)

**Usage**:

```console
$ autovnet rtfm tun redirect [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Codename of active tun.  [default: default]

**Options**:

* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/ipam/config]
* `-P, --proto TEXT`: Protocol to match.  [default: udp]
* `-m, --match-port INTEGER`: Destination port to match. (E.g. the port that matches your payload)  [required]
* `-s, --match-src TEXT`: Source IP (or CIDR) to match. Default: any
* `-t, --to-port INTEGER`: When a packet matches, rewrite the dest port to this port. (E.g. the port that matches your listener.)  [required]
* `-b, --background`: Do not wait for CTRL+C, require manual --delete (or stopping the tun)  [default: False]
* `--delete`: Delete this rule instead of creating it. (Rules still exist after a tun is torn down!)  [default: False]
* `-v, --verbose`: Enable verbose output during container invocations  [default: False]
* `--help`: Show this message and exit.

#### `autovnet rtfm tun server`

Start an RTFM tun server

This is intended to run on the autovnet server.
Only run on a player machine if you trust other players.
(They could pivot through your tun server to you local network!)

**Usage**:

```console
$ autovnet rtfm tun server [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-N, --nebula-network TEXT`: Nebula virtual network to use.
* `-c, --clean`: Delete any previous IP assignments.  [default: False]
* `-b, --nebula-bind-ip TEXT`: Bind address for nebula server.
* `-P, --nebula-bind-port INTEGER`: Bind port (UDP) for nebula server.
* `-R, --nebula-remote-port INTEGER`: [ADVANCED] Remote bind port for tun API. Not intended to be set by humans.
* `-d, --dest-ip TEXT`: [ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: [ADVANCED] Local port for nebula data
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/tun]
* `-e, --enable-pivot`: [!DANGER!] Allow other players to pivot through your tun server to IPs given by --allow-subnet (private IPs).  [default: False]
* `-E, --enable-pivot-avn`: [!DANGER!] Allow other players to pivot through your tun server to IPs controlled by autovnet  [default: False]
* `-a, --allow-subnet TEXT`: [ADVANCED] Network to allow for --enable-pivot.  [default: ]
* `-v, --verbose`: Enable verbose output during container invocations  [default: False]
* `--help`: Show this message and exit.

#### `autovnet rtfm tun up`

Mount and connect to another player's nebula service

**Usage**:

```console
$ autovnet rtfm tun up [OPTIONS] [OPT]
```

**Arguments**:

* `[OPT]`: IP:PORT or codename to mount.

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-d, --dest-ip TEXT`: [ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!  [default: 127.0.0.1]
* `-N, --new`: Force a new IP assignment. Don't be greedy, only request what you need.  [default: False]
* `-p, --dest-port INTEGER`: [ADVANCED] Destination port you will use to access the service (default: random).
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/ipam/config]
* `-r, --route TEXT`: IPv4Network (X.X.X./Y) to route through the tun server. (Server must have pre-approved the network.)
* `-i, --interval INTEGER`: If set, roll new IP every --interval seconds.
* `-t, --tokens INTEGER`: Number of random tokens, if using -R  [default: 2]
* `-T, --ttl-inc INTEGER`: [ADVANCED] Increase the IP TTL of outbound tun packets by this amount. Generally, you should set to the number of intermediate hops between you and the autovnet server, as shown by traceroute.  [default: 1]
* `-R, --register-random`: Register a random DNS name on each roll.  [default: False]
* `-a, --register TEXT`: Re-register this DNS name for each roll. Can be combined with -R.
* `-A, --random-name`: Like register, but generate a random name to use. Can be combined with -R.  [default: False]
* `-s, --server TEXT`: Override IP:PORT of nebula lighthouse (UDP)
* `--verbose-build`: [ADVANCED] Show build output.  [default: False]
* `--help`: Show this message and exit.

### `autovnet rtfm vnc`

Securely screenshare between players.

**Usage**:

```console
$ autovnet rtfm vnc [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `export`: Run a view-only VNC server to screenshare...
* `mount`: Mount and connect to another player's...

#### `autovnet rtfm vnc export`

Run a view-only VNC server to screenshare with other players.

The server is locked down to disallow any remote control.
Because `autovnet rtfm svc` is used, only other autovnet players can mount and connect to your screen.

Currently uses x11vnc under the hood, so it probably won't work if you're using Wayland or anything not standard Xorg.

The default behavior is to share the current / primary screen at native resolution.
Since most VNC clients do not support client-side scaling, players with larger-than-average monitors should generally
--scale their output down to something more reasonable for others to consume.

--scale 1600x900 is generally a good choice, which is legible on 4k monitors while behaving reasonably on a 1920x1080 (or less) display / laptop.

If necessary, you can simply export screenshares at the same time, with different --scale options.
Then, players can choose which stream resolution is most comfortable for them.

If players are remote, your stream quality is limited by the host's upload speed, but at least on a local virtual, 10Gbps network, the quality can be pristine.
Clients have control of the stream quality, so instruct your players to mount the stream at an acceptable --quality as appropriate for your network.

The default settings for clients sacrifices some stream quality for massively reduced data transfer.

**Usage**:

```console
$ autovnet rtfm vnc export [OPTIONS] [CODENAME]
```

**Arguments**:

* `[CODENAME]`: Previous codename to load

**Options**:

* `-n, --network TEXT`: Simulated network to draw a random IP from.
* `-d, --dest-ip TEXT`: [ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: Local bind port for the server. (default: random)
* `--display TEXT`: [ADVANCED] The X display to share.  [default: :0]
* `-x, --x-socks TEXT`: [ADVANCED] The directory containing X sockets.  [default: /tmp/.X11-unix/]
* `-X, --x-auth TEXT`: [ADVANCED] The path to the .Xauthority file  [default: /run/user/1000/.mutter-Xwaylandauth.IABYR1]
* `-P, --require-password`: Require password to connect. Not really more secure, but might make you feel better.  [default: False]
* `-s, --scale TEXT`: Scale your screenshare resolution. Can be a resolution, fraction, decimal, etc. See '-scale': https://linux.die.net/man/1/x11vnc.  [default: ]
* `-e, --env-file TEXT`: [ADVANCED] Environment file to pass to docker-compose.
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/vnc]
* `--help`: Show this message and exit.

#### `autovnet rtfm vnc mount`

Mount and connect to another player's view-only vnc export.

Don't worry, you can't accidentally mess up the presenter - your session is forced to be view-only by the server,
so you cannot move their mouse, type, or resize their remote screen.


The default settings for clients sacrifices some stream quality for massively reduced data transfer.
The --quality level you choose impacts the amount of data the presenter has to "upload" to your system.

If they are remote (not on the same virtual infrastructure as you), then all players watching the stream will be
limited by the upload speed of the presenter.

It's likely that many players could concurrently watch a single presenter, as if in a classroom setting,
but this implementation is absolutely not optimal for that use case, since the amount of data the presenter must upload
scales linearly with the number of connected clients - each client receives a separate, complete stream from the presenter.

Classroom-style presentations will work best if the presenter has excellent upload speed from their ISP,
or better, if the machine running the vnc export is centrally located in your game infrastructure,
likely hosted on-prem with corpoarate internet or in the cloud.

But the primary use case is ad-hoc sharing where 1 presenter recieves technical assistance from a few mentors,
or a single preseneter is watched by a small team (e.g. < ~10 players) at a time, with other players starting
their own exports as needed for real-time collaboration.

A more scalable solution for classroom-style streaming, with 1 presenter and N watchers may eventually be added.
As a workaround, a centrally hosted VM (or fellow player with better upload speed) could mount the presenter's stream, then `vnc export` their
own display for others to mount.
Since ISP upload speed is by far the bottleneck, this strategy should signficantly increase the number of players that can watch concurrently.

Quality Levels

* `potato`: The lowest quality possible. If even `potato` is too much for your upload speed, RIP.

    - 4k Matrix Rain torture test: ~3 MiB/s up
    - 4k active terminal: ~200 KiB/s up

* `low`: Try to maintain legibility, but sacrifice as much quality as possible to minimize bandwidth needs.

    - 4k Matrix Rain torture test: ~4 MiB/s up

* `default`: The default quality, aims to be reasonably crisp while still signficantly reducing bandwidth needs.

    - 4k Matrix Rain torture test: ~5 MiB/s up
    - 4k active terminal: ~250 KiB/s up

* `mid`: Middle ground between `default` and `high`. Approximately the same quality as xtightvncviewer's default.

    - 4k Matrix Rain torture test: ~6 MiB/s up

* `high`: Almost full quality, very little lossy compression.

    - 4k Matrix Rain torture test: ~15 MiB/s up
    - 4k active terminal: ~1 MiB/s up

* `ultra`: Minimum compression, low CPU load and close to lossless.

    - 4k Matrix Rain torture test: ~30 MiB/s up

* `raw`: All the bytes, mimimum CPU load, but extremely bandwidth intensive. Only use if you and the presenter are local.
    - 4k Matrix Rain torture test: ~200 MiB/s up
    - 4k active terminal: ~10 MiB/s up

With the rough bandwidth estimates above, it should be possible for a remote player with a paltry 5 Mbps ISP upload speed
(typical for residential internet in many regions) to share default quality, mostly terminal streams with 1-2 concurrent watchers.

**Usage**:

```console
$ autovnet rtfm vnc mount [OPTIONS] OPT
```

**Arguments**:

* `OPT`: IP:PORT or codename to mount.  [required]

**Options**:

* `-d, --dest-ip TEXT`: [ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!  [default: 127.0.0.1]
* `-p, --dest-port INTEGER`: [ADVANCED] Destination port you will use to access the service (default: random).
* `-D, --payload-dir TEXT`: Directory to save command metadata.  [default: /opt/rtfm/vnc]
* `-V, --viewer TEXT`: [ADVANCED] The vnc client program to run.  [default: xtightvncviewer]
* `-N, --no-vnc-options`: [ADVANCED] Only pass IP:PORT to --viewer, no extra options. Disables --quality settings.  [default: False]
* `-Q, --quality [potato|low|default|mid|high|ultra|raw]`: The stream quality to request. If bandwidth is limited, work with the presenter to choose an approriate setting.
* `-C, --set-codename TEXT`: Set the codename for the mount.
* `--help`: Show this message and exit.

## `autovnet tmp`

Create tmp dirs

**Usage**:

```console
$ autovnet tmp [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `clean`: Delete all autovnet tmp directories
* `dir`: Create and print an autovnet tmp / scratch...
* `shell`: Start a shell in a clean tmp directory

### `autovnet tmp clean`

Delete all autovnet tmp directories

**Usage**:

```console
$ autovnet tmp clean [OPTIONS]
```

**Options**:

* `-D, --payload-dir TEXT`: Directory to create tmp dir in.  [default: /opt/rtfm/tmp]
* `--help`: Show this message and exit.

### `autovnet tmp dir`

Create and print an autovnet tmp / scratch directory

**Usage**:

```console
$ autovnet tmp dir [OPTIONS]
```

**Options**:

* `-D, --payload-dir TEXT`: Directory to create tmp dir in.  [default: /opt/rtfm/tmp]
* `--help`: Show this message and exit.

### `autovnet tmp shell`

Start a shell in a clean tmp directory

**Usage**:

```console
$ autovnet tmp shell [OPTIONS]
```

**Options**:

* `-D, --payload-dir TEXT`: Directory to create tmp dir in.  [default: /opt/rtfm/tmp]
* `-s, --shell TEXT`: Alternate shell to use.
* `-k, --keep`: Keep the tmp dir after the shell exits.  [default: False]
* `--help`: Show this message and exit.

## `autovnet type`

Execute command, writing a payload to STDIN.

**Usage**:

```console
$ autovnet type [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--help`: Show this message and exit.

**Commands**:

* `it`: Execute cmd interactively, but ask a ghost to...

### `autovnet type it`

Execute cmd interactively, but ask a ghost to type text into STDIN.

Prints a special file path that you can write to (e.g. with echo / cat).

    * any text you write will be typed by the ghost

Unix only (lol)
WARNING: gigahax

**Usage**:

```console
$ autovnet type it [OPTIONS] ARGS...
```

**Arguments**:

* `ARGS...`: Command to invoke. (Use -- my -c ommand --with flags.)  [required]

**Options**:

* `-f, --file TEXT`: Files to type into the 
* `--help`: Show this message and exit.
