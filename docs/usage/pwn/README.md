# pwn

[TOC]

`autovnet` integrates directly with your favorite hacking tools and frameworks (and some custom ones) to make you, a Red Team player as efficient as possible.

This is part of [RTFM](../../rtfm/README.md): hacking with `autovnet` should be so easy, you could easily solo dozens of Blue Teams with a bit of practice.
And to each Blue Team, you would appear like dozens / hundreds / thousands of unique malicious actors.

## Adding Tools / Frameworks

Have a favorite tool / hacking framework that could be integrated into `autovnet`?

Tools that are easy to run in a `docker` container are strongly prefferred.
Official `docker` images + `docker-compose.yml` exaples are even better.

For now, open an issue requesting your favorite tool be added.
Many tools are already planned, and eventually, `autovnet` will be open to contributions.
