# Shared Notes

[TOC]

## Intro

Real-time, collaborative note-taking / document editing is critical for success in many wargames.

These notes are not a replacement for long-lived documentation in source control (e.g. git), but in an ideal world, all known information
about a particular target network / system, any actions taken against it, and any details about payloads deployed or persistence left should be documented "live",
as the player is actively engaging with the system.

By keeping these notes in a shared place, there is a timeline of actions taken, other new players can learn from your approach,
and other experienced players will know exactly where you left off, how your persistence is configured, any known credentials, etc. to continue hacking while you're away.

Writing things down shared both your knowledge and your effort - if you don't write things down,
your teammates may very well have to re-learn and re-do anything you already did, possibly in conflicting ways (e.g. accidentally overwriting your persistence with theirs) :)

## [HedgeDoc](https://hedgedoc.org/)

[HedgeDoc](https://hedgedoc.org/) is basically Google Docs, but good, and self-hosted. (Techincally, it's very similar to [etherpad](https://etherpad.org/), if you are familar with that.)

It supports real-time collaborative editing (like Google Docs), but everything is Markdown, with live previews!

Because the document format is Markdown, it supports formated code blocks, and is easy to work with, copy, paste, export, and transfer (binary documents bad).

With `autovnet`, one player hosts the HedgeDoc server, and any interested players mount the service locally.

Because of how HedgeDoc uses internal urls, all players must mount the service on the same local port that matches the host's server configuration.

Since hacker notes are one of the most important collaboration features, `hedgedoc` gets the honor of defaulting to `127.0.0.1:1337`.

That is, when following the example below, all players will be able to go to `http://127.0.0.1:1337/home` in their browser access the shared HedgeDoc instance.

## Folders?

One potential suprise for new users is that (at least until HedgeDoc 2.X), there is really no concept of folders for saving notes (each "page" / "document" is called a note).

Each note has a unique URL and is totally independent.
If you create a new note, a random URL is generated, and it is super difficult for anyone (including future-you) to find that note again later.

So, to keep things manageable and discoverable in a large, multi-player environment, the recommended instructions (which are printed for you when you start the server):
* Uses a custom URL /home as the "landing page" for your instance
* Asks everyone to create new pages by create a dangling link to it, e.g. `[Blue Team 01 Notes](/blue_01)`, then clicking the link
    - HedgeDoc is configured to create any page that does not exist when you access it

This strategy gives every page a helpful URL, and ensures that every page can be reached by following links from `/home`.

So while the lack of real directory trees may be unituitive at first, use your `/home` page to document your logical document tree however you want, and you should be fine.

## `hedgedoc export`

Player 1 hosts the HedgeDoc instance:
```bash
$ autovnet rtfm hedgedoc
[+] autovnet rtfm hedgedoc crowded_hate
[+] connect : firefox 127.0.0.1:1337
[+] mount   : autovnet rtfm hedgedoc mount -C crowded_hate 10.241.153.70:36513
...
```

As the host, they receive a helpful block of recommended instructions, including example instructions they can send to other players.

Following the printed instructions are recommended, since they are intended to help you keep your notes organized.

The host (Player 1) can run the generated `connect` command to access the instance.
```bash
firefox 127.0.0.1:1337
```

## `hedgedoc mount`

Then, Player 2 runs the generated `mount` command to mount the service locally to their `127.0.0.1:1337`

```bash
$ autovnet rtfm hedgedoc mount -C crowded_hate 10.241.153.70:36513
[+] autovnet rtfm svc mount crowded_hate
[+] mounted locally: http://127.0.0.1:1337/home
```

Now Player 2 can also access the shared HedgeDoc server at `127.0.0.1:1337`.
The `mount` command above will open the `hedgedoc` server in Player 2's default browser.

## Security

Under the hood, `autovnet rtfm hedgedoc` uses `autovnet rtfm svc` to securely share the service.

That means that all data in encrypted in transit, and other players with the `rtfm.key` from the `autovnet` game config can access the HedgeDoc server.

So, it's the perfect place to keep your hacker notes as you hack :)

## Backups

To give you some peace of mind that your notes are safe, `autovnet` will, by default, take a periodic backup of your HedgeDoc instance.

See the help menu for all the options available to control (or disable) this behavior.

The process for restoring the backup is pretty straightforward (adapted from https://docs.hedgedoc.org/setup/docker/#restore)

This section is about "reverting" a hedgedoc instance to a previous, good state on the same node.

If you would like to restore to a different node or tolerate node failure, see [Warm Spare](#warm-spare) and [Move HedgeDoc Instance](#move-hedgedoc-instance).

_Note: database restore techniques will not affect uploaded files, which are saved in /opt/rtfm/hedgedoc/code_name/uploads`_

## Recommended Restore

1. Identify the backup you would like to restore
    * By default, it will be stored in `/opt/rtfm/hedgedoc/code_name/backups/`

2. Stop your existing `hedgedoc` command, if it is still running.

3. Test your backup before you commit to it:
```bash
$ autovnet rtfm hedgedoc export -q --init-sql /opt/rtfm/hedgedoc/code_name/backups/pg_dump.XXXXX.sql.gz
```

This will create a new instance / code_name for you to inspect locally.
No other players will be connected to it, because it uses a different codename / `autovnet` IP.

You'll need to copy the contents of `/opt/rtfm/hedgedoc/old_code_name/uploads` to `/opt/rtfm/hedgedoc/new_code_name/uploads` for uploaded files / images to render correctly.

3. When satified, restart the primary `hedgedoc` instance and reset the database in place (this will permantently delete any data currently in the database)
```bash
# Note: Now we also pass code_name (the original code name of the old instance)
$ autovnet rtfm hedgedoc export -q --init-sql /opt/rtfm/hedgedoc/code_name/backups/pg_dump.XXXXX.sql.gz code_name
...
[!] --init-sql provided but database may already exist! Import anyway? [y/N]: y
```

Because you used your existing codename, no players need to take any action (other than refreshing their browser).

Alternatively, you could give all players the new `mount` command from `2.` and ask them all to switch to the new instance.

## Alternate / Manual Restore

1. Identify the backup you would like to restore
    * By default, it will be stored in `/opt/rtfm/hedgedoc/code_name/backups/`

2. Ensure that your autovnet hedgedoc command is still running
    * Note: you can restore your backup on any system, or on a clean hedgedoc instance with a different codename

3. Find the container names for the hedgedoc app and database

```bash
$ docker ps | grep hedgedoc
0fd7be3c8060   quay.io/hedgedoc/hedgedoc:1.9.2     "/usr/local/bin/dock…"   2 minutes ago   Up 2 minutes (healthy)   127.0.0.1:1337->1337/tcp, 3000/tcp   hedgedoc_nostalgic_improvement_app_1
2ca39ff24736   postgres:latest                     "docker-entrypoint.s…"   2 minutes ago   Up 2 minutes             5432/tcp                             hedgedoc_nostalgic_improvement_database_1
```
    * Here, the app is `hedgedoc_nostalgic_improvement_app_1` and the database is `hedgedoc_nostalgic_improvement_database_1`
    * It should always match `hedgedoc_CODE_NAME_[app|database]_1`

4. Kill the app container

```bash
# Use your app container name:
$ docker kill hedgedoc_nostalgic_improvement_app_1
```

5. Restore the database backup (will delete anything currently in this database)

```bash
# Use your database container name:
gunzip -c < pg_dump.sql.gz | docker exec -i hedgedoc_nostalgic_improvement_database_1 psql -U hedgedoc
```

6. If restoring to a different code name or different system, copy the old `/opt/rtfm/hedgedoc/code_name/uploads` to the new `code_name`

## Warm Spare

If your notes are mission critical, you are senstive to downtime, or you need to tolerate node failures with minimal data loss,
you can leverage the `fs` command's `--mirror` option to keep a warm spare of your hedgedoc instance.

Simply `fs export` the whole `code_name` directory on the `hedgedoc` host, and `fs mount --mirror` it on one or more secondary nodes.

The host exports the `nostalgic_improvement` directory:
```bash
$ autovnet rtfm fs export /opt/rtfm/hedgedoc/nostalgic_improvement
[+] autovnet rtfm fs export -C dull_finish
[+] export: /opt/rtfm/hedgedoc/nostalgic_improvement
[+] mount: autovnet rtfm fs mount -C dull_finish 10.234.30.41:56334
```

Secondary players mirror the whole directory
```bash
$ autovnet rtfm fs mount -C dull_finish 10.234.30.41:56334 --mirror
[+] autovnet rtfm fs mount dull_finish
[+] mountpoint: /opt/rtfm/fs/dull_finish/mnt
[+] mirror every 60s to /opt/rtfm/fs/dull_finish/mirror
```

This will back up all exported backup files from `backups/`, all user-uploaded files from `uploads/`, and the `postgres` database files from `data/`.

It's _possible_ but not guaranteed that a warm replica could be started as-is without restoring the database.
That is, since the database files were copied, if you copy or move `/opt/rtfm/fs/dull_finish/mirror` to `/opt/rtfm/hedgedoc/nostalgic_improvement`
on a secondary node, you may be able to simply `autovnet rtfm hedgedoc nostalgic_improvement` to transparently replace a failed node.

However, [this is not guaranteed to work](https://www.postgresql.org/docs/current/backup-file.html)
unless your old node still exists and you can [gracefully move your instance to another node](#move-hedgedoc-instance).

So if the database errors out or things are broken, just restore from one of the backups that were also replicated.

## Move HedgeDoc Instance

If you HedgeDoc instance is fine and your node still exists, you can gracefully move the whole instance to another node, with minimal downtime,
by basically doing the [postgres filesystem backup strategy](https://www.postgresql.org/docs/current/backup-file.html), but with autovnet.

This is useful if you underestimated your disk space needs and ran out, or if you need to take a node down for maintenance.

1. On the host, `fs export` the `code_name` directory:

```bash
$ autovnet rtfm fs export /opt/rtfm/hedgedoc/nostalgic_improvement
[+] autovnet rtfm fs export -C dull_finish
[+] export: /opt/rtfm/hedgedoc/nostalgic_improvement
[+] mount: autovnet rtfm fs mount -C dull_finish 10.234.30.41:56334
```

2. On the new node, `fs mount` + `--mirror`, wait for the confirmation, then exit (`CTRL+C`)
```bash
$ autovnet rtfm fs mount -C dull_finish 10.234.30.41:56334 --mirror
[+] autovnet rtfm fs mount dull_finish
[+] mountpoint: /opt/rtfm/fs/dull_finish/mnt
[+] mirror every 60s to /opt/rtfm/fs/dull_finish/mirror
[2021-12-28T14:11:28] Successful mirror to /opt/rtfm/fs/dull_finish/mirror
^C
```

3. On the host, stop running the `rtfm hedgedoc` command, and wait for it to gracefully exit

4. On the new node, re-mount and `--mirror` with ``--mirror-checksum`` (see https://www.postgresql.org/docs/current/backup-file.html)
    * After the success confirmation, `CTRL+C` again.
```bash
$ autovnet rtfm fs mount dull_finish --mirror --mirror-checksum
[+] autovnet rtfm fs mount dull_finish
[+] mountpoint: /opt/rtfm/fs/dull_finish/mnt
[+] mirror every 60s to /opt/rtfm/fs/dull_finish/mirror
[2021-12-28T14:13:57] Successful mirror to /opt/rtfm/fs/dull_finish/mirror
^C
```

5. On the new node, move the mirror directory to `/opt/rtfm/hedgedoc/CODE_NAME`

```bash
mv /opt/rtfm/fs/dull_finish/mirror /opt/rtfm/hedgedoc/nostalgic_improvement
```

6. Now the new node's `/opt/rtfm/hedgedoc/CODE_NAME` directory should be an exact copy of the old node's, so you can start the new instance on the new node as if it were native to that node.
    * Because of the under-the-hood design, regular players don't need to make any changes or re-mount the service - the new instance will transparently replace the old one
    * If appropriate, update any [warm spare](#warm-spare) nodes to start replicating the new instance
```bash
# On new node
$ autovnet rtfm hedgedoc -q nostalgic_improvement
[+] autovnet rtfm hedgedoc nostalgic_improvement
[+] connect : firefox 127.0.0.1:1337
[+] mount   : autovnet rtfm svc mount -C nostalgic_improvement 10.213.134.254:39684 -p 1337
```

## Initalize HedgeDoc Instance

It might be helpful to import a previous HedgeDoc instance full of notes and helpful resources into a brand-new game environment.
* For example, maybe you have some standard pages for orientation and [ROE](https://csrc.nist.gov/glossary/term/roe) that you re-use between games.

This is easy using the automatic backup feature described above.
```bash
# You can set --force-init-sql to avoid the confirmation prompt if you are scripting
$ autovnet rtfm hedgedoc export -q --init-sql pg_dump.XXXXX.sql.gz
```

If needed, you can populate `/opt/rtfm/hedgedoc/code_name/uploads` with any uploaded files / images from your previous instance.
