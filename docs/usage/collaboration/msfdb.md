# msfdb Sharing

[TOC]

## Intro

Metasploit-Framework can track lots of things in a `postgres` database.

Many players can connect to a shared database (`msfdb`) using the `db_connect` command or by [creating a config file](https://www.offensive-security.com/metasploit-unleashed/using-databases/).

The database is populated by lots of `msf` modules, and can be a great way to stay organized an collaborate with your team!

You can even stay further organized using `workspaces`, which are like virtual databases that only contains data about a particular set of targets
(e.g. a particular Blue Team, customer network, or target VMs for a particular lab)

See https://www.offensive-security.com/metasploit-unleashed/using-databases/ for more details on how you can make use of a shared database for collaboration.

### `postgres` conflict

**Note: to use the `msfdb` feature of `autovnet`, ensure that `postgres` is not running and do not manually set up msfdb**

These commands may be helpful for getting `postgres` / a manual `msfdb` out of the way.

Trust us - the `autovnet`-ified version is better.

If you have data in your postgres database that you want to keep, use `pg_dump` (cli) or `db_export` (`msfconsole` command) to save your data to import later.

```bash
# This will disable postgres and prevent anything from starting it accidentally
# Note: autovnet will do this for you when you run an 'rtfm msfdb' command
sudo systemctl mask --now postgres

# This will delete any data you saved to your manual msfdb instance
sudo msfdb delete
```

# `msfdb export`

As you might expect, sharing an `msfdb` instance is much like sharing other services.

There is 1 host (here, Player 1) that `export`'s the `msfdb` and 1-N clients `mount` it locally on their systems, all nice and secure.

**To avoid confusion and possible data loss, your host's postgres service will be disabled.**

You should use the `autovnet` provided commands to manage your `msfdb` instances.

Player 1:
```bash
$ autovnet rtfm msfdb export                                                                                          1 ⨯
[+] (sudo) Disabling postgresql service
[+] Fixing database permissions...
[sudo] password for kali:
[+] autovnet rtfm msfdb scintillating_target
[+] Waiting for database...
[+] (sudo) Intializing msfdb...
[+] Creating database user 'msf'
[+] Creating databases 'msf'
[+] Creating databases 'msf_test'
[+] Creating configuration file '/usr/share/metasploit-framework/config/database.yml'
[+] Creating initial database schema
[+] mount : autovnet rtfm msfdb mount -C scintillating_target --db-connect msf:LYfHFy8lwRF3NGKucTSAgjNaTfOXiiGbaeLVtypWlQs=@localhost:5432/msf 10.248.155.136:53606
```

Intializing the database properly is a little tricky, but `autovnet` takes care of it for us and prints out the only output we care about, the big `mount` command.

During the intialization, you may be prompted for your `sudo` password.
If you dislike this, you can set up passwordless sudo - we briefly need to elevate our privleges to prevent permission issues when we run the long-lived msfdb instance with minimum privs.

Note: the host (Player 1) does not need to `mount` the `msfdb` - it's already local and their next `msfconsole` will use it.

# `msfdb mount`

The giant `mount` command can then be copied by Players 2-N:
```bash
$ autovnet rtfm msfdb mount -C scintillating_target --db-connect msf:LYfHFy8lwRF3NGKucTSAgjNaTfOXiiGbaeLVtypWlQs=@localhost:5432/msf 10.248.155.136:53606
[+] autovnet rtfm msfdb mount scintillating_target
[+] (sudo) Disabling postgresql service
[+] Saving /opt/rtfm/msfdb/scintillating_target/database.yml
[+] Updating ~/.msf4/database.yml
[+] mounted locally   : 127.0.0.1:5432
[+] manual db_connect : db_connect --name scintillating_target msf:LYfHFy8lwRF3NGKucTSAgjNaTfOXiiGbaeLVtypWlQs=@127.0.0.1:5432/msf
```

A manual `db_connect` is printed, so you can connect an existing `msfconsole` instance to the database.

But the `~/.msf4/database.yml` file that was also created will cause new `msfconsole` instances to automatically connect to the database, no action required!

# Using a Shared `msfdb`

Once multiple players are sharing an `msfdb` instance, you can easily verify it is working.

All players should show `connected`:
```bash
# msfconsole
> db_status
[*] Connected to msf. Connection type: postgresql.
```

If one player runs a command that updates the database, all players see it.

As a quick test, we can use `db_nmap` to ping some of the `autovnet` server's IPs.

Player 1:
```bash
#msfconsole
> db_nmap -sn 10.192.1.1
```

Player 2:
```bash
#msfconsole
> db_nmap -sn 10.192.2.2
```

Both Players:
```bash
#msfconsole
> hosts

Hosts
=====

address     mac  name  os_name  os_flavor  os_sp  purpose  info  comments
-------     ---  ----  -------  ---------  -----  -------  ----  --------
10.192.1.1
10.192.2.2
```

# Data Location and Backups

The database files are locate on the host (`export`-er) 's machine in `/opt/rtfm/msfdb/CODE_NAME/data`.

By default, the host and all players who mount the database will make automatic, local backups to `/opt/rtfm/msfdb/CODE_NAME/backups`.

If necessary, a backup can be restored to a new `msfdb export` instance easily:

On the new `msfdb` host
```bash
# Example:
$ autovnet rtfm msfdb export --no-msfdb-init --init-sql /opt/rtfm/msfdb/OLD_CODENAME/backups/pg_dump.XXXXX.sql.gz
```

So, you get all the benefits of centralized data management with most of the benefits of decentralized data management,
since the backups are stored redundantly across many nodes!


Note: you can also use this technique to run an msfdb on an infrastructure node that doesn't even have metasploit installed.

If you're feeling particularly adventurous, you could even use this techique to share and abitrary `postgres` database between nodes.
