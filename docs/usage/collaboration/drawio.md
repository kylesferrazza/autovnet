# drawio

[TOC]

# Intro

[drawio](https://www.diagrams.net/) (spelled `diagrams.net`) is a simple tool for creating beautiful diagrams you can easily export and share.

It's particularly handy for creating network diagrams, and work great with [HedgeDoc](hedgedoc.md).

It's a static site that processes all the data in your browser, much like [CyberChef](cyberchef.md) so this intergation is super simple.

# `drawio export`

Start a new `drawio` instance:
```bash
$ autovnet rtfm drawio export 
[+] autovnet rtfm drawio export handy_emergency
[+] connect : xdg-open http://127.0.0.1:36560
[+] mount   : autovnet rtfm drawio mount -C handy_emergency 10.197.202.166:62120
[*] Waiting for drawio ...
[+] drawio is ready!
```

# `drawio mount`

Connect to another player's `drawio` instance:
```bash
$ autovnet rtfm drawio mount -C handy_emergency 10.197.202.166:62120
[+] autovnet rtfm drawio mount handy_emergency
[*] Waiting for drawio ...
[+] connect: xdg-open http://127.0.0.1:40778
[+] drawio is ready!
```
