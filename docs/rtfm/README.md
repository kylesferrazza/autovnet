# RTFM

[TOC]

## Motivation

Virtually all "Red vs Blue" exercises share some common characteristics:

* Many Red Teamers, many Blue Teamers / Blue Team networks

* Blue Team machines are virtualized inside a competition / game environment

* The Cyber Range administrator has network administration access (i.e. "ISP"-level access) for all Blue and Red Teams


Unfortunately, all "Red vs Blue" excercises also struggle with common problems:

* Connections from Red -> Blue and Blue -> Red are never realistic

* Sometimes connections Blue -> Red aren't even possible (e.g. Red behind NAT)

* Sometimes connections Blue -> Red aren't actually feasible (e.g. single pivot host, Red Team fights over ports - only one Red Teamer can bind to :443)

* There is often a 1-1 mapping between Red Team box and the IP Blue Team sees (i.e. all traffic from X.X.X.X can immediately be attributed as malicious activity from the same actor)

* Many solutions for catching callbacks (e.g. port forwards via `iptables` on a pivot box) just don't work when Red Teamers BYOD their Kali VMs behind a host firewall, host NAT, etc

* Some of the fancier 'solutions' ('just assign yourself a static ip on a virtual subinterface, bro', thousands of VPN configs, thousands of iptables rules, etc.) are inherently not scalable, inevitably lead to ip conflicts between Red Teamers, inevitably lead to Red Teamers disconnecting from the game network whenever they attempt to make an IP change
    - At _best_ Blue Team may encounter hundreds of IP addresses in their logs

Essentially, Red Teamers are forced to either hack from their 'Red' IP address (unrealistic - like a cybercriminal hacking from their 'residental' IP address) or forced to share a single, 'Gray' IP on a NAT or pivot box (unrealistic - like if 100% of internet traffic came from the same VPN endpoint)


## Red Team Force Multiplication (`RTFM`)

The big idea / desired end-goal is to provide `1->N` Red Teaming, where `1` Red Teamer can trivially emulate `N` distinct actors.

The first (and probably most important step) to achieving true `RTFM` to allowing Red Teamers to self-service "provision" virtual IP addresses they can use for their activities.

`autovnet` is part of the `autovrtfm` suite, which provides the intfrastructure and tooling for `RTFM` in Cyber Exercies / Wargames.


## RTFM: Blue Incident Response

Most `autovnet` commands will immediately "provision" a random, emphemeral IP by default.

This removes all incentive for Blue Team to block by IP address, and brings unprecendented realism to Wargames - there is an internet full of bad actors, not a handful.

However, `autovnet` is designed so Red Teamers _intentionally and strategically re-use_ IP addresses, when appropriate.

By beaconing to a particular C2 IP, serving multiple malicious files from a particular C2 IP, etc.,
Blue Team can perform meangiful Incident Response / Threat Hunting to correlate related malicious activity and attribute it to a specifc actor.

By using multiple sets of distinct [TTPs](https://csrc.nist.gov/glossary/term/tactics_techniques_and_procedures) paired with non-overlapping sets of C2 IPs,
even a single Red Teamer can emulate multiple [APTs](https://csrc.nist.gov/glossary/term/advanced_persistent_threat) concurrently.
