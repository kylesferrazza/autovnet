# Paradigm

[TOC]

## Comparison to Other Infrastructure Designs

This diagram compares the the `RTFM` model to other strategies for Wargame infrastructure that you may be familar with.
* This diagram is intentionally abstract
    - **Red Nodes**: Entity controlled by Red Team
    - **Red Lines**: Traffic to / from Red Team
    - **Blue Nodes**: Entity controlled by Blue Team
    - **Green Nodes**: Entity controlled by Scoring Team / Scoring System
    - **Green Lines**: Traffic to / from Scoring Team
* The focus here is how the network **appears** to the Blue Team. See the [Example Deployment Topology](autovnet.md#example-deployment) for a more accurate network diagram of an `autovnet` deployment.

![rtfm.svg](../img/rtfm.svg)

## 1-1 Red Teaming

This is the most basic infrastructure, and what you get when all Red Teamers are on the same network segment and can directly route to the Blue Team.

You may be thinking: _But I just ask all my Red Teamers to statically configure their own virtual subinterfaces_.
* This is still fundamentally still 1-1 Red Teaming, and is a terrible idea for a variety of reasons (see [goals.md](goals.md)).

## N-1 Red Teaming (NAT)

This is what most Wargames adopt when they realize that Blue Team can just block the Red Team subnet / a handful of Red Team IPs and still get points from the scoring system.
* Here, a network device uses [NAT](https://en.wikipedia.org/wiki/Network_address_translation) to deliver all traffic (both Red and Green) from a single IP. 

## N-1 Red Teaming (NAT + Jumpbox)

This is what Wargames adopt when they try implementing the NAT strategy, then remember how NAT works.
* With virtually every NAT-type deployment, reverse connections (Blue -> Red) are not feasible enough to be useful
    - Red Team would have to fight over configuring port forwards on the firewall

Adding some Red-controlled Jumpboxes just pushes the port conflicts to the Jumpbox
* Everyone still wants to bind to `:443`, and only one player can per Jumpbox.

## RTFM: autovnet

`autovnet` is a new paradigm for Wargame infrastructure, and is the infrastructure your infrastructure wants to be when it grows up.

It is client-driven and opt-in, so _autovnet players_ - that is, Red Team and the Scoring Team - have full control over how their traffic will look to the Blue Team.

`autovnet` scales to _literally billions_ of IP addresses with **zero** additional performance impact,
and transparently manages and redirects the network traffic to / from your favorite existing tools (no modifications required).

Additionally, each player can concurrently utilize as many of these virtual IPs as they want, they never need to disconnect from the game network, and "provisioning" a new IP is instant, free, and requires almost no additional compute resources (e.g. `sshd` forking a child process to handle the traffic is absolutely neglibile compared to dynamically provisioning a pivot VM, even on-prem).

See [goals.md](goals.md) - `autovnet` enables true `RTFM`.

## RTFM: Blue Incident Response

Most `autovnet` commands will immediately "provision" a random, emphemeral IP by default.

This removes all incentive for Blue Team to block by IP address, and brings unprecendented realism to Wargames - there is an internet full of bad actors, not a handful.

However, `autovnet` is designed so Red Teamers _intentionally and strategically re-use_ IP addresses, when appropriate.

By beaconing to a particular C2 IP, serving multiple malicious files from a particular C2 IP, etc.,
Blue Team can perform meangiful Incident Response / Threat Hunting to correlate related malicious activity and attribute it to a specifc actor.

By using multiple sets of distinct [TTPs](https://csrc.nist.gov/glossary/term/tactics_techniques_and_procedures) paired with non-overlapping sets of C2 IPs,
even a single Red Teamer can emulate multiple [APTs](https://csrc.nist.gov/glossary/term/advanced_persistent_threat) concurrently.
