# v0.10.0

* [FEATURE] Add `autovnet rtfm sliver`, an integration of [sliver](https://github.com/BishopFox/sliver)
* [FEATURE] Add `autovnet rtfm tun redirect` to allow even more flexibility in traffic manipulation
  - This makes enables payloads that use ports you may not be able to actually actually bind to
* [FEATURE] Enable docker buildkit for faster docker builds
* [FIX] Disable `hedgedoc` `Content-Security-Policy` to make it publicly exposable through `listen`
* [FIX] Fix tab completion for files

# v0.9.0

* [FEATURE] Add `autovnet rtfm merlin`, an integration of [merlin](https://github.com/Ne0nd0g/merlin)
* [FIX] Fix `docker` cleanup for some commands
* [FIX] Fix `config init` error during APT intialization
* [FIX] Fix bug in `tun` causing permission issues for users based on `uid` mismatch
* [FIX] Fix bug in `tun` preventing important iptables rules from being created
* [MISC] Allow `tun` clients to pivot to the `autovnet` simluated network over the `tun`
  - Adds flexibility is working around VPN gateway / route issues

# v0.8.0

* [FEATURE] Add `autovnet pasta` command for rendering copy-pasteable docs / examples
* [FIX] Fix `vnc mount` shutdown
* [FIX] Fix `tun` assignment regression (`lighthouse_ip` accidentally issued to first client)
* [MISC] Bump `hedgedoc` version to `v1.9.3`
* [MISC] Bump `matrix` version to `v0.8.9`

# v0.7.0

* [FEATURE] Add `autovnet apt` command suite for creating and managing APT profiles
* [FIX] Fix and upgrade `focalboard` integration
* [FIX] Fix various bugs in IP generation

# v0.6.1

* [DOCS] Add docs for `dns` commands
* [DOCS] Improve setup docs
* [FEATURE] Register some automatic `dns` names for the `autovnet` server's true IP: `avn.avn`, `autovnet.avn`, `ns.avn`, and `core.avn`
* [MISC] Bump Kali version to `2022.2`
* [FEATURE] Add `autovnet tmp` command for easy scratch directories

# v0.6.0

* [FIX] Fix manual invocations of `autovnet tun server` without `-e`
* [FIX] Fix `.autovnet-update` bug preventing in-place upgrades
* [FIX] Remove outdated `rtfm-*` docker images on upgrade
* [MISC] Bump Ubuntu version to `22.04: Jammy Jellyfish`
* [FEATURE] Add `autovnet rtfm dns` for in-game DNS registration
* [FEATURE] Multiplex `autovnet rtfm listen` primitive (`ssh` control sockets) for increased performance
* [FEATURE] Add optional `dns` registration to `autovnet rtfm listen`
* [FEATURE] Add optional `dns` registration to `autovnet rtfm tun`
* [FEATURE] Add network design / deployment examples

# v0.5.0

* [FEATURE] Add `autovnet rtfm tun`, a new core feature adding support for arbitrary `IP` protocols (`state-of-the-art++`)

# v0.4.0

* [FEATURE] Add `autovnet rtfm drawio` for secure, collaborative diagram creation, powered by [drawio / diagrams,net](https://www.diagrams.net/)
* [FEATURE] Add `autovnet rtfm gitea` for secure, collaborative development, powered by [gitea](https://gitea.io/en-us/)
* [FEATURE] Add `autovnet rtfm focalboard` for secure, collaborative project management, powered by [focalboard](https://www.focalboard.com/)
* [FEATURE] Add `autovnet rtfm matrix` for secure, collaborative chat, powered by [matrix](https://matrix.org/) and [element](https://element.io/)
* [FEATURE] Add `autovnet rtfm nextcloud` for secure, collaborative [cyberchef](https://gchq.github.io/CyberChef/) sharing
* [FEATURE] Add `autovnet rtfm nextcloud` for secure, collaborative [nextcloud](https://nextcloud.com/) sharing
* [FEATURE] Add `autovnet rtfm owncast` for secure, collaborative live streaming, powered by [owncast](https://owncast.online/)
* [FEATURE] Rework `autovnet rtfm hedgedoc` - add support for `https`, refactor internals
* [FEATURE] Add `autovnet rtfm docs` for awesome, local docs, powered by [mkdocs](https://www.mkdocs.org/)
* [FEATURE] Create [https://autovtools.gitlab.io/autovrtfm/autovnet-docs/](https://autovtools.gitlab.io/autovrtfm/autovnet-docs/) as the canonical docs site

# v0.3.1

* [FIX] Fix hedgedoc mount immediately disconnecting
* [FIX] Fix hedgedoc export printing the wrong codename command

# v0.3.0

* [FEATURE] Add `autovnet rtfm svc` command for secure, collaborative service sharing
* [FEATURE] Add `autovnet rtfm hedgedoc` command for secure, collaborative hacker notes, powered by [HedgeDoc](https://hedgedoc.org/)
* [FEATURE] Add `autovnet rtfm vnc` command for secure, collaborative screensharing
* [FEATURE] Add `autovnet rtfm msfdb` command to securely share [Metasploit-Framework databases](https://www.offensive-security.com/metasploit-unleashed/using-databases/)
* [FEATURE] Add `autovnet rtfm password` command to generate temporary game passwords that meet complexity requirements
* [FIX] Cleanup docker images after use
* [FIX] Add automatic deconfliction (best-effort) to mitigate birthday problem
* [FIX] Fix signal handling to gracefully terminate children

# v0.2.0

* [FEATURE] Add `autovnet rtfm fs` command for collaborative directory sharing
* [FIX] Fix tab completion for codenames, hopefully everywhere
* [FEATURE] Change data directory to /opt/rtfm (instead of /opt/tools) to be less generic
    * Now configurable in `~/.autovnet/config.yaml` (`data_path`)

# v0.1.0

* Completed baseline documentation for installation and usage
* Streamlined initial server and client setup
* Serialize and assign codenames to everything, so anything can be recreated with similar semantics
* Refactor internal listener code to be less yikes
* Add easy, encrypted file sharing for Red Teamers
* Switch to [black](https://github.com/psf/black) for formatting

# v0.0.0

* Intial release
* Includes core functionality, demonstrably viable under game conditions
