# In the Wild

[TOC]

Have you used `autovnet` for something cool and want a shoutout? Open an issue!

## [Currently Unnamed Competition]
* `autovnet` has been battle-tested in large-scale Cyber competitions with hundreds of concurrent players

## [BishopFox](https://bishopfox.com/)
* `autovnet` was featured by [BishopFox](https://bishopfox.com/) in their [pen testing tools of 2021 roundup](https://bishopfox.com/blog/pen-testing-tools-2021)
