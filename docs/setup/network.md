# Network Design

This document is for network admins designing / creating the networks that `autovnet` will be used in.
You can skip it if you are an regular `autovnet` user.

To use `autovnet` to its fullest and or to set your event / class / cyber exercise / competition up for success, you need a good network design.

Review [the basic topolgy](../rtfm/autovnet/#topology) to get an idea for the logical topology recommended for an `autonvet` deployment.

This guide focuses on concrete examples and IP address schemes that you can create in your lab.

While you can use `autovnet` in basically any environment that meets the minimum requirements -
including _the cloud_, a physical network, 100% FOSS hypervisors, or on your laptop (with VMs) -
these examples use [`vSphere`](https://www.vmware.com/products/vsphere.html) with [pfSense](https://www.pfsense.org/) as the firewall.

* This may affect some terminology (like `port group`, `domain override`, etc.), so translate the concept as needed to your match your infrastructure.

These examples will assume multiple Blue Team(s) or Blue Team systems and multiple Red Team players.

Again, `autovnet` is extremely flexible. These examples are only examples, and not the only way to design a functional network.

## What Network Block Should I Use For Simluation?

* These diagrams use `10.192.0.0/10`
* `240.0.0.0/4` is a better choice for most use cases
* See [the configuration guide](configure.md#generate-config) for more discussion

## Minimal

This is an example topolgy useful for testing `autovnet`.
It includes 1 Blue Team and multiple Red Team members.

![docs/img/ex/network_min.svg](../img/ex/network_min.svg)

* `VLANs` are configured at the hypervisor level (e.g. `vSphere` port groups) to provide L2 isolation
    - Do not configure `VLANs` in `pfSense`, just attach 3 network adapters to the firewall


## Full Example

This is an example topology that scales to many Blue Teams and many Red Team players.

You can adjust the subnets to meet your needs, but this general design should scale to hundreds of Blue Teams and Red Teamers.

![docs/img/ex/network_full.svg](../img/ex/network_full.svg)

* Here, each Blue Team has their own firewall that they can configure, and a unique subnet
* The game firewall is responsible for routing traffic for each team's subnet to their team's firewall
    - Static routes on the game firewall are recommended
    - This design allows Red Team to reach all Blue Team networks

* Remember that each Blue Team firewall should be configured to
    - Allow all from WAN
    - Allow Private Networks / Bogon Networks
    - Disable Outbound NAT

* The game firewall should probably have outbound NAT (to WAN) enabled, but no other NAT enabled
    - This is the default for `pfSense`

## pfSense Examples

### Gateways

To implement the full example, add a `gateway` for each team's firewall.

The `gateway` that is the `autovnet server` is also shown.

![docs/img/ex/pfsense/gateways.png](../img/ex/pfsense/gateways.png)

### Route

To implement the full example, add a `static route` for each team's subnet.

The static route for `autovnet` traffic is also shown, which uses the `autovnet server` `gateway`

![docs/img/ex/pfsense/routes.png](../img/ex/pfsense/routes.png)


### DNS


#### Game Firewall

The recommended `dns` configuration for `pfSense` is `DNS Forwarder` with a `Domain Override` for the `*.avn` (or your configured game `TLD`).

This configurations allows the `autovnet server` to respond to `dns` queries for `*.avn` domains.

![docs/img/ex/pfsense/dns_forwarder.png](../img/ex/pfsense/dns_forwarder.png)

#### Blue Team Firewalls

If each Blue Team has their own firewall (e.g. a `pfSense` firewall), you need to make sure their DNS is set to forward all queries to your Game Firewall.

* Services > DNS Resolve > Uncheck _Enable_ > Apply
* System > Advanced > Admin Access > Check _Disable DNS Rebinding Checks_ > Save
* Services > DNS Forwarder > Check _Enable_ > Apply

You can test resolving `ns.avn` from different locations in your network to see if your `dns` is set up correctly.
