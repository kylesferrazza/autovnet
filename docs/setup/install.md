# Install

[TOC]

## Overview

`autovnet` contains a server component and a client component.

* The `autovnet server` should run in a central location reachable by all Blue / Red systems
* The `autovnet clients` run on Red systems and allow Red Teamers to control the `autovnet server`
* No modifications are needed to any Blue systems
* The Network Administrator should add a static firewall route such that all simulated networks are routed to the `autovnet server`
    - This is the 'trick' that makes `autovnet` work in an exercise environment - the Network Administrator is the default gateway (effective 'ISP'), and can send packets where they please

## Requirements

### _autovnet server_

Tested on Ubuntu 22.04 and Kali 2022.2.
Yes, you can get it working on other distros, but YMMV and there won't be good docs.

#### Logical Requirements

**You do not need to manually install anything if you are going to use [the easy install](#easy-install), which you should**

* Recent Linux with standard utilities (e.g. `sudo`, `ip`, `ssh`, `curl`)
* `Python >= 3.9` (required by some dependencies)
    - Recent Python 3 (e.g. >= 3.7) is fine for most features
* Your system must support a reasonable version of `docker` (the easy installer will install it for you)
* Root privs
    - Specifically, a non-root user with `sudo` access
* Outbound Internet access (at least to install and start the `autovnet server`)
    - Eventual support for easy installation in air-gapped networks is planned
* The ability / permission to route an arbitrary network block to this system
    - The recommended way is to add a static firewall route to a firewall that sees all outbound Red / Blue traffic.
* Blue <-> `autovnet server` and Red <-> `autovnet server` traffic should be unfiltered
    - Obviously, your firewall rules must accomodate getting the packets to / from the `autovnet server`

### _autovnet client_

Kali 2022.2.

Later versions will probably also work, but the latest official release of Kali does not, open an issue.

## Diagram

For reference, this is the network topology we are aiming for:
![deployment.svg](../img/deployment.svg)

## Easy Instructions

These instructions will install the `autovnet` program.

The `autovnet server` (part of the Red Team infrastructure) and all `autovnet clients` (the Red Team player machines) need to install `autovnet`.

### Easy Install

If running a supported platform, the easy install script is **highly recommended**.

Current supported platforms (other versions likely work, but are not verified):

* Ubuntu 22.04
* Kali 2022.2

No promises, but if you install docker manually first, the easy install script may work on other distros.

Otherwise, keep reading for the semi-manual install instructions that are easier to adapt to your machine.


**As a regular user (not root):**
```bash
curl -s https://gitlab.com/autovtools/autovrtfm/autovnet/-/raw/main/autovnet/scripts/.autovnet-update | bash
```
Follow the printed instructions: you should reboot before continuing if this is your first time running the script.


Test your install with:
```bash
~/autovnet/scripts/test_install.sh
```

#### Permission Denied

**If you already had `docker` installed before the easy installer, you may get this error - you need to be a member of the `docker` group**

If you get 'Permission Denied' from docker:

1. Reboot and try again
2. If you _still_ get permission denied:

    * As your regular user:
```bash
sudo usermod -aG docker $USER
sudo reboot
```

### Easy Update

#### Users

Once you have installed `autovnet`, you can easily update with an included helper script (already added to your PATH for your convenience)

As the user you used to install autovnet:
```bash
.autovnet-update
```

#### Server

To update an `autovnet server` in place:

```bash
# Stop and remove the autovnet service
sudo systemctl stop autovnet.service
autovnet rtfm server uninstall

# You can switch to an alternate stream here, if desired
.autovnet-update

# Reinstall the autovnet server
autovnet rtfm server install
```

### Alternate Streams

#### `test` stream

If you want to get the latest (unstable) features as soon as they might work, you can switch to the `test` branch like this:

```bash
SHELL_RC=~/".$(basename "$(echo "${SHELL}")rc")"
echo 'export AUTOVNET_INSTALL_BRANCH=test' >> "${SHELL_RC}"
. "${SHELL_RC}"
```

Then, each call to `.autovnet-update` will pull updates from the `test` branch.

You can switch back to `main` by editing your `SHELL_RC` file and re-running `.autovnet-update`

#### `dev` stream

There is also a `dev` branch, but it will contain work-in-progress commits that may prevent `autovnet` from functioning at all.

It's the bleeding edge, and most users are better off using `test` for personal testing and `main` (or even a [pinned release](#pinned-releases))
for use on game day.

```bash
SHELL_RC=~/".$(basename "$(echo "${SHELL}")rc")"
echo 'export AUTOVNET_INSTALL_BRANCH=dev' >> "${SHELL_RC}"
. "${SHELL_RC}"
```

#### Pinned Releases

In the (hopefully) unlikely event that a release introduces a major regression that you cannot wait to be fixed, you can substitue a tag name
(e.g. `v0.3.0`) for the branch name to force a revert to a previous, pinned version.

#### Installing from an Alternate Stream

> Note: not recommended for most users

If you are testing a branch like `test` or `dev` to make sure it is safe to merge, you should do a clean install straight from that stream.

##### Example: Install from `dev` stream

```bash
curl -s https://gitlab.com/autovtools/autovrtfm/autovnet/-/raw/dev/autovnet/scripts/.autovnet-update | AUTOVNET_INSTALL_BRANCH=dev bash
```
##### Example: Install from `test` stream

```bash
curl -s https://gitlab.com/autovtools/autovrtfm/autovnet/-/raw/test/autovnet/scripts/.autovnet-update | AUTOVNET_INSTALL_BRANCH=test bash
```

## Manual Install

**Manual installation is not recommmended, and these docs may not be kept as up-to-date as the actual install scripts in [../scripts/](../scripts/)**

If you prefer to do things the hard way, or the easy install scripts don't work for your setup, these docs will help you manually install `autovnet`.

### General dependencies

Stuff you probably already have:
```bash
sudo apt update
sudo DEBIAN_FRONTEND=noninteractive apt install -y openssh-client \
    git \
    curl \
    python3 \
    python3-venv
```

### Docker Repo

Source: https://docs.docker.com/engine/install/

Docker dependencies
```bash
sudo DEBIAN_FRONTEND=noninteractive apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```
#### Kali 2022.2

```bash
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

DEBIAN_NAME=bullseye
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian ${DEBIAN_NAME} stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

#### Ubuntu 22.04

Docker repo
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

### Docker Install

```bash
sudo apt update
sudo apt DEBIAN_FRONTEND=noninteractive install -y docker-ce docker-ce-cli containerd.io
```

Configure

```bash
sudo usermod -aG docker $USER
newgrp docker

```
NOTE: newgrp docker is temporary / for this shell only

Log out and log back in before using autovnet.

Test

Docker >= 20.10.9 tested
```bash
docker -v

docker run hello-world
```

Install docker-compose

See https://docs.docker.com/compose/install/ for latest stable version

```bash

VERSION=1.29.2
sudo curl -L "https://github.com/docker/compose/releases/download/${VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

docker-compose -v
```

### Python `venv`

```bash
mkdir -p ~/.venv/
python3 -m venv ~/.venv/rtfm

ACTIVATE=". ${HOME}/.venv/rtfm/bin/activate"
eval $ACTIVATE && echo "${ACTIVATE}" >> "${HOME}/.$(basename "${SHELL}")rc"
pip install --upgrade pip
```

### (Client) Install RTFM dependencies

Install these dependencies on any client (Red) systems.
```bash
# proxychains4
sudo apt install -y proxychains4

# pwncat
pip install git+https://github.com/calebstewart/pwncat.git

# (optional) socat
sudo apt install -y socat
```

### Install `autovnet`

```bash
cd
git clone https://gitlab.com/autovtools/autovrtfm/autovnet.git
pushd autovnet
./reinstall.sh
popd

. "${HOME}/.$(basename "${SHELL}")rc"
```

### (Optional) Manually update `autovnet`

If a newer version of `autovnet is released, it's easy to upgrade in place.
1. Stop any current `autovnet` processes
2. Pull latest code and reinstall

```bash
cd autovnet/
# git checkout a different branch, if you like
git pull
./reinstall.sh

. "${HOME}/.$(basename "${SHELL}")rc"
```

### Fix Tab Completion (`zsh`)
`autovnet` has full tab completion!

If it isn't working for you, see https://github.com/tiangolo/typer/issues/180

To get it working on `zsh` (the new default shell for Kali), you may need to run this after installing, if you installed manually:
```bash
echo 'compinit -D' >> ~/.zshrc
```

### Verify `autovnet` is installed
```bash
$ autovnet -h
Usage: autovnet [OPTIONS] COMMAND [ARGS]...

$ which autovnet
/home/$USER/.venv/rtfm/bin/autovnet
```

### Next Steps

Once you have `autovnet` installed on your `autovnet server` and at least one `autovnet client` machine, proceed to [setup](../setup).

The setup guide will walk you through configuring the `autovnet server` in server mode, and the `autovnet client(s)` in client mode.


### Updating `autovnet`

Once you have a working `autovnet` installation, you can update to the latest release with

```bash
.autovnet-update
```

See [Updating `autovnet`](#updating-autovnet) for details.
