# Quick Start

[TOC]

Taking your Red Teaming to the next level with `autovnet` is easy!

## Background

If you don't understand what `autovnet` is or why you want to use it, start with:

* [RTFM: Intro](../rtfm)
* [RTFM: Paradigm](../rtfm/paradigm.md)
* [RTFM: `autovnet`](../rtfm/autovnet.md)

### Demos

You may also be interested in some of the demos:

> Note: the demos may be outdated, but can help give the gist of how `autovnet` works in action

* [Demo: Intro](../demo)
* [Demo: Proxy](../demo/proxy.md)
* [Demo: Callback](../demo/callback.md)

## Setup

Ready to use `autovnet` on your own?

### Local Install

If you just want to try it out right now with just 1 VM,
[see the local install guide](local.md)

### Real Install

> Note:
> If you are a Cyber Range / infrastructure administator, you care about the `autovnet server` instructions.
> If you are a player / participant, you care about the `autovnet client` instructions.

When you're ready to use `autovnet` for real:

* [Install `autovnet`](install.md)
* [Configure `autovnet`](configure.md)
